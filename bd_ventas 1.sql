create database ventas;
use ventas;

create table cliente (
id_cliente int not null primary key auto_increment,
nombre varchar (150) not null,
apellido varchar (150) not null,
direccion varchar (150) not null,
fecha_nacimiento date not null,
telefono varchar (10),
email varchar (150)
);

create table modo_pago (
num_pago int not null primary key auto_increment,
nombre varchar (150) not null,
otros_detalles varchar (150) not null
);

create table categoria (
id_categoria int not null primary key auto_increment,
nombre varchar (150) not null,
descripcion varchar (150) not null
);

create table producto (
id_producto int not null primary key auto_increment,
nombre varchar (150) not null,
precio decimal (5,2) not null,
stock int not null,
id_categoria int not null,
constraint fk_cat foreign key (id_categoria) references categoria (id_categoria)
);

create table factura (
num_factura int not null primary key auto_increment,
id_cliente int not null,
fecha date,
num_pago int not null,
constraint fk_cli foreign key (id_cliente) references cliente (id_cliente),
constraint fk_np foreign key (num_pago) references modo_pago (num_pago)
);

create table detalle (
num_detalle int not null primary key auto_increment,
id_factura int not null,
id_producto int not null,
cantidad int not null,
precio decimal (5,2) not null,
constraint fk_fact foreign key (id_factura) references factura (num_factura),
constraint fk_prod foreign key (id_producto) references producto (id_producto)
);

CREATE PROCEDURE `sp_insercion_factura` (
in p_num_factura int,
in p_cliente int, 
in p_num_pago int,
in p_id_prod int,
in p_cantidad int
)


USE `ventas`;
DROP procedure IF EXISTS `sp_insercion_factura`;

DELIMITER $$
USE `ventas`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insercion_factura`(
in p_num_factura int,
in p_cliente int, 
in p_num_pago int,
in p_id_prod int,
in p_cantidad int
)
BEGIN
declare v_precio decimal (5,2);
declare v_num_factura int;
set v_precio = (select precio * p_cantidad from producto where id_producto = p_id_prod);

select num_factura into v_num_factura from factura ORDER BY num_factura DESC LIMIT 1;

insert into factura (num_factura, id_cliente,fecha,num_pago) values (default, p_cliente, current_date(),p_num_pago);

insert into detalle (num_detalle, id_factura, id_producto, cantidad, precio) values  (default,v_num_factura,p_id_prod,pcantidad,v_precio) ;

END$$



CALL sp_insercion_factura(null,2,1,3,2);


INSERT INTO `ventas`.`categoria` (`id_categoria`, `nombre`, `descripcion`) VALUES ('1', 'lacteos', 'productos lacteos');
INSERT INTO `ventas`.`categoria` (`id_categoria`, `nombre`, `descripcion`) VALUES ('2', 'embutidos', 'embutidos');
INSERT INTO `ventas`.`categoria` (`id_categoria`, `nombre`, `descripcion`) VALUES ('3', 'enlatados', 'enlatados');

INSERT INTO `ventas`.`producto` (`id_producto`, `nombre`, `precio`, `stock`, `id_categoria`) VALUES ('1', 'leche', '1.00', '30', '1');
INSERT INTO `ventas`.`producto` (`id_producto`, `nombre`, `precio`, `stock`, `id_categoria`) VALUES ('2', 'queso', '1.50', '20', '1');
INSERT INTO `ventas`.`producto` (`id_producto`, `nombre`, `precio`, `stock`, `id_categoria`) VALUES ('3', 'jamon', '2.00', '20', '2');
INSERT INTO `ventas`.`producto` (`id_producto`, `nombre`, `precio`, `stock`, `id_categoria`) VALUES ('4', 'salami', '2.00', '20', '2');
INSERT INTO `ventas`.`producto` (`id_producto`, `nombre`, `precio`, `stock`, `id_categoria`) VALUES ('5', 'jugo durazno', '0.60', '50', '3');
INSERT INTO `ventas`.`producto` (`id_producto`, `nombre`, `precio`, `stock`, `id_categoria`) VALUES ('6', 'coca cola', '0.60', '100', '3');

INSERT INTO `ventas`.`modo_pago` (`num_pago`, `nombre`, `otros_detalles`) VALUES ('1', 'tarjeta', 'visa o mastercard');
INSERT INTO `ventas`.`modo_pago` (`num_pago`, `nombre`, `otros_detalles`) VALUES ('2', 'efectivo', 'billete y monedas');

INSERT INTO `ventas`.`cliente` (`id_cliente`, `nombre`, `apellido`, `direccion`, `fecha_nacimiento`, `telefono`, `email`) VALUES ('1', 'Jose', 'Jose', 'Mexico', '1980-06-03', '123456789', 'jose@gmail.com');
INSERT INTO `ventas`.`cliente` (`id_cliente`, `nombre`, `apellido`, `direccion`, `fecha_nacimiento`, `telefono`, `email`) VALUES ('2', 'Maria Jose', 'Acosta', 'Soyapango', '2000-05-15', '67894120', 'mar@gmail.com');


