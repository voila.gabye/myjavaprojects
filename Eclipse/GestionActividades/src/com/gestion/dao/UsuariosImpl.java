package com.gestion.dao;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import com.gestion.model.Usuarios;
import com.gestion.util.AbstractFacade;
import com.gestion.util.Dao;

@Transactional
public class UsuariosImpl extends AbstractFacade<Usuarios> implements Dao<Usuarios> {

	private SessionFactory sf;

	public UsuariosImpl(SessionFactory sf, Class<Usuarios> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}

}
