package com.gestion.dao;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import com.gestion.model.Actividades;
import com.gestion.util.AbstractFacade;
import com.gestion.util.Dao;

@Transactional
public class ActividadesImpl extends AbstractFacade<Actividades> implements Dao<Actividades> {

	private SessionFactory sf;

	public ActividadesImpl(SessionFactory sf, Class<Actividades> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}

}
