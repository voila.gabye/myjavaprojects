package com.gestion.dao;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import com.gestion.model.FechasEspeciales;
import com.gestion.util.AbstractFacade;
import com.gestion.util.Dao;

@Transactional
public class FechasEspecialesImpl extends AbstractFacade<FechasEspeciales> implements Dao<FechasEspeciales> {

	private SessionFactory sf;

	public FechasEspecialesImpl(SessionFactory sf, Class<FechasEspeciales> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}

}
