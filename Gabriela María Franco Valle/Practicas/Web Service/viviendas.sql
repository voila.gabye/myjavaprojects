create database bienes;
use bienes;
create table viviendas (
id int not null auto_increment primary key,
c_habit int not null,
c_banios int not null,
colonia varchar (150) not null,
precio decimal (8,2) not null,
tamanio float not null,
municipio varchar (150) not null,
departamento varchar (150) not null,
categoria int not null,
negociable enum('V','D','A','M') not null,
estado  boolean default 1
);

insert into viviendas values (1,5,2,'Montserrat',20000.00,71.16606523,'San Salvador','San Salvador',1,'d',default);