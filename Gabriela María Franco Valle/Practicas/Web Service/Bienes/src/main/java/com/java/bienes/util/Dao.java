package com.java.bienes.util;

/**
 *
 * @author gabriela.francousam
 */
import com.java.bienes.model.Viviendas;
import java.util.Hashtable;


public interface Dao<T> {

    public Hashtable<Integer, Viviendas> create(T e);

    public Hashtable<Integer, T> read();

    public Hashtable<Integer, Viviendas> update(T e);

    public Hashtable<Integer, Viviendas> delete(T e);
}
