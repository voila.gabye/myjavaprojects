
package com.java.bienes.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gabriela.francousam
 */
@Entity
@Table(name = "viviendas")
@XmlRootElement

public class Viviendas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id")
    private Integer id;

    @Column(name = "c_habit")
    private int cHabit;

    @Column(name = "c_banios")
    private int cBanios;

    @Column(name = "colonia")
    private String colonia;
   
    @Column(name = "precio")
    private Double precio;

    @Column(name = "tamanio")
    private float tamanio;

    @Column(name = "municipio")
    private String municipio;

    @Column(name = "departamento")
    private String departamento;

    @Column(name = "categoria")
    private int categoria;
  
    @Column(name = "negociable")
    private String negociable;
    
    @Column(name = "estado")
    private Boolean estado;

    public Viviendas() {
    }

    public Viviendas(Integer id) {
        this.id = id;
    }

    public Viviendas(Integer id, int cHabit, int cBanios, String colonia, Double precio, float tamanio, String municipio, String departamento, int categoria, String negociable) {
        this.id = id;
        this.cHabit = cHabit;
        this.cBanios = cBanios;
        this.colonia = colonia;
        this.precio = precio;
        this.tamanio = tamanio;
        this.municipio = municipio;
        this.departamento = departamento;
        this.categoria = categoria;
        this.negociable = negociable;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCHabit() {
        return cHabit;
    }

    public void setCHabit(int cHabit) {
        this.cHabit = cHabit;
    }

    public int getCBanios() {
        return cBanios;
    }

    public void setCBanios(int cBanios) {
        this.cBanios = cBanios;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public float getTamanio() {
        return tamanio;
    }

    public void setTamanio(float tamanio) {
        this.tamanio = tamanio;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public int getCategoria() {
        return categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }

    public String getNegociable() {
        return negociable;
    }

    public void setNegociable(String negociable) {
        this.negociable = negociable;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Viviendas)) {
            return false;
        }
        Viviendas other = (Viviendas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.java.bienes.Impl.Viviendas[ id=" + id + " ]";
    }
    
}
