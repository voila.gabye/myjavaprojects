package com.java.bienes.util;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.java")
public class BienesApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(BienesApplication.class, args);
	}

}
