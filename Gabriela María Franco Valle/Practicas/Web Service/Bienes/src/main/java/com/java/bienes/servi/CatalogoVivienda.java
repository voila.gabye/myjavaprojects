package com.java.bienes.servi;


import com.java.bienes.model.Viviendas;
import com.java.bienes.util.Dao;
import java.util.Hashtable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author gabriela.francousam
 */
@RestController
@RequestMapping("/vivi")
public class CatalogoVivienda {

    @Autowired
    Dao viviendas;

    /*
	 * Peticion: GET
	 * Funcion: Devuelve todas las viviendas
     */
    @RequestMapping(value = "/obt", method = RequestMethod.GET)
    public Hashtable<Integer, Viviendas> getViviendas() {
        return viviendas.read();
    }

    /*
	 * Peticion: POST
	 * Funcion: Registra una vivienda
	 **/
    @RequestMapping(value = "/reg", method = RequestMethod.POST)
    public Hashtable<Integer, Viviendas> regViviendas(@RequestBody Viviendas v) {
        return viviendas.create(v);
    }

    /*
	 * Peticion: PUT
	 * Funcion: Modifica una vivienda
	 **/
    @RequestMapping(value = "/mod", method = RequestMethod.PUT)
    public Hashtable<Integer, Viviendas> modViviendas(@RequestBody Viviendas v) {
        return viviendas.update(v);
    }

    /*
	 * Peticion: DELETE
	 * Funcion: Borra una categoria
	 **/
    @RequestMapping(value = "/bor", method = RequestMethod.DELETE)
    public Hashtable<Integer, Viviendas> borCategorias(@RequestBody Viviendas v) {
        return viviendas.delete(v);
    }
}
