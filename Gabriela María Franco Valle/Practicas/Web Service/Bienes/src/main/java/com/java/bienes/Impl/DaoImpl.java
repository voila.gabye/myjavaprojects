package com.java.bienes.Impl;

import java.util.List;

/**
 *
 * @author gabriela.francousam
 */
public abstract class DaoImpl<T> {

    private Class<T> e;
    private List<T> lsEntity;

    public DaoImpl(Class<T> e) {
        this.e = e;
    }

    protected abstract void dataSource();

    public void create(T e) {
        lsEntity.add(e);
    }

    public List<T> read() {
        return lsEntity;
    }

    public void update(T e) {
        this.delete(e);
        this.create(e);
    }

    public void delete(T e) {
        lsEntity.remove(e);
    }
}
