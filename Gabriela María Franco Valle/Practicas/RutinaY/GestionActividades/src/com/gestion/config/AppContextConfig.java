package com.gestion.config;

import java.util.Properties;

import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.gestion.dao.ActividadesImpl;
import com.gestion.dao.EstadosImpl;
import com.gestion.dao.FaseActImpl;
import com.gestion.dao.FasesImpl;
import com.gestion.dao.FechasEspecialesImpl;
import com.gestion.dao.PersonasImpl;
import com.gestion.dao.TipoUsuarioImpl;
import com.gestion.dao.UsuariosImpl;
import com.gestion.model.Actividades;
import com.gestion.model.Estados;
import com.gestion.model.FaseAct;
import com.gestion.model.Fases;
import com.gestion.model.FechasEspeciales;
import com.gestion.model.Personas;
import com.gestion.model.TipoUsuario;
import com.gestion.model.Usuarios;
import com.gestion.util.Dao;



@Configuration
@ComponentScan(basePackages = "com.gestion")
@EnableTransactionManagement(proxyTargetClass = true)
public class AppContextConfig {

	@Bean(name = "dataSource")
	public DataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/gestion_actividades?useSSL=false");
		dataSource.setUsername("root");
		dataSource.setPassword("root");
		return dataSource;
	}

	private Properties getHibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.show_sql", "true");
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		return properties;
	}

	@Autowired
	@Bean(name = "sessionFactory")
	public SessionFactory getsessionFactory(DataSource dataSource) {
		LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
		sessionBuilder.scanPackages("com.gestion.model");
		sessionBuilder.addProperties(getHibernateProperties());
		return sessionBuilder.buildSessionFactory();
	}

	@Autowired
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);
		return transactionManager;
	}

	@Autowired
	@Bean(name = "persona")
	public Dao Personas(SessionFactory sessionFactory) {
		return new PersonasImpl(sessionFactory, Personas.class);
	}
	
	@Autowired
	@Bean(name = "tipoUsuario")
	public Dao TipoUsuario(SessionFactory sessionFactory) {
		return new TipoUsuarioImpl(sessionFactory, TipoUsuario.class);
	}
	
	@Autowired
	@Bean(name = "usuario")
	public Dao Usuarios(SessionFactory sessionFactory) {
		return new UsuariosImpl(sessionFactory, Usuarios.class);
	}
	
	@Autowired
	@Bean(name = "fechaEspecial")
	public Dao FechasEspeciales(SessionFactory sessionFactory) {
		return new FechasEspecialesImpl(sessionFactory, FechasEspeciales.class);
	}
	
	@Autowired
	@Bean(name = "estados")
	public Dao Estados(SessionFactory sessionFactory) {
		return new EstadosImpl(sessionFactory, Estados.class);
	}
	
	@Autowired
	@Bean(name = "fases")
	public Dao Fases(SessionFactory sessionFactory) {
		return new FasesImpl(sessionFactory, Fases.class);
	}
	
	@Autowired
	@Bean(name = "fasesAct")
	public Dao FaseAct(SessionFactory sessionFactory) {
		return new FaseActImpl(sessionFactory, FaseAct.class);
	}
	
	@Autowired
	@Bean(name = "actividad")
	public Dao Actividades(SessionFactory sessionFactory) {
		return new ActividadesImpl(sessionFactory, Actividades.class);
	}
	
}
