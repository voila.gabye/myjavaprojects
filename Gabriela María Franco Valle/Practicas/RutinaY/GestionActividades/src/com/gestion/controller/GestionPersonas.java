package com.gestion.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import com.gestion.dao.PersonasImpl;
import com.gestion.model.Personas;
import com.gestion.model.TipoUsuario;
import com.gestion.model.Usuarios;
import com.gestion.util.Dao;

@Component
@SessionScope
@ManagedBean
public class GestionPersonas implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	@Qualifier("persona")
	public Dao<Personas> persona;
	public PersonasImpl per;

	@Autowired
	@Qualifier("tipoUsuario")
	public Dao<TipoUsuario> tipoUsuario;

	@Autowired
	@Qualifier("usuario")
	public Dao<Usuarios> usuario;

	private Personas person = new Personas();
	private TipoUsuario tipoUs = new TipoUsuario();
	private Usuarios users = new Usuarios();

	private int idPer;
	
	private List<Personas> listPerson;
	private List<TipoUsuario> listTipoUs;
	private List<Usuarios> listUsers;
	private Personas listBoth;
	
	public Personas getPerson() {
		return person;
	}

	public void setPerson(Personas person) {
		this.person = person;
	}

	public TipoUsuario getTipoUs() {
		return tipoUs;
	}

	public void setTipoUs(TipoUsuario tipoUs) {
		this.tipoUs = tipoUs;
	}

	public Usuarios getUsers() {
		return users;
	}

	public void setUsers(Usuarios users) {
		this.users = users;
	}

	
	public int getIdPer() {
		return idPer;
	}

	public void setIdPer(int idPer) {
		this.idPer = idPer;
	}

	public List<Personas> getListPerson() {
		this.listPerson = persona.findAll();
		return listPerson;
	}

	public void setListPerson(List<Personas> listPerson) {
		this.listPerson = listPerson;
	}

	public List<TipoUsuario> getListTipoUs() {
		this.listTipoUs = tipoUsuario.findAll();
		return listTipoUs;
	}

	public void setListTipoUs(List<TipoUsuario> listTipoUs) {
		this.listTipoUs = listTipoUs;
	}

	public List<Usuarios> getListUsers() {
		this.listUsers = usuario.findAll();
		return listUsers;
	}

	public void setListUsers(List<Usuarios> listUsers) {
		this.listUsers = listUsers;
	}
	

	public Personas getListBoth() {
		this.listBoth = per.findPer(getIdPer());
		return listBoth;
	}

	public void setListBoth(Personas listBoth) {
		this.listBoth = listBoth;
	}

	@PostConstruct
	
	public void init () {
		person = new Personas();
		tipoUs = new TipoUsuario();
		users = new Usuarios();		
	}
	
	/*M�todos de Persona*/
	public void createPersona () {
		try {
			/*Guardar Persona*/
			persona.create(person); 
			/*Guardar Usuario*/
			users.setTipoUsuario(tipoUs);
        	users.setPersonas(person);
			usuario.create(users);
			init ();
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	public void editPersona () {
		try {
			/*Editar Persona */
			persona.edit(person);
			/*Editar Usuario*/
			users.setTipoUsuario(tipoUs);
        	users.setPersonas(person);
			usuario.edit(users);
			init ();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public void findPersonById (Personas person) {
		try {
			this.person = person;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*M�todos usuarios */
	
	
	public void findAllUsers () {
		try {
			usuario.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void findUsersById (Usuarios users) {
		try {
			this.users = users;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
