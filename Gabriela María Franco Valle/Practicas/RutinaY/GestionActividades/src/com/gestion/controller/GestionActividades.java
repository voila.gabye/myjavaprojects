package com.gestion.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;
import com.gestion.model.Actividades;
import com.gestion.model.Estados;
import com.gestion.model.FaseAct;
import com.gestion.model.Fases;
import com.gestion.model.FechasEspeciales;
import com.gestion.model.Usuarios;
import com.gestion.util.Dao;

@Component
@SessionScope
@ManagedBean
public class GestionActividades implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	@Qualifier("fechaEspecial")
	public Dao<FechasEspeciales> fechaEspecial;
	
	@Autowired
	@Qualifier("estados")
	public Dao<Estados> estados;
	
	@Autowired
	@Qualifier("fases")
	public Dao<Fases> fases;
	
	@Autowired
	@Qualifier("fasesAct")
	public Dao<FaseAct> fasesAct;
	
	@Autowired
	@Qualifier("actividad")
	public Dao<Actividades> actividad;
	
	@Autowired
	@Qualifier("usuario")
	public Dao<Usuarios> usuario;
	
	private FechasEspeciales fechasEspe = new FechasEspeciales();
	private Estados estado = new Estados();
	private Fases fase = new Fases();
	private FaseAct faseAct = new FaseAct();
	private Actividades actividades = new Actividades();
	private Usuarios users = new Usuarios();
	
	private List<FechasEspeciales> listEspecial;
	private List<Estados> listEstados;
	private List<Fases> listFases;
	private List<FaseAct> listFaseAct;
	private List<Actividades> listActividades;
	private List<Usuarios> listUsers;
	
	private int estadoAct =1;
	
	
	public int getEstadoAct() {
		return estadoAct;
	}
	public void setEstadoAct(int estadoAct) {
		this.estadoAct = estadoAct;
	}
	public FechasEspeciales getFechasEspe() {
		return fechasEspe;
	}
	public void setFechasEspe(FechasEspeciales fechasEspe) {
		this.fechasEspe = fechasEspe;
	}
	
	public Estados getEstado() {
		return estado;
	}
	public void setEstado(Estados estado) {
		this.estado = estado;
	}
	public Fases getFase() {
		return fase;
	}
	public void setFase(Fases fase) {
		this.fase = fase;
	}
	public FaseAct getFaseAct() {
		return faseAct;
	}
	public void setFaseAct(FaseAct faseAct) {
		this.faseAct = faseAct;
	}
	public Actividades getActividades() {
		return actividades;
	}
	public void setActividades(Actividades actividades) {
		this.actividades = actividades;
	}
	
	public Usuarios getUsers() {
		return users;
	}
	public void setUsers(Usuarios users) {
		this.users = users;
	}
	public List<FechasEspeciales> getListEspecial() {
		this.listEspecial = fechaEspecial.findAll();
		return listEspecial;
	}
	public void setListEspecial(List<FechasEspeciales> listEspecial) {
		this.listEspecial = listEspecial;
	}
	public List<Estados> getListEstados() {
		this.listEstados = estados.findAll();
		return listEstados;
	}
	public void setListEstados(List<Estados> listEstados) {
		this.listEstados = listEstados;
	}
	public List<Fases> getListFases() {
		this.listFases = fases.findAll();
		return listFases;
	}
	public void setListFases(List<Fases> listFases) {
		this.listFases = listFases;
	}
	public List<FaseAct> getListFaseAct() {
		this.listFaseAct = fasesAct.findAll();
		return listFaseAct;
	}
	public void setListFaseAct(List<FaseAct> listFaseAct) {
		this.listFaseAct = listFaseAct;
	}
	public List<Actividades> getListActividades() {
		this.listActividades = actividad.findAll();
		return listActividades;
	}
	public void setListActividades(List<Actividades> listActividades) {
		this.listActividades = listActividades;
	}
	
	public List<Usuarios> getListUsers() {
		this.listUsers = usuario.findAll();
		return listUsers;
	}

	public void setListUsers(List<Usuarios> listUsers) {
		this.listUsers = listUsers;
	}
	
@PostConstruct
	
	public void init () {
	fechasEspe = new FechasEspeciales();
	estado = new Estados();
	fase = new Fases();
	faseAct = new FaseAct();
	actividades = new Actividades();
	users = new Usuarios();		
	}


	/*M�todos de Fechas Especiales*/
	public void createFechaEsp() {
		try {
			fechaEspecial.create(fechasEspe);
			init ();
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	public void editFechaEsp() {
		try {
			fechaEspecial.edit(fechasEspe);
			init ();
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	public void deleteFechaEsp (FechasEspeciales fespecial) {
		try {
			this.fechasEspe = fespecial;
			fechaEspecial.remove(fechasEspe);
			init ();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void findFechaEspById (FechasEspeciales fespecial) {
		this.fechasEspe = fespecial;
	}
	
	/*M�todos Actividades*/
	
	public void createAct () {
		try {
			estado.setIdEstado(estadoAct);
			actividades.setEstados(estado);
			actividades.setUsuarios(users);
			System.out.println("fecha" + actividades.getFecha());
			actividad.create(actividades);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void editAct() {
		try {
			actividades.setEstados(estado);
			actividades.setUsuarios(users);
			actividad.edit(actividades);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void findActById (Actividades act) {
		this.actividades = act;
	}
	
	public void deleteAct (Actividades act) {
		try {
			this.actividades = act;
			actividad.remove(actividades);;
			init ();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void fech () {
	try {
		listEspecial = fechaEspecial.findAll() ;
		fechasEspe.getFecha();
	} catch (Exception e) {
		e.printStackTrace();
	}
		
	}
}
