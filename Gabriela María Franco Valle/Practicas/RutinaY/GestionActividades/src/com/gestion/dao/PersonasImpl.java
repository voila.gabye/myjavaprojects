package com.gestion.dao;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import com.gestion.model.Personas;
import com.gestion.model.Usuarios;
import com.gestion.util.AbstractFacade;
import com.gestion.util.Dao;


@Transactional
public class PersonasImpl extends AbstractFacade<Personas> implements Dao<Personas> {

	private SessionFactory sf;

	public PersonasImpl(SessionFactory sf, Class<Personas> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}

	
	public Personas findPer(int idPersona) {
		try {
			Personas persona = new Personas();

			CriteriaBuilder cb = sf.getCriteriaBuilder();
			CriteriaQuery<Personas> cq = cb.createQuery(Personas.class);
			Root<Personas> root = cq.from(Personas.class);
			Join<Personas, Usuarios> join = root.join("id_persona");
			cq.where(cb.equal(join.get("id_persona"), idPersona));

			persona = sf.getCurrentSession().createQuery(cq).getSingleResult();
			return persona;
		} catch (Exception e) {
			return null;
		}
	}
	
}
