package com.gestion.dao;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import com.gestion.model.Estados;
import com.gestion.util.AbstractFacade;
import com.gestion.util.Dao;

@Transactional
public class EstadosImpl extends AbstractFacade<Estados> implements Dao<Estados> {

	private SessionFactory sf;

	public EstadosImpl(SessionFactory sf, Class<Estados> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}

}
