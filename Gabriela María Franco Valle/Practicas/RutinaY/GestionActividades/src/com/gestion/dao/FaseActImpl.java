package com.gestion.dao;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import com.gestion.model.FaseAct;
import com.gestion.util.AbstractFacade;
import com.gestion.util.Dao;

@Transactional
public class FaseActImpl extends AbstractFacade<FaseAct> implements Dao<FaseAct> {

	private SessionFactory sf;

	public FaseActImpl(SessionFactory sf, Class<FaseAct> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}

}
