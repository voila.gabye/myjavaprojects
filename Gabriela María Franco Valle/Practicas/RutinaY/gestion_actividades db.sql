create database gestion_actividades;
use gestion_actividades;

create table personas(
id_persona int not null primary key auto_increment,
nombre varchar (150) not null,
apellido varchar (150) not null,
telefono varchar (10) not null
);

create table tipo_usuario(
id_tipo_us int not null primary key auto_increment,
tipo_usuario varchar (13) not null
);

create table usuarios(
id_usuario int not null primary key auto_increment,
user varchar (150) not null,
password varchar (150) not null,
email varchar (150) not null,
tipo_usuario int not null,
id_persona int not null,
constraint us_per foreign key (id_persona) references personas (id_persona),
constraint us_tip foreign key (tipo_usuario) references tipo_usuario (id_tipo_us)
);

create table fechas_especiales(
id_especial int not null primary key auto_increment,
fecha date not null,
nombre varchar (150) not null
);

create table fases(
id_fase int not null primary key auto_increment,
fase varchar(10) not null
);

create table estados(
id_estado int not null primary key auto_increment,
estado varchar(9) not null
);


create table actividades(
id_actividades int not null primary key auto_increment,
nombre varchar (150) not null,
descripcion varchar (250) not null,
fecha datetime not null,
usuario int not null,
estado int not null,
constraint acti_us foreign key (usuario) references usuarios (id_usuario),
constraint acti_estado foreign key (estado) references estados (id_estado)
);

create table fase_act (
id_fase_act int not null primary key auto_increment,
fase int not null,
actividad int not null,
fecha_cambio datetime,
constraint fase_fase foreign key (fase) references fases (id_fase),
constraint fase_act foreign key (actividad) references actividades (id_actividades)
);