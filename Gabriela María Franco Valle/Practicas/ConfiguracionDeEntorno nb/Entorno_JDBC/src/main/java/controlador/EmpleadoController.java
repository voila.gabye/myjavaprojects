package controlador;

import conexion.Conexion;
import conexion.Conexion2;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.sql.PreparedStatement;
import javax.faces.bean.ManagedBean;
import modelo.Empleado;

@ManagedBean
@SessionScoped
public class EmpleadoController implements Serializable {

    Conexion conn;
    Conexion2 conn2;

    public EmpleadoController(Conexion conn, Conexion2 conn2) {
        this.conn = conn;
        this.conn2 = conn2;
    }

    public void guardar(Empleado empleado) {
        String sql = "insert into empleado values (?,?,?,?,?,?,?,?,?)";
        try {
            
            
            
            PreparedStatement ps = conn.connectar().prepareStatement(sql);
            ps.setInt(1, empleado.getIdEmpleado());
            ps.setString(1, empleado.getPrimer_nombre());
            ps.setString(2, empleado.getSegundo_nombre());
            ps.setString(3, empleado.getPrimer_apellido());
            ps.setString(4, empleado.getSegundo_apellido());
            ps.setString(5, empleado.getCorreo());
            ps.setString(6, empleado.getTel_fijo());
            ps.setString(7, empleado.getTel_movil());
          
        } catch (Exception e) {
        }

    }

}
