
package facade;

import conexion.Conexion;
import conexion.Conexion2;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import modelo.Empleado;



public class EmpleadoFacade implements Serializable {


    Conexion conn;
    Conexion2 conn2;

    public EmpleadoFacade(Conexion conn, Conexion2 conn2) {
        this.conn = conn;
        this.conn2 = conn2;
    }

    public boolean guardar(Empleado empleado) {
        String sql = "insert into empleado values (?,?,?,?,?,?,?,?,?)";
        
        Empleado emple = empleado.getIdEmpleado ();
        
        try {
            PreparedStatement ps = conn.connectar().prepareStatement(sql);
            ps.setInt (1, empleado.getIdEmpleado ());
            ps.setString(2, empleado.getPrimer_nombre());
            ps.setString(3, empleado.getSegundo_nombre());
            ps.setString(4, empleado.getPrimer_apellido());
            ps.setString(5, empleado.getSegundo_apellido());
            ps.setString(6, empleado.getCorreo());
            ps.setString(7, empleado.getTel_fijo());
            ps.setString(8, empleado.getTel_movil());
            ps.setInt (9, emple.getJefe);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
        
        public boolean actualizar (Empleado empleado) {
        String sql = "update empleado set primer_nombre=?, segundo_nombre=?, primer_apellido=?, segundo_apellido=?, correo=?, tel_fijo=?, tel_movil=?, jefe=? where idEmpleado=?";
        
        Empleado emple = empleado.getIdEmpleado ();
        
        try {
            PreparedStatement ps = conn.connectar().prepareStatement(sql);
            ps.setString(1, empleado.getPrimer_nombre());
            ps.setString(2, empleado.getSegundo_nombre());
            ps.setString(3, empleado.getPrimer_apellido());
            ps.setString(4, empleado.getSegundo_apellido());
            ps.setString(5, empleado.getCorreo());
            ps.setString(6, empleado.getTel_fijo());
            ps.setString(7, empleado.getTel_movil);
            ps.setInt (8, emple.getJefe);
            ps.setInt (9, empleado.getIdEmpleado ());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }

        public List <Empleado> consultarAll () {
            String sql = "select * from empleado";
            try {
                PreparedStatement ps = conn.connectar().prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                List <Empleado>  lista = new LinkedList <>();
                Empleado empleado;
                Empleado emple;
                
                while (rs.next()) {
                emple = new Empleado(rs.getInt ("idEmpleado"));
                empleado = new Empleado (rs.getInt ("idEmpleado"));
                empleado.setJefe (emple);
                empleado.setPrimer_nombre (rs.getString ("primer_nombre"));
                empleado.setSegundo_nombre (rs.getString ("segundo_nombre"));
                empleado.setPrimer_apellido (rs.getString ("primer_apellido"));
                empleado.setSegundo_apellido (rs.getString ("segundo_apellido"));
                empleado.setCorreo (rs.getString ("correo"));
                empleado.setTel_fijo (rs.getString ("tel_fijo"));
                empleado.setTel_movil (rs.getString ("tel_movil"));
                lista.add (empleado);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
    
    public List <Empleado> consultarById (int idEmpleado) {
            String sql = "select * from empleado where idEmpleado=?";
            try {
                PreparedStatement ps = conn.connectar().prepareStatement(sql);
                ps.setInt (1, idEmpleado);
                ResultSet rs = ps.executeQuery();
                List <Empleado>  lista = new LinkedList <>();
                Empleado emple;
                Empleado empleado;
                while (rs.next()) {
                emple = new Empleado(rs.getInt ("idEmpleado"));
                empleado = new Empleado (rs.getInt ("idEmpleado"));
                empleado.setJefe (emple);
                empleado.setPrimer_nombre (rs.getString ("primer_nombre"));
                empleado.setSegundo_nombre (rs.getString ("segundo_nombre"));
                empleado.setPrimer_apellido (rs.getString ("primer_apellido"));
                empleado.setSegundo_apellido (rs.getString ("segundo_apellido"));
                empleado.setCorreo (rs.getString ("correo"));
                empleado.setTel_fijo (rs.getString ("tel_fijo"));
                empleado.setTel_movil (rs.getString ("tel_movil"));
                lista.add (empleado);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }


      public boolean eliminar(int idEmpleado) {

      String sql = "delete from empleado where idEmpleado=?";
       try {
       PreparedStatement ps = conn.connectar().prepareStatement(sql);
       ps.setInt(1, idEmpleado);
       ps.executeUpdate();
       return true;
       } catch (Exception e) {
       return false;
       }
    }
}

