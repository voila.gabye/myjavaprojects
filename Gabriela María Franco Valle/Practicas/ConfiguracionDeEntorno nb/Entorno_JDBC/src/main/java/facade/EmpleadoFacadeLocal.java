package facade;

import java.util.List;
import javax.ejb.Local;
import modelo.Empleado;

@Local
public interface EmpleadoFacadeLocal {

    void guardar(Empleado empleado);

    void editar(Empleado empleado);

    void eliminar(Empleado empleado);

    List<Empleado> consultarAll();

}
