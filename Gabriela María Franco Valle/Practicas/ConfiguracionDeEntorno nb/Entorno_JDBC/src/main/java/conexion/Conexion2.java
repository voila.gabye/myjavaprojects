package conexion;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion2 {

    static String bd = "bd2";
    static String user = "root";
    static String pass = "root";
    static String url = "jdbc:mysql://localhost/" + bd + "?useSSL=false";
    Connection conn = null;

    public Conexion2() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            conn = DriverManager.getConnection(url, user, pass);
            if (conn != null) {
                System.out.println("Exito en la conexion");
            }
        } catch (Exception e) {
            System.out.println("Error en la conexion");
        }
    }

    public Connection connectar() {
        return conn;
    }

    public void desconectar() throws Exception {
        conn.close();
    }
}
