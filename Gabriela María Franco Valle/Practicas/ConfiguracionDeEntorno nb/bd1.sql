create database bd1;
use bd1;

create table empleado (
idEmpleado int not null primary key auto_increment,
primer_nombre varchar (150) not null,
segundo_nombre varchar (150) not null,
primer_apellido varchar (150) not null,
segundo_apellido varchar (150) not null,
correo varchar (150) not null,
tel_fijo varchar (150) not null,
tel_movil varchar (150) not null,
jefe int null,
constraint fk_jef foreign key (jefe) references empleado (idEmpleado)
);