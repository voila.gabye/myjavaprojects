package com.reportex.crl;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import com.reportex.dao.ProductoImpl;
import com.reportex.dao.ReporteProductoImpl;
import com.reportex.model.Categoria;
import com.reportex.model.Producto;
import com.reportex.model.ReporteProducto;
import com.reportex.util.Dao;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Component
@SessionScope
@ManagedBean
public class GestionProductos {

	@Autowired
	@Qualifier("producto")
	public Dao<Producto> producto;

	@Autowired
	@Qualifier("categoria")
	public Dao<Categoria> categoria;

	@Autowired
	@Qualifier("viewProd")
	public ReporteProductoImpl viewProducto;
	
	private Producto prod = new Producto();
	private Categoria cat = new Categoria();

	private List<Producto> listProd;
	private List<Categoria> listCat;
	private List<ReporteProducto> listViewProd;
	

	public Producto getProd() {
		return prod;
	}

	public void setProd(Producto prod) {
		this.prod = prod;
	}

	public Categoria getCat() {
		return cat;
	}

	public void setCat(Categoria cat) {
		this.cat = cat;
	}

	public List<Producto> getListProd() {
		this.listProd = producto.findAll();
		return listProd;
	}

	public void setListProd(List<Producto> listProd) {
		this.listProd = listProd;
	}

	public List<Categoria> getListCat() {
		this.listCat = categoria.findAll();
		return listCat;
	}

	public void setListCat(List<Categoria> listCat) {
		this.listCat = listCat;
	}
	

	public List<ReporteProducto> getListViewProd() {
		this.listViewProd = viewProducto.detalleProd();
		return listViewProd;
	}

	public void setListViewProd(List<ReporteProducto> listViewProd) {
		this.listViewProd = listViewProd;
	}

	@PostConstruct

	public void init() {
		prod = new Producto();
		cat = new Categoria();
	}

	
	public void mostrarProductos() throws JRException, IOException {

		File jasper = new File(
				FacesContext.getCurrentInstance().getExternalContext().getRealPath("reportes/detallesProductos.jasper"));
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("param", "GF");
		JasperPrint jasperprint = JasperFillManager.fillReport(jasper.getPath(), parametros,
				new JRBeanCollectionDataSource(getListViewProd()));

		/* Visualizar en el navegador */
		byte[] bytes = JasperRunManager.runReportToPdf(jasper.getPath(), parametros,
				new JRBeanCollectionDataSource(getListViewProd()));
		HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
				.getResponse();
		response.setContentType("application/pdf");
		response.setContentLength(bytes.length);
		ServletOutputStream os = response.getOutputStream();
		os.write(bytes, 0, bytes.length);
		os.flush();
		os.close();
		FacesContext.getCurrentInstance().responseComplete();
	}
}
