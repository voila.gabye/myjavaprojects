package com.reportex.config;

import java.util.Properties;

import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.reportex.dao.CategoriaImpl;
import com.reportex.dao.ClienteImpl;
import com.reportex.dao.ProductoImpl;
import com.reportex.dao.ReporteProductoImpl;
import com.reportex.model.Categoria;
import com.reportex.model.Cliente;
import com.reportex.model.Producto;
import com.reportex.model.ReporteProducto;
import com.reportex.util.Dao;



@Configuration
@ComponentScan(basePackages = "com.reportex")
@EnableTransactionManagement(proxyTargetClass = true)
public class AppContextConfig {

	@Bean(name = "dataSource")
	public DataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://usam-sql.sv.cds:3306/facturacion?useSSL=false");
		dataSource.setUsername("kz");
		dataSource.setPassword("kzroot");
		return dataSource;
	}

	private Properties getHibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.show_sql", "true");
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		return properties;
	}

	@Autowired
	@Bean(name = "sessionFactory")
	public SessionFactory getsessionFactory(DataSource dataSource) {
		LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
		sessionBuilder.scanPackages("com.reportex.model");
		sessionBuilder.addProperties(getHibernateProperties());
		return sessionBuilder.buildSessionFactory();
	}

	@Autowired
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);
		return transactionManager;
	}
	
	@Autowired
	@Bean(name = "viewProd")
	public ReporteProductoImpl viewProd (SessionFactory sessionFactory) {
		return new ReporteProductoImpl(sessionFactory, ReporteProducto.class);
	}
	
	@Autowired
	@Bean(name = "producto")
	public Dao producto (SessionFactory sessionFactory) {
		return new ProductoImpl(sessionFactory, Producto.class);
	}
	
	@Autowired
	@Bean(name = "categoria")
	public Dao categoria (SessionFactory sessionFactory) {
		return new CategoriaImpl(sessionFactory, Categoria.class);
	}

	@Autowired
	@Bean(name = "cliente")
	public Dao cliente (SessionFactory sessionFactory) {
		return new ClienteImpl(sessionFactory, Cliente.class);
	}
}
