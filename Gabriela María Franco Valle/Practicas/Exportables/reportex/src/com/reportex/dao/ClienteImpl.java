package com.reportex.dao;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.reportex.model.Cliente;
import com.reportex.util.AbstractFacade;
import com.reportex.util.Dao;

@Transactional
public class ClienteImpl extends AbstractFacade<Cliente> implements Dao<Cliente> {

	private SessionFactory sf;

	public ClienteImpl(SessionFactory sf, Class<Cliente> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}

}