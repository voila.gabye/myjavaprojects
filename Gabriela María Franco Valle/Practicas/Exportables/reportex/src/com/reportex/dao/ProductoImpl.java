package com.reportex.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.transaction.annotation.Transactional;

import com.reportex.model.Producto;
import com.reportex.util.Dao;
import com.reportex.util.AbstractFacade;


@Transactional
public class ProductoImpl extends AbstractFacade<Producto> implements Dao<Producto> {

	private SessionFactory sf;

	public ProductoImpl(SessionFactory sf, Class<Producto> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}

	
	public List<Producto> detalleProd (){
		  List<Producto> list = new ArrayList<>();
	        try {
	        	session=sf.getCurrentSession();
	            Transaction  tx=session.getTransaction();
	            list = session.createNativeQuery("select * from view_productos").getResultList();
	        } catch (Exception e) {
	            e.printStackTrace();
	            return null;
	        }
          
	        return list;
	    }
	
	
	
}
