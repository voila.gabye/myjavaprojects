package com.reportex.dao;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.reportex.model.Factura;
import com.reportex.util.AbstractFacade;
import com.reportex.util.Dao;

@Transactional
public class FacturaImpl  extends AbstractFacade<Factura> implements Dao<Factura> {

	private SessionFactory sf;

	public FacturaImpl(SessionFactory sf, Class<Factura> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}

}