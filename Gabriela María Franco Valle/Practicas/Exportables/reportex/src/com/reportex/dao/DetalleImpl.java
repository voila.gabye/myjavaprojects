package com.reportex.dao;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.reportex.model.Detalle;
import com.reportex.util.AbstractFacade;
import com.reportex.util.Dao;

@Transactional
public class DetalleImpl extends AbstractFacade<Detalle> implements Dao<Detalle> {

	private SessionFactory sf;

	public DetalleImpl(SessionFactory sf, Class<Detalle> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}

}