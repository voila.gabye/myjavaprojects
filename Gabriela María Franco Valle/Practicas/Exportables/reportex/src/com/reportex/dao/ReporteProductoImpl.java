package com.reportex.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.transaction.annotation.Transactional;

import com.reportex.model.ReporteProducto;
import com.reportex.util.AbstractFacade;
import com.reportex.util.Dao;

@Transactional
public class ReporteProductoImpl extends AbstractFacade<ReporteProducto> implements Dao<ReporteProducto> {

	private SessionFactory sf;

	public ReporteProductoImpl(SessionFactory sf, Class<ReporteProducto> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}

	
	public List<ReporteProducto> detalleProd (){
		  List<ReporteProducto> list = new ArrayList<>();
	        try {
	        	session=sf.getCurrentSession();
	            Transaction  tx=session.getTransaction();
	            list = session.createNativeQuery("select * from view_productos").getResultList();
	            
	            Iterator<ReporteProducto> it= list.iterator();
	            while(it.hasNext()) {
                	       
	            }
	            
	        } catch (Exception e) {
	            e.printStackTrace();
	            return null;
	        }

	        return list;
	}
}
