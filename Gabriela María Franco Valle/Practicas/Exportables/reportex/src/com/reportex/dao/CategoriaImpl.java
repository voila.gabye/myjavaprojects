package com.reportex.dao;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.reportex.model.Categoria;
import com.reportex.util.AbstractFacade;
import com.reportex.util.Dao;

@Transactional
public class CategoriaImpl extends AbstractFacade<Categoria> implements Dao<Categoria> {

		private SessionFactory sf;

		public CategoriaImpl(SessionFactory sf, Class<Categoria> entityClass) {
			super(entityClass);
			this.sf = sf;
		}

		@Override
		protected SessionFactory sessionFactory() {
			return sf;
		}

	}
