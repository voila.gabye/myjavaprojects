package com.reportex.util;

import java.util.List;

public interface Dao<T> {

    public void create(T t);

    public void remove(T t);

    public void edit(T t);

    public T find(Object e);

    public List<T> findAll();


}