create database ventas;
use ventas;

create table cliente (
id_cliente int not null primary key auto_increment,
nombre varchar (150) not null,
apellido varchar (150) not null,
direccion varchar (150) not null,
fecha_nacimiento date not null,
telefono varchar (10),
email varchar (150)
);

create table modo_pago (
num_pago int not null primary key auto_increment,
nombre varchar (150) not null,
otros_detalles varchar (150) not null
);

create table categoria (
id_categoria int not null primary key auto_increment,
nombre varchar (150) not null,
descripcion varchar (150) not null
);

create table producto (
id_producto int not null primary key auto_increment,
nombre varchar (150) not null,
precio decimal (5,2) not null,
stock int not null,
id_categoria int not null,
constraint fk_cat foreign key (id_categoria) references categoria (id_categoria)
);

create table factura (
num_factura int not null primary key auto_increment,
id_cliente int not null,
fecha date,
num_pago int not null,
constraint fk_cli foreign key (id_cliente) references cliente (id_cliente),
constraint fk_np foreign key (num_pago) references modo_pago (num_pago)
);

create table detalle (
num_detalle int not null primary key auto_increment,
id_factura int not null,
id_producto int not null,
cantidad int not null,
precio decimal (5,2) not null,
constraint fk_fact foreign key (id_factura) references factura (num_factura),
constraint fk_prod foreign key (id_producto) references producto (id_producto)
);