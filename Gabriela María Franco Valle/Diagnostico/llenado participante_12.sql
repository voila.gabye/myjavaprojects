INSERT INTO `participante_12`.`modo_pago` (`num_pago`, `nombre`, `otros_detalles`) VALUES ('1', 'efectivo', 'default');
INSERT INTO `participante_12`.`modo_pago` (`num_pago`, `nombre`, `otros_detalles`) VALUES ('2', 'tarjeta de credito', 'default');
INSERT INTO `participante_12`.`modo_pago` (`num_pago`, `nombre`, `otros_detalles`) VALUES ('3', 'tarjeta de regalo', 'default');
INSERT INTO `participante_12`.`modo_pago` (`num_pago`, `nombre`, `otros_detalles`) VALUES ('4', 'cheque', 'para empresas');

INSERT INTO `participante_12`.`categoria` (`nombre`, `descripcion`) VALUES ('1', 'entrada', 'dulces y saladas');
INSERT INTO `participante_12`.`categoria` (`nombre`, `descripcion`) VALUES ('2', 'mariscos', 'de agua salada');
INSERT INTO `participante_12`.`categoria` (`nombre`, `descripcion`) VALUES ('3', 'carnes', 'rojas y blancas');
INSERT INTO `participante_12`.`categoria` (`nombre`, `descripcion`) VALUES ('4', 'postres', 'dulces');
INSERT INTO `participante_12`.`categoria` (`nombre`, `descripcion`) VALUES ('5', 'bebidas', 'embotelladas y enlatadas');


INSERT INTO `participante_12`.`producto` (`id_producto`, `nombre`, `precio`, `stock`, `id_categoria`) VALUES ('1', 'Ensalada César', '4.50', '30', '1');
INSERT INTO `participante_12`.`producto` (`id_producto`, `nombre`, `precio`, `stock`, `id_categoria`) VALUES ('2', 'Caldo de res', '1.50', '30', '1');
INSERT INTO `participante_12`.`producto` (`id_producto`, `nombre`, `precio`, `stock`, `id_categoria`) VALUES ('3', 'Pescado frito', '5.00', '20', '2');
INSERT INTO `participante_12`.`producto` (`id_producto`, `nombre`, `precio`, `stock`, `id_categoria`) VALUES ('4', 'Camarones al ajillo', '5.00', '50', '2');
INSERT INTO `participante_12`.`producto` (`id_producto`, `nombre`, `precio`, `stock`, `id_categoria`) VALUES ('5', 'Carne asada', '4.50', '30', '3');
INSERT INTO `participante_12`.`producto` (`id_producto`, `nombre`, `precio`, `stock`, `id_categoria`) VALUES ('6', 'Pollo encebollado', '4.00', '30', '3');
INSERT INTO `participante_12`.`producto` (`id_producto`, `nombre`, `precio`, `stock`, `id_categoria`) VALUES ('7', 'Cheese cake', '2.50', '30', '4');
INSERT INTO `participante_12`.`producto` (`id_producto`, `nombre`, `precio`, `stock`, `id_categoria`) VALUES ('8', 'Tiramisu', '2.50', '30', '4');
INSERT INTO `participante_12`.`producto` (`id_producto`, `nombre`, `precio`, `stock`, `id_categoria`) VALUES ('9', 'Coca Cola', '0.60', '50', '5');
INSERT INTO `participante_12`.`producto` (`id_producto`, `nombre`, `precio`, `stock`, `id_categoria`) VALUES ('10', 'Fanta', '0.60', '50', '5');
INSERT INTO `participante_12`.`producto` (`id_producto`, `nombre`, `precio`, `stock`, `id_categoria`) VALUES ('11', 'Petit variedad', '0.60', '50', '5');
INSERT INTO `participante_12`.`producto` (`id_producto`, `nombre`, `precio`, `stock`, `id_categoria`) VALUES ('12', 'Jugo del valle', '0.50', '50', '5');