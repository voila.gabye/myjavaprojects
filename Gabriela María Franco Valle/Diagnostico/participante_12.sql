create database participante_12;
use participante_12;

create table cliente (
id_cliente int not null auto_increment primary key,
nombre varchar (150) not null,
apellido varchar (150) not null,
direccion varchar (150) not null,
fecha_nacimiento date not null,
telefono varchar (11) not null,
email varchar (150) not null
);

create table modo_pago (
num_pago int not null auto_increment primary key,
nombre varchar (150) not null,
otros_detalles varchar (150) not null
);

create table factura (
num_factura int not null auto_increment primary key,
id_cliente int not null,
fecha date not null,
num_pago int not null,
constraint fact_client foreign key (id_cliente) references cliente(id_cliente) ON DELETE CASCADE ON UPDATE CASCADE,
constraint fact_pago foreign key (num_pago) references modo_pago(num_pago) ON DELETE CASCADE ON UPDATE CASCADE
);


create table categoria (
id_categoria int not null auto_increment primary key,
nombre varchar (150) not null,
descripcion varchar (200) not null
);

create table producto (
id_producto int not null auto_increment primary key,
nombre varchar (150) not null,
precio decimal (6,2) not null,
stock int not null,
id_categoria int not null,
constraint pro_cat foreign key (id_categoria) references categoria(id_categoria)  ON DELETE CASCADE ON UPDATE CASCADE
);


create table detalle (
num_detalle int not null auto_increment primary key,
id_factura int not null,
id_producto int not null,
cantidad int not null,
precio decimal (6,2) not null,
constraint det_fact foreign key (id_factura) references factura (num_factura)  ON DELETE CASCADE ON UPDATE CASCADE,
constraint det_pro foreign key (id_producto) references producto (id_producto)  ON DELETE CASCADE ON UPDATE CASCADE
);

DELIMITER //
CREATE PROCEDURE sp_insercion_factura(
IN id_cliente int,
IN fecha date ,
IN num_pago int
)
BEGIN
INSERT INTO factura (id_cliente,fecha,num_pago)
values (id_cliente,fecha,num_pago);
end//


DELIMITER //
create trigger tr_dism_stock
after insert on detalle
for each row
begin
update producto set stock = stock_new.cantidad
where id_producto = new.id_producto;
end
//
