package controller;

import ejb.ClienteFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import modelo.Cliente;

@Named(value = "clienteController")
@SessionScoped
public class ClienteController implements Serializable {

    public ClienteController() {
    }

    @EJB
    private ClienteFacadeLocal clienteEJB;
    private Cliente cliente;
    private List<Cliente> lista;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    

    public void init (){
    }
    
    public void findAll() {
    
    }
    
}
