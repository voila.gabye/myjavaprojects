package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "factura")
public class Factura implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "num_factura")
    private int num_factura;

    @ManyToOne
    @Column(name = "id_cliente")
    private Cliente id_cliente;

    @Column(name = "fecha")
    private String fecha;
    
    @ManyToOne
    @Column(name = "num_pago")
    private Modo_Pago num_pago;

    public int getNum_factura() {
        return num_factura;
    }

    public void setNum_factura(int num_factura) {
        this.num_factura = num_factura;
    }

    public Cliente getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Cliente id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Modo_Pago getNum_pago() {
        return num_pago;
    }

    public void setNum_pago(Modo_Pago num_pago) {
        this.num_pago = num_pago;
    }


    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + this.num_factura;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Factura other = (Factura) obj;
        if (this.num_factura != other.num_factura) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Factura{" + "num_factura=" + num_factura + '}';
    }
    
}
