
package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "modo_pago")
public class Modo_Pago implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "num_pago")
    private int num_pago;
    
    @Column(name = "nombre")
    private String nombre;
    
    @Column(name = "otros_detalles")
    private String otros_detalles;

    public int getNum_pago() {
        return num_pago;
    }

    public void setNum_pago(int num_pago) {
        this.num_pago = num_pago;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getOtros_detalles() {
        return otros_detalles;
    }

    public void setOtros_detalles(String otros_detalles) {
        this.otros_detalles = otros_detalles;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + this.num_pago;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Modo_Pago other = (Modo_Pago) obj;
        if (this.num_pago != other.num_pago) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modo_Pago{" + "num_pago=" + num_pago + '}';
    }
    
    
}
