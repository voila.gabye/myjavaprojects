
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Factura;


@Local
public interface FacturaFacadeLocal {

    void create(Factura factura);

    void edit(Factura factura);

    void remove(Factura factura);

    Factura find(Object id);

    List<Factura> findAll();

}
