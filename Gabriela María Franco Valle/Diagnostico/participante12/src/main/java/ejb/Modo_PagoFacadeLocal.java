
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Modo_Pago;


@Local
public interface Modo_PagoFacadeLocal {

    void create(Modo_Pago modo_Pago);

    void edit(Modo_Pago modo_Pago);

    void remove(Modo_Pago modo_Pago);

    Modo_Pago find(Object id);

    List<Modo_Pago> findAll();

    
}
