
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Detalle;

@Local
public interface DetalleFacadeLocal {

    void create(Detalle detalle);

    void edit(Detalle detalle);

    void remove(Detalle detalle);

    Detalle find(Object id);

    List<Detalle> findAll();
}
