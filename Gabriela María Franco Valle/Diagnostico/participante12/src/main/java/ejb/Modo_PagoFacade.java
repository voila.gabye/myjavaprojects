
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Modo_Pago;


@Stateless
public class Modo_PagoFacade extends AbstractFacade<Modo_Pago> implements Modo_PagoFacadeLocal {

    @PersistenceContext(unitName = "participantePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public Modo_PagoFacade() {
        super(Modo_Pago.class);
    }
    
}
