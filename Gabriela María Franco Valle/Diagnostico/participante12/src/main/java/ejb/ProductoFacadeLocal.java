
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Producto;


@Local
public interface ProductoFacadeLocal {

    void create(Producto producto);

    void edit(Producto producto);

    void remove(Producto producto);

    Producto find(Object id);

    List<Producto> findAll();

}
