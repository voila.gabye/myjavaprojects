<%-- 
    Document   : mostrarServiSoldado
    Created on : 08-20-2019, 03:57:47 PM
    Author     : susana.menjivarusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<%
    HttpSession sesion = request.getSession();

    String usuario;
    if (sesion.getAttribute("usuario") != null) {
        usuario = sesion.getAttribute("usuario").toString();
    } else {
        response.sendRedirect("login.jsp");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/materialize.css" rel="stylesheet" type="text/css"/>
        <title>Servicios Realizados</title>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
        <div class="container">
            <div class="card-panel">
                <h5 class="white-text head-panel center-align"> Consulta Servicios del Soldado </h5>
                <table class="highlight responsive-table tabla">
                    <thead>
                    <th>Soldado</th>
                    <th>Servicio a Realizar</th>
                    <th>Fecha</th>
                    <th> Acciones</th>
                    </thead>
                    <c:forEach items="${lista}" var="l">
                        <tr>
                            <td>${l.idsoldado.nombre}</td>
                            <td>${l.idservicio.servicio}</td>
                            <td>${l.fecha}</td>
                            <td>
                                <a class="btn deep-orange darken-4" href="servicioSoldado?action=eliminar&id=<c:out value="${l.idserviciosoldado}"/>">Eliminar</a>
                                <a class="btn light-green darken-2" href="servicioSoldado?action=consultarById&id=<c:out value="${l.idserviciosoldado}"/>">Actualizar</a>

                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
