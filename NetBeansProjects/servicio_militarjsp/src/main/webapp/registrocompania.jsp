<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<%
    HttpSession sesion = request.getSession();

    String usuario;
    if (sesion.getAttribute("usuario") != null) {
        usuario = sesion.getAttribute("usuario").toString();
    } else {
        response.sendRedirect("login.jsp");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro Compania</title>
        <link href="css/materialize.css" rel="stylesheet" type="text/css"/>
        <script src="js/materialize.js" type="text/javascript"></script>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
        <div class="container">
            <div class="card-panel z-depth-5">
                <h5 class="white-text head-panel center-align"> Registro Compañia </h5>
                <div class="formu">
                    <form method="post" action="compania?action=insertar">
                        <div class="row">
                            <div class="input-field col x12 s12 m12 l12">
                                <input id="codigo" type="text" class="validate" name="codigo" >
                                <label for="codigo">Código de Compañia</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m12 l12">
                                <input id="compania" type="text" class="validate" name="compania" >
                                <label for="compania">Nombre de la Compañia</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m12 l12">
                                <input id="actividad" type="text" class="validate" name="actividad" >
                                <label for="actividad">Actividad</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field ">
                                <button class="btn lime darken-2">Enviar</button>
                                ${msg}
                            </div>
                        </div>
                    </form>
                    <a class="btn light-green darken-2" href="compania?action=consultar">Consultar</a>
                </div>
            </div>
        </div>
    </body>
</html>


