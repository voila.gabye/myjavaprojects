

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<%
    HttpSession sesion = request.getSession();

    String usuario;
    if (sesion.getAttribute("usuario") != null) {
        usuario = sesion.getAttribute("usuario").toString();
    } else {
        response.sendRedirect("login.jsp");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Consulta</title>
        <link href="css/materialize.css" rel="stylesheet" type="text/css"/>
        <script src="js/materialize.js" type="text/javascript"></script>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
        <div class="container">
                <div class="card-panel">
                    <h5 class="white-text head-panel center-align"> Consulta Compañia Cuartel </h5>
                    <table class="highlight responsive-table tabla">
                        <thead>
                            <tr>
                                <th>Id Compañia Cuartel</th>
                                <th>Cuartel</th>
                                <th>Compañia</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            <c:forEach items="${lista}" var="ver">
                                <tr>
                                    <td>${ver.idcompaniacuartel}</td>
                                    <td>${ver.idcuartel.nombre}</td>
                                    <td>${ver.idcompania.compania}</td>

                                    <td>
                                        <a href="companiacuartel?action=eliminar&idcompaniacuartel=${ver.idcompaniacuartel}" class="deep-orange darken-4 btn">Eliminar</a>
                                        <a href="companiacuartel?action=consultarById&idcompaniacuartel=${ver.idcompaniacuartel}" class="light-green darken-2 btn">Actualizar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
    </body>
</html>