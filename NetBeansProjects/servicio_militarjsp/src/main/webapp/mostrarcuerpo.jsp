<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<%
    HttpSession sesion = request.getSession();

    String usuario;
    if (sesion.getAttribute("usuario") != null) {
        usuario = sesion.getAttribute("usuario").toString();
    } else {
        response.sendRedirect("login.jsp");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Consulta</title>
        <link href="css/materialize.css" rel="stylesheet" type="text/css"/>
        <script src="js/materialize.js" type="text/javascript"></script>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
        <div class="container">
                <div class="card-panel">
                    <h5 class="white-text head-panel center-align"> Consulta Cuerpo </h5>
                    <table class="highlight responsive-table tabla">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Codigo</th>
                                <th>Nombre del Cuerpo</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            <c:forEach items="${lista}" var="ver">
                                <tr>
                                    <td>${ver.idcuerpo}</td>
                                    <td>${ver.codigo}</td>
                                    <td>${ver.denominacion}</td>
                                    <td>
                                        <a href="cuerpo?action=eliminar&idcuerpo=${ver.idcuerpo}" class="deep-orange darken-4 btn">Eliminar</a>
                                        <a href="cuerpo?action=consultarById&idcuerpo=${ver.idcuerpo}" class="light-green darken-2 btn">Actualizar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>