<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro Usuario</title>
         <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="css/materialize.css" rel="stylesheet" type="text/css"/>  
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <script src="js/materialize.js" type="text/javascript"></script>
        <script src="http://ie7-js.googlecode.com/svn/version/2.0(beta3)/IE8.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="card z-depth-5 center-align">
                <h3 class="black-text center-align">Registro Usuario</h3>
                <div class="formu2">
                    <form method="post" action="usuario?action=insertar">
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="codigo" type="text" class="validate" name="usuario" >
                                <label for="codigo">Nombre de usuario</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="servicio" type="password" class="validate" name="pass" >
                                <label for="servicio">Contraseña</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                                <button class="btn blue-grey darken-1">Enviar</button>
                                ${msg}
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <a class="btn blue-grey lighten-2" href="login.jsp">Iniciar Sesión</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>