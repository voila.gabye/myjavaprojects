<%-- 
    Document   : registrarServiSoldado
    Created on : 08-20-2019, 03:58:14 PM
    Author     : susana.menjivarusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<%
    HttpSession sesion = request.getSession();

    String usuario;
    if (sesion.getAttribute("usuario") != null) {
        usuario = sesion.getAttribute("usuario").toString();
    } else {
        response.sendRedirect("login.jsp");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/materialize.css" rel="stylesheet" type="text/css"/>
        <title>Registrar Servicio</title>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
        <div class="container">
            <div class="card-panel z-depth-5">
                <h5 class="white-text head-panel center-align"> Registro Servicio Soldado</h5>
                <div class="formu">
                    <form action="servicioSoldado" method="post">
                        <div class="row">
                            <input type="hidden" name="idserviciosoldado" id="id" value="${serviSoldado.idserviciosoldado}">
                            <div class="input-field col s12">                      
                                <input type="text" name="fecha" id="fecha" value="${serviSoldado.fecha}">
                                <label for="fecha">Fecha de realización</label>
                            </div>
                        </div>

                        <div class = "row">
                            <div class="input-field col s12">   
                                <select name="idsoldado" id="soldado"> 
                                    <option disabled selected >Seleccione Soldado</option>
                                    <c:forEach items="${soldados}" var="s">
                                        <c:choose>
                                            <c:when test="${s.idsoldado==serviSoldado.idsoldado.idsoldado}">
                                                <option value="${s.idsoldado}" selected="">${s.nombre}</option>
                                            </c:when>  
                                            <c:otherwise>
                                                <option value="${s.idsoldado}">${s.nombre}</option>
                                            </c:otherwise>
                                        </c:choose>                               
                                    </c:forEach>
                                </select>  
                                <label>Soldado</label>
                            </div>    
                        </div>

                        <div class = "row">
                            <div class="input-field col s12"> 

                                <select name="idsoldado" id="soldado"> 
                                    <option disabled selected >Seleccione Servicio</option>
                                    <c:forEach items="${servicios}" var="se">
                                        <c:choose>
                                            <c:when test="${se.idservicio==serviSoldado.idservicio.idservicio}">
                                                <option value="${se.idservicio}" selected="">${se.servicio}</option>
                                            </c:when>  
                                            <c:otherwise>
                                                <option value="${se.idservicio}">${se.servicio}</option>
                                            </c:otherwise>
                                        </c:choose>                               
                                    </c:forEach>
                                </select>   
                                <label>Servicio</label>
                            </div>    
                        </div>
                        <div class="row">
                            <div class="input-field">
                                <button class="btn lime darken-2">Enviar</button>
                                ${msg}
                            </div>
                        </div>
                    </form>
                    <a class="btn light-green darken-2" href="servicioSoldado?action=consultar">Consultar</a>
                </div>
            </div>
            <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script> 
            <script src="js/materialize.min.js" type="text/javascript"></script>
            <script>
                $(document).ready(function () {
                    $('select').formSelect();
                });
            </script>
    </body>
</html>
