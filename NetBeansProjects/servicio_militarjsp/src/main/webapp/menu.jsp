

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<%
    HttpSession sesion = request.getSession();

    String usuario;
    if (sesion.getAttribute("usuario") != null) {
        usuario = sesion.getAttribute("usuario").toString();
    } else {
        response.sendRedirect("login.jsp");
    }
%>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Menu</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="css/materialize.css" rel="stylesheet" type="text/css"/>  
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <script src="js/materialize.js" type="text/javascript"></script>
        <script src="http://ie7-js.googlecode.com/svn/version/2.0(beta3)/IE8.js"></script>

    </head>
    <body>
        <jsp:include page="header.jsp" />
        <ul id="slide-out" class="sidenav collapsible blue-grey blue-grey darken-3">
            <li><div class="user-view ">
                    <div class="background white">

                    </div>
                    <a href="index.jsp"><img class="circle" src="img/usersoldier.png"></a>
                     <i class="material-icons ic">account_circle</i> <span class="blue-grey-text">${usuario}</span>
                    <br>
                    <a href="login?action=logout"><i class="material-icons ic">exit_to_app</i><span class="blue-grey-text"> Cerrar Sesión</span></a>

                </div></li>
            <li>
                <div class="collapsible-header blue-grey darken-3 hoverable">
                    <i class="material-icons iw">person</i>
                    Soldado
                </div>
                <div class="collapsible-body collection">
                    <a href="soldado?action=registro" class="collection-item  white-text blue-grey darken-2 hoverable">Registrar Soldado</a>
                    <a href="soldado?action=consultar" class="collection-item  white-text blue-grey darken-2 hoverable">Mostrar Soldado</a>
                    <a href="servicioSoldado?action=insertar" class="collection-item  white-text blue-grey darken-2 hoverable">Registrar Servicio Soldado</a>
                    <a href="servicioSoldado?action=consultar" class="collection-item  white-text blue-grey darken-2 hoverable">Mostrar Servicio Soldado</a>
                </div>
            </li>

            <li>
                <div class="collapsible-header blue-grey darken-3 hoverable">
                    <i class="material-icons iw">grade</i>
                    Institución
                </div>
                <div class="collapsible-body collection">
                    <a href="registrocuerpo.jsp" class="collection-item  white-text blue-grey darken-2 hoverable">Registrar Cuerpo</a>
                    <a href="cuerpo?action=consultar" class="collection-item  white-text blue-grey darken-2 hoverable">Consulta Cuerpo</a>
                    <a href="registrocompania.jsp" class="collection-item  white-text blue-grey darken-2 hoverable">Registrar Compañia</a>
                    <a href="compania?action=consultar" class="collection-item  white-text blue-grey darken-2 hoverable">Consulta Compañia</a>
                    <a href="registroservicio.jsp" class="collection-item white-text blue-grey darken-2 hoverable">Registrar Servicio</a>
                    <a href="servicio?action=consultar" class="collection-item  white-text blue-grey darken-2 hoverable">Consulta Servicio</a>
                </div>
            </li>

            <li>
                <div class="collapsible-header blue-grey darken-3 hoverable">
                    <i class="material-icons iw">gps_fixed</i>
                    Ubicación
                </div>
                <div class="collapsible-body collection">
                    <a href="registrocuartel.jsp" class="collection-item  white-text blue-grey darken-2 hoverable">Registrar Cuartel</a>
                    <a href="cuartel?action=consultar" class="collection-item  white-text blue-grey darken-2 hoverable">Consultar Cuartel</a>
                    <a href="companiacuartel?action=registro" class="collection-item  white-text blue-grey darken-2 hoverable">Registrar Compañia Cuartel</a>
                    <a href="companiacuartel?action=consultar" class="collection-item  white-text blue-grey darken-2 hoverable">Consultar Compañia Cuartel</a>
                </div>
            </li>
        </ul>  

        <a href="#" data-target="slide-out" class="sidenav-trigger button-collapse show-on-medium-and-up "><i class="material-icons mi">menu</i></a>

        <script>
            document.addEventListener('DOMContentLoaded', function () {
                M.AutoInit();
            });


        </script>
    </body>
</html>
