
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<%
    HttpSession sesion = request.getSession();

    String usuario;
    if (sesion.getAttribute("usuario") != null) {
        usuario = sesion.getAttribute("usuario").toString();
    } else {
        response.sendRedirect("login.jsp");
    }
%>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Inicio</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="css/materialize.css" rel="stylesheet" type="text/css"/>  
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <script src="js/materialize.js" type="text/javascript"></script>
    </head>
    <body>

        <jsp:include page="menu.jsp" />

        <br>
        <div class="slider">
            <ul class="slides">
                <li>
                    <img src="https://www.fuerzaarmada.mil.sv/wp-content/uploads/2018/06/IMG_8429.jpg"> <!-- random image -->
                </li>
                <li>
                    <img src="https://www.fuerzaarmada.mil.sv/wp-content/uploads/2018/06/IMG_8410.jpg"> <!-- random image -->
                </li>
                <li>
                    <img src="https://www.fuerzaarmada.mil.sv/wp-content/uploads/2018/06/IMG_8465.jpg"> <!-- random image -->
                </li>
                <li>
                    <img src="https://www.fuerzaarmada.mil.sv/wp-content/uploads/2018/06/IMG_8396.jpg"> <!-- random image -->
                </li>
            </ul>
        </div>
        <script>
            $(document).ready(function () {
                $('.slider').slider();
            });
        </script>
    </body>
</html>
