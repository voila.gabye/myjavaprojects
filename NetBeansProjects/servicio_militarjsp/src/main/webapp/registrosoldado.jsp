<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<%
    HttpSession sesion = request.getSession();

    String usuario;
    if (sesion.getAttribute("usuario") != null) {
        usuario = sesion.getAttribute("usuario").toString();
    } else {
        response.sendRedirect("login.jsp");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <title>Registro Soldados</title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
        <link href="css/materialize.css" rel="stylesheet" type="text/css"/>  
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/materialize.min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                $('select').formSelect();
                $('.datepicker').datepicker();
            });
        </script>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
        <div class="container">
            <div class="card-panel z-depth-5">
                <h5 class="white-text head-panel center-align">Registro Soldado</h5>
                <div class="formu">
                    <form method="post" action="soldado?action=insertar">
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="codigo" type="text" class="validate lime-text" name="codigo" >
                                <label for="codigo">Codigo de Soldado</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                                <input id="nombre" type="text" class="validate" name="nombre" >
                                <label for="nombre">Nombre del soldado</label>
                            </div>


                            <div class="input-field col s6">
                                <input id="apellido" type="text" class="validate" name="apellido" >
                                <label for="apellido">Apellido del soldado</label>
                            </div>
                        </div>


                        <div class="row">
                            <div class="input-field col s6">
                                <select name="idcompaniacuartel" >
                                    <option value="" disabled selected >Seleccione compañia y cuartel respectivos</option>
                                    <c:forEach items="${listacc}" var="compania_cuartel">
                                        <option value = "${compania_cuartel.idcompaniacuartel}">${compania_cuartel.idcompania.compania}: ${compania_cuartel.idcuartel.nombre}</option>
                                    </c:forEach>
                                </select>  
                                <label>Compañia y Cuartel</label>
                            </div>


                            <div class="input-field col s6">
                                <select name="idcuerpo">
                                    <option disabled="true" selected="true">Seleccione cuerpo respectivo</option>
                                    <c:forEach items="${listacuerpo}" var="cuerpo">
                                        <option value = "${cuerpo.idcuerpo}">${cuerpo.denominacion}</option>
                                    </c:forEach>
                                </select> 
                                <label>Nombre del cuerpo</label>
                            </div>
                        </div> 

                        <div class="row">
                            <div class="input-field col s12">
                                <input id="graduacion" type="text"  class="datepicker" name="graduacion" >
                                <label for="graduacion">Graduacion del soldado</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <button class="btn lime darken-2 waves-effect waves-light">Enviar</button>
                                ${msg}
                            </div>
                        </div>

                    </form>

                    <a class="btn light-green darken-2 waves-effect  waves-light" href="soldado?action=consultar">Consultar</a>
                </div>
            </div>
        </div>

    </body>
</html>
