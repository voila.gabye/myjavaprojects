<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<%
    HttpSession sesion = request.getSession();

    String usuario;
    if (sesion.getAttribute("usuario") != null) {
        usuario = sesion.getAttribute("usuario").toString();
    } else {
        response.sendRedirect("login.jsp");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Actualizar Cuerpo</title>
        <link href="css/materialize.css" rel="stylesheet" type="text/css"/>
        <script src="js/materialize.js" type="text/javascript"></script>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
        <div class="container">
            <div class="card-panel z-depth-5">
                <h5 class="white-text head-panel center-align">Actualizar Cuerpo</h5> 
                <div class="formu">
                    <form  method="post" action="cuerpo?action=actualizar">
                        <c:forEach items="${lista}" var="ver">
                            <div class="row">
                                  <input id="idcuerpo" type="hidden" class="validate" name="idcuerpo" value="${ver.idcuerpo}" readonly="true">
                                <div class="input-field col s12">
                                    <input id="codigo" type="text" class="validate" name="codigo" value="${ver.codigo}">
                                    <label for="codigo">Codigo de cuerpo</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="denominacion" type="text" class="validate" name="denominacion" value="${ver.denominacion}">
                                    <label for="denominacion">Nombre del cuerpo</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <button class="btn lime darken-2">Actualizar</button>
                                    ${msg}
                                </div>
                            </div>
                        </c:forEach>
                    </form>
                    <a class="btn light-green darken-2" href="cuerpo?action=consultar">Consultar</a>
                </div>
            </div>
        </div>
    </body>
</html>