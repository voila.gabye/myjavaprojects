<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<%
    HttpSession sesion = request.getSession();

    String usuario;
    if (sesion.getAttribute("usuario") != null) {
        usuario = sesion.getAttribute("usuario").toString();
    } else {
        response.sendRedirect("login.jsp");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <title>Registro Soldado Compania</title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="css/materialize.css" rel="stylesheet" type="text/css"/>  
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/materialize.min.js" type="text/javascript"></script>

        <script>
            $(document).ready(function () {
                $('select').material_select();
            });
        </script>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
           <div class="container">
            <div class="card-panel z-depth-5">
                <h5 class="white-text head-panel center-align"> Registro Compañia Cuartel </h5>
                       <div class="formu">
                            <form method="post" action="companiacuartel?action=insertar">
                                <div class = "row">
                                    <label class>Cuartel</label>
                                    <select name="idcuartel">
                                        <option disabled selected >Seleccione Cuartel</option>
                                        <c:forEach items="${listacuartel}" var="cuartel"> 
                                            <option value="${cuartel.idcuartel}">${cuartel.nombre}</option>
                                        </c:forEach>
                                    </select>
                                </div>



                                <div class = "row">
                                    <label class>Compania</label>
                                    <select name="idcompania">
                                        <option disabled selected >Seleccione Compañia</option>
                                        <c:forEach items="${listacompa}" var="compa"> 
                                            <option value="${compa.idcompania}">${compa.compania}</option>
                                        </c:forEach>
                                    </select>
                                </div>


                                <div class="row">
                                    <div class="input-field">
                                        <br>
                                        <button class="btn lime darken-2">Enviar</button>
                                        ${msg}
                                    </div>
                                </div>
                            </form>
                            <a class="btn light-green darken-2" href="companiacuartel?action=consultar">Consultar</a>
                            </div>
                            </div>
                            </div> 
                            </body>
                            </html>

