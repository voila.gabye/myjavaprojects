<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<%
    HttpSession sesion = request.getSession();

    String usuario;
    if (sesion.getAttribute("usuario") != null) {
        usuario = sesion.getAttribute("usuario").toString();
    } else {
        response.sendRedirect("login.jsp");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <title>Actualizar Soldado Compania</title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
        <link href="css/materialize.css" rel="stylesheet" type="text/css"/>
        <link rel = "stylesheet"  href = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
        <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>       
        <script src = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js">
        </script> 
        <script>
            $(document).ready(function () {
                M.AutoInit();
            });
        </script>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
        <div class="container">
            <div class="card-panel z-depth-5">
                <h5 class="white-text head-panel center-align">Actualizar Compañia Cuartel</h5>
                <div class="formu">
                    <form class="col s12" method="post" action="companiacuartel?action=actualizar">
                        <c:forEach items="${lista}" var="ver">

                            <div class="row">
                                <input name="idcompaniacuartel" type="hidden" value="${ver.idcompaniacuartel}"/>
                                <div class="input-field col s12">
                                    <select name="idcuartel">
                                        <c:forEach items="${listacuartel}" var="cuartel">
                                            <c:choose>
                                                <c:when  test="${cuartel.idcuartel==ver.idcuartel.idcuartel}">
                                                    <option value="${cuartel.idcuartel}" selected="">${cuartel.nombre}</option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="${cuartel.idcuartel}">${cuartel.nombre}</option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </select>
                                    <label>Seleccione el Cuartel</label>
                                </div>
                            </div>

                            <div class = "row">
                                <div class="input-field col s12">
                                    <select name="idcompania">
                                        <c:forEach items="${listacompania}" var="compania">
                                            <c:choose>
                                                <c:when  test="${compania.idcompania==ver.idcompania.idcompania}">
                                                    <option value="${compania.idcompania}" selected="">${compania.compania}</option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="${compania.idcompania}">${compania.compania}</option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </select>
                                    <label>Seleccione una Compañia</label>
                                </div>
                            </div>


                            <div class="row">
                                <div class="input-field col s12">
                                    <br>
                                    <button class="btn lime darken-2">Enviar</button>
                                    ${msg}
                                </div>
                            </div>
                        </c:forEach>
                    </form>
                    <a class="btn light-green darken-2" href="soldado?action=consultar">Consultar</a>
                </div>
            </div>
        </div>
    </body>
</html>