<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<%
    HttpSession sesion = request.getSession();

    String usuario;
    if (sesion.getAttribute("usuario") != null) {
        usuario = sesion.getAttribute("usuario").toString();
    } else {
        response.sendRedirect("login.jsp");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Consulta</title>
        <link href="css/materialize.css" rel="stylesheet" type="text/css"/>
        <script src="js/materialize.js" type="text/javascript"></script>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
        <div class="container">
                <div class="card-panel">
                    <h5 class="white-text head-panel center-align"> Consulta Compañia </h5>
                    <table class="highlight responsive-table tabla">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Codigo</th>
                                <th>Nombre de la Compania</th>
                                <th>Actividad</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            <c:forEach items="${lista}" var="ver">
                                <tr>
                                    <td>${ver.idcompania}</td>
                                    <td>${ver.codigo}</td>
                                    <td>${ver.compania}</td>
                                    <td>${ver.actividad}</td>
                                    <td>
                                        <a href="compania?action=eliminar&idcompania=${ver.idcompania}" class="deep-orange darken-4 btn">Eliminar</a>
                                        <a href="compania?action=consultarById&idcompania=${ver.idcompania}" class="light-green darken-2 btn">Actualizar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                     <br><br>
                    <div class="row">
                        <a class="btn lime darken-2" href="registrocompania.jsp">Nueva Compañia</a>                
                    </div>
                </div>
            </div>
    </body>
</html>