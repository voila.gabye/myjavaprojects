package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import modelo.UsuarioBean;

public class UsuarioDao {

    Conexion conn;
    String sql;
    PreparedStatement ps;
    ResultSet rs;
    UsuarioBean bean;

    public UsuarioDao(Conexion conn) {
        this.conn = conn;
    }
    
    public boolean login(String usuario,String pass)
    {
        sql="select usuario,pass from usuario where usuario=? and pass=?";
        try {
            ps=conn.conectar().prepareStatement(sql);
            ps.setString(1, usuario);
            ps.setString(2, pass);
            
            rs=ps.executeQuery();
            
            while(rs.next())
            {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean insertar(UsuarioBean bean) {
        sql = "insert into usuario values(?,?,?)";

        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, bean.getIdusuario());
            ps.setString(2, bean.getUsuario());
            ps.setString(3, bean.getPass());

            ps.executeUpdate();

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean actaulizar(UsuarioBean bean) {
        sql = "update usuario set usuario=?,pass=? where idusuario=?";

        try {
            ps = conn.conectar().prepareStatement(sql);

            ps.setString(1, bean.getUsuario());
            ps.setString(2, bean.getPass());
            ps.setInt(3, bean.getIdusuario());

            ps.executeUpdate();

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean eliminar(int idusuario) {
        sql = "delete from usuario where idusuario=?";

        try {
            ps = conn.conectar().prepareStatement(sql);

            ps.setInt(1, idusuario);

            ps.executeUpdate();

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<UsuarioBean> consultar() {
        sql = "select * from usuario";

        try {
            ps = conn.conectar().prepareStatement(sql);
            rs = ps.executeQuery();

            List<UsuarioBean> lista = new ArrayList<>();

            while (rs.next()) {
                bean = new UsuarioBean(rs.getInt("idusuario"));
                bean.setUsuario(rs.getString("usuario"));
                bean.setPass(rs.getString("pass"));

                lista.add(bean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public List<UsuarioBean> consultarById(int idusuario) {
        sql = "select * from usuario where idusuario=?";

        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idusuario);
            rs = ps.executeQuery();

            List<UsuarioBean> lista = new ArrayList<>();

            while (rs.next()) {
                bean = new UsuarioBean(rs.getInt("idusuario"));
                bean.setUsuario(rs.getString("usuario"));
                bean.setPass(rs.getString("pass"));

                lista.add(bean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

}
