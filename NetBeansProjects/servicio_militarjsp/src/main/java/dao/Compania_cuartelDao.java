package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.CompaniaBean;
import modelo.Compania_cuartelBean;
import modelo.CuartelBean;

public class Compania_cuartelDao {

    Conexion conn;
    ResultSet rs;
    PreparedStatement ps;
    String sql;
    Compania_cuartelBean comcuab;

    public Compania_cuartelDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(Compania_cuartelBean comcuab) {
        sql = "insert into compania_cuartel values(?,?,?)";

        CuartelBean cuartebean = comcuab.getIdcuartel();
        CompaniaBean compabean = comcuab.getIdcompania();

        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, comcuab.getIdcompaniacuartel());
            ps.setInt(2, cuartebean.getIdcuartel());
            ps.setInt(3, compabean.getIdcompania());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean actualizar(Compania_cuartelBean comcuab) {
        sql = "update compania_cuartel set idcuartel=?,idcompania=? where idcompaniacuartel=?";

        CuartelBean cuartebean = comcuab.getIdcuartel();
        CompaniaBean compabean = comcuab.getIdcompania();

        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, cuartebean.getIdcuartel());
            ps.setInt(2, compabean.getIdcompania());
            ps.setInt(3, comcuab.getIdcompaniacuartel());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<Compania_cuartelBean> consultar() {
        sql = "select idcompaniacuartel, nombre, compania from compania_cuartel inner join cuartel on cuartel.idcuartel = compania_cuartel.idcuartel inner join compania on compania.idcompania = compania_cuartel.idcompania order by idcompaniacuartel asc";
        try {
            ps = conn.conectar().prepareStatement(sql);
            rs = ps.executeQuery();
            List<Compania_cuartelBean> lista = new LinkedList<>();

            CuartelBean cuartebean;
            CompaniaBean compabean;

            while (rs.next()) {
                cuartebean = new CuartelBean(0);
                cuartebean.setNombre(rs.getString("nombre"));

                compabean = new CompaniaBean(0);
                compabean.setCompania(rs.getString("compania"));

                comcuab = new Compania_cuartelBean(rs.getInt("idcompaniacuartel"));
                comcuab.setIdcuartel(cuartebean);
                comcuab.setIdcompania(compabean);
                lista.add(comcuab);
            }
            return lista;

        } catch (Exception e) {
            return null;
        }
    }

    public List<Compania_cuartelBean> consultarById(int idcompaniacuartel) {
        sql = "select * from compania_cuartel where idcompaniacuartel=?";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idcompaniacuartel);
            rs = ps.executeQuery();
            List<Compania_cuartelBean> lista = new LinkedList<>();

            CuartelBean cuartebean;
            CompaniaBean compabean;

            while (rs.next()) {
                cuartebean = new CuartelBean(rs.getInt("idcuartel"));
                compabean = new CompaniaBean(rs.getInt("idcompania"));

                comcuab = new Compania_cuartelBean(rs.getInt("idcompaniacuartel"));
                comcuab.setIdcuartel(cuartebean);
                comcuab.setIdcompania(compabean);
                lista.add(comcuab);
            }
            return lista;

        } catch (Exception e) {
            return null;
        }
    }

    public boolean eliminar(int idcompaniacuartel) {
        sql = "delete from compania_cuartel where idcompaniacuartel=?";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idcompaniacuartel);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
