package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.CompaniaBean;

public class CompaniaDao {

    Conexion conn;

    public CompaniaDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(CompaniaBean compab) {
        String sql = "insert into compania values (?,?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, compab.getIdcompania());
            ps.setString(2, compab.getCodigo());
            ps.setString(3, compab.getCompania());
            ps.setString(4, compab.getActividad());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean actualizar(CompaniaBean compab) {
        String sql = "update compania set codigo=?, compania=?, actividad=? where idcompania=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, compab.getCodigo());
            ps.setString(2, compab.getCompania());
            ps.setString(3, compab.getActividad());
            ps.setInt(4, compab.getIdcompania());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<CompaniaBean> consultar() {
        String sql = "select * from compania";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<CompaniaBean> lista = new LinkedList<>();
            CompaniaBean compab;
            while (rs.next()) {
                compab = new CompaniaBean(rs.getInt("idcompania"));
                compab.setCodigo(rs.getString("codigo"));
                compab.setCompania(rs.getString("compania"));
                compab.setActividad(rs.getString("actividad"));
                lista.add(compab);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public List<CompaniaBean> consultarById(int idcompania) {
        String sql = "select * from compania where idcompania=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idcompania);
            ResultSet rs = ps.executeQuery();
            List<CompaniaBean> lista = new LinkedList<>();
            CompaniaBean compab;
            while (rs.next()) {
                compab = new CompaniaBean(rs.getInt("idcompania"));
                compab.setCodigo(rs.getString("codigo"));
                compab.setCompania(rs.getString("compania"));
                compab.setActividad(rs.getString("actividad"));
                lista.add(compab);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean eliminar(int idcompania) {
        String sql = "delete from compania where idcompania=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idcompania);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
