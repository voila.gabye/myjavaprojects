package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.CuerpoBean;

public class CuerpoDao {

    Conexion conn;
    String sql;
    PreparedStatement ps;
    ResultSet rs;
    CuerpoBean bean;

    public CuerpoDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(CuerpoBean bean) {
        sql = "insert into cuerpo values(?,?,?)";

        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, bean.getIdcuerpo());
            ps.setString(2, bean.getCodigo());
            ps.setString(3, bean.getDenominacion());

            ps.executeUpdate();

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean actualizar(CuerpoBean bean) {
        sql = "update cuerpo set codigo=?,denominacion=? where idcuerpo=?";

        try {
            ps = conn.conectar().prepareStatement(sql);

            ps.setString(1, bean.getCodigo());
            ps.setString(2, bean.getDenominacion());
            ps.setInt(3, bean.getIdcuerpo());

            ps.executeUpdate();

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean eliminar(int idcuerpo) {
        sql = "delete from cuerpo where idcuerpo=?";

        try {
            ps = conn.conectar().prepareStatement(sql);

            ps.setInt(1, idcuerpo);

            ps.executeUpdate();

            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public List<CuerpoBean> consultar()
    {
        sql="select * from cuerpo";
        try {
            ps=conn.conectar().prepareStatement(sql);
            rs=ps.executeQuery();
            
            List<CuerpoBean> lista=new LinkedList<>();
            
            while(rs.next())
            {
                bean=new CuerpoBean(rs.getInt("idcuerpo"));
                bean.setCodigo(rs.getString("codigo"));
                bean.setDenominacion(rs.getString("denominacion"));
                
                lista.add(bean);
            }
            
            return lista;
            
        } catch (Exception e) {
            return null;
        }
    }
    public List<CuerpoBean> consultarById(int idcuerpo)
    {
        sql="select * from cuerpo where idcuerpo=?";
        try {
            ps=conn.conectar().prepareStatement(sql);
            ps.setInt(1, idcuerpo);
            rs=ps.executeQuery();
            
            List<CuerpoBean> lista=new LinkedList<>();
            
            while(rs.next())
            {
                bean=new CuerpoBean(rs.getInt("idcuerpo"));
                bean.setCodigo(rs.getString("codigo"));
                bean.setDenominacion(rs.getString("denominacion"));
                
                lista.add(bean);
            }
            
            return lista;
            
        } catch (Exception e) {
            return null;
        }
    }
}
