
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.ServicioBean;


public class ServicioDao {
    Conexion conn;
    String sql;
    PreparedStatement ps;
    ResultSet rs;
    ServicioBean bean;

    public ServicioDao(Conexion conn) {
        this.conn = conn;
    }
    
    public boolean insertar(ServicioBean bean)
    {
        sql="insert into servicio values(?,?,?,?)";
        
        try {
            ps=conn.conectar().prepareStatement(sql);
            ps.setInt(1, bean.getIdservicio());
            ps.setString(2, bean.getCodigo());
            ps.setString(3, bean.getServicio());
            ps.setString(4, bean.getDescripcion());
            
            ps.executeUpdate();
            
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public boolean actualizar(ServicioBean bean)
    {
        sql="update servicio set codigo=?,servicio=?,descripcion=? where idservicio=?";
        
        try {
            ps=conn.conectar().prepareStatement(sql);
            
            ps.setString(1, bean.getCodigo());
            ps.setString(2, bean.getServicio());
            ps.setString(3, bean.getDescripcion());
            ps.setInt(4, bean.getIdservicio());
            
            ps.executeUpdate();
            
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public boolean eliminar(int idservicio)
    {
        sql="delete from servicio where idservicio=?";
        
        try {
            ps=conn.conectar().prepareStatement(sql);
            ps.setInt(1, idservicio);
            
            ps.executeUpdate();
            
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public List<ServicioBean> consultar()
    {
        sql="select * from servicio";
        
        try {
            ps=conn.conectar().prepareStatement(sql);
            rs=ps.executeQuery();
            
            List<ServicioBean> lista=new LinkedList<>();
            
            while(rs.next())
            {
                bean=new ServicioBean(rs.getInt("idservicio"));
                bean.setCodigo(rs.getString("codigo"));
                bean.setServicio(rs.getString("servicio"));
                bean.setDescripcion(rs.getString("descripcion"));
                
                lista.add(bean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
     public List<ServicioBean> consultarById(int idservicio)
    {
        sql="select * from servicio where idservicio=?";
        
        try {
            ps=conn.conectar().prepareStatement(sql);
            ps.setInt(1, idservicio);
            rs=ps.executeQuery();
            
            List<ServicioBean> lista=new LinkedList<>();
            
            while(rs.next())
            {
                bean=new ServicioBean(rs.getInt("idservicio"));
                bean.setCodigo(rs.getString("codigo"));
                bean.setServicio(rs.getString("servicio"));
                bean.setDescripcion(rs.getString("descripcion"));
                
                lista.add(bean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
}
