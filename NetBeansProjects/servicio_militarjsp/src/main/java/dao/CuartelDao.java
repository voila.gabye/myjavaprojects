package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.CuartelBean;

public class CuartelDao {

    Conexion conn;
    String sql;

    public CuartelDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(CuartelBean cuartel) {
        sql = "insert into cuartel values(?,?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, cuartel.getIdcuartel());
            ps.setString(2, cuartel.getCodigo());
            ps.setString(3, cuartel.getNombre());
            ps.setString(4, cuartel.getDireccion());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean actualizar(CuartelBean cuartel) {
        sql = "update cuartel set codigo=?, nombre=?, direccion=? where idcuartel=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, cuartel.getCodigo());
            ps.setString(2, cuartel.getNombre());
            ps.setString(3, cuartel.getDireccion());
            ps.setInt(4, cuartel.getIdcuartel());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<CuartelBean> consultar() {
        sql = "select * from cuartel";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<CuartelBean> lista = new LinkedList<>();
            CuartelBean cuartel;
            while (rs.next()) {
                cuartel = new CuartelBean(rs.getInt("idcuartel"));
                cuartel.setCodigo(rs.getString("codigo"));
                cuartel.setNombre(rs.getString("nombre"));
                cuartel.setDireccion(rs.getString("direccion"));
                lista.add(cuartel);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public List<CuartelBean> consultarById(int idcuartel) {
        sql = "select * from cuartel where idcuartel=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idcuartel);
            ResultSet rs = ps.executeQuery();
            List<CuartelBean> lista = new LinkedList<>();
            CuartelBean cuartel;
            while (rs.next()) {
                cuartel = new CuartelBean(rs.getInt("idcuartel"));
                cuartel.setCodigo(rs.getString("codigo"));
                cuartel.setNombre(rs.getString("nombre"));
                cuartel.setDireccion(rs.getString("direccion"));
                lista.add(cuartel);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean eliminar(int idcuartel) {
        sql = "delete from cuartel where idcuartel=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idcuartel);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
