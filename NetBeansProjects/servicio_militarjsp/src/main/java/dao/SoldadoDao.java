package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.Compania_cuartelBean;
import modelo.CuartelBean;
import modelo.CuerpoBean;
import modelo.SoldadoBean;

public class SoldadoDao {

     Conexion conn;
    String sql;
    PreparedStatement ps;

    public SoldadoDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(SoldadoBean soldado) {
        sql = "insert into soldado values(?,?,?,?,?,?,?)";
        Compania_cuartelBean compania_cuartel = soldado.getIdcompaniacuartel();
        CuerpoBean cuerpo = soldado.getIdcuerpo();
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, soldado.getIdsoldado());
            ps.setString(2, soldado.getCodigo());
            ps.setInt(3, compania_cuartel.getIdcompaniacuartel());
            ps.setString(4, soldado.getNombre());
            ps.setString(5, soldado.getApellido());
            ps.setString(6, soldado.getGraduacion());
            ps.setInt(7, cuerpo.getIdcuerpo());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean actualizar(SoldadoBean soldado) {
        sql = "update soldado set codigo=?, idcompaniacuartel=?, nombre=?, apellido=?, graduacion=?, idcuerpo=? where idsoldado=?";
        Compania_cuartelBean compania_cuartel = soldado.getIdcompaniacuartel();
        CuerpoBean cuerpo = soldado.getIdcuerpo();
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, soldado.getCodigo());
            ps.setInt(2, compania_cuartel.getIdcompaniacuartel());
            ps.setString(3, soldado.getNombre());
            ps.setString(4, soldado.getApellido());
            ps.setString(5, soldado.getGraduacion());
            ps.setInt(6, cuerpo.getIdcuerpo());
            ps.setInt(7, soldado.getIdsoldado());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<SoldadoBean> consultar() {
        sql = "select s.idsoldado,concat(co.compania,' ',cu.nombre) as companiacuartel,s.codigo,s.nombre,s.apellido,s.graduacion,c.denominacion from soldado s inner join compania_cuartel as cc on cc.idcompaniacuartel=s.idcompaniacuartel inner join compania as co on co.idcompania=cc.idcompania inner join cuartel as cu on cu.idcuartel=cc.idcuartel inner join cuerpo c on s.idcuerpo=c.idcuerpo";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<SoldadoBean> lista = new LinkedList<>();

            SoldadoBean soldado;
            Compania_cuartelBean compania_cuartel;
            CuerpoBean cuerpo;

            while (rs.next()) {
                CuartelBean cuartelBean = new CuartelBean(0);
                cuartelBean.setNombre(rs.getString("companiacuartel"));

                compania_cuartel = new Compania_cuartelBean(0);
                compania_cuartel.setIdcuartel(cuartelBean);

                cuerpo = new CuerpoBean(0);
                cuerpo.setDenominacion(rs.getString("c.denominacion"));

                soldado = new SoldadoBean(rs.getInt("idsoldado"));

                soldado.setCodigo(rs.getString("codigo"));
                soldado.setIdcompaniacuartel(compania_cuartel);
                soldado.setNombre(rs.getString("nombre"));
                soldado.setApellido(rs.getString("apellido"));
                soldado.setGraduacion(rs.getString("graduacion"));
                soldado.setIdcuerpo(cuerpo);
                lista.add(soldado);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public List<SoldadoBean> consultarById(int idsoldado) {
        sql = "select * from soldado where idsoldado=?";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idsoldado);
            ResultSet rs = ps.executeQuery();
            List<SoldadoBean> lista = new LinkedList<>();

            SoldadoBean soldado;
            Compania_cuartelBean compania_cuartel;
            CuerpoBean cuerpo;

            while (rs.next()) {
                compania_cuartel = new Compania_cuartelBean(rs.getInt("idcompaniacuartel"));

                cuerpo = new CuerpoBean(rs.getInt("idcuerpo"));

                soldado = new SoldadoBean(rs.getInt("idsoldado"));
                soldado.setCodigo(rs.getString("codigo"));
                soldado.setIdcompaniacuartel(compania_cuartel);
                soldado.setNombre(rs.getString("nombre"));
                soldado.setApellido(rs.getString("apellido"));
                soldado.setGraduacion(rs.getString("graduacion"));
                soldado.setIdcuerpo(cuerpo);
                lista.add(soldado);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean eliminar(int idsoldado) {
        sql = "delete from soldado where idsoldado=?";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idsoldado);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
