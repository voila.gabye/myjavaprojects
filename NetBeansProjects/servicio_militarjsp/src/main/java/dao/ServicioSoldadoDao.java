package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.ServicioBean;
import modelo.ServicioSoldadoBean;
import modelo.SoldadoBean;

public class ServicioSoldadoDao {

    Conexion conn;

    public ServicioSoldadoDao(Conexion conn) {
        this.conn = conn;
    }

    public void insertar(ServicioSoldadoBean serviS) {
        String sql = "insert into servicio_soldado values(?,?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, serviS.getIdserviciosoldado());
            SoldadoBean soldado = serviS.getIdsoldado();
            ps.setInt(2, soldado.getIdsoldado());
            ServicioBean servicio = serviS.getIdservicio();
            ps.setInt(3, servicio.getIdservicio());
            ps.setString(4, serviS.getFecha());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void actualizar(ServicioSoldadoBean serviS) {
        String sql = "update servicio_soldado set idsoldado=?,idservicio=?,fecha=? where idserviciosoldado=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            SoldadoBean soldado = serviS.getIdsoldado();
            ps.setInt(1, soldado.getIdsoldado());
            ServicioBean servicio = serviS.getIdservicio();
            ps.setInt(2, servicio.getIdservicio());
            ps.setString(3, serviS.getFecha());
            ps.setInt(4, serviS.getIdserviciosoldado());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void eliminar(int id) {
        String sql = "delete from servicio_soldado where idserviciosoldado=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<ServicioSoldadoBean> consultar() {
        String sql = "select ss.idserviciosoldado,concat(s.nombre,\" \",s.apellido) as nombre,se.servicio,ss.fecha "
                + "from servicio_soldado as ss join soldado as s on s.idsoldado=ss.idsoldado join servicio as se "
                + "on se.idservicio=ss.idservicio;";
        List<ServicioSoldadoBean> lista = new LinkedList<>();
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ServicioSoldadoBean servSol = new ServicioSoldadoBean(rs.getInt("idserviciosoldado"));
                SoldadoBean soldado = new SoldadoBean(0);
                soldado.setNombre(rs.getString("nombre"));
                ServicioBean servicio = new ServicioBean(0);
                servicio.setServicio(rs.getString("servicio"));
                servSol.setFecha(rs.getString("fecha"));
                servSol.setIdservicio(servicio);
                servSol.setIdsoldado(soldado);
                lista.add(servSol);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public ServicioSoldadoBean consultarById(int id) {
        String sql = "select * from servicio_soldado where idserviciosoldado=?";
        ServicioSoldadoBean servS = new ServicioSoldadoBean(0);
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                servS.setIdserviciosoldado(rs.getInt("idserviciosoldado"));
                ServicioBean servicio = new ServicioBean(rs.getInt("idservicio"));
                SoldadoBean soldado = new SoldadoBean(rs.getInt("idsoldado"));
                servS.setFecha(rs.getString("fecha"));
                servS.setIdservicio(servicio);
                servS.setIdsoldado(soldado);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return servS;
    }

}
