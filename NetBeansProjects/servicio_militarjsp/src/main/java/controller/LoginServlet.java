
package controller;

import conexion.Conexion;
import dao.UsuarioDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.UsuarioBean;

public class LoginServlet extends HttpServlet {

    String msg;
    boolean respuesta;
    RequestDispatcher rd;
    Conexion conn = new Conexion();
    UsuarioBean bean;
    UsuarioDao dao=new UsuarioDao(conn);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");

        switch (action) {
            case "login":
                login(request, response);
                break;
            case "logout":
                logout(request, response);
                break;
            default:
                throw new AssertionError();
        }
    }

    protected void login(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String usuario=request.getParameter("usuario");
        String pass=request.getParameter("pass");
        
        respuesta=dao.login(usuario, pass);
        
        if(respuesta)
        {
            HttpSession session=request.getSession();
            session.setAttribute("usuario", usuario);
            response.sendRedirect("index.jsp");
        }
        else{
            
            msg="Error en usuario o contraseña";
            request.setAttribute("msg", msg);
            rd=request.getRequestDispatcher("login.jsp");
            rd.forward(request, response);
        }
        

    }

    protected void logout(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session=request.getSession();
        session.invalidate();
        response.sendRedirect("login.jsp");
        

    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}