package controller;

import conexion.Conexion;
import dao.CuartelDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.CuartelBean;

public class CuartelServlet extends HttpServlet {

    Conexion conn = new Conexion();
    CuartelDao cuartelDao = new CuartelDao(conn);
    RequestDispatcher rd;
    String msg;
    boolean respuesta;
    CuartelBean cuartel;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "actualizar":
                actualizar(request, response);
                break;
            case "consultar":
                consultar(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
        }
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String codigo = request.getParameter("codigo");
        String nombre = request.getParameter("nombre");
        String direccion = request.getParameter("direccion");

        cuartel = new CuartelBean(0);
        cuartel.setCodigo(codigo);
        cuartel.setNombre(nombre);
        cuartel.setDireccion(direccion);

        respuesta = cuartelDao.insertar(cuartel);
        if (respuesta) {
            msg = "Cuartel Guardado";
        } else {
            msg = "Cuartel no Guardado";
        }
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("registrocuartel.jsp");
        rd.forward(request, response);
    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idcuartel = Integer.parseInt(request.getParameter("idcuartel"));
        String codigo = request.getParameter("codigo");
        String nombre = request.getParameter("nombre");
        String direccion = request.getParameter("direccion");

        cuartel = new CuartelBean(idcuartel);
        cuartel.setCodigo(codigo);
        cuartel.setNombre(nombre);
        cuartel.setDireccion(direccion);

        respuesta = cuartelDao.actualizar(cuartel);
        List<CuartelBean> lista = cuartelDao.consultar();
        if (respuesta) {
            msg = "Cuartel Actualizado";
        } else {
            msg = "Cuartel no Actualizado";
        }
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarcuartel.jsp");
        rd.forward(request, response);
    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<CuartelBean> lista = cuartelDao.consultar();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarcuartel.jsp");
        rd.forward(request, response);
    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idcuartel = Integer.parseInt(request.getParameter("idcuartel"));
        List<CuartelBean> lista = cuartelDao.consultarById(idcuartel);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("actualizarcuartel.jsp");
        rd.forward(request, response);
    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idcuartel = Integer.parseInt(request.getParameter("idcuartel"));
        respuesta = cuartelDao.eliminar(idcuartel);
        List<CuartelBean> lista = cuartelDao.consultar();
        if (respuesta) {
            msg = "Cuartel Eliminado";
        } else {
            msg = "Cuartel no Eliminado";
        }
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarcuartel.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
