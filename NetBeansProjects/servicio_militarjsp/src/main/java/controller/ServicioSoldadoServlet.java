
package controller;

import conexion.Conexion;
import dao.ServicioDao;
import dao.ServicioSoldadoDao;
import dao.SoldadoDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.ServicioBean;
import modelo.ServicioSoldadoBean;
import modelo.SoldadoBean;

public class ServicioSoldadoServlet extends HttpServlet {

    Conexion conn=new Conexion();
    ServicioDao servicioDao=new ServicioDao(conn);
    SoldadoDao soldadoDao=new SoldadoDao(conn);
    ServicioSoldadoDao serviSoldadoDao=new ServicioSoldadoDao(conn);
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
            case "consultarById":
                consultarById(request, response);
            case "consultar":
                consultar(request, response);
            case "eliminar":
                eliminar(request, response);

        }
    }
    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.setAttribute("servicios", servicioDao.consultar());
        request.setAttribute("soldados", soldadoDao.consultar());
        
        
        RequestDispatcher rd;
        rd=request.getRequestDispatcher("/registrarServiSoldado.jsp");
        rd.forward(request, response);
    }
    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id=Integer.parseInt(request.getParameter("id"));
        request.setAttribute("serviSoldado", serviSoldadoDao.consultarById(id));
        request.setAttribute("servicios", servicioDao.consultar());
        request.setAttribute("soldados", soldadoDao.consultar());
        
        RequestDispatcher rd;
        rd=request.getRequestDispatcher("/registrarServiSoldado.jsp");
        rd.forward(request, response);
    }
    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("lista", serviSoldadoDao.consultar());
        
        RequestDispatcher rd;
        rd=request.getRequestDispatcher("/mostrarServiSoldado.jsp");
        rd.forward(request, response);
    }
    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id=Integer.parseInt(request.getParameter("id"));
        serviSoldadoDao.eliminar(id);
        
        request.setAttribute("lista", serviSoldadoDao.consultar());
        
        RequestDispatcher rd;
        rd=request.getRequestDispatcher("/mostrarServiSoldado.jsp");
        rd.forward(request, response);
    }
  
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String cod=request.getParameter("idserviciosoldado");
        SoldadoBean soldado=new SoldadoBean(Integer.parseInt(request.getParameter("idsoldado")));
        ServicioBean servicio=new ServicioBean(Integer.parseInt(request.getParameter("idservicio")));
        ServicioSoldadoBean serviSol=new ServicioSoldadoBean(0);
        serviSol.setIdservicio(servicio);
        serviSol.setIdsoldado(soldado);
        serviSol.setFecha(request.getParameter("fecha"));
        
        if (cod==null || cod.isEmpty()) {
            try {
                serviSoldadoDao.insertar(serviSol);
                request.setAttribute("lista", serviSoldadoDao.consultar());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            int idserviciosoldado=Integer.parseInt(cod);
            serviSol.setIdserviciosoldado(idserviciosoldado);
            serviSoldadoDao.actualizar(serviSol);
            request.setAttribute("lista", serviSoldadoDao.consultar());
        }
        
        RequestDispatcher rd;
        rd=request.getRequestDispatcher("/mostrarServiSoldado.jsp");
        rd.forward(request, response);
    }

   
    

}
