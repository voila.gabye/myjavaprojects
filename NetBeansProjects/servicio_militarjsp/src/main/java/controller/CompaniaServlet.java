package controller;

import conexion.Conexion;
import dao.CompaniaDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.CompaniaBean;

public class CompaniaServlet extends HttpServlet {

    Conexion conn = new Conexion();
    CompaniaDao compad = new CompaniaDao(conn);
    RequestDispatcher rd;
    String msg;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "actualizar":
                actualizar(request, response);
                break;
            case "consultar":
                consultar(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            default:
                throw new AssertionError();
        }
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String codigo = request.getParameter("codigo");
        String compania = request.getParameter("compania");
        String actividad = request.getParameter("actividad");

        CompaniaBean compab = new CompaniaBean(0);
        compab.setCodigo(codigo);
        compab.setCompania(compania);
        compab.setActividad(actividad);

        boolean respuesta = compad.insertar(compab);
        if (respuesta) {
            msg = "Registro fue guardado con éxito";
        } else {
            msg = "Registro no fue guardado";
        }

        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/registrocompania.jsp");
        rd.forward(request, response);
    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idcompania = Integer.parseInt(request.getParameter("idcompania"));
        String codigo = request.getParameter("codigo");
        String compania = request.getParameter("compania");
        String actividad = request.getParameter("actividad");

        CompaniaBean compab = new CompaniaBean(idcompania);
        compab.setCodigo(codigo);
        compab.setCompania(compania);
        compab.setActividad(actividad);

        boolean respuesta = compad.actualizar(compab);
        List<CompaniaBean> lista = compad.consultar();
        if (respuesta) {
            msg = "Registro fue actualizado con éxito";
        } else {
            msg = "Registro no fue actualizado";
        }
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/mostrarcompania.jsp");
        rd.forward(request, response);
    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<CompaniaBean> lista = compad.consultar();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/mostrarcompania.jsp");
        rd.forward(request, response);
    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idcompania = Integer.parseInt(request.getParameter("idcompania"));
        List<CompaniaBean> lista = compad.consultarById(idcompania);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/actualizarcompania.jsp");
        rd.forward(request, response);
    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idcompania = Integer.parseInt(request.getParameter("idcompania"));
        boolean respuesta = compad.eliminar(idcompania);
        List<CompaniaBean> lista = compad.consultar();
        if (respuesta) {
            msg = "Registro fue eliminado con éxito";
        } else {
            msg = "Registro no fue eliminado";
        }
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/mostrarcompania.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
