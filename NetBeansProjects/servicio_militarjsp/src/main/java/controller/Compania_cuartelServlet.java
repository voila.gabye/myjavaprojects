
package controller;

import conexion.Conexion;
import dao.CompaniaDao;
import dao.Compania_cuartelDao;
import dao.CuartelDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.CompaniaBean;
import modelo.Compania_cuartelBean;
import modelo.CuartelBean;


public class Compania_cuartelServlet extends HttpServlet {

     String msg;
    boolean respuesta;
    RequestDispatcher rd;
    Conexion conn = new Conexion();
    Compania_cuartelDao comcuad = new Compania_cuartelDao(conn);
    Compania_cuartelBean comcuab;
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       String action = request.getParameter("action");

        switch (action) {
            case "registro":
                registro(request, response);
                break;
            case "insertar":
                insertar(request, response);
                break;
            case "actualizar":
                actualizar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            case "consultar":
                consultar(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            default:
                registro(request, response);
        }
    }
    
    protected void registro(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        CuartelDao cuarteld = new CuartelDao(conn);
        List<CuartelBean> listacuartel = cuarteld.consultar();

        CompaniaDao consultad = new CompaniaDao(conn);
        List<CompaniaBean> listacompa = consultad.consultar();

        request.setAttribute("listacuartel", listacuartel);
        request.setAttribute("listacompa", listacompa);
        rd = request.getRequestDispatcher("registrocompaniacuartel.jsp");
        rd.forward(request, response);
    }
    
     protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idcuartel = Integer.parseInt(request.getParameter("idcuartel"));
        int idcompania = Integer.parseInt(request.getParameter("idcompania"));

        CuartelBean cuarteb = new CuartelBean(idcuartel);
        CompaniaBean compab = new CompaniaBean(idcompania);

        comcuab = new Compania_cuartelBean(0);
        comcuab.setIdcuartel(cuarteb);
        comcuab.setIdcompania(compab);

        respuesta = comcuad.insertar(comcuab);
        if (respuesta) {
            msg = "Registro fue guardado con éxito";
        } else {
            msg = "Registro no fue guardado";
        }
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("companiacuartel?action=registro");
        rd.forward(request, response);
    }
     
      protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idcompaniacuartel = Integer.parseInt(request.getParameter("idcompaniacuartel"));
        int idcuartel = Integer.parseInt(request.getParameter("idcuartel"));
        int idcompania = Integer.parseInt(request.getParameter("idcompania"));

        CuartelBean cuarteb = new CuartelBean(idcuartel);
        CompaniaBean compab = new CompaniaBean(idcompania);

        comcuab = new Compania_cuartelBean(idcompaniacuartel);
        comcuab.setIdcuartel(cuarteb);
        comcuab.setIdcompania(compab);

        respuesta = comcuad.actualizar(comcuab);
        List<Compania_cuartelBean> lista = comcuad.consultar();
        if (respuesta) {
            msg = "Registro fue actualizado con éxito";
        } else {
            msg = "Registro no fue actualizado";
        }

        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarcompaniacuartel.jsp");
        rd.forward(request, response);
    }
      
      protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Compania_cuartelBean> lista = comcuad.consultar();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarcompaniacuartel.jsp");
        rd.forward(request, response);
    }
      
      protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idcompaniacuartel = Integer.parseInt(request.getParameter("idcompaniacuartel"));
        List<Compania_cuartelBean> lista = comcuad.consultarById(idcompaniacuartel);

        CuartelDao cuarted = new CuartelDao(conn);
        List<CuartelBean> listacuartel = cuarted.consultar();

        CompaniaDao compad = new CompaniaDao(conn);
        List<CompaniaBean> listacompania = compad.consultar();

        request.setAttribute("lista", lista);
        request.setAttribute("listacuartel", listacuartel);
        request.setAttribute("listacompania", listacompania);
        rd = request.getRequestDispatcher("actualizarcompaniacuartel.jsp");
        rd.forward(request, response);
    }
       protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idcompaniacuartel = Integer.parseInt(request.getParameter("idcompaniacuartel"));
        comcuad.eliminar(idcompaniacuartel);

        List<Compania_cuartelBean> lista = comcuad.consultar();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarcompaniacuartel.jsp");
        rd.forward(request, response);
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
