
package controller;

import conexion.Conexion;
import dao.UsuarioDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.UsuarioBean;

public class UsuarioServlet extends HttpServlet {

    String msg;
    boolean respuesta;
    RequestDispatcher rd;
    Conexion conn = new Conexion();
    UsuarioBean bean;
    UsuarioDao dao=new UsuarioDao(conn);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");

        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "actualizar":
                actualizar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            case "consultar":
                consultar(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            default:
                throw new AssertionError();
        }
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String usuario=request.getParameter("usuario");
        String pass=request.getParameter("pass");
        
        bean=new UsuarioBean(0);
        bean.setUsuario(usuario);
        bean.setPass(pass);
        
        respuesta=dao.insertar(bean);
        if(respuesta)
        {
            msg="Usuario registrado";
        }
        else
        {
            msg="Error al registrar usuario";
        }
        request.setAttribute("msg",  msg);
        rd=request.getRequestDispatcher("registrousuario.jsp");
        rd.forward(request, response);

    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int idusuario=Integer.parseInt(request.getParameter("idusuario"));
        String usuario=request.getParameter("usuario");
        String pass=request.getParameter("pass");
        
        bean=new UsuarioBean(idusuario);
        bean.setUsuario(usuario);
        bean.setPass(pass);
        
        respuesta=dao.actaulizar(bean);
        
        List<UsuarioBean> lista=dao.consultar();
        request.setAttribute("lista", lista);
        rd=request.getRequestDispatcher("mostrarusuario.jsp");
        rd.forward(request, response);

    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idusuario=Integer.parseInt(request.getParameter("idusuario"));
        
        dao.eliminar(idusuario);
        List<UsuarioBean> lista=dao.consultar();
        request.setAttribute("lista", lista);
        rd=request.getRequestDispatcher("mostrarusuario.jsp");
        rd.forward(request, response);

    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        List<UsuarioBean> lista=dao.consultar();
        request.setAttribute("lista", lista);
        rd=request.getRequestDispatcher("mostrarusuario.jsp");
        rd.forward(request, response);

    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int idusuario=Integer.parseInt(request.getParameter("idusuario"));
        List<UsuarioBean> lista=dao.consultarById(idusuario);
        request.setAttribute("lista", lista);
        rd=request.getRequestDispatcher("actualizarusuario.jsp");
        rd.forward(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}