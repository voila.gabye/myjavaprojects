
package controller;

import conexion.Conexion;
import dao.CuerpoDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.CuerpoBean;

public class CuerpoServlet extends HttpServlet {

    String msg;
    boolean respuesta;
    RequestDispatcher rd;
    Conexion conn = new Conexion();
    CuerpoBean bean;
    CuerpoDao dao = new CuerpoDao(conn);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");

        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "actualizar":
                actualizar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            case "consultar":
                consultar(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            default:
                throw new AssertionError();
        }
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String codigo = request.getParameter("codigo");
        String denominacion = request.getParameter("denominacion");

        bean = new CuerpoBean(0);
        bean.setCodigo(codigo);
        bean.setDenominacion(denominacion);

        respuesta = dao.insertar(bean);

        if (respuesta) {
            msg = "Exito Cuerpo Guardado";

        } else {
            msg = "Error al guardar cuerpo";
        }
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("registrocuerpo.jsp");
        rd.forward(request, response);

    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idcuerpo = Integer.parseInt(request.getParameter("idcuerpo"));
        String codigo = request.getParameter("codigo");
        String denominacion = request.getParameter("denominacion");

        bean = new CuerpoBean(idcuerpo);
        bean.setCodigo(codigo);
        bean.setDenominacion(denominacion);

        respuesta = dao.actualizar(bean);

        List<CuerpoBean> lista = dao.consultar();

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarcuerpo.jsp");
        rd.forward(request, response);

    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idcuerpo = Integer.parseInt(request.getParameter("idcuerpo"));

        respuesta = dao.eliminar(idcuerpo);

        List<CuerpoBean> lista = dao.consultar();

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarcuerpo.jsp");
        rd.forward(request, response);

    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<CuerpoBean> lista = dao.consultar();

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarcuerpo.jsp");
        rd.forward(request, response);

    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idcuerpo = Integer.parseInt(request.getParameter("idcuerpo"));

        List<CuerpoBean> lista = dao.consultarById(idcuerpo);
        
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("actualizarcuerpo.jsp");
        rd.forward(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}

