
package controller;

import conexion.Conexion;
import dao.ServicioDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.ServicioBean;

public class ServicioServlet extends HttpServlet {

    String msg;
    boolean respuesta;
    RequestDispatcher rd;
    Conexion conn = new Conexion();
    ServicioBean bean;
    ServicioDao dao = new ServicioDao(conn);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");

        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "actualizar":
                actualizar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            case "consultar":
                consultar(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            default:
                throw new AssertionError();
        }
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String codigo = request.getParameter("codigo");
        String servicio = request.getParameter("servicio");
        String descripcion = request.getParameter("descripcion");

        bean = new ServicioBean(0);
        bean.setCodigo(codigo);
        bean.setServicio(servicio);
        bean.setDescripcion(descripcion);

        respuesta = dao.insertar(bean);

        if (respuesta) {
            msg = "Exito servicio guardado";
        } else {
            msg = "Error al guardar servicio";
        }
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("registroservicio.jsp");
        rd.forward(request, response);

    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idservicio = Integer.parseInt(request.getParameter("idservicio"));
        String codigo = request.getParameter("codigo");
        String servicio = request.getParameter("servicio");
        String descripcion = request.getParameter("descripcion");

        bean = new ServicioBean(idservicio);
        bean.setCodigo(codigo);
        bean.setServicio(servicio);
        bean.setDescripcion(descripcion);

        respuesta = dao.actualizar(bean);

        if (respuesta) {
            msg = "Exito servicio actualizado";
        } else {
            msg = "Error al actualizar servicio";
        }

        List<ServicioBean> lista = dao.consultar();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarservicio.jsp");
        rd.forward(request, response);

    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idservicio = Integer.parseInt(request.getParameter("idservicio"));

        respuesta = dao.eliminar(idservicio);

        List<ServicioBean> lista = dao.consultar();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarservicio.jsp");
        rd.forward(request, response);

    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<ServicioBean> lista = dao.consultar();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarservicio.jsp");
        rd.forward(request, response);

    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idservicio = Integer.parseInt(request.getParameter("idservicio"));

        List<ServicioBean> lista = dao.consultarById(idservicio);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("actualizarservicio.jsp");
        rd.forward(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}