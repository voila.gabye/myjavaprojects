
package controller;

import conexion.Conexion;
import dao.Compania_cuartelDao;
import dao.CuerpoDao;
import dao.SoldadoDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Compania_cuartelBean;
import modelo.CuerpoBean;
import modelo.SoldadoBean;

public class SoldadoServlet extends HttpServlet {

    Conexion conn = new Conexion();
    SoldadoDao soldadoDao = new SoldadoDao(conn);
    SoldadoBean soldado;
    RequestDispatcher rd;
    String msg;
    boolean respuesta;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");

        switch (action) {
            case "registro":
                registro(request, response);
                break;
            case "insertar":
                insertar(request, response);
                break;
            case "actualizar":
                actualizar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            case "consultar":
                consultar(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            default:
                registro(request, response);
        }
    }

    protected void registro(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Compania_cuartelDao compania_cuartelDao = new Compania_cuartelDao(conn);
        List<Compania_cuartelBean> listacc = compania_cuartelDao.consultar();

        CuerpoDao cuerpoDao = new CuerpoDao(conn);
        List<CuerpoBean> listacuerpo = cuerpoDao.consultar();

        request.setAttribute("lista", soldadoDao.consultar());
        request.setAttribute("listacc", listacc);
        request.setAttribute("listacuerpo", listacuerpo);
        rd = request.getRequestDispatcher("registrosoldado.jsp");
        rd.forward(request, response);
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String codigo = request.getParameter("codigo");
        int idcompaniacuartel = Integer.parseInt(request.getParameter("idcompaniacuartel"));
        String nombre = request.getParameter("nombre");
        String apellido = request.getParameter("apellido");
        String graduacion = request.getParameter("graduacion");
        int idcuerpo = Integer.parseInt(request.getParameter("idcuerpo"));

        Compania_cuartelBean compania_cuartel = new Compania_cuartelBean(idcompaniacuartel);
        CuerpoBean cuerpo = new CuerpoBean(idcuerpo);

        soldado = new SoldadoBean(0);
        soldado.setCodigo(codigo);
        soldado.setIdcompaniacuartel(compania_cuartel);
        soldado.setNombre(nombre);
        soldado.setApellido(apellido);
        soldado.setGraduacion(graduacion);
        soldado.setIdcuerpo(cuerpo);
        respuesta = soldadoDao.insertar(soldado);

        if (respuesta) {
            msg = "Soldado Guardado";
        } else {
            msg = "Soldado no Guardado";
        }
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("soldado?action=registro");
        rd.forward(request, response);
    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idsoldado = Integer.parseInt(request.getParameter("idsoldado"));
        String codigo = request.getParameter("codigo");
        int idcompaniacuartel = Integer.parseInt(request.getParameter("idcompaniacuartel"));
        String nombre = request.getParameter("nombre");
        String apellido = request.getParameter("apellido");
        String graduacion = request.getParameter("graduacion");
        int idcuerpo = Integer.parseInt(request.getParameter("idcuerpo"));

        Compania_cuartelBean compania_cuartel = new Compania_cuartelBean(idcompaniacuartel);
        CuerpoBean cuerpo = new CuerpoBean(idcuerpo);

        soldado = new SoldadoBean(idsoldado);
        soldado.setCodigo(codigo);
        soldado.setIdcompaniacuartel(compania_cuartel);
        soldado.setNombre(nombre);
        soldado.setApellido(apellido);
        soldado.setGraduacion(graduacion);
        soldado.setIdcuerpo(cuerpo);

        respuesta = soldadoDao.actualizar(soldado);
        List<SoldadoBean> lista = soldadoDao.consultar();

        if (respuesta) {
            msg = "Soldado Actualizado";
        } else {
            msg = "Soldado no Actualizado";
        }
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarsoldado.jsp");
        rd.forward(request, response);
    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idsoldado = Integer.parseInt(request.getParameter("idsoldado"));
        respuesta = soldadoDao.eliminar(idsoldado);
        List<SoldadoBean> lista = soldadoDao.consultar();

        if (respuesta) {
            msg = "Soldado Eliminado";
        } else {
            msg = "Soldado no Eliminado";
        }
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarsoldado.jsp");
        rd.forward(request, response);
    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<SoldadoBean> lista = soldadoDao.consultar();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarsoldado.jsp");
        rd.forward(request, response);
    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idsoldado = Integer.parseInt(request.getParameter("idsoldado"));
        List<SoldadoBean> lista = soldadoDao.consultarById(idsoldado);

        Compania_cuartelDao compania_cuartelDao = new Compania_cuartelDao(conn);
        List<Compania_cuartelBean> listacc = compania_cuartelDao.consultar();

        CuerpoDao cuerpoDao = new CuerpoDao(conn);
        List<CuerpoBean> listacuerpo = cuerpoDao.consultar();

        request.setAttribute("lista", lista);
        request.setAttribute("listacc", listacc);
        request.setAttribute("listacuerpo", listacuerpo);
        rd = request.getRequestDispatcher("actualizarsoldado.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}