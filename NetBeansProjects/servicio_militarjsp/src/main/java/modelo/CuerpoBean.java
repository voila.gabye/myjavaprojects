package modelo;

public class CuerpoBean {

   private int idcuerpo ;
   private String codigo ;
   private String denominacion ;

    public CuerpoBean(int idcuerpo) {
        this.idcuerpo = idcuerpo;
    }

    public int getIdcuerpo() {
        return idcuerpo;
    }

    public void setIdcuerpo(int idcuerpo) {
        this.idcuerpo = idcuerpo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDenominacion() {
        return denominacion;
    }

    public void setDenominacion(String denominacion) {
        this.denominacion = denominacion;
    }
}
