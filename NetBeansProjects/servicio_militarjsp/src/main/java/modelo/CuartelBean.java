package modelo;

public class CuartelBean {

    private int idcuartel;
    private String codigo;
    private String nombre;
    private String direccion;

    public CuartelBean(int idcuartel) {
        this.idcuartel = idcuartel;
    }

    public int getIdcuartel() {
        return idcuartel;
    }

    public void setIdcuartel(int idcuartel) {
        this.idcuartel = idcuartel;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

}
