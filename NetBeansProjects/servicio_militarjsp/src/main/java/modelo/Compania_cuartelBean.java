
package modelo;


public class Compania_cuartelBean {
   
    
    private int idcompaniacuartel;
    private CuartelBean idcuartel;
    private CompaniaBean idcompania;

    public Compania_cuartelBean(int idcompaniacuartel) {
        this.idcompaniacuartel = idcompaniacuartel;
    }

    public int getIdcompaniacuartel() {
        return idcompaniacuartel;
    }

    public void setIdcompaniacuartel(int idcompaniacuartel) {
        this.idcompaniacuartel = idcompaniacuartel;
    }

    public CuartelBean getIdcuartel() {
        return idcuartel;
    }

    public void setIdcuartel(CuartelBean idcuartel) {
        this.idcuartel = idcuartel;
    }

    public CompaniaBean getIdcompania() {
        return idcompania;
    }

    public void setIdcompania(CompaniaBean idcompania) {
        this.idcompania = idcompania;
    }
    
}
