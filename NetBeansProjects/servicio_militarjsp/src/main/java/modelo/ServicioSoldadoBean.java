package modelo;


public class ServicioSoldadoBean {
    private int idserviciosoldado;
    private SoldadoBean idsoldado;
    private ServicioBean idservicio;
    private String fecha;

    public ServicioSoldadoBean(int id) {
        this.idserviciosoldado=id;
    }

    public int getIdserviciosoldado() {
        return idserviciosoldado;
    }

    public void setIdserviciosoldado(int idserviciosoldado) {
        this.idserviciosoldado = idserviciosoldado;
    }

    public SoldadoBean getIdsoldado() {
        return idsoldado;
    }

    public void setIdsoldado(SoldadoBean idsoldado) {
        this.idsoldado = idsoldado;
    }

    public ServicioBean getIdservicio() {
        return idservicio;
    }

    public void setIdservicio(ServicioBean idservicio) {
        this.idservicio = idservicio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    
}
