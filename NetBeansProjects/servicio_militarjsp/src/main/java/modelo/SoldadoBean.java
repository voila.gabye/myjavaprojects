
package modelo;

public class SoldadoBean {

   private int idsoldado;
    private String codigo;
    private Compania_cuartelBean idcompaniacuartel;
    private String nombre;
    private String apellido;
    private String graduacion;
    private CuerpoBean idcuerpo;

    public SoldadoBean(int idsoldado) {
        this.idsoldado = idsoldado;
    }

    public int getIdsoldado() {
        return idsoldado;
    }

    public void setIdsoldado(int idsoldado) {
        this.idsoldado = idsoldado;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Compania_cuartelBean getIdcompaniacuartel() {
        return idcompaniacuartel;
    }

    public void setIdcompaniacuartel(Compania_cuartelBean idcompaniacuartel) {
        this.idcompaniacuartel = idcompaniacuartel;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getGraduacion() {
        return graduacion;
    }

    public void setGraduacion(String graduacion) {
        this.graduacion = graduacion;
    }

    public CuerpoBean getIdcuerpo() {
        return idcuerpo;
    }

    public void setIdcuerpo(CuerpoBean idcuerpo) {
        this.idcuerpo = idcuerpo;
    }

}