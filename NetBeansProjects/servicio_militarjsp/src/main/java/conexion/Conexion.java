package conexion;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {

    static String db = "serviciomilitar";
    static String user = "root";
    static String pass = "root";
    static String url = "jdbc:mysql://localhost:3306/" + db + "?useSSL=false";
    static String driverClass = "com.mysql.jdbc.Driver";
    Connection conn = null;

    public Conexion() {

        try {
            Class.forName(driverClass);
            conn = DriverManager.getConnection(url, user, pass);

            if (conn != null) {
                System.out.println("Exito en la conexion");
            }
        } catch (Exception e) {
            System.out.println("Error en la conexion");
        }
    }

    public Connection conectar() {
        return conn;
    }

    public void desconectar() throws Exception {
        conn.close();
    }

}
