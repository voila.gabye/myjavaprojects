<%-- 
    Document   : index.jsp
    Created on : 11-06-2019, 04:40:52 PM
    Author     : Elena
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <title>Read</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </head>
    <body>

        <div class="container" style="margin-top:40px;"> 
            <p><strong>Spring con JSP</strong></p>

            <div><a href="add.jsp" >Agregar</a> </div>
            <br>
            <div>
                <table border="1">
                    <tr>
                        <th>Usuario</th>
                        <th>Nombre</th>
                        <th>Pasatiempo</th>
                        <th>Action</th>
                    </tr>
                    <c:forEach var="per" items="${listPers}">
                        <tr>
                            <td>${per.usuario}</td>
                            <td>${per.nombre}</td>
                            <td>${per.pasatiempo}</td>
                            <td>
                                <a href="update/<c:out value='${per.id}'/>">Update</a> 
                                <a href="delete/<c:out value='${per.id}'/>">Delete</a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </body>
</html>
