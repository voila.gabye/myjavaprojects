package springJSP.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import springJSP.modelo.Perfil;
import springJSP.utils.Dao;

/**
 *
 * @author gabriela.francousam
 */
@Repository
public class PerfilD implements Dao<Perfil> {

    @Autowired
    Connection conn;
    private PreparedStatement p;
    private String[] acc = {
        "insert into perfil(usuario,nombre,pasatiempo) values(?,?,?)",
        "select * from perfil order by id"};

    private Perfil per;
    private List<Perfil> list;

    public PerfilD() {
    }

    @Override
    public void create(Perfil t) {
        try {
            p = conn.prepareStatement(acc[0]);
            p.setString(1, t.getUsuario());
            p.setString(2, t.getNombre());
            p.setString(3, t.getPasatiempo());
            p.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error en Perfil: Al crear");
        } finally {
            System.out.println("Recuerde cerrar la conexion");
        }
    }

    @Override
    public List<Perfil> read() {
        try {
            p = conn.prepareStatement(acc[1]);
            ResultSet rs = p.executeQuery();
            list = new ArrayList<Perfil>();
            while (rs.next()) {
                per = new Perfil();
                per.setId(rs.getLong("id"));
                per.setUsuario(rs.getString("usuario"));
                per.setNombre(rs.getString("nombre"));
                per.setPasatiempo(rs.getString("pasatiempo"));
                list.add(per);
            }
            return list;
        } catch (SQLException e) {
            System.out.println("Error al leer el perfil");
            return null;
        } finally {
            System.out.println("Recuerde cerrar la conexion");
        }
    }

    @Override
    public Perfil readBy(Object id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void update(Perfil t) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void delete(Perfil t) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
