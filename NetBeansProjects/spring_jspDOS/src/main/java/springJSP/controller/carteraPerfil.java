package springJSP.controller;

import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import springJSP.modelo.Perfil;
import springJSP.utils.Dao;

/**
 *
 * @author gabriela.francousam
 */
public class carteraPerfil {

    @Autowired
    private Dao profile;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ModelAndView create(@RequestParam("usuario") String us, @RequestParam("nombre") String nom,
            @RequestParam("pasatiempo") String pasa, ModelAndView mv) {
        Perfil per = new Perfil();
        per.setUsuario(us);
        per.setNombre(nom);
        per.setPasatiempo(pasa);
        profile.create(per);
        mv.setViewName("add");
        return mv;
    }

    @RequestMapping(value = "/read")
    public ModelAndView read(ModelAndView model) throws IOException {
        List<Perfil> listPer = profile.read();
        model.addObject("listPers", listPer);
        model.setViewName("index");
        return model;
    }

}
