package springJSP.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import springJSP.impl.PerfilD;

/**
 *
 * @author gabriela.francousam
 */
@Configuration
@EnableWebMvc
@ComponentScan("springJSP")
public class Inversion {

    @Bean
    InternalResourceViewResolver viewRes() {
        InternalResourceViewResolver r = new InternalResourceViewResolver();
        r.setPrefix("/");
        r.setSuffix(".jsp");
        return r;
    }

    @Bean
    public Connection getConex() {
        Connection con;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/perfiles?useSSL=false", "root", "root");
              System.out.println("Exito en la conexion");
        } catch (SQLException e) {
            con = null;
              System.out.println("Error en la conexion");
        } catch (ClassNotFoundException cx) {
            con = null;
        }
        return con;
    }

    @Bean
    public Dao profile() {
        return new PerfilD();
    }
}
