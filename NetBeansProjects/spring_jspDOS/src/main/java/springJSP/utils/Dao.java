package springJSP.utils;

import java.util.List;

/**
 *
 * @author gabriela.francousam
 */
public interface Dao<T> {

    public void create(T t);

    public List<T> read();

    public T readBy(Object id);

    public void update(T t);

    public void delete(T t);
}
