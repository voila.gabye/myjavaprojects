<%-- 
    Document   : registrarTorneo
    Created on : 08-15-2019, 08:48:24 AM
    Author     : susana.menjivarusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <title>Registro/Edición de Torneo</title>
    </head>
    <body>
         <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Registro Torneo</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-6 text-light">
                    <form method="post" action="torneo">
                        <div class="form-group">
                            <label for="id">Id</label>
                            <input class="form-control" name="idtorneo" id="id" readonly="" type="text" value="${torneo.idtorneo}">
                        </div>
                        <div class="form-group">
                            <label for="nom">Nombre del Torneo</label>
                            <input name="nombre" id="nom" class="form-control" type="text" value="${torneo.nombre}">
                        </div>
                       <div class="form-group">
                            <label for="fechai">Fecha de Inicio</label>
                            <input name="fecha_inicio" id="fechai" class="form-control" type="date" value="${torneo.fecha_inicio}">
                        </div>
                        <div class="form-group">
                            <label for="fechaf">Fecha de Finalización</label>
                            <input name="fecha_fin" id="fechaf" class="form-control" type="date" value="${torneo.fecha_fin}">
                        </div>
                        <div class="form-group">
                            <label for="pais">Seleccione el pais donde se realizará</label>
                            <select name="idpais" class="form-control">
                                <c:forEach items="${paises}" var="p">
                                    <c:choose>
                                        <c:when test="${p.idpais==torneo.pais.idpais}">
                                            <option value="${p.idpais}" selected="">${p.pais}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${p.idpais}">${p.pais}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                        </div>
                        <button class="btn btn-outline-success">Enviar</button> 
                    </form>
                    <br>
                    <a class="btn btn-outline-info" href="torneo?action=consultar" role="button">Consultar</a>   
                </div>

            </div>

            <div class="row">
                <br>
                <div class="col-12">


                </div>
            </div>

        </div>
    </body>
</html>
