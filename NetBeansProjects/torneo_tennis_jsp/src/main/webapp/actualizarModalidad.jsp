<%-- 
    Document   : actualizarModalidad
    Created on : 08-15-2019, 04:10:15 PM
    Author     : gabriela.francousam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Actualizar</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
          <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Actualizar Modalidad</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-6 text-light">
                    <form action="modal?action=actualizar" method="POST">
                        <c:forEach items="${lista}" var ="ver"> 
                            <div class="form-group">
                                <label> Id Modalidad </label>
                                <input class="form-control" name="idmodal" value ="${ver.idmodal}">
                            </div>
                            <div class="form-group">
                                <label> Nombre </label>
                                <input class="form-control" name="nombre" value ="${ver.nombre}">
                            </div>
                            <button type="submit" class="btn btn-outline-success">Enviar</button> 
                        </c:forEach>
                    </form>
                    ${msg}
                    <br>
                    <a class="btn btn-outline-info" href="modal?action=consultarAll" role="button">Consultar</a>   
                </div>

            </div>

            <div class="row">
                <br>
                <div class="col-12">

                </div>
            </div>

        </div>
    </body>
</html>
