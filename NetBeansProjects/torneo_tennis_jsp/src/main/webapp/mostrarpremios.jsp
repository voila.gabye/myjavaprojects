<%-- 
    Document   : mostrarpremios
    Created on : 08-14-2019, 02:21:24 PM
    Author     : susana.menjivarusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <title>Premios</title>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light" >Consulta Premio</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-12">
                    <br>
                    <table class="table table-borderless table-striped table-hover text-light">
                        <thead class="text-warning">
                        <th>Premio</th>
                        <th>Fase</th>
                        <th>Acciones</th>

                        </thead>
                        <tbody>
                            <c:forEach items="${premios}" var="p">
                                <tr>
                                    <td><c:out value="${p.premio}"/></td>
                                    <td><c:out value="${p.fase.fase}"/></td>
                                    <td>
                                        <a href="premio?action=eliminar&id=<c:out value="${p.idpremio}"/>" class="btn btn-danger">Eliminar</a>
                                        <a href="premio?action=consultarById&id=<c:out value="${p.idpremio}"/>" class="btn btn-primary">Actualizar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    
                    <a href="premio?action=insertar" class="btn btn-outline-success">Registrar nuevo premio</a>
                </div>
                <div class="col-md-1"></div>
            </div>
    </body>
</html>
