<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Inicio</title>


        <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        <jsp:include page="menu.jsp" />

        <!-- pageContent -->
        <section class="full-width pageContent">
            <section class="full-width text-center">
                <!-- Tiles -->
                <article class="full-width tile">
                    <a href="jugador?action=consultar">
                        <div class="tile-text">
                            <span class="text-condensedLight">
                                ${conteoJugadores}<br>
                                <small>Jugadores</small>
                            </span>
                            <br/>
                            <img src="img/jugadores.jpg" class="tile-icon" width="150"/>
                        </div>
                        <i class="zmdi zmdi-account tile-icon"></i>
                    </a>
                </article>

                <article class="full-width tile">
                    <a href="arbitro?action=consultarAll">
                        <div class="tile-text">
                            <span class="text-condensedLight">
                                ${conteoArbitros}<br>
                                <small>Arbitros</small>
                            </span>
                            <br/>
                            <img src="img/arbitro.jpg" class="tile-icon" width="150"/>
                        </div>
                        <i class="zmdi zmdi-account tile-icon"></i>
                    </a>
                </article>


                <article class="full-width tile">
                    <a href="equipo?action=consultar">
                        <div class="tile-text">
                            <span class="text-condensedLight">
                                ${conteoEquipos}<br>
                                <small>Equipos</small>
                            </span>
                            <br/>
                            <img src="img/equipo.jpg" class="tile-icon" width="150"/>
                        </div>
                        <i class="zmdi zmdi-account tile-icon"></i>
                    </a>
                </article>

   
                                
                                <article class="full-width tile">
                     <a href="pista?action=consultarAll">
                        <div class="tile-text">
                            <span class="text-condensedLight">
                                ${conteoPistas}<br>
                                <small>Pistas</small>
                            </span>
                            <br/>
                            <img src="img/pista.jpg" class="tile-icon" width="150"/>
                        </div>
                        <i class="zmdi zmdi-account tile-icon"></i>
                    </a>
                </article>
                                
                                
               
                                
                                 <article class="full-width tile">
                     <a href="entrenador?action=consultarAll">
                        <div class="tile-text">
                            <span class="text-condensedLight">
                                  ${conteoEntrenadores}<br>
                                <small>Entrenadores</small>
                            </span>
                            <br/>
                            <img src="img/entrenador.jpg" class="tile-icon" width="150"/>
                        </div>
                        <i class="zmdi zmdi-account tile-icon"></i>
                    </a>
                </article>
                                
                                

                <article class="full-width tile">
                    <a href="jugador?action=consultar">
                        <div class="tile-text">
                            <span class="text-condensedLight">
                                ${conteoPartidos}<br>
                                <small>Partidos</small>
                            </span>
                            <br/>
                            <img src="img/partidos.jpg" class="tile-icon" width="150"/>
                        </div>
                        <i class="zmdi zmdi-account tile-icon"></i>
                    </a>
                </article>



            </section>
        </section>
    </body>
</html>