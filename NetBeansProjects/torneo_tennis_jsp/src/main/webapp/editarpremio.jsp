<%-- 
    Document   : editarpremio
    Created on : 08-14-2019, 02:24:32 PM
    Author     : susana.menjivarusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Registro/Edición de Premios</title>
    </head>
    <body>
         <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Premio</h1> </center>
        </header>
        <div class="container">
            <div class="col-6 text-light">        
                <form method="post" action="premio">
                    <div class="form-group">
                        <label for="id">Id</label>
                        <input class="form-control" name="idpremio" id="id" readonly="" type="text" value="${premio.idpremio}">
                    </div>
                    <div class="form-group">
                        <label for="modal">Premio</label>
                        <input name="premio" id="pista" class="form-control" type="text" value="${premio.premio}">
                    </div>
                    <div class="form-group">
                        <label>Fase</label>
                        <select name="idfase" class="form-control">
                            <c:forEach items="${fases}" var="f">
                                <c:choose>
                                    <c:when test="${f.idfase==premio.fase.idfase}">
                                        <option value="${f.idfase}" selected="">${f.fase}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${f.idfase}">${f.fase}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-outline-success">Enviar</button> 
                </form>
                <br>
                <a class="btn btn-outline-info" href="premio?action=consultar" role="button">Consultar</a>   
            </div>

            <div class="row">
                <br>
                <div class="col-12">

                </div>
            </div>

        </div>
    </body>
</html>