
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Registro Genero</title>
    </head>
    <body>
           <jsp:include page="menu.jsp" />
        <br><br><br><br>
         <header>
            <center><h1 class="text-light">Registro Genero</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <div class="col-6 text-light">
                    <form action="genero?action=insertar" method="POST">
                        <div class="form-group">
                            <label>Genero</label>
                            <input class="form-control" name="genero">
                        </div>
                        <button class="btn btn-outline-success">Enviar</button> 
                    </form>
                    ${msg}
                    <br>
                     <a class="btn btn-outline-info" href="genero?action=consultar" role="button">Consultar</a>   
                </div>          
            </div>
        </div>
    </body>
</html>
