
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Registro de Jugador Partido</title>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Registro Jugador Partido</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-6 text-light">
                    <form method="post" action="jugador_partido?action=insertar">
                        <div class="form-group">
                        <label>Seleccione Jugador</label>
                        <select name="idjugador" class="form-control">
                            <c:forEach items="${listajugadores}" var="jugadores">
                                <option value="${jugadores.idjugador}">${jugadores.nombre}</option>
                            </c:forEach>
                        </select>
                        </div>
                        <div class="form-group">
                        <label>Seleccione Equipo</label>
                        <select name="idpartido" class="form-control">
                            <c:forEach items="${listapartidos}" var="partidos">
                                <option value="${partidos.idpartido}">${partidos.lugarT.pista.pista}</option>
                            </c:forEach>
                        </select>
                        </div>
                        <div class="form-group">
                        <label>Resultado</label>
                        <input type="text" class="form-control" name="resultado"/>
                        </div>
                        <div class="form-group">
                        <label>Ganador</label>
                        <select name="ganador" class="form-control">
                            <option value="true">SI</option>
                            <option value="false">NO</option>
                        </select>
                        </div>
                        <button class="btn btn-outline-success">Enviar</button> 
                    </form>
                    ${msg}
                    <br>
                     <a href="jugador_partido?action=consultar" class="btn btn-outline-info">Consultar</a>
                    
                </div>
                <div class="col-4">
                   
                </div>
            </div>
        </div>
    </body>
</html>
