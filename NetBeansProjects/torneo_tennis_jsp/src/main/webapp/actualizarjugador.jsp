<%-- 
    Document   : registrojugador
    Created on : 08-15-2019, 01:49:35 PM
    Author     : fredy.alfarousam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Actualizar</title>
    </head>
    <body>
         <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Actualizar Jugador</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-6 text-light">
                    <form method="post" action="jugador?action=actualizar">
                        <c:forEach items="${lista}" var="jugador">
                            <div class="form-group">
                                <label>Id</label>
                                <input class="form-control" name="idjugador" value="${jugador.idjugador}"/>
                            </div>
                            <div class="form-group">
                                <label>Nombre del jugador</label>
                                <input class="form-control" name="nombre" value="${jugador.nombre}"/>
                            </div>
                            
                            <div class="form-group">
                                <label>Seleccione su genero</label>
                                <select name="idgenero" class="form-control">
                                    <c:forEach items="${listagenero}" var="generos">
                                        <c:choose>
                                            <c:when  test="${generos.idgenero==jugador.idgenero.idgenero}">
                                                <option value="${generos.idgenero}" selected="">${generos.genero}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${generos.idgenero}">${generos.genero}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </div>
                            
                            
                            <div class="form-group">
                                <label>Seleccione equipo</label>
                                <select name="idequipo"  class="form-control">
                                    <c:forEach items="${listaequipos}" var="eqipos">
                                        <c:choose>
                                            <c:when test="${eqipos.idequipo==jugador.idequipo.idequipo}">
                                                <option value="${eqipos.idequipo}" selected="">${eqipos.idpais.pais}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${eqipos.idequipo}">${eqipos.idpais.pais}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-outline-success">Enviar</button> 
                        </c:forEach>
                    </form>
                    ${msg}
                    <br>
                    <a class="btn btn-outline-info" href="jugador?action=consultarAll" role="button">Consultar</a>   
                </div>

            </div>

            <div class="row">
                <br>
                <div class="col-12">

                </div>
            </div>

        </div>
    </body>
</html>
