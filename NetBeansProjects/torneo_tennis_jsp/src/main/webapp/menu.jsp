<%-- 
    Document   : menu
    Created on : 08-16-2019, 01:10:20 PM
    Author     : gabriela.francousam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Menu</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">
            <div class="container">
                <a class="navbar-brand" href="inicio.jsp">
                    The Grand Slam</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Jugadores
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item text-success" href="jugador?action=registro">Registro Jugador</a>
                                <a class="dropdown-item text-success"  href="jugador?action=consultar">Consulta Jugador</a>
                                <a class="dropdown-item text-success" href="registroGenero.jsp">Registro Genero</a>
                                <a class="dropdown-item text-success" href="genero?action=consultar">Consulta Genero</a>
                                <a class="dropdown-item text-success" href="registroPais.jsp">Registro Pais</a>
                                <a class="dropdown-item text-success" href="pais?action=consultarAll">Consulta Pais</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Entrenadores
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item text-success" href="registroEntrenador.jsp">Registro Entrenador</a>
                                <a class="dropdown-item text-success" href="entrenador?action=consultarAll">Consulta Entrenador</a>
                                <a class="dropdown-item text-success" href="entrenador_jugador?action=registro">Entrenadores y Jugadores</a>
                                <a class="dropdown-item text-success" href="entrenador_jugador?action=consultar">Consulta Entrenadores y Jugadores</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Torneo
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item text-success" href="torneo?action=insertar">Registro Torneo</a>
                                <a class="dropdown-item text-success" href="torneo?action=consultar">Consulta Torneo</a>
                                <a class="dropdown-item text-success" href="ciudad?action=insertar">Registro Ciudad</a>
                                <a class="dropdown-item text-success" href="ciudad?action=consultar">Consulta Ciudad</a>
                                <a class="dropdown-item text-success" href="lugarTorneo?action=insertar">Registro Lugar Torneo</a>
                                <a class="dropdown-item text-success" href="lugarTorneo?action=consultar">Consulta Lugar Torneo</a>
                                <a class="dropdown-item text-success" href="registroPista.jsp">Registro Pista</a>
                                <a class="dropdown-item text-success" href="pista?action=consultarAll">Consulta Pista</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Equipos
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item text-success" href="equipo?action=insertar"> Registro Equipo</a>
                                <a class="dropdown-item text-success" href="equipo?action=consultar">Consulta Equipo</a>
                                <a class="dropdown-item text-success" href="registroPais.jsp">Registro Pais</a>
                                <a class="dropdown-item text-success" href="pais?action=consultarAll">Consulta Pais</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Encuentros
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item text-success" href="jugador_partido?action=registro">Registro Jugador Partido</a>
                                <a class="dropdown-item text-success" href="jugador_partido?action=consultar">Consulta Jugador Partido</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Partidos
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item text-success" href="partido?action=insertar">Registro Partido</a>
                                <a class="dropdown-item text-success" href="partido?action=consultar">Consulta Partido</a>
                                <a class="dropdown-item text-success" href="premio?action=insertar">Registro Premio</a>
                                <a class="dropdown-item text-success" href="premio?action=consultar">Consulta Premio</a>
                                <a class="dropdown-item text-success" href="registroFase.jsp">Registro Fase</a>
                                <a class="dropdown-item text-success" href="fase?action=consultarAll">Consulta Fase</a>
                                <a class="dropdown-item text-success" href="registroArbitro.jsp">Registro Arbitro</a>
                                <a class="dropdown-item text-success" href="arbitro?action=consultarAll">Consulta Arbitro</a>
                                <a class="dropdown-item text-success" href="registroModalidad.jsp">Registro Modalidad</a>
                                <a class="dropdown-item text-success" href="modal?action=consultarAll">Consulta Modalidad</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
