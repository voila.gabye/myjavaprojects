<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mostrar Entrenador Jugador</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
         <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Consulta Entrenadores y Jugadores</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-12">
                    <br>
                    <table class="table table-borderless table-hover text-light">
                        <thead class="text-warning">
                            <tr>
                                <th>Id</th>
                                <th>Jugador</th>
                                <th>Entrenador</th>
                                <th>Fecha Inicio</th>
                                <th>Fecha Fin</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${lista}" var="entrenadorjugador">
                                <tr>
                                    <td>${entrenadorjugador.identrenaJuga}</td>
                                    <td>${entrenadorjugador.idjugador.nombre}</td>
                                    <td>${entrenadorjugador.identrenador.nombre}</td>
                                    <td>${entrenadorjugador.fecha_inicio}</td>
                                    <td>${entrenadorjugador.fecha_fin}</td>
                                    <td>
                                        <a class="btn btn-danger" href="entrenador_jugador?action=eliminar&identrenaJuga=${entrenadorjugador.identrenaJuga}">Eliminar</a>
                                        <a class="btn btn-primary" href="entrenador_jugador?action=consultarById&identrenaJuga=${entrenadorjugador.identrenaJuga}">Actualizar</a>
                                    </td>
                                </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                     <br>
                    <a href="entrenador_jugador?action=registro" class="btn btn-outline-success">Registrar Nuevos Entrenadores y Jugadores</a>
                </div>
            </div>
        </div>
    </body>
</html>
