

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Consulta</title>
    </head>
    <body>
          <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Actualizar Genero</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-6 text-light">
                    <form action="genero?action=actualizar" method="POST">
                        <c:forEach items="${lista}" var="ver">
                            <div class="form-group">
                                <label>Id</label>
                                <input class="form-control" name="idgenero" value="${ver.idgenero}">
                            </div>
                            <div class="form-group">
                                <label>Genero</label>
                                <input class="form-control" name="genero" value="${ver.genero}">
                            </div>
                               <button type="submit" class="btn btn-outline-success">Enviar</button> 
                        </c:forEach>
                    </form>
                    ${msg}
                 <br>
                    <a class="btn btn-outline-info" href="genero?action=consultar" role="button">Consultar</a>   
                </div>

            </div>

            <div class="row">
                <br>
                <div class="col-12">

                </div>
            </div>

        </div>
    </body>
</html>
