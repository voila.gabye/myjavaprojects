<%-- 
    Document   : mostrarpaises
    Created on : 08-13-2019, 10:27:39 AM
    Author     : fredy.alfarousam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Consulta Pais</title>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light" >Consulta Pais</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-12">
                    <br>
                    <table class="table table-borderless table-striped table-hover text-light">
                        <thead class="text-warning">
                            <tr>
                                <th>Id</th>
                                <th>Pais</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${lista}" var="paises">
                                <tr>
                                    <td>${paises.idpais}</td>
                                    <td>${paises.pais}</td>
                                    <td> 
                                        <a href="pais?action=eliminar&idpais=${paises.idpais}"  class="btn btn-danger">Eliminar</a>
                                        <a href="pais?action=consultarById&idpais=${paises.idpais}"  class="btn btn-primary">Actualizar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table> 
                     <br>
                    <a href="registroPais.jsp" class="btn btn-outline-success">Registrar Nuevo Pais</a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">

                </div>
            </div>
        </div>
    </div>  
</body>
</html>
