<%-- 
    Document   : mostrarciudades
    Created on : 08-15-2019, 09:27:06 AM
    Author     : susana.menjivarusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Ciudades</title>
    </head>
    <body>
          <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Consulta Ciudad</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-12">
                    <br>
                    <table class="table table-borderless table-hover text-light">
                        <thead class="text-warning">
                        <th>Id</th>
                        <th>Ciudad</th>
                        <th>Pais</th>
                        <th>Acciones</th>
                        </thead>
                        <tbody>
                            <c:forEach items="${lista}" var="lista">
                                <tr>
                                    <td><c:out value="${lista.idciudad}"/></td>
                                    <td><c:out value="${lista.ciudad}"/></td>
                                    <td><c:out value="${lista.idpais.pais}"/></td>
                                    <td>
                                        <a href="ciudad?action=eliminar&id=<c:out value="${lista.idciudad}"/>" class="btn btn-danger"> Eliminar</a>
                                        <a href="ciudad?action=consultarById&id=<c:out value="${lista.idciudad}"/>" class="btn btn-primary"> Actualizar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <br>
                    <a href="ciudad?action=insertar"  class="btn btn-outline-success">Registrar Nueva Ciudad</a>
                </div>
                <div class="col-md-1"></div>
            </div>
    </body>
</html>
