<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Arbitro</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Registro Arbitro</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-6 text-light">
                    <form action="arbitro?action=insertar" method="POST">
                        <div class="form-group">
                            <label>Nombre Arbitro</label>
                            <input class="form-control" name="nombre">
                        </div>
                        <button class="btn btn-outline-success">Enviar</button> 
                    </form>
                    ${msg}
                    <br>
                    <a class="btn btn-outline-info" href="arbitro?action=consultarAll" role="button">Consultar</a>   
                </div>

                <div class="row">
                    <br>
                    <div class="col-12">


                    </div>
                </div>

            </div>
    </body>
</html>
