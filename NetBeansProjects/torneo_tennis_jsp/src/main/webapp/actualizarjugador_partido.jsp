
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Actualizar</title>
    </head>
    <body>
         <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Actualizar Jugador y Partidos</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-6 text-light">
                    <form method="post" action="jugador_partido?action=actualizar">
                        <c:forEach items="${lista}" var="ver">
                            <div class="form-group">
                            <label>Id</label>
                            <input class="form-control" type="text" value="${ver.idjugadorP}" name="idjugadorP"/>
                            </div>
                            <div class="form-group">
                            <label>Seleccione Jugador</label>
                            <select name="idjugador" class="form-control">
                                <c:forEach items="${listajugadores}" var="jugadores">
                                    <c:choose>
                                        <c:when test="${ver.idjugador.idjugador==jugadores.idjugador}">
                                            <option selected="" value="${jugadores.idjugador}">${jugadores.nombre}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${jugadores.idjugador}">${jugadores.nombre}</option>
                                        </c:otherwise>
                                    </c:choose>
                                    <option value="${jugadores.idjugador}">${jugadores.nombre}</option>
                                </c:forEach>
                            </select>
                            </div>
                             <div class="form-group">
                            <label>Seleccione Partido</label>
                            <select name="idpartido" class="form-control">
                                <c:forEach items="${listapartidos}" var="partidos">
                                    <c:choose>
                                        <c:when test="${ver.idpartido.idpartido==partidos.idpartido}">
                                            <option selected="" value="${partidos.idpartido}">${partidos.lugarT.pista.pista}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${partidos.idpartido}">${partidos.lugarT.pista.pista}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                             </div>
                             <div class="form-group">
                            <label>Resultado</label>
                            <input class="form-control" name="resultado" value="${ver.resultado}"/>
                             </div>
                              <div class="form-group">
                            <label>Ganador</label>
                            <select name="ganador" class="form-control">
                                <c:choose>
                                    <c:when test="${ver.ganador==true}">
                                        <option value="${ver.ganador}">SI</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${ver.ganador}">NO</option>
                                    </c:otherwise>
                                </c:choose>
                                <option value="true">SI</option>
                                <option value="false">NO</option>
                            </select>
                        </c:forEach>
                              </div>
                            <button type="submit" class="btn btn-outline-success">Enviar</button> 
                     </form>
                   
                    <br>
                    <a class="btn btn-outline-info" href="jugador_partido?action=consultar" role="button">Consultar</a>   
                </div>

            </div>

            <div class="row">
                <br>
                <div class="col-12">

                </div>
            </div>

        </div>
    </body>
</html>