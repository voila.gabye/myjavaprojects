<%-- 
    Document   : mostrarpartidos
    Created on : 08-16-2019, 09:01:06 AM
    Author     : susana.menjivarusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Torneos</title>
    </head>
    <body>
          <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Consulta Partidos</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-12">
                    <br>
                    <table class="table table-borderless table-hover text-light">
                        <thead class="text-warning">
                    <th>Lugar</th>
                    <th>Modalidad</th>
                    <th>Arbitro</th>
                    <th>Fase</th>
                    <th>Premio</th>
                    <th>Acciones</th>
                    </thead>
                    <tbody>
                        <c:forEach items="${lista}" var="lista">
                            <tr>
                                <td><c:out value="${lista.lugarT.pista.pista}"/></td>
                                <td><c:out value="${lista.modalidad.nombre}"/></td>
                                <td><c:out value="${lista.arbitro.nombre}"/></td>
                                <td><c:out value="${lista.fase.fase}"/></td>
                                <td><c:out value="${lista.premio.premio}"/></td>
                                <td>
                                    <a href="partido?action=eliminar&id=<c:out value="${lista.idpartido}"/>" class="btn btn-danger">Eliminar</a>
                                    <a href="partido?action=consultarById&id=<c:out value="${lista.idpartido}"/>" class="btn btn-primary">Actualizar</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <br>
                <a href="partido?action=insertar" class="btn btn-outline-success">Registrar Nuevo Partido</a>
            </div>
            <div class="col-md-1"></div>
        </div>
        </div>
    </body>
</html>
