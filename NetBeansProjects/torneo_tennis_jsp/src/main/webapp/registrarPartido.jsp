<%-- 
    Document   : registrarPartido
    Created on : 08-16-2019, 09:00:38 AM
    Author     : susana.menjivarusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Registro/Edición de Torneo</title>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Registro Partido</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-6 text-light">
            <form method="post" action="partido">
                <label for="id">ID</label>
                <input class="form-control form-group" name="idpartido" id="id" readonly="" type="text" value="${partido.idpartido}">                
                <label for="lugar">Seleccione el Lugar del Partido</label>
                <select name="idlugar" class="form-control form-group" id="lugar">
                    <c:forEach items="${lugares}" var="l">
                        <c:choose>
                            <c:when test="${l.idlugar==partido.lugarT.idlugar}">
                                <option value="${l.idlugar}" selected="">${l.pista.pista}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${l.idlugar}">${l.pista.pista}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
                <label for="modal">Seleccione una Modalidad</label>
                <select name="idmodal" class="form-control form-group" id="modal">
                    <c:forEach items="${modalidades}" var="m">
                        <c:choose>
                            <c:when test="${m.idmodal==partido.modalidad.idmodal}">
                                <option value="${m.idmodal}" selected="">${m.nombre}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${m.idmodal}">${m.nombre}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
                <label for="arbi">Seleccione un Arbitro</label>
                <select name="idarbi" class="form-control form-group" id="arbi">
                    <c:forEach items="${arbitros}" var="a">
                        <c:choose>
                            <c:when test="${a.idarbi==partido.arbitro.idarbi}">
                                <option value="${a.idarbi}" selected="">${a.nombre}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${a.idarbi}">${a.nombre}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
                <label for="fase">Seleccione una Fase</label>
                <select name="idfase" class="form-control form-group" id="fase">
                    <c:forEach items="${fases}" var="f">
                        <c:choose>
                            <c:when test="${f.idfase==partido.fase.idfase}">
                                <option value="${f.idfase}" selected="">${f.fase}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${f.idfase}">${f.fase}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
                <label for="premio">Seleccione un Premio</label>
                <select name="idpremio" class="form-control form-group" id="premio">
                    <c:forEach items="${premios}" var="p">
                        <c:choose>
                            <c:when test="${p.idpremio==partido.premio.idpremio}">
                                <option value="${p.idpremio}" selected="">${p.premio}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${p.idpremio}">${p.premio}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
                 <button class="btn btn-outline-success">Enviar</button> 
            </form>
             <br>
                    <a class="btn btn-outline-info" href="fase?action=consultarAll" role="button">Consultar</a>   
        </div>
            </div>
    </body>
</html>
