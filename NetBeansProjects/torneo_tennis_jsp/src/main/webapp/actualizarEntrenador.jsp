<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Actualizar Entrenador</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
      <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Actualizar Entrenador</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-6 text-light">
                    <c:forEach items="${lista}" var="vr">
                        <form action="entrenador?action=actualizar" method="POST">
                            <div class="form-group">
                                <label>Id</label>
                                <input class="form-control" name="identre" value="${vr.identre}">
                            </div>
                            <div class="form-group">
                                <label>Nombre Entrenador</label>
                                <input class="form-control" name="nombre" value="${vr.nombre}">
                            </div>
                            <button type="submit" class="btn btn-outline-success">Enviar</button> 
                        </c:forEach>
                    </form>
                    <br>
                    <a class="btn btn-outline-info" href="entrenador?action=consultarAll" role="button">Consultar</a>   
                </div>

            </div>

            <div class="row">
                <br>
                <div class="col-12">

                </div>
            </div>

        </div>
    </body>
</html>
