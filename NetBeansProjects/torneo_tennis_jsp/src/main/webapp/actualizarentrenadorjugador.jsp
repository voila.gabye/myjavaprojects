<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Actualizar Entrenador Jugador</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Actualizar Entrenador Jugador</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-6 text-light">
                   <form method="POST" action="entrenador_jugador?action=actualizar">
                        <c:forEach items="${lista}" var="entrenadorjugador">
                            <div class="form-group">
                            <label>Id</label>
                            <input class="form-control" name="identrenaJuga" value="${entrenadorjugador.identrenaJuga}"/>
                            </div>
                            <div class="form-group">
                            <label>Seleccione al Jugador</label>
                            <select name="idjugador" class="form-control">
                                <c:forEach items="${listajugador}" var="jugador">
                                    <c:choose>
                                        <c:when test="${jugador.idjugador==entrenadorjugador.idjugador.idjugador}">
                                            <option value="${jugador.idjugador}" selected="">${jugador.nombre}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${jugador.idjugador}">${jugador.nombre}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                            </div>
                            <div class="form-group">
                            <label>Seleccione Entrenador</label>
                            <select name="identrenador" class="form-control">
                                <c:forEach items="${listaentrenador}" var="entrenador">
                                    <c:choose>
                                        <c:when test="${entrenador.identre==entrenadorjugador.identrenador.identre}">
                                            <option value="${entrenador.identre}" selected="">${entrenador.nombre}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${entrenador.identre}">${entrenador.nombre}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                            </div>
                            <div class="form-group">
                            <label>Fecha Inicio</label>
                            <input type="Date" class="form-control" name="fecha_inicio" value="${entrenadorjugador.fecha_inicio}"/>
                            </div>
                            <div class="form-group">
                            <label>Fecha Fin</label>
                            <input type="Date" class="form-control" name="fecha_fin" value="${entrenadorjugador.fecha_fin}"/>
                            </div>
                            <button class="btn btn-outline-success">Enviar</button>
                        </c:forEach>
                    </form>
                    </br>
                    <a class="btn btn-outline-info" href="entrenador_jugador?action=consultar">Consultar</a>
                </div>
                <div class="col-4">
                </div>
            </div>
        </div>
    </body>
</html>
