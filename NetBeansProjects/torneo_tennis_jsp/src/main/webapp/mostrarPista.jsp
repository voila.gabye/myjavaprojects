<%-- 
    Document   : mostrarpistas
    Created on : 08-13-2019, 10:38:13 AM
    Author     : susana.menjivarusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Pistas</title>
    </head>
    <body>
          <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light" >Consulta Pista</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-12">
                    <br>
                    <table class="table table-borderless table-striped table-hover text-light">
                        <thead class="text-warning">
                        <th>Id</th>
                        <th>Pista</th>
                        <th>Acciones</th>
                        </thead>
                        <tbody>
                            <c:forEach items="${lista}" var="pistas">
                                <tr>
                                    <td><c:out value="${pistas.idpista}"/></td>
                                    <td><c:out value="${pistas.pista}"/></td>
                                    <td>
                                        <a href="pista?action=eliminar&id=<c:out value="${pistas.idpista}"/>" class="btn btn-danger">Eliminar</a>
                                        <a href="pista?action=consultarById&id=<c:out value="${pistas.idpista}"/>" class="btn btn-primary">Actualizar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <br>
                    <a href="registroPista.jsp" class="btn btn-outline-success">Registrar Nueva Pista</a>
                </div>
                <div class="col-md-1"></div>
            </div>
    </body>
</html>
