<%-- 
    Document   : registropista
    Created on : 08-13-2019, 11:04:21 AM
    Author     : susana.menjivarusam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Registro Pistas</title>
    </head>
    <body>
       <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Registro Pista</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-6 text-light">
                    <form action="pista?action=insertar" method="post">
                        <div class="form-group"> 
                            <label for="pista">Nombre de Pista</label>
                            <input name="pista" id="pista" class="form-control" type="text">
                        </div>
                        <button class="btn btn-outline-success">Enviar</button> 
                    </form>
                    ${msg}
                    <br>
                    <a class="btn btn-outline-info" href="pista?action=consultarAll" role="button">Consultar</a>   
                </div>

            </div>

            <div class="row">
                <br>
                <div class="col-12">


                </div>
            </div>

        </div>
    </body>
</html>
