<%-- 
    Document   : mostrargenero
    Created on : 08-13-2019, 11:27:35 AM
    Author     : fredy.alfarousam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Consulta</title>
    </head>
    <body>
         <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light" >Consulta Genero</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-12">
                    <br>
                    <table class="table table-borderless table-striped table-hover text-light">
                        <thead class="text-warning">
                            <tr>
                                <th>Id</th>
                                <th>Género</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${lista}" var="genero">
                                <tr>
                                    <td>${genero.idgenero}</td>
                                    <td>${genero.genero}</td>
                                    <td>
                                        <a href="genero?action=eliminar&idgenero=${genero.idgenero}" class="btn btn-danger">Eliminar</a>
                                        <a href="genero?action=consultarById&idgenero=${genero.idgenero}" class="btn btn-primary">Actualizar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table> 
                    ${msg}
                     <br>
                    <a href="registroGenero.jsp" class="btn btn-outline-success">Registrar Nuevo Genero</a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">

                </div>
            </div>
        </div>
    </div>  
</body>
</html>
