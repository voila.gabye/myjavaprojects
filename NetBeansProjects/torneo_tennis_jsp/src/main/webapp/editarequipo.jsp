<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro/Edición de Equipos</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
         <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Equipos</h1> </center>
        </header>
        <div class="container">
            <div class="col-6 text-light">        
                <form method="POST" action="equipo">
                    <label for="id">ID</label>
                    <input class="form-control form-group" name="idequipo" id="id" readonly="" type="text" value="${equipo.idequipo}">
                    <select name="idpais" class="form-control form-group">
                        <c:forEach items="${pais}" var="p">
                            <c:choose>
                                <c:when test="${p.idpais==equipo.idpais.idpais}">
                                    <option value="${p.idpais}" selected="">${p.pais}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${p.idpais}">${p.pais}</option>
                                </c:otherwise>
                            </c:choose>

                        </c:forEach>
                    </select>
                     <button type="submit" class="btn btn-outline-success">Enviar</button> 
                </form>
                <br>
                <a class="btn btn-outline-info" href="equipo?action=consultar" role="button">Consultar</a>   
            </div>

            <div class="row">
                <br>
                <div class="col-12">

                </div>
            </div>

        </div>
    </body>
</html>
