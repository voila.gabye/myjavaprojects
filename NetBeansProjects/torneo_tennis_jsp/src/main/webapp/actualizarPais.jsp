<%-- 
    Document   : registropais
    Created on : 08-13-2019, 10:36:41 AM
    Author     : fredy.alfarousam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Actualizar</title>
    </head>
    <body>
         <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Actualizar Pais</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-6 text-light">
                    <form method="post" action="pais?action=actualizar">
                        <div class="form-group">
                            <c:forEach items="${lista}" var="pais">
                                <label>Id Pais</label>
                                <input class="form-control" name="idpais" value="${pais.idpais}">
                            </div>
                            <div class="form-group">
                                <label>Nombre del Pais</label>
                                <input class="form-control" name="pais" value="${pais.pais}">
                            </div>
                            <button type="submit" class="btn btn-outline-success">Enviar</button> 
                        </c:forEach>
                    </form>
                    ${msg}
                    <br>
                    <a class="btn btn-outline-info" href="pais?action=consultarAll" role="button">Consultar</a>   
                </div>

            </div>

            <div class="row">
                <br>
                <div class="col-12">

                </div>
            </div>

        </div>
    </body>
</html>
