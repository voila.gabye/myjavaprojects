<%-- 
    Document   : mostrarlugartorneo
    Created on : 08-15-2019, 02:34:35 PM
    Author     : susana.menjivarusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Lugares del torneo</title>
    </head>
    <body>
         <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Consulta Torneo</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-12">
                    <br>
                    <table class="table table-borderless table-hover text-light">
                        <thead class="text-warning">                
                        <th>Ciudad</th>
                        <th>Pista</th>
                        <th>Nombre del Torneo</th>
                        <th>Acciones</th>
                        </thead>
                        <tbody>
                            <c:forEach items="${lista}" var="lista">
                                <tr>
                                    <td><c:out value="${lista.ciudad.ciudad}"/></td>
                                    <td><c:out value="${lista.pista.pista}"/></td>
                                    <td><c:out value="${lista.torneo.nombre}"/></td>
                                    <td>
                                        <a href="lugarTorneo?action=eliminar&id=<c:out value="${lista.idlugar}"/>"  class="btn btn-danger">Eliminar</a>
                                        <a href="lugarTorneo?action=consultarById&id=<c:out value="${lista.idlugar}"/>" class="btn btn-primary">Actualizar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <br>
                    <a href="lugarTorneo?action=insertar" class="btn btn-outline-success">Registrar Nuevo Lugar de Torneo</a>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </body>
</html>