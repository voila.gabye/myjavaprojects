<%-- 
    Document   : registrarCiudad
    Created on : 08-15-2019, 09:48:27 AM
    Author     : susana.menjivarusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Registro/Edición de Ciudades</title>
    </head>
       <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Registro Ciudad</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-6 text-light">
            <form method="post" action="ciudad">
                <label for="id">Id</label>
                <input class="form-control form-group" name="idciudad" id="id" readonly="" type="text" value="${ciudad.idciudad}">
                <label for="nom">Nombre de Ciudad</label>
                <input name="ciudad" id="nom" class="form-control form-group" type="text" value="${ciudad.ciudad}">                
                <label for="pais">Seleccione el pais donde se realizará</label>
                <select name="idpais" class="form-control form-group">
                    <c:forEach items="${paises}" var="p">
                        <c:choose>
                            <c:when test="${p.idpais==ciudad.idpais.idpais}">
                                <option value="${p.idpais}" selected="">${p.pais}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${p.idpais}">${p.pais}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
                 <button class="btn btn-outline-success">Enviar</button>
            </form>
         <br>
                    <a class="btn btn-outline-info" href="ciudad?action=consultar" role="button">Consultar</a>   
                </div>

            </div>

            <div class="row">
                <br>
                <div class="col-12">


                </div>
            </div>

        </div>
    </body>
</html>
