<%-- 
    Document   : editarPista
    Created on : 08-13-2019, 11:29:41 AM
    Author     : susana.menjivarusam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Registro Pistas</title>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Actualizar Pista</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-6 text-light">
                    <form  action="pista?action=actualizar" method="post">
                        <label for="id">Id</label>
                        <input class="form-control form-group" name="idpista" id="id" readonly="" type="text" value="${pista.idpista}">
                        <label for="pista">Nombre de pista</label>
                        <input name="pista" id="pista" class="form-control form-group" type="text" value="${pista.pista}">
                        <button type="submit" class="btn btn-outline-success">Enviar</button> 
                    </form>
                <br>
                    <a class="btn btn-outline-info" href="pista?action=consultarAll" role="button">Consultar</a>   
                </div>
            </div>
    </body>
</html>
