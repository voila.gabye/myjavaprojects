<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Equipos</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
       <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Consulta Equipo </h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-12">
                    <br>
                    <table class="table table-borderless table-hover text-light">
                        <thead class="text-warning">
                        <th>Equipo</th>
                        <th>Pais</th>
                        <th>Acciones</th>
                        </thead>
                        <tbody>
                            <c:forEach items="${equipo}" var="e">
                                <tr>
                                    <td><c:out value="${e.idequipo}"/></td>
                                    <td><c:out value="${e.idpais.pais}"/></td>
                                    <td>
                                        <a href="equipo?action=eliminar&id=<c:out value="${e.idequipo}"/>" class="btn btn-danger">Eliminar</a>
                                        <a href="equipo?action=consultarById&id=<c:out value="${e.idequipo}"/>" class="btn btn-primary">Actualizar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <br>
                    <a href="equipo?action=insertar" class="btn btn-outline-success" type="sumbimt">Registrar Nuevo Equipo</a>
                </div>
                <div></div>
            </div>
        </div>
    </body>
</html>
