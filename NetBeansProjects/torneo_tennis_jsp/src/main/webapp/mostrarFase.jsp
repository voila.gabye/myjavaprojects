
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Consulta Fase</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
         <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Consulta Fase</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-12">
                    <br>
                    <table class="table table-borderless table-hover text-light">
                        <thead class="text-warning">
                            <tr>
                                <th> Id Fase </th>
                                <th> Fase </th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${lista}" var="ver"> 
                                <tr>
                                    <td>${ver.idfase}</td>
                                    <td>${ver.fase}</td>
                                    <td>
                                        <a href="fase?action=eliminar&idfase=${ver.idfase}" class="btn btn-danger">Eliminar</a>
                                        <a href="fase?action=consultarById&idfase=${ver.idfase}" class="btn btn-primary">Actualizar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table> 
                    ${msg}
                </div>
            </div>
            <div class="row">
                <div class="col-12">

                </div>

            </div>
        </div>
</body>
</html>
