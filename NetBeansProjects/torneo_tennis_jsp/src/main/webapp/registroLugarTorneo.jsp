<%-- 
    Document   : registroLugarTorneo
    Created on : 08-15-2019, 02:39:32 PM
    Author     : susana.menjivarusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Registro/Edición de Lugares para el Torneo</title>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Registro Lugar del Torneo</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-6 text-light">
            <form method="post" action="lugarTorneo">
                <label for="id">Id</label>
                <input class="form-control form-group" name="idlugar" id="id" readonly="" type="text" value="${lugarTorneo.idlugar}">                
                <label for="pais">Seleccione una ciudad</label>
                <select name="ciudad" class="form-control form-group">
                    <c:forEach items="${ciudades}" var="c">
                        <c:choose>
                            <c:when test="${c.idciudad==lugarTorneo.ciudad.idciudad}">
                                <option value="${c.idciudad}" selected="">${c.ciudad}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${c.idciudad}">${c.ciudad}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
                <label for="pais">Seleccione una pista</label>
                <select name="pista" class="form-control form-group">
                    <c:forEach items="${pistas}" var="p">
                        <c:choose>
                            <c:when test="${p.idpista==lugarTorneo.pista.idpista}">
                                <option value="${p.idpista}" selected="">${p.pista}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${p.idpista}">${p.pista}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
                <label for="pais">Seleccione un torneo</label>
                <select name="torneo" class="form-control form-group">
                    <c:forEach items="${torneos}" var="t">
                        <c:choose>
                            <c:when test="${t.idtorneo==lugarTorneo.torneo.idtorneo}">
                                <option value="${t.idtorneo}" selected="">${t.nombre}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${t.idtorneo}">${t.nombre}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
                <button class="btn btn-outline-success">Enviar</button> 
            </form>
        <br>
                    <a class="btn btn-outline-info" href="lugarTorneo?action=consultar" role="button">Consultar</a>   
                </div>

            </div>

            <div class="row">
                <br>
                <div class="col-12">


                </div>
            </div>

        </div>
    </body>
</html>
