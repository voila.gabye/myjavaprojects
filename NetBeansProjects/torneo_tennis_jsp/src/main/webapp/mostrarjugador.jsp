<%-- 
    Document   : mostrarjugador
    Created on : 08-15-2019, 02:16:21 PM
    Author     : fredy.alfarousam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Consulta Jugador</title>
    </head>
    <body>
         <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Consulta Jugador</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-12">
                    <br>
                    <table class="table table-borderless table-hover text-light">
                        <thead class="text-warning">
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Genero</th>
                                <th>Equipo</th>
                                <th>Acciones</th>

                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${lista}" var="jugadores">
                                <tr>
                                    <td>${jugadores.idjugador}</td>
                                    <td>${jugadores.nombre}</td>
                                    <td>${jugadores.idgenero.genero}</td>
                                    <td>${jugadores.idequipo.idpais.pais}</td>
                                    <td><a class="btn btn-danger" href="jugador?action=eliminar&idjugador=${jugadores.idjugador}">Eliminar</a>
                                        <a class="btn btn-primary" href="jugador?action=consultarById&idjugador=${jugadores.idjugador}">Actualizar</a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                     <br>
                    <a href="jugador?action=registro" class="btn btn-outline-success">Registrar Nuevo Jugador</a>
                </div>
            </div>
        </div>

    </body>
</html>
