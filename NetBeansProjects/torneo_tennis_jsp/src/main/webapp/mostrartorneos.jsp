<%-- 
    Document   : mostrartorneos
    Created on : 08-15-2019, 08:37:23 AM
    Author     : susana.menjivarusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Torneos</title>
    </head>
    <body>
          <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Consulta Torneo</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-12">
                    <br>
                    <table class="table table-borderless table-hover text-light">
                        <thead class="text-warning">
                        <th>Nombre</th>
                        <th>Fecha de Inicio</th>
                        <th>Fecha de Finalización</th>
                        <th>Pais</th>
                        <th>Acciones</th>
                        </thead>
                        <tbody>
                            <c:forEach items="${lista}" var="lista">
                                <tr>
                                    <td><c:out value="${lista.nombre}"/></td>
                                    <td><c:out value="${lista.fecha_inicio}"/></td>
                                    <td><c:out value="${lista.fecha_fin}"/></td>
                                    <td><c:out value="${lista.pais.pais}"/></td> 
                                    <td>
                                        <a href="torneo?action=eliminar&id=<c:out value="${lista.idtorneo}"/>" class="btn btn-danger">Eliminar</a>
                                        <a href="torneo?action=consultarById&id=<c:out value="${lista.idtorneo}"/>" class="btn btn-primary">Actualizar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <br>
                    <a href="torneo?action=insertar" class="btn btn-outline-success">Registrar Nuevo Torneo</a>
                </div>
                <div class="col-md-1"></div>
            </div>
    </body>
</html>
