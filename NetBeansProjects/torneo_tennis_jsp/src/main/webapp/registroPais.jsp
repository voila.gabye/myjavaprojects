<%-- 
    Document   : registropais
    Created on : 08-13-2019, 10:36:41 AM
    Author     : fredy.alfarousam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Pais</title>
    </head>
    <body>
       <jsp:include page="menu.jsp" />
        <br><br><br><br>
       <header>
            <center><h1 class="text-light">Registro Pais</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
               <div class="col-6 text-light">
                    <form action="pais?action=insertar" method="POST">
                         <div class="form-group">
                        <label>Nombre del Pais</label>
                        <input class="form-control" name="pais">
                        </div>
                        <button class="btn btn-outline-success">Enviar</button> 
                    </form>
                    ${msg}
                    <br>
                     <a class="btn btn-outline-info" href="pais?action=consultarAll" role="button">Consultar</a>   
                </div>    
                    <div class="col-6"></div>
            </div>
            <div class="row">
                <br>
                <div class="col-12">
                </div>
            </div>
        </div>
    </body>
</html>
