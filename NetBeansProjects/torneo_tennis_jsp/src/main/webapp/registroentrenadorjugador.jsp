<%-- 
    Document   : registroentrenadorjugador
    Created on : 08-16-2019, 11:36:31 AM
    Author     : jhonatan.acevedousam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Entrenador Jugador</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
       <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Registro Entrenadores y Jugadores</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-6 text-light">
                    <form action="entrenador_jugador?action=insertar" method="POST">
                        <div class="form-group">
                        <label>Seleccione Jugador</label>
                        <select name="idjugador" class="form-control">
                            <c:forEach items="${listajugador}" var="jugador">
                                <option value="${jugador.idjugador}">${jugador.nombre}</option>
                            </c:forEach>
                        </select>
                        </div>
                        <div class="form-group">
                        <label>Seleccione Entrenador</label>
                        <select name="identrenador" class="form-control">
                            <c:forEach items="${listaentrenador}" var="entrenador">
                                <option value="${entrenador.identre}">${entrenador.nombre}</option>
                            </c:forEach>
                        </select>
                        </div>
                        <div class="form-group">
                        <label>Fecha Inicio</label>
                        <input type="Date" class="form-control" name="fecha_inicio" id="fecha_inicio"/>
                        </div>
                        <div class="form-group">
                        <label>Fecha Fin</label>
                        <input type="Date" class="form-control" name="fecha_fin" id="fecha_fin"/>
                        </div>
                         <button class="btn btn-outline-success">Enviar</button> 
                    </form>
                     ${msg}
                     <br>
                      <a class="btn btn-outline-info" href="entrenador_jugador?action=consultar">Consultar</a>
                </div>
                <div class="col-4">
                  
                   
                </div>
            </div> 
        </div>
    </body>
</html>
