<%-- 
    Document   : mostrarjugador_partido
    Created on : 08-16-2019, 08:59:55 AM
    Author     : fredy.alfarousam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Jugadores Partidos</title>
    </head>
    <body>
        <jsp:include page="menu.jsp" />
        <br><br><br><br>
        <header>
            <center><h1 class="text-light">Jugadores y Partidos</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <br>
                <div class="col-12">
                    <br>
                    <table class="table table-borderless table-hover text-light">
                        <thead class="text-warning">
                            <tr>
                                <th>Id</th>
                                <th>Jugador</th>
                                <th>Partido</th>
                                <th>Resultado</th>
                                <th>Ganador</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${lista}" var="ver">
                                <tr>
                                    <td>${ver.idjugadorP}</td>
                                    <td>${ver.idjugador.nombre}</td>
                                    <td>${ver.idpartido.lugarT.pista.pista}</td>
                                    <td>${ver.resultado}</td>
                                    <td><c:choose>
                                            <c:when test="${ver.ganador==true}">
                                                Si
                                            </c:when>
                                            <c:otherwise>
                                                No
                                            </c:otherwise>
                                        </c:choose></td>
                                    <td>
                                        <a href="jugador_partido?action=eliminar&idjugadorP=${ver.idjugadorP}" class="btn btn-danger">Eliminar</a>
                                        <a href="jugador_partido?action=consultarById&idjugadorP=${ver.idjugadorP}" class="btn btn-primary">Actualizar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                      <br>
                    <a href="jugador_partido?action=registro" class="btn btn-outline-success">Registrar Nuevos Jugadores y Partidos</a>
                </div>
            </div>
        </div>
    </body>
</html>
