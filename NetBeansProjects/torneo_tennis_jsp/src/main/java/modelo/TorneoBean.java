package modelo;


public class TorneoBean {
    private int idtorneo;
    private String nombre;
    private String fecha_inicio;
    private String fecha_fin;
    private PaisBean pais;

    public TorneoBean(int id) {
        this.idtorneo=id;
    }

    public int getIdtorneo() {
        return idtorneo;
    }

    public void setIdtorneo(int idtorneo) {
        this.idtorneo = idtorneo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(String fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public PaisBean getPais() {
        return pais;
    }

    public void setPais(PaisBean pais) {
        this.pais = pais;
    }
    
    
}
