package modelo;

public class ArbitroBean {

    private int idarbi;
    private String nombre;

    public ArbitroBean(int idarbi) {
        this.idarbi = idarbi;
    }

    public int getIdarbi() {
        return idarbi;
    }

    public void setIdarbi(int idarbi) {
        this.idarbi = idarbi;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
