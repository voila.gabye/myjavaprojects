package modelo;

public class EntrenadorBean {

    private int identre;
    private String nombre;

    public EntrenadorBean(int identre) {
        this.identre = identre;
    }

    public int getIdentre() {
        return identre;
    }

    public void setIdentre(int identre) {
        this.identre = identre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
