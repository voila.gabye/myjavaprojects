package modelo;

public class JugadorBean {

    private int idjugador;
    private String nombre;
    private GeneroBean idgenero;
    private EquipoBean idequipo;
    
    

    public JugadorBean(int idjugador) {
        this.idjugador = idjugador;
    }

    public EquipoBean getIdequipo() {
        return idequipo;
    }

    public void setIdequipo(EquipoBean idequipo) {
        this.idequipo = idequipo;
    }
    
    public int getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(int idjugador) {
        this.idjugador = idjugador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public GeneroBean getIdgenero() {
        return idgenero;
    }

    public void setIdgenero(GeneroBean idgenero) {
        this.idgenero = idgenero;
    }
    
    

}
