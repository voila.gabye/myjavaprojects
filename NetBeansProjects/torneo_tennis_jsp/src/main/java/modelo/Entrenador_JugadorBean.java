package modelo;

import java.util.Date;

public class Entrenador_JugadorBean {
    
    private int identrenaJuga;
    private JugadorBean idjugador;
    private EntrenadorBean identrenador;
    private Date fecha_inicio;
    private Date fecha_fin;

    public Entrenador_JugadorBean(int identrenaJuga) {
        this.identrenaJuga = identrenaJuga;
    }

    public int getIdentrenaJuga() {
        return identrenaJuga;
    }

    public void setIdentrenaJuga(int identrenaJuga) {
        this.identrenaJuga = identrenaJuga;
    }

    public JugadorBean getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(JugadorBean idjugador) {
        this.idjugador = idjugador;
    }

    public EntrenadorBean getIdentrenador() {
        return identrenador;
    }

    public void setIdentrenador(EntrenadorBean identrenador) {
        this.identrenador = identrenador;
    }

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public Date getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(Date fecha_fin) {
        this.fecha_fin = fecha_fin;
    }
    
    
}
