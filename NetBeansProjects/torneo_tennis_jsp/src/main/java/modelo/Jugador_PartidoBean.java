package modelo;

public class Jugador_PartidoBean {

    private int idjugadorP;
    private JugadorBean idjugador;
    private PartidoBean idpartido;
    private String resultado;
    private boolean ganador;

    public Jugador_PartidoBean(int idjugadorP) {
        this.idjugadorP = idjugadorP;
    }
    
    public int getIdjugadorP() {
        return idjugadorP;
    }

    public void setIdjugadorP(int idjugadorP) {
        this.idjugadorP = idjugadorP;
    }

    public JugadorBean getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(JugadorBean idjugador) {
        this.idjugador = idjugador;
    }

    public PartidoBean getIdpartido() {
        return idpartido;
    }

    public void setIdpartido(PartidoBean idpartido) {
        this.idpartido = idpartido;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public boolean getGanador() {
        return ganador;
    }

    public void setGanador(boolean ganador) {
        this.ganador = ganador;
    }
}
