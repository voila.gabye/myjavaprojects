package modelo;


public class PremioBean {
    private int idpremio;
    private double premio;
    private FaseBean fase;

    public PremioBean(int id) {
        this.idpremio=id;
    }

    public int getIdpremio() {
        return idpremio;
    }

    public void setIdpremio(int idpremio) {
        this.idpremio = idpremio;
    }

    public double getPremio() {
        return premio;
    }

    public void setPremio(double premio) {
        this.premio = premio;
    }

    public FaseBean getFase() {
        return fase;
    }

    public void setFase(FaseBean fase) {
        this.fase = fase;
    }
    
    
    
    
    
    
}
