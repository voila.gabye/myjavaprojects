package modelo;

public class PistaBean {

    private int idpista;
    private String pista;

    public PistaBean(int id) {
        this.idpista = id;
    }

    public int getIdpista() {
        return idpista;
    }

    public void setIdpista(int idpista) {
        this.idpista = idpista;
    }

    public String getPista() {
        return pista;
    }

    public void setPista(String pista) {
        this.pista = pista;
    }

}
