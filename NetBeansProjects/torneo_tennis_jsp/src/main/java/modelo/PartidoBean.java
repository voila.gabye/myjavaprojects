package modelo;


public class PartidoBean {
   private int idpartido;
   private Lugar_TorneoBean lugarT;
   private ModalidadBean modalidad;
   private ArbitroBean arbitro;
   private FaseBean fase;
   private PremioBean premio;

    public PartidoBean(int id) {
        this.idpartido=id;
    }

    public int getIdpartido() {
        return idpartido;
    }

    public void setIdpartido(int idpartido) {
        this.idpartido = idpartido;
    }

    public Lugar_TorneoBean getLugarT() {
        return lugarT;
    }

    public void setLugarT(Lugar_TorneoBean lugarT) {
        this.lugarT = lugarT;
    }

    public ModalidadBean getModalidad() {
        return modalidad;
    }

    public void setModalidad(ModalidadBean modalidad) {
        this.modalidad = modalidad;
    }

    public ArbitroBean getArbitro() {
        return arbitro;
    }

    public void setArbitro(ArbitroBean arbitro) {
        this.arbitro = arbitro;
    }

    public FaseBean getFase() {
        return fase;
    }

    public void setFase(FaseBean fase) {
        this.fase = fase;
    }

    public PremioBean getPremio() {
        return premio;
    }

    public void setPremio(PremioBean premio) {
        this.premio = premio;
    }
   
    
   
}
