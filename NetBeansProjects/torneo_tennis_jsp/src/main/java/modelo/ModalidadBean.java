package modelo;

public class ModalidadBean {

    private int idmodal;
    private String nombre;

    public ModalidadBean(int idmodal) {
        this.idmodal = idmodal;
    }

    public int getIdmodal() {
        return idmodal;
    }

    public void setIdmodal(int idmodal) {
        this.idmodal = idmodal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
