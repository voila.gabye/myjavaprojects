package modelo;

public class EquipoBean {
    
    private int idequipo;
    private PaisBean idpais;

    public EquipoBean(int idequipo) {
        this.idequipo = idequipo;
    }

    public int getIdequipo() {
        return idequipo;
    }

    public void setIdequipo(int idequipo) {
        this.idequipo = idequipo;
    }

    public PaisBean getIdpais() {
        return idpais;
    }

    public void setIdpais(PaisBean idpais) {
        this.idpais = idpais;
    }
    
    
}
