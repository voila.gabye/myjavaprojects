package modelo;

public class FaseBean {

    private int idfase;
    private String fase;

    public FaseBean(int idfase) {
        this.idfase = idfase;
    }

    public int getIdfase() {
        return idfase;
    }

    public void setIdfase(int idfase) {
        this.idfase = idfase;
    }

    public String getFase() {
        return fase;
    }

    public void setFase(String fase) {
        this.fase = fase;
    }

}
