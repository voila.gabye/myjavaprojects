
package modelo;


public class CiudadBean {
    private int idciudad;
    private String ciudad;
    private PaisBean idpais; 

    public CiudadBean(int idciudad) {
        this.idciudad = idciudad;
    }

    public int getIdciudad() {
        return idciudad;
    }

    public void setIdciudad(int idciudad) {
        this.idciudad = idciudad;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public PaisBean getIdpais() {
        return idpais;
    }

    public void setIdpais(PaisBean idpais) {
        this.idpais = idpais;
    }   
}
