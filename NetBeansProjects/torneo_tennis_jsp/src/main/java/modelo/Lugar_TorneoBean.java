package modelo;


public class Lugar_TorneoBean {
    private int idlugar;
    private CiudadBean ciudad;
    private PistaBean pista;
    private TorneoBean torneo;

    public Lugar_TorneoBean(int id) {
        this.idlugar=id;
    }

    public int getIdlugar() {
        return idlugar;
    }

    public void setIdlugar(int idlugar) {
        this.idlugar = idlugar;
    }

    public CiudadBean getCiudad() {
        return ciudad;
    }

    public void setCiudad(CiudadBean ciudad) {
        this.ciudad = ciudad;
    }

    public PistaBean getPista() {
        return pista;
    }

    public void setPista(PistaBean pista) {
        this.pista = pista;
    }

    public TorneoBean getTorneo() {
        return torneo;
    }

    public void setTorneo(TorneoBean torneo) {
        this.torneo = torneo;
    }
    
    
    
    
}
