package controller;

import conexion.Conexion;
import dao.ModalidadDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.ModalidadBean;

public class ModalidadServlet extends HttpServlet {

    Conexion conn = new Conexion();
    ModalidadDao modad = new ModalidadDao(conn);
    RequestDispatcher rd;
    String msg;
    boolean respuesta;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "actualizar":
                actualizar(request, response);
                break;
            case "consultarAll":
                consultarAll(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            default:
                throw new AssertionError();
        }
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nombre = request.getParameter("nombre");

        ModalidadBean modab = new ModalidadBean(0);
        modab.setNombre(nombre);

        respuesta = modad.insertar(modab);
        if (respuesta) {
            msg = "Registro fue guardado con éxito";
        } else {
            msg = "Registro no fue guardado";
        }

        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/registroModalidad.jsp");
        rd.forward(request, response);

    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idmodal = Integer.parseInt(request.getParameter("idmodal"));
        String nombre = request.getParameter("nombre");

        ModalidadBean modab = new ModalidadBean(idmodal);
        modab.setNombre(nombre);

        respuesta = modad.actualizar(modab);
        List<ModalidadBean> lista = modad.consultarAll();
        if (respuesta) {
            msg = "Registro fue actualizado con éxito";
        } else {
            msg = "Registro no fue actualizado";
        }
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/mostrarModalidad.jsp");
        rd.forward(request, response);
    }

    protected void consultarAll(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<ModalidadBean> lista = modad.consultarAll();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/mostrarModalidad.jsp");
        rd.forward(request, response);
    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idmodal = Integer.parseInt(request.getParameter("idmodal"));
        List<ModalidadBean> lista = modad.consultarById(idmodal);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/actualizarModalidad.jsp");
        rd.forward(request, response);
    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idmodal = Integer.parseInt(request.getParameter("idmodal"));
        respuesta = modad.eliminar(idmodal);
        List<ModalidadBean> lista = modad.consultarAll();
        if (respuesta) {
            msg = "Registro fue eliminado con éxito";
        } else {
            msg = "Registro no fue eliminado";
        }
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/mostrarModalidad.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
