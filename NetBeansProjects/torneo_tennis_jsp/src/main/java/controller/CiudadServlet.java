package controller;

import conexion.Conexion;
import dao.CiudadDao;
import dao.PaisDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.CiudadBean;
import modelo.PaisBean;

public class CiudadServlet extends HttpServlet {

    Conexion conn = new Conexion();
    CiudadDao ciudadDao = new CiudadDao(conn);
    PaisDao paisDao = new PaisDao(conn);
    RequestDispatcher rd;
    String msg;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
            case "consultarById":
                consultarById(request, response);
            case "consultar":
                consultar(request, response);
            case "eliminar":
                eliminar(request, response);

        }
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd;
        request.setAttribute("paises", paisDao.consultarAll());

        rd = request.getRequestDispatcher("/registrarCiudad.jsp");
        rd.forward(request, response);

    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        RequestDispatcher rd;
        request.setAttribute("paises", paisDao.consultarAll());
        request.setAttribute("ciudad", ciudadDao.consultarById(id));
        rd = request.getRequestDispatcher("/registrarCiudad.jsp");
        rd.forward(request, response);
    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher rd;

        request.setAttribute("lista", ciudadDao.consultarAll());
        rd = request.getRequestDispatcher("/mostrarciudades.jsp");
        rd.forward(request, response);
    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        RequestDispatcher rd;
        ciudadDao.eliminar(id);
        request.setAttribute("lista", ciudadDao.consultarAll());
        rd = request.getRequestDispatcher("/mostrarciudades.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String cod = request.getParameter("idciudad");
        CiudadBean ciudad = new CiudadBean(0);
        ciudad.setCiudad(request.getParameter("ciudad"));
        PaisBean pais = new PaisBean(0);
        pais.setIdpais(Integer.parseInt(request.getParameter("idpais")));
        ciudad.setIdpais(pais);

        if (cod == null || cod.isEmpty()) {
            ciudadDao.insertar(ciudad);
            request.setAttribute("lista", ciudadDao.consultarAll());
        } else {
            int idciudad = Integer.parseInt(cod);
            ciudad.setIdciudad(idciudad);
            ciudadDao.actualizar(ciudad);
            request.setAttribute("lista", ciudadDao.consultarAll());
        }

        RequestDispatcher rd;

        rd = request.getRequestDispatcher("/mostrarciudades.jsp");
        rd.forward(request, response);
    }

}
