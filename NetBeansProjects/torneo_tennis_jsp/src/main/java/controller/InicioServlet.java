
package controller;

import conexion.Conexion;
import dao.ArbitroDao;
import dao.EntrenadorDao;
import dao.EquipoDao;
import dao.JugadorDao;
import dao.PartidoDao;
import dao.PistaDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class InicioServlet extends HttpServlet {

    Conexion conn=new Conexion();
    EquipoDao equipoDao=new EquipoDao(conn);
    JugadorDao jugadorDao=new JugadorDao(conn);
    PistaDao pistaDao=new PistaDao(conn);
    EntrenadorDao entrenadorDao=new EntrenadorDao(conn);
    ArbitroDao arbitroDao=new ArbitroDao(conn);
    PartidoDao partidoDao=new PartidoDao(conn);
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "inicio":
                inicio(request, response);

        }
    }

    protected void inicio(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.setAttribute("conteoEquipos", equipoDao.getNumEquipos());
        request.setAttribute("conteoJugadores", jugadorDao.getNumJugadores());
        request.setAttribute("conteoPistas", pistaDao.getNumPistas());
        request.setAttribute("conteoEntrenadores", entrenadorDao.getNumEntrenadores());
        request.setAttribute("conteoArbitros", arbitroDao.getNumAarbitros());
	request.setAttribute("conteoPartidos", partidoDao.getNumPartido());
        
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("/inicio.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
