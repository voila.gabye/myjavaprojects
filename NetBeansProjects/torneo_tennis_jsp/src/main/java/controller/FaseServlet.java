package controller;

import conexion.Conexion;
import dao.FaseDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.FaseBean;

public class FaseServlet extends HttpServlet {

    Conexion conn = new Conexion();
    FaseDao fased = new FaseDao(conn);
    RequestDispatcher rd;
    String msg;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "actualizar":
                actualizar(request, response);
                break;
            case "consultarAll":
                consultarAll(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            default:
                throw new AssertionError();
        }
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String fase = request.getParameter("fase");

        FaseBean faseb = new FaseBean(0);
        faseb.setFase(fase);

        boolean respuesta = fased.insertar(faseb);
        if (respuesta) {
            msg = "Registro fue guardado con éxito";
        } else {
            msg = "Registro no fue guardado";
        }

        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/registroFase.jsp");
        rd.forward(request, response);

    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idfase = Integer.parseInt(request.getParameter("idfase"));
        String fase = request.getParameter("fase");

        FaseBean faseb = new FaseBean(idfase);
        faseb.setFase(fase);

        boolean respuesta = fased.actualizar(faseb);
        List<FaseBean> lista = fased.consultarAll();
        if (respuesta) {
            msg = "Registro fue actualizado con éxito";
        } else {
            msg = "Registro no fue actualizado";
        }
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/mostrarFase.jsp");
        rd.forward(request, response);
    }

    protected void consultarAll(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<FaseBean> lista = fased.consultarAll();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/mostrarFase.jsp");
        rd.forward(request, response);
    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idfase = Integer.parseInt(request.getParameter("idfase"));
        List<FaseBean> lista = fased.consultarById(idfase);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/actualizarFase.jsp");
        rd.forward(request, response);
    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idfase = Integer.parseInt(request.getParameter("idfase"));
        boolean respuesta = fased.eliminar(idfase);
        List<FaseBean> lista = fased.consultarAll();
        if (respuesta) {
            msg = "Registro fue eliminado con éxito";
        } else {
            msg = "Registro no fue eliminado";
        }
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/mostrarFase.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
