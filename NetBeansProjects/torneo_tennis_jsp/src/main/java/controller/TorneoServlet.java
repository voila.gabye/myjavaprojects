
package controller;

import conexion.Conexion;
import dao.PaisDao;
import dao.TorneoDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.PaisBean;
import modelo.TorneoBean;

public class TorneoServlet extends HttpServlet {

    Conexion conn = new Conexion();
    TorneoDao torneoDao=new TorneoDao(conn);
    PaisDao paisDao=new PaisDao(conn);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
            case "consultarById":
                consultarById(request, response);
            case "consultar":
                consultar(request, response);
            case "eliminar":
                eliminar(request, response);

        }
    }
    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd;
        request.setAttribute("paises", paisDao.consultarAll());
        
        rd = request.getRequestDispatcher("/registrarTorneo.jsp");
        rd.forward(request, response);
    }
    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd;
        int id=Integer.parseInt(request.getParameter("id"));
        request.setAttribute("torneo", torneoDao.consultarById(id));
        request.setAttribute("paises", paisDao.consultarAll());
        
        rd = request.getRequestDispatcher("/registrarTorneo.jsp");
        rd.forward(request, response);
    }
    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd;
        
        request.setAttribute("lista", torneoDao.consultar());
        
        rd = request.getRequestDispatcher("/mostrartorneos.jsp");
        rd.forward(request, response);
    }
    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd;
        int id=Integer.parseInt(request.getParameter("id"));
        torneoDao.eliminar(id);
        request.setAttribute("lista", torneoDao.consultar());
        rd = request.getRequestDispatcher("/mostrartorneos.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        TorneoBean torneo=new TorneoBean(0);
        String cod=request.getParameter("idtorneo");
        torneo.setNombre(request.getParameter("nombre"));
        torneo.setFecha_inicio(request.getParameter("fecha_inicio"));
        torneo.setFecha_fin(request.getParameter("fecha_fin"));
        PaisBean pais=new PaisBean(0);
        pais.setIdpais(Integer.parseInt(request.getParameter("idpais")));
        torneo.setPais(pais);
        
        if (cod==null || cod.isEmpty()) {
            torneoDao.insertar(torneo);
            request.setAttribute("lista", torneoDao.consultar());
        }else{
            int idtorneo=Integer.parseInt(cod);
            torneo.setIdtorneo(idtorneo);
            torneoDao.editar(torneo);
            request.setAttribute("lista", torneoDao.consultar());
        }
        
        RequestDispatcher rd;
        
        rd = request.getRequestDispatcher("/mostrartorneos.jsp");
        rd.forward(request, response);
    }

}