package controller;

import conexion.Conexion;
import dao.EquipoDao;
import dao.PaisDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.EquipoBean;
import modelo.PaisBean;

public class EquipoServlet extends HttpServlet {

    Conexion conn = new Conexion();
    EquipoDao equipoDao = new EquipoDao(conn);
    PaisDao paisDao = new PaisDao(conn);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
            case "consultarById":
                consultarById(request, response);
            case "consultar":
                consultar(request, response);
            case "eliminar":
                eliminar(request, response);
        }
    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        EquipoBean equipo = equipoDao.consultarById(id);
        RequestDispatcher rd;
        request.setAttribute("pais", paisDao.consultarAll());
        request.setAttribute("equipo", equipo);
        rd = request.getRequestDispatcher("/editarequipo.jsp");
        rd.forward(request, response);
    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<EquipoBean> lista = equipoDao.consultar();

        RequestDispatcher rd;
        request.setAttribute("equipo", lista);
        rd = request.getRequestDispatcher("/mostrarequipo.jsp");
        rd.forward(request, response);
    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        equipoDao.eliminar(id);
        List<EquipoBean> lista = equipoDao.consultar();
        RequestDispatcher rd;
        request.setAttribute("equipo", lista);
        rd = request.getRequestDispatcher("/mostrarequipo.jsp");
        rd.forward(request, response);
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd;
        request.setAttribute("pais", paisDao.consultarAll());

        rd = request.getRequestDispatcher("/editarequipo.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String cod = request.getParameter("idequipo");
        int idpais = Integer.parseInt(request.getParameter("idpais"));
        EquipoBean e = new EquipoBean(0);
        PaisBean pais = new PaisBean(0);
        pais.setIdpais(idpais);
        e.setIdpais(pais);

        if (cod == null || cod.isEmpty()) {

            equipoDao.insertar(e);
            request.setAttribute("equipo", equipoDao.consultar());
        } else {
            int idequipo = Integer.parseInt(cod);
            e.setIdequipo(idequipo);
            equipoDao.editar(e);
            request.setAttribute("equipo", equipoDao.consultar());
        }

        RequestDispatcher rd;
        rd = request.getRequestDispatcher("/mostrarequipo.jsp");
        rd.forward(request, response);
    }

}
