package controller;

import conexion.Conexion;
import dao.ArbitroDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.ArbitroBean;

public class ArbitroServlet extends HttpServlet {

    Conexion conn = new Conexion();
    ArbitroDao ard = new ArbitroDao(conn);
    RequestDispatcher rd;
    boolean respuesta;
    String msg;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "actualizar":
                actualizar(request, response);
                break;
            case "consultarAll":
                consultarAll(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
        }
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String nombre = request.getParameter("nombre");
        ArbitroBean arb = new ArbitroBean(0);
        arb.setNombre(nombre);
        respuesta = ard.insertar(arb);
        if (respuesta) {
            msg = "Registro Guardado";
        } else {
            msg = "error";
        }
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/registroArbitro.jsp");
        rd.forward(request, response);

    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idarbi = Integer.parseInt(request.getParameter("idarbi"));
        String nombre = request.getParameter("nombre");
        ArbitroBean arb = new ArbitroBean(idarbi);
        arb.setNombre(nombre);
        respuesta = ard.actualizar(arb);
        List<ArbitroBean> lista = ard.consultarAll();
        if (respuesta) {
            msg = "Registro Actualizado";
        } else {
            msg = "error";
        }
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/mostrarArbitro.jsp");
        rd.forward(request, response);

    }

    protected void consultarAll(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<ArbitroBean> lista = ard.consultarAll();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/mostrarArbitro.jsp");
        rd.forward(request, response);

    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idarbi = Integer.parseInt(request.getParameter("idarbi"));
        List<ArbitroBean> lista = ard.consultarById(idarbi);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/actualizarArbitro.jsp");
        rd.forward(request, response);

    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idarbi = Integer.parseInt(request.getParameter("idarbi"));
        respuesta = ard.eliminar(idarbi);
        List<ArbitroBean> lista = ard.consultarAll();
        if (respuesta) {
            msg = "Registro eliminado";
        } else {
            msg = "error";
        }
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/mostrarArbitro.jsp");
        rd.forward(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
