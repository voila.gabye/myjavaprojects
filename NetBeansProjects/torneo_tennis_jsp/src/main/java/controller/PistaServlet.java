
package controller;

import conexion.Conexion;
import dao.PistaDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.PistaBean;

public class PistaServlet extends HttpServlet {

    Conexion conn = new Conexion();
    PistaDao pistaDao = new PistaDao(conn);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
            case "actualizar":
                actualizar(request, response);
            case "consultarById":
                consultarById(request, response);
            case "consultarAll":
                consultar(request, response);
            case "eliminar":
                eliminar(request, response);

        }

    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nombrepista = request.getParameter("pista");
        
        PistaBean pista = new PistaBean(0);
        pista.setPista(nombrepista);
        
        boolean res = pistaDao.insertar(pista);
        String msg;
        if (res) {
            msg = "exito al guardar";
        } else {
            msg = "error al guardar";
        }
        List<PistaBean> lista = pistaDao.consultarAll();
        RequestDispatcher rd;
        request.setAttribute("lista", lista);
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/registroPista.jsp");
        rd.forward(request, response);

    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idpista = Integer.parseInt(request.getParameter("idpista"));
        String nombrepista = request.getParameter("pista");
        PistaBean pista = new PistaBean(0);
        pista.setIdpista(idpista);
        pista.setPista(nombrepista);
        boolean res = pistaDao.actualizar(pista);
        String msg;
        if (res) {
            msg = "exito al editar";
        } else {
            msg = "error al editar";
        }
        List<PistaBean> lista = pistaDao.consultarAll();
        RequestDispatcher rd;
        request.setAttribute("lista", lista);
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/mostrarPista.jsp");
        rd.forward(request, response);
    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));

        PistaBean pista = pistaDao.consultarById(id);
        RequestDispatcher rd;
        request.setAttribute("pista", pista);
        rd = request.getRequestDispatcher("/actualizarPista.jsp");
        rd.forward(request, response);
    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<PistaBean> lista = pistaDao.consultarAll();
        RequestDispatcher rd;
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/mostrarPista.jsp");
        rd.forward(request, response);
    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        boolean resp = pistaDao.eliminar(id);
        String msg;
        if (resp) {
            msg = "exito al eliminar";
        } else {
            msg = "error al eliminar";
        }
        List<PistaBean> lista = pistaDao.consultarAll();
        RequestDispatcher rd;
        request.setAttribute("lista", lista);
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/mostrarPista.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
