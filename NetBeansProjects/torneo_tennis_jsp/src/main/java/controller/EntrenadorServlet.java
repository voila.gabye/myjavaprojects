package controller;

import conexion.Conexion;
import dao.EntrenadorDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.EntrenadorBean;

public class EntrenadorServlet extends HttpServlet {

    Conexion conn = new Conexion();
    EntrenadorDao end = new EntrenadorDao(conn);
    RequestDispatcher rd;
    boolean respuesta;
    String msg;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "actualizar":
                actualizar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            case "consultarAll":
                consultarAll(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
             default:
                throw new AssertionError();
        }
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String nombre = request.getParameter("nombre");

        EntrenadorBean enb = new EntrenadorBean(0);
        enb.setNombre(nombre);

        respuesta = end.insertar(enb);
        if (respuesta) {
            msg = "Entrenador Guardado";
        } else {
            msg = "Error";
        }
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/registroEntrenador.jsp");
        rd.forward(request, response);
    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int identre = Integer.parseInt(request.getParameter("identre"));
        String nombre = request.getParameter("nombre");

        EntrenadorBean enb = new EntrenadorBean(identre);
        enb.setNombre(nombre);

        respuesta = end.actualizar(enb);
        List<EntrenadorBean> lista = end.consultarAll();
        if (respuesta) {
            msg = "Entrenador Actualizado";
        } else {
            msg = "Error";
        }
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/mostrarEntrenador.jsp");
        rd.forward(request, response);

    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int identre = Integer.parseInt(request.getParameter("identre"));
        respuesta = end.eliminar(identre);
        List<EntrenadorBean> lista = end.consultarAll();
        if (respuesta) {
            msg = "Entrenador Eliminado";
        } else {
            msg = "Error";
        }
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/mostrarEntrenador.jsp");
        rd.forward(request, response);
    }

    protected void consultarAll(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<EntrenadorBean> lista = end.consultarAll();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/mostrarEntrenador.jsp");
        rd.forward(request, response);
    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int identre = Integer.parseInt(request.getParameter("identre"));
        List<EntrenadorBean> lista = end.consultarById(identre);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/actualizarEntrenador.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
