package controller;

import conexion.Conexion;
import dao.GeneroDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.GeneroBean;

public class GeneroServlet extends HttpServlet {

    String msg;
    boolean respuesta;
    RequestDispatcher rd;
    Conexion conn = new Conexion();
    GeneroDao gend = new GeneroDao(conn);
    GeneroBean genb;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");

        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "actualizar":
                actualizar(request, response);
                break;
            case "consultar":
                consultar(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            default:
                throw new AssertionError();
        }
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String genero = request.getParameter("genero");

        genb = new GeneroBean(0);
        genb.setGenero(genero);

        respuesta = gend.insertar(genb);
        if (respuesta) {
            msg = "Genero Guardado";
        } else {
            msg = "Error al guardar genero";
        }
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("registroGenero.jsp");
        rd.forward(request, response);

    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idgenero = Integer.parseInt(request.getParameter("idgenero"));
        String genero = request.getParameter("genero");

        genb = new GeneroBean(idgenero);
        genb.setGenero(genero);

        respuesta = gend.actualizar(genb);
        List<GeneroBean> lista = gend.consultar();

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarGenero.jsp");
        rd.forward(request, response);
    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<GeneroBean> lista = gend.consultar();

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarGenero.jsp");
        rd.forward(request, response);

    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idgenero = Integer.parseInt(request.getParameter("idgenero"));
        List<GeneroBean> lista = gend.consultarById(idgenero);

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("actualizarGenero.jsp");
        rd.forward(request, response);
    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idgenero = Integer.parseInt(request.getParameter("idgenero"));
        gend.eliminar(idgenero);
        List<GeneroBean> lista = gend.consultar();

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarGenero.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
