package controller;

import conexion.Conexion;
import dao.PaisDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.PaisBean;

public class PaisServlet extends HttpServlet {

    String msg;
    boolean respuesta;
    RequestDispatcher rd;
    Conexion conn = new Conexion();
    PaisDao dao = new PaisDao(conn);
    PaisBean bean;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");

        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "actualizar":
                actualizar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            case "consultarAll":
                consultar(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            default:
                throw new AssertionError();
        }
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String pais = request.getParameter("pais");

        bean = new PaisBean(0);
        bean.setPais(pais);

        respuesta = dao.insertar(bean);
        if (respuesta) {
            msg = "Pais Guardado";
        } else {
            msg = "Error al guardar pais";
        }

        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("registroPais.jsp");
        rd.forward(request, response);

    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idpais = Integer.parseInt(request.getParameter("idpais"));
        String pais = request.getParameter("pais");

        bean = new PaisBean(idpais);
        bean.setPais(pais);

       respuesta=  dao.actualizar(bean);
        List<PaisBean> lista = dao.consultarAll();

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarPais.jsp");
        rd.forward(request, response);
    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idpais = Integer.parseInt(request.getParameter("idpais"));
        dao.eliminar(idpais);
        List<PaisBean> lista = dao.consultarAll();

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarPais.jsp");
        rd.forward(request, response);
    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<PaisBean> lista = dao.consultarAll();

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarPais.jsp");
        rd.forward(request, response);

    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idpais = Integer.parseInt(request.getParameter("idpais"));
        List<PaisBean> lista = dao.consultarById(idpais);

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("actualizarPais.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
