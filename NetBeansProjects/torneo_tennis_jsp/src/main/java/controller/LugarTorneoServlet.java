package controller;

import conexion.Conexion;
import dao.CiudadDao;
import dao.Lugar_TorneoDao;
import dao.PistaDao;
import dao.TorneoDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.CiudadBean;
import modelo.Lugar_TorneoBean;
import modelo.PistaBean;
import modelo.TorneoBean;

public class LugarTorneoServlet extends HttpServlet {

    Conexion conn = new Conexion();
    Lugar_TorneoDao lugarTDao = new Lugar_TorneoDao(conn);
    CiudadDao ciudadDao = new CiudadDao(conn);
    PistaDao pistaDao = new PistaDao(conn);
    TorneoDao torneoDao = new TorneoDao(conn);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
            case "consultarById":
                consultarById(request, response);
            case "consultar":
                consultar(request, response);
            case "eliminar":
                eliminar(request, response);

        }
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd;
        request.setAttribute("ciudades", ciudadDao.consultarAll());
        request.setAttribute("pistas", pistaDao.consultarAll());
        request.setAttribute("torneos", torneoDao.consultar());

        rd = request.getRequestDispatcher("/registroLugarTorneo.jsp");
        rd.forward(request, response);
    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));

        RequestDispatcher rd;
        request.setAttribute("lugarTorneo", lugarTDao.consultarById(id));
        request.setAttribute("ciudades", ciudadDao.consultarAll());
        request.setAttribute("pistas", pistaDao.consultarAll());
        request.setAttribute("torneos", torneoDao.consultar());
        rd = request.getRequestDispatcher("/registroLugarTorneo.jsp");
        rd.forward(request, response);
    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher rd;
        request.setAttribute("lista", lugarTDao.consultar());
        rd = request.getRequestDispatcher("/mostrarlugaresTorneo.jsp");
        rd.forward(request, response);
    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        lugarTDao.eliminar(id);
        RequestDispatcher rd;
        request.setAttribute("lista", lugarTDao.consultar());
        rd = request.getRequestDispatcher("/mostrarlugaresTorneo.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String cod = request.getParameter("idlugar");
        Lugar_TorneoBean lugarT = new Lugar_TorneoBean(0);
        CiudadBean ciudad = new CiudadBean(Integer.parseInt(request.getParameter("ciudad")));
        lugarT.setCiudad(ciudad);
        PistaBean pista = new PistaBean(Integer.parseInt(request.getParameter("pista")));
        lugarT.setPista(pista);
        TorneoBean torneo = new TorneoBean(Integer.parseInt(request.getParameter("torneo")));
        lugarT.setTorneo(torneo);

        if (cod == null || cod.isEmpty()) {
            lugarTDao.insertar(lugarT);
            request.setAttribute("lista", lugarTDao.consultar());
        } else {
            int idlugar = Integer.parseInt(cod);
            lugarT.setIdlugar(idlugar);
            lugarTDao.editar(lugarT);
            request.setAttribute("lista", lugarTDao.consultar());
        }

        RequestDispatcher rd;
        rd = request.getRequestDispatcher("/mostrarlugaresTorneo.jsp");
        rd.forward(request, response);
    }

}
