package controller;

import conexion.Conexion;
import dao.JugadorDao;
import dao.Jugador_PartidoDao;
import dao.PartidoDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.JugadorBean;
import modelo.Jugador_PartidoBean;
import modelo.PartidoBean;

public class Jugador_PartidoServlet extends HttpServlet {

    String msg;
    boolean respuesta;
    RequestDispatcher rd;
    Conexion conn = new Conexion();
    Jugador_PartidoDao dao = new Jugador_PartidoDao(conn);
    Jugador_PartidoBean bean;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");

        switch (action) {
            case "registro":
                registro(request, response);
                break;
            case "insertar":
                insertar(request, response);
                break;
            case "actualizar":
                actualizar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            case "consultar":
                consultar(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            default:
                throw new AssertionError();
        }
    }

    protected void registro(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        JugadorDao jugadorDao = new JugadorDao(conn);
        List<JugadorBean> listajugadores = jugadorDao.consultar();

        PartidoDao partidoDao = new PartidoDao(conn);
        List<PartidoBean> listapartidos = partidoDao.consultar();

        request.setAttribute("listajugadores", listajugadores);
        request.setAttribute("listapartidos", listapartidos);
        rd = request.getRequestDispatcher("registrojugador_partido.jsp");
        rd.forward(request, response);
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idjugador = Integer.parseInt(request.getParameter("idjugador"));
        int idpartido = Integer.parseInt(request.getParameter("idpartido"));
        String resultado = request.getParameter("resultado");
        boolean ganador = Boolean.parseBoolean(request.getParameter("ganador"));

        JugadorBean jugadorBean = new JugadorBean(idjugador);
        PartidoBean partidoBean = new PartidoBean(idpartido);

        bean = new Jugador_PartidoBean(0);
        bean.setIdjugador(jugadorBean);
        bean.setIdpartido(partidoBean);
        bean.setResultado(resultado);
        bean.setGanador(ganador);

        respuesta = dao.insertar(bean);

        if (respuesta) {
            msg = "Exito registro guardado";
        } else {
            msg = "Error al guardar";
        }
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("jugador_partido?action=registro");
        rd.forward(request, response);

    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idjugadorP = Integer.parseInt(request.getParameter("idjugadorP"));
        int idjugador = Integer.parseInt(request.getParameter("idjugador"));
        int idpartido = Integer.parseInt(request.getParameter("idpartido"));
        String resultado = request.getParameter("resultado");
        boolean ganador = Boolean.parseBoolean(request.getParameter("ganador"));

        JugadorBean jugadorBean = new JugadorBean(idjugador);
        PartidoBean partidoBean = new PartidoBean(idpartido);

        bean = new Jugador_PartidoBean(idjugadorP);
        bean.setIdjugador(jugadorBean);
        bean.setIdpartido(partidoBean);
        bean.setResultado(resultado);
        bean.setGanador(ganador);

        respuesta = dao.actualizar(bean);

        if (respuesta) {
            msg = "Exito registro Actualizado";
        } else {
            msg = "Error al Actualizar";
        }

        List<Jugador_PartidoBean> lista = dao.consultarAll();
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarjugador_partido.jsp");
        rd.forward(request, response);

    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idjugadorP = Integer.parseInt(request.getParameter("idjugadorP"));
        dao.eliminar(idjugadorP);

        List<Jugador_PartidoBean> lista = dao.consultarAll();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarjugador_partido.jsp");
        rd.forward(request, response);

    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Jugador_PartidoBean> lista = dao.consultarAll();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarjugador_partido.jsp");
        rd.forward(request, response);

    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idjugadorP = Integer.parseInt(request.getParameter("idjugadorP"));
        List<Jugador_PartidoBean> lista = dao.consultarById(idjugadorP);

        JugadorDao jugadorDao = new JugadorDao(conn);
        List<JugadorBean> listajugadores = jugadorDao.consultar();

        PartidoDao partidoDao = new PartidoDao(conn);
        List<PartidoBean> listapartidos = partidoDao.consultar();

        request.setAttribute("listajugadores", listajugadores);
        request.setAttribute("listapartidos", listapartidos);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("actualizarjugador_partido.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
