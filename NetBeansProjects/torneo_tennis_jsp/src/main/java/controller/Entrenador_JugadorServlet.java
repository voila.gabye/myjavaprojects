
package controller;

import conexion.Conexion;
import dao.EntrenadorDao;
import dao.Entrenador_JugadorDao;
import dao.JugadorDao;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.EntrenadorBean;
import modelo.Entrenador_JugadorBean;
import modelo.JugadorBean;

public class Entrenador_JugadorServlet extends HttpServlet {

    Conexion conn = new Conexion();
    Entrenador_JugadorDao entrenadorjugadorDao = new Entrenador_JugadorDao(conn);
    Entrenador_JugadorBean entrenadorjugador;
    RequestDispatcher rd;
    boolean respuesta;
    String msg;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {

        String action = request.getParameter("action");

        switch (action) {
            case "registro":
                registro(request, response);
                break;
            case "insertar":
                insertar(request, response);
                break;
            case "actualizar":
                actualizar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            case "consultar":
                consultar(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            default:
                registro(request, response);
        }
    }

    protected void registro(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        JugadorDao jugadorDao = new JugadorDao(conn);
        List<JugadorBean> listajugador = jugadorDao.consultar();

        EntrenadorDao entrenadorDao = new EntrenadorDao(conn);
        List<EntrenadorBean> listaentrenador = entrenadorDao.consultarAll();

        request.setAttribute("listajugador", listajugador);
        request.setAttribute("listaentrenador", listaentrenador);
        rd = request.getRequestDispatcher("registroentrenadorjugador.jsp");
        rd.forward(request, response);
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {

        DateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        int idjugador = Integer.parseInt(request.getParameter("idjugador"));
        int identrenador = Integer.parseInt(request.getParameter("identrenador"));
        java.util.Date myDate = formato.parse(request.getParameter("fecha_inicio"));
        java.sql.Date sqlDate = new java.sql.Date(myDate.getTime());
        java.util.Date myDate2 = formato.parse(request.getParameter("fecha_fin"));
        java.sql.Date sqlDate2 = new java.sql.Date(myDate2.getTime());

        JugadorBean jugador = new JugadorBean(idjugador);
        EntrenadorBean entrenador = new EntrenadorBean(identrenador);

        entrenadorjugador = new Entrenador_JugadorBean(0);
        entrenadorjugador.setIdjugador(jugador);
        entrenadorjugador.setIdentrenador(entrenador);
        entrenadorjugador.setFecha_inicio(sqlDate);
        entrenadorjugador.setFecha_fin(sqlDate2);

        respuesta = entrenadorjugadorDao.insertar(entrenadorjugador);

        if (respuesta) {
            msg = "Registro Guardado";
        } else {
            msg = "Registro no Guardado";
        }
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("entrenador_jugador?action=registro");
        rd.forward(request, response);
    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {

        DateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        int identrenaJuga = Integer.parseInt(request.getParameter("identrenaJuga"));
        int idjugador = Integer.parseInt(request.getParameter("idjugador"));
        int identrenador = Integer.parseInt(request.getParameter("identrenador"));
        java.util.Date myDate = formato.parse(request.getParameter("fecha_inicio"));
        java.sql.Date sqlDate = new java.sql.Date(myDate.getTime());
        java.util.Date myDate2 = formato.parse(request.getParameter("fecha_fin"));
        java.sql.Date sqlDate2 = new java.sql.Date(myDate2.getTime());

        JugadorBean jugador = new JugadorBean(idjugador);
        EntrenadorBean entrenador = new EntrenadorBean(identrenador);

        entrenadorjugador = new Entrenador_JugadorBean(identrenaJuga);
        entrenadorjugador.setIdjugador(jugador);
        entrenadorjugador.setIdentrenador(entrenador);
        entrenadorjugador.setFecha_inicio(sqlDate);
        entrenadorjugador.setFecha_fin(sqlDate2);

        respuesta = entrenadorjugadorDao.actualizar(entrenadorjugador);
        List<Entrenador_JugadorBean> lista = entrenadorjugadorDao.consultar();

        if (respuesta) {
            msg = "Registro Actualizado";
        } else {
            msg = "Registro no Actualizado";
        }
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarentrenadorjugador.jsp");
        rd.forward(request, response);
    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int identrenaJuga = Integer.parseInt(request.getParameter("identrenaJuga"));
        entrenadorjugadorDao.eliminar(identrenaJuga);
        List<Entrenador_JugadorBean> lista = entrenadorjugadorDao.consultar();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarentrenadorjugador.jsp");
        rd.forward(request, response);
    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<Entrenador_JugadorBean> lista = entrenadorjugadorDao.consultar();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarentrenadorjugador.jsp");
        rd.forward(request, response);
    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int identrenaJuga = Integer.parseInt(request.getParameter("identrenaJuga"));
        List<Entrenador_JugadorBean> lista = entrenadorjugadorDao.consultarById(identrenaJuga);

        JugadorDao jugadorDao = new JugadorDao(conn);
        List<JugadorBean> listajugador = jugadorDao.consultar();

        EntrenadorDao entrenadorDao = new EntrenadorDao(conn);
        List<EntrenadorBean> listaentrenador = entrenadorDao.consultarAll();

        request.setAttribute("lista", lista);
        request.setAttribute("listajugador", listajugador);
        request.setAttribute("listaentrenador", listaentrenador);
        rd = request.getRequestDispatcher("actualizarentrenadorjugador.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(Entrenador_JugadorServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(Entrenador_JugadorServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
