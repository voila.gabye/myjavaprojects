package controller;

import conexion.Conexion;
import dao.FaseDao;
import dao.PremioDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.FaseBean;
import modelo.PremioBean;

public class PremioServlet extends HttpServlet {

    Conexion conn = new Conexion();
    PremioDao premioDao = new PremioDao(conn);
    FaseDao faseDao=new FaseDao(conn);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
            case "consultarById":
                consultarById(request, response);
            case "consultar":
                consultar(request, response);
            case "eliminar":
                eliminar(request, response);

        }
    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        PremioBean premio = premioDao.consultarById(id);
        RequestDispatcher rd;
        request.setAttribute("fases", faseDao.consultarAll());
        request.setAttribute("premio", premio);
        rd = request.getRequestDispatcher("/editarpremio.jsp");
        rd.forward(request, response);
    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<PremioBean> lista = premioDao.consultar();

        RequestDispatcher rd;
        request.setAttribute("premios", lista);
        rd = request.getRequestDispatcher("/mostrarpremios.jsp");
        rd.forward(request, response);
    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        premioDao.eliminar(id);
        List<PremioBean> lista = premioDao.consultar();
        RequestDispatcher rd;
        request.setAttribute("premios", lista);
        rd = request.getRequestDispatcher("/mostrarpremios.jsp");
        rd.forward(request, response);
    }
    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd;
        request.setAttribute("fases", faseDao.consultarAll());
        
        rd = request.getRequestDispatcher("/editarpremio.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String cod = request.getParameter("idpremio");
        double premio = Double.parseDouble(request.getParameter("premio"));
        int idfase = Integer.parseInt(request.getParameter("idfase"));
        PremioBean p = new PremioBean(0);
        FaseBean fase = new FaseBean(0);
        fase.setIdfase(idfase);
        p.setFase(fase);
        p.setPremio(premio);

        if (cod == null || cod.isEmpty()) {

            premioDao.insertar(p);
            request.setAttribute("premios", premioDao.consultar());
        } else {
            int idpremio = Integer.parseInt(cod);
            p.setIdpremio(idpremio);
            premioDao.editar(p);
            request.setAttribute("premios", premioDao.consultar());
        }

        RequestDispatcher rd;
        rd = request.getRequestDispatcher("/mostrarpremios.jsp");
        rd.forward(request, response);
    }

}