
package controller;

import conexion.Conexion;
import dao.ArbitroDao;
import dao.FaseDao;
import dao.Lugar_TorneoDao;
import dao.ModalidadDao;
import dao.PartidoDao;
import dao.PremioDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.ArbitroBean;
import modelo.FaseBean;
import modelo.Lugar_TorneoBean;
import modelo.ModalidadBean;
import modelo.PartidoBean;
import modelo.PistaBean;
import modelo.PremioBean;

public class PartidoServlet extends HttpServlet {

    Conexion conn=new Conexion();
    PartidoDao partidoDao=new PartidoDao(conn);
    Lugar_TorneoDao lugarTdao=new Lugar_TorneoDao(conn);
    ModalidadDao modalidadDao=new ModalidadDao(conn);
    ArbitroDao arbitroDao=new ArbitroDao(conn);
    FaseDao faseDao=new FaseDao(conn);
    PremioDao premioDao=new PremioDao(conn);
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
            case "consultarById":
                consultarById(request, response);
            case "consultar":
                consultar(request, response);
            case "eliminar":
                eliminar(request, response);

        }
        
    }
    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.setAttribute("lugares", lugarTdao.llenarSelect());
        request.setAttribute("modalidades", modalidadDao.consultarAll());
        request.setAttribute("arbitros", arbitroDao.consultarAll());
        request.setAttribute("fases", faseDao.consultarAll());
        request.setAttribute("premios", premioDao.consultar());
        
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("/registrarPartido.jsp");
        rd.forward(request, response);
    }
    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int id=Integer.parseInt(request.getParameter("id"));
        request.setAttribute("partido", partidoDao.consultarById(id));
        request.setAttribute("lugares", lugarTdao.llenarSelect());
        request.setAttribute("modalidades", modalidadDao.consultarAll());
        request.setAttribute("arbitros", arbitroDao.consultarAll());
        request.setAttribute("fases", faseDao.consultarAll());
        request.setAttribute("premios", premioDao.consultar());
        
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("/registrarPartido.jsp");
        rd.forward(request, response);
    }
    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("lista", partidoDao.consultar());
        
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("/mostrarpartidos.jsp");
        rd.forward(request, response);
    }
    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id=Integer.parseInt(request.getParameter("id"));
        partidoDao.eliminar(id);
        request.setAttribute("lista", partidoDao.consultar());
        
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("/mostrarpartidos.jsp");
        rd.forward(request, response);
    }
   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String cod=request.getParameter("idpartido");
        PartidoBean partido=new PartidoBean(0);
        Lugar_TorneoBean lugarT=new Lugar_TorneoBean(Integer.parseInt(request.getParameter("idlugar")));
        ModalidadBean modalidad=new ModalidadBean(Integer.parseInt(request.getParameter("idmodal")));
        partido.setLugarT(lugarT);
        partido.setModalidad(modalidad);
        ArbitroBean arbitro=new ArbitroBean(Integer.parseInt(request.getParameter("idarbi")));
        FaseBean fase=new FaseBean(Integer.parseInt(request.getParameter("idfase")));
        partido.setArbitro(arbitro);
        partido.setFase(fase);
        PremioBean premio=new PremioBean(Integer.parseInt(request.getParameter("idpremio")));
        partido.setPremio(premio);
        
        if (cod==null || cod.isEmpty()) {
            partidoDao.insertar(partido);
            request.setAttribute("lista", partidoDao.consultar());
        }else{
            int idpartido=Integer.parseInt(cod);
            partido.setIdpartido(idpartido);
            partidoDao.editar(partido);
            request.setAttribute("lista", partidoDao.consultar());
        }
        
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("/mostrarpartidos.jsp");
        rd.forward(request, response);
    }

    

}