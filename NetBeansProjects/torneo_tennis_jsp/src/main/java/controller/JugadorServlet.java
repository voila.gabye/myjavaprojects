
package controller;

import conexion.Conexion;
import dao.EquipoDao;
import dao.GeneroDao;
import dao.JugadorDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.EquipoBean;
import modelo.GeneroBean;
import modelo.JugadorBean;

public class JugadorServlet extends HttpServlet {

    String msg;
    boolean respuesta;
    RequestDispatcher rd;
    Conexion conn = new Conexion();
    JugadorDao dao = new JugadorDao(conn);
    JugadorBean bean;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");

        switch (action) {
            case "registro":
                registro(request, response);
                break;
            case "insertar":
                insertar(request, response);
                break;
            case "actualizar":
                actualizar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            case "consultar":
                consultar(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            default:
                registro(request, response);
        }
    }

    protected void registro(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        GeneroDao generoDao = new GeneroDao(conn);
        List<GeneroBean> lista = generoDao.consultar();

        EquipoDao equipoDao = new EquipoDao(conn);
        List<EquipoBean> listaequipos = equipoDao.consultar();

        request.setAttribute("lista", lista);
        request.setAttribute("listaequipos", listaequipos);
        rd = request.getRequestDispatcher("registrojugador.jsp");
        rd.forward(request, response);

    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String nombre = request.getParameter("nombre");
        int idgenero = Integer.parseInt(request.getParameter("idgenero"));
        int idequipo = Integer.parseInt(request.getParameter("idequipo"));

        GeneroBean generoBean = new GeneroBean(idgenero);
        EquipoBean equipoBean = new EquipoBean(idequipo);

        bean = new JugadorBean(0);
        bean.setNombre(nombre);
        bean.setIdgenero(generoBean);
        bean.setIdequipo(equipoBean);

        respuesta = dao.insertar(bean);

        if (respuesta) {
            msg = "Jugador registrado";
        } else {
            msg = "Error al registrar";
        }
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("jugador?action=registro");
        rd.forward(request, response);
    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idjugador = Integer.parseInt(request.getParameter("idjugador"));
        String nombre = request.getParameter("nombre");
        int idgenero = Integer.parseInt(request.getParameter("idgenero"));
        int idequipo = Integer.parseInt(request.getParameter("idequipo"));

        GeneroBean generoBean = new GeneroBean(idgenero);
        EquipoBean equipoBean = new EquipoBean(idequipo);

        bean = new JugadorBean(idjugador);
        bean.setNombre(nombre);
        bean.setIdgenero(generoBean);
        bean.setIdequipo(equipoBean);

        respuesta = dao.actualizar(bean);

        if (respuesta) {
            msg = "Jugador Actualizado";
        } else {
            msg = "Error al Actualizar";
        }

        List<JugadorBean> lista = dao.consultar();

        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarjugador.jsp");
        rd.forward(request, response);

    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idjugador = Integer.parseInt(request.getParameter("idjugador"));
        dao.eliminar(idjugador);

        List<JugadorBean> lista = dao.consultar();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarjugador.jsp");
        rd.forward(request, response);

    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<JugadorBean> lista = dao.consultar();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("mostrarjugador.jsp");
        rd.forward(request, response);
    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idjugador = Integer.parseInt(request.getParameter("idjugador"));
        List<JugadorBean> lista = dao.consultarById(idjugador);

        GeneroDao generoDao = new GeneroDao(conn);
        List<GeneroBean> listagenero = generoDao.consultar();

        EquipoDao equipoDao = new EquipoDao(conn);
        List<EquipoBean> listaequipos = equipoDao.consultar();

        request.setAttribute("lista", lista);
        request.setAttribute("listaequipos", listaequipos);
        request.setAttribute("listagenero", listagenero);
        rd = request.getRequestDispatcher("actualizarjugador.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}