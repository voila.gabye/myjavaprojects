package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.JugadorBean;
import modelo.Jugador_PartidoBean;
import modelo.Lugar_TorneoBean;
import modelo.PartidoBean;
import modelo.PistaBean;

public class Jugador_PartidoDao {

    ResultSet rs;
    PreparedStatement ps;
    String sql;
    Conexion conn;
    Jugador_PartidoBean bean;

    public Jugador_PartidoDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(Jugador_PartidoBean bean) {
        sql = "insert into jugador_partido values(?,?,?,?,?)";

        JugadorBean jugadorBean = bean.getIdjugador();
        PartidoBean partidoBean = bean.getIdpartido();

        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, bean.getIdjugadorP());
            ps.setInt(2, jugadorBean.getIdjugador());
            ps.setInt(3, partidoBean.getIdpartido());
            ps.setString(4, bean.getResultado());
            ps.setBoolean(5, bean.getGanador());

            ps.executeUpdate();

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean actualizar(Jugador_PartidoBean bean) {
        sql = "update jugador_partido set idjugador=?,idpartido=?,resultado=?,ganador=? where idjugadorP=?";

        JugadorBean jugadorBean = bean.getIdjugador();
        PartidoBean partidoBean = bean.getIdpartido();

        try {
            ps = conn.conectar().prepareStatement(sql);

            ps.setInt(1, jugadorBean.getIdjugador());
            ps.setInt(2, partidoBean.getIdpartido());
            ps.setString(3, bean.getResultado());
            ps.setBoolean(4, bean.getGanador());
            ps.setInt(5, bean.getIdjugadorP());

            ps.executeUpdate();

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean eliminar(int idjugadorP) {
        sql = "delete from jugador_partido where idjugadorP=?";

        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idjugadorP);

            ps.executeUpdate();

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<Jugador_PartidoBean> consultarById(int idjugadorP) {
        sql = "select * from jugador_partido where idjugadorP=?";

        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idjugadorP);
            rs = ps.executeQuery();

            List<Jugador_PartidoBean> lista = new LinkedList<>();
            PartidoBean partidoBean;
            JugadorBean jugadorBean;

            while (rs.next()) {
                partidoBean = new PartidoBean(rs.getInt("idpartido"));
                jugadorBean = new JugadorBean(rs.getInt("idjugador"));

                bean = new Jugador_PartidoBean(rs.getInt("idjugadorP"));
                bean.setIdjugador(jugadorBean);
                bean.setIdpartido(partidoBean);
                bean.setResultado(rs.getString("resultado"));
                bean.setGanador(rs.getBoolean("ganador"));

                lista.add(bean);
            }
            return lista;

        } catch (Exception e) {
            return null;
        }
    }

    public List<Jugador_PartidoBean> consultarAll() {
        sql = "select idjugadorP,nombre,pista,resultado,ganador from jugador_partido "
                + " inner join jugador on jugador.idjugador=jugador_partido.idjugador"
                + " inner join partido on partido.idpartido=jugador_partido.idpartido inner join lugar_torneo on lugar_torneo.idlugar=partido.idlugar"
                + " inner join pista on lugar_torneo.idpista=pista.idpista;";

        try {
            ps = conn.conectar().prepareStatement(sql);
            rs = ps.executeQuery();

            List<Jugador_PartidoBean> lista = new LinkedList<>();
            PartidoBean partidoBean;
            JugadorBean jugadorBean;
            PistaBean pistaBean;
            Lugar_TorneoBean lugar_TorneoBean;

            while (rs.next()) {
                partidoBean = new PartidoBean(0);
                
                jugadorBean = new JugadorBean(0);
                jugadorBean.setNombre(rs.getString("nombre"));
                
                pistaBean=new PistaBean(0);
                pistaBean.setPista(rs.getString("pista"));
                
                
                lugar_TorneoBean=new Lugar_TorneoBean(0);
                lugar_TorneoBean.setPista(pistaBean);
                
                partidoBean=new PartidoBean(0);
                partidoBean.setLugarT(lugar_TorneoBean);
                
                

                bean = new Jugador_PartidoBean(rs.getInt("idjugadorP"));
                bean.setIdjugador(jugadorBean);
                bean.setIdpartido(partidoBean);
                bean.setResultado(rs.getString("resultado"));
                bean.setGanador(rs.getBoolean("ganador"));

                lista.add(bean);
            }
            return lista;

        } catch (Exception e) {
            return null;
        }
    }

}
