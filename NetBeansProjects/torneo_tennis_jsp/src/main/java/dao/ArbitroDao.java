package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.ArbitroBean;

public class ArbitroDao {

    Conexion conn = new Conexion();

    public ArbitroDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(ArbitroBean arb) {
        String sql = "insert into arbitro values(?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, arb.getIdarbi());
            ps.setString(2, arb.getNombre());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean actualizar(ArbitroBean arb) {
        String sql = "update arbitro set nombre=? where idarbi=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, arb.getNombre());
            ps.setInt(2, arb.getIdarbi());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<ArbitroBean> consultarAll() {
        String sql = "select * from arbitro";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<ArbitroBean> lista = new LinkedList<>();
            ArbitroBean arb;
            while (rs.next()) {
                arb = new ArbitroBean(rs.getInt("idarbi"));
                arb.setNombre(rs.getString("nombre"));
                lista.add(arb);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public List<ArbitroBean> consultarById(int idarbi) {
        String sql = "select * from arbitro where idarbi=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idarbi);
            ResultSet rs = ps.executeQuery();
            List<ArbitroBean> lista = new LinkedList<>();
            ArbitroBean arb;
            while (rs.next()) {
                arb = new ArbitroBean(rs.getInt("idarbi"));
                arb.setNombre(rs.getString("nombre"));
                lista.add(arb);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean eliminar(int idarbi) {
        String sql = "delete from arbitro where idarbi=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idarbi);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public int getNumAarbitros(){
    String sql="select count(nombre) as arbitros from arbitro;";
    int conteo=0;
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);
            ResultSet rs=ps.executeQuery();
            while (rs.next()) {
                conteo=rs.getInt("arbitros");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conteo;
    }
}
