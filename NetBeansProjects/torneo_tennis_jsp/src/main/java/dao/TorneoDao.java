package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.PaisBean;
import modelo.TorneoBean;


public class TorneoDao {
    Conexion conn;

    public TorneoDao(Conexion conn) {
        this.conn=conn;
    }
    
    public void insertar(TorneoBean torneo){
    String sql="insert into torneo values(?,?,?,?,?)";
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);
            ps.setInt(1, torneo.getIdtorneo());
            ps.setString(2, torneo.getNombre());
            ps.setString(3, torneo.getFecha_inicio());
            ps.setString(4, torneo.getFecha_fin());
            PaisBean pais=torneo.getPais();
            ps.setInt(5, pais.getIdpais());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void editar(TorneoBean torneo){
    String sql="update torneo set nombre=?,fecha_inicio=?,fecha_fin=?,idpais=? where idtorneo=?";
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);            
            ps.setString(1, torneo.getNombre());
            ps.setString(2, torneo.getFecha_inicio());
            ps.setString(3, torneo.getFecha_fin());
            PaisBean pais=torneo.getPais();
            ps.setInt(4, pais.getIdpais());
            ps.setInt(5, torneo.getIdtorneo());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void eliminar(int id){
    String sql="delete from torneo where idtorneo=?";
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public List<TorneoBean> consultar(){
    String sql="select t.idtorneo,t.nombre,t.fecha_inicio,t.fecha_fin,p.pais from torneo as t join pais as p on p.idpais=t.idpais;";
    List<TorneoBean> lista=new LinkedList<>();
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);
            ResultSet rs=ps.executeQuery();
            while (rs.next()) {
                TorneoBean torneo=new TorneoBean(rs.getInt("idtorneo"));
                torneo.setNombre(rs.getString("nombre"));
                torneo.setFecha_inicio(rs.getString("fecha_inicio"));
                torneo.setFecha_fin(rs.getString("fecha_fin"));
                PaisBean pais=new PaisBean(0);
                pais.setPais(rs.getString("pais"));
                torneo.setPais(pais);
                lista.add(torneo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    public TorneoBean consultarById(int id){
    String sql="select * from torneo where idtorneo=?";
    TorneoBean torneo=new TorneoBean(0);
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs=ps.executeQuery();
            while (rs.next()) {
                torneo.setIdtorneo(rs.getInt("idtorneo"));
                torneo.setNombre(rs.getString("nombre"));
                torneo.setFecha_inicio(rs.getString("fecha_inicio"));
                torneo.setFecha_fin(rs.getString("fecha_fin"));
                PaisBean pais=new PaisBean(0);
                pais.setIdpais(rs.getInt("idpais"));
                torneo.setPais(pais);                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return torneo;
    }
}
