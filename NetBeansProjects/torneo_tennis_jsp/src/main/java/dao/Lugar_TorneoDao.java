package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.CiudadBean;
import modelo.Lugar_TorneoBean;
import modelo.PistaBean;
import modelo.TorneoBean;

public class Lugar_TorneoDao {
    Conexion conn;

    public Lugar_TorneoDao(Conexion conn) {
    this.conn=conn;
    }
    
    public void insertar(Lugar_TorneoBean lugarT){
    String sql="insert into lugar_torneo values(?,?,?,?)";
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);
            ps.setInt(1, lugarT.getIdlugar());
            CiudadBean ciudad=lugarT.getCiudad();
            ps.setInt(2, ciudad.getIdciudad());
            PistaBean pista=lugarT.getPista();
            ps.setInt(3, pista.getIdpista());
            TorneoBean torneo=lugarT.getTorneo();
            ps.setInt(4, torneo.getIdtorneo());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void editar(Lugar_TorneoBean lugarT){
    String sql="update lugar_torneo set idciudad=?,idpista=?,idtorneo=? where idlugar=?";
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);            
            CiudadBean ciudad=lugarT.getCiudad();
            ps.setInt(1, ciudad.getIdciudad());
            PistaBean pista=lugarT.getPista();
            ps.setInt(2, pista.getIdpista());
            TorneoBean torneo=lugarT.getTorneo();
            ps.setInt(3, torneo.getIdtorneo());
            ps.setInt(4, lugarT.getIdlugar());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void eliminar(int id){
    String sql="delete from lugar_torneo where idlugar=?";
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public Lugar_TorneoBean consultarById(int id){
    String sql="select * from lugar_torneo where idlugar=?";
    Lugar_TorneoBean lugarT=new Lugar_TorneoBean(0);
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs=ps.executeQuery();
            while (rs.next()) {
                lugarT.setIdlugar(rs.getInt("idlugar"));
                CiudadBean ciudad=new CiudadBean(0);
                ciudad.setIdciudad(rs.getInt("idciudad"));
                lugarT.setCiudad(ciudad);
                PistaBean pista=new PistaBean(id);
                pista.setIdpista(rs.getInt("idpista"));
                lugarT.setPista(pista);
                TorneoBean torneo=new TorneoBean(0);
                torneo.setIdtorneo(rs.getInt("idtorneo"));
                lugarT.setTorneo(torneo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lugarT;
    
    }
    
    public List<Lugar_TorneoBean> consultar(){
    String sql="select lt.idlugar, c.ciudad,p.pista,t.nombre from lugar_torneo as lt join ciudad as c on c.idciudad=lt.idciudad "
            + "join pista as p on p.idpista=lt.idpista join torneo as t on t.idtorneo=lt.idtorneo;";
    List<Lugar_TorneoBean> lista=new LinkedList<>();
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);
            ResultSet rs=ps.executeQuery();
            while (rs.next()) {
                Lugar_TorneoBean lugarT=new Lugar_TorneoBean(rs.getInt("idlugar"));
                CiudadBean ciudad=new CiudadBean(0);
                ciudad.setCiudad(rs.getString("ciudad"));
                PistaBean pista=new PistaBean(0);
                pista.setPista(rs.getString("pista"));
                TorneoBean torneo=new TorneoBean(0);
                torneo.setNombre(rs.getString("nombre"));
                lugarT.setCiudad(ciudad);
                lugarT.setPista(pista);
                lugarT.setTorneo(torneo);
                lista.add(lugarT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public List<Lugar_TorneoBean> llenarSelect(){
    String sql="select lt.idlugar,p.pista from lugar_torneo as lt join pista as p on p.idpista=lt.idpista;";
    List<Lugar_TorneoBean> lista=new LinkedList<>();
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);
            ResultSet rs=ps.executeQuery();
            while (rs.next()) {
                Lugar_TorneoBean lugarT=new Lugar_TorneoBean(rs.getInt("idlugar"));
                PistaBean pista=new PistaBean(0);
                pista.setPista(rs.getString("pista"));
                lugarT.setPista(pista);
                lista.add(lugarT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }  
}
