package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.FaseBean;
import modelo.PremioBean;

public class PremioDao {

    Conexion conn;

    public PremioDao(Conexion conn) {
        this.conn = conn;
    }

    public void insertar(PremioBean premio) {
        String sql = "insert into premio values(?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, premio.getIdpremio());
            ps.setDouble(2, premio.getPremio());
            FaseBean fase = premio.getFase();
            ps.setInt(3, fase.getIdfase());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void editar(PremioBean premio) {
        String sql = "update premio set premio=?, idfase=? where idpremio=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setDouble(1, premio.getPremio());
            FaseBean fase = premio.getFase();
            ps.setInt(2, fase.getIdfase());
            ps.setInt(3, premio.getIdpremio());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<PremioBean> consultar() {
        String sql = "select p.idpremio, p.premio, f.fase from premio as p join fase as f on p.idfase=f.idfase;";
        List<PremioBean> lista = new LinkedList<>();
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                PremioBean premio = new PremioBean(rs.getInt("idpremio"));
                premio.setPremio(rs.getDouble("premio"));
                FaseBean fase = new FaseBean(0);
                fase.setFase(rs.getString("fase"));
                premio.setFase(fase);
                lista.add(premio);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public PremioBean consultarById(int id) {
        String sql = "select * from premio where idpremio=?";
        PremioBean premio = new PremioBean(0);
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                premio.setIdpremio(rs.getInt("idpremio"));
                FaseBean fase = new FaseBean(0);
                fase.setIdfase(rs.getInt("idfase"));
                premio.setPremio(rs.getDouble("premio"));
                premio.setFase(fase);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return premio;
    }

    public void eliminar(int id) {
        String sql = "delete from premio where idpremio=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
