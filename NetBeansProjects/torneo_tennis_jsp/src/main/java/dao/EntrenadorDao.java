package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.EntrenadorBean;

public class EntrenadorDao {

    Conexion conn = new Conexion();

    public EntrenadorDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(EntrenadorBean enb) {
        String sql = "INSERT INTO entrenador VALUES(?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, enb.getIdentre());
            ps.setString(2, enb.getNombre());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean actualizar(EntrenadorBean enb) {
        String sql = "UPDATE entrenador SET nombre=? WHERE identre=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, enb.getNombre());
            ps.setInt(2, enb.getIdentre());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean eliminar(int identre) {
        String sql = "DELETE FROM entrenador WHERE identre=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, identre);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<EntrenadorBean> consultarAll() {
        String sql = "SELECT * FROM entrenador";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<EntrenadorBean> lista = new LinkedList<>();
            EntrenadorBean enb;
            while (rs.next()) {
                enb = new EntrenadorBean(rs.getInt("identre"));
                enb.setNombre(rs.getString("nombre"));
                lista.add(enb);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public List<EntrenadorBean> consultarById(int identre) {
        String sql = "SELECT * FROM entrenador WHERE identre=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, identre);
            ResultSet rs = ps.executeQuery();
            List<EntrenadorBean> lista = new LinkedList<>();
            EntrenadorBean enb;
            while (rs.next()) {
                enb = new EntrenadorBean(rs.getInt("identre"));
                enb.setNombre(rs.getString("nombre"));
                lista.add(enb);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
    
    public int getNumEntrenadores(){
    String sql="select count(nombre) as entrenadores from entrenador;";
    int conteo=0;
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);
            ResultSet rs=ps.executeQuery();
            while (rs.next()) {
                conteo=rs.getInt("entrenadores");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conteo;
    }
}
