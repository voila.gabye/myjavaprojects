package dao;

import conexion.Conexion;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.EntrenadorBean;
import modelo.Entrenador_JugadorBean;
import modelo.JugadorBean;

public class Entrenador_JugadorDao {
    
    Conexion conn;

    public Entrenador_JugadorDao(Conexion conn) {
        this.conn = conn;
    }
    
    public boolean insertar(Entrenador_JugadorBean entrenadorjugador){
    String sql = "insert into entrenador_jugador values (?,?,?,?,?)";
        JugadorBean jugador = entrenadorjugador.getIdjugador();
        EntrenadorBean entrenador = entrenadorjugador.getIdentrenador();
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, entrenadorjugador.getIdentrenaJuga());
            ps.setInt(2, jugador.getIdjugador());
            ps.setInt(3, entrenador.getIdentre());
            ps.setDate(4, (Date) entrenadorjugador.getFecha_inicio());
            ps.setDate(5, (Date) entrenadorjugador.getFecha_fin());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean actualizar(Entrenador_JugadorBean entrenadorjugador){
    String sql = "update entrenador_jugador set idjugador=?, identrenador=?, fecha_inicio=?, fecha_fin=? where identrenaJuga=?";
        JugadorBean jugador = entrenadorjugador.getIdjugador();
        EntrenadorBean entrenador = entrenadorjugador.getIdentrenador();
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, jugador.getIdjugador());
            ps.setInt(2, entrenador.getIdentre());
            ps.setDate(3, (Date) entrenadorjugador.getFecha_inicio());
            ps.setDate(4, (Date) entrenadorjugador.getFecha_fin());
            ps.setInt(5, entrenadorjugador.getIdentrenaJuga());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean eliminar(int identrenaJuga){
    String sql = "delete from entrenador_jugador where identrenaJuga=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, identrenaJuga);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public List<Entrenador_JugadorBean> consultar(){
    String sql = "select en.identrenaJuga, j.nombre, e.nombre,en.fecha_inicio, en.fecha_fin from entrenador_jugador as en join jugador as j on en.idjugador=j.idjugador join entrenador as e on en.identrenador=e.identre;";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Entrenador_JugadorBean> lista = new LinkedList<>();
            
            JugadorBean jugador;
            EntrenadorBean entrenador;
            Entrenador_JugadorBean entrenadorjugador;
            
            while (rs.next()) {                
                jugador = new JugadorBean(0);
                jugador.setNombre(rs.getString("j.nombre"));
                
                entrenador = new EntrenadorBean(0);
                entrenador.setNombre(rs.getString("e.nombre"));
                
                entrenadorjugador = new Entrenador_JugadorBean(rs.getInt("identrenaJuga"));
                entrenadorjugador.setIdjugador(jugador);
                entrenadorjugador.setIdentrenador(entrenador);
                entrenadorjugador.setFecha_inicio(rs.getDate("fecha_inicio"));
                entrenadorjugador.setFecha_fin(rs.getDate("fecha_fin"));
                
                lista.add(entrenadorjugador);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<Entrenador_JugadorBean> consultarById(int identrenaJuga){
    String sql = "select * from entrenador_jugador where identrenaJuga=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, identrenaJuga);
            ResultSet rs = ps.executeQuery();
            List<Entrenador_JugadorBean> lista = new LinkedList<>();
            
            JugadorBean jugador;
            EntrenadorBean entrenador;
            Entrenador_JugadorBean entrenadorjugador;
            
            while (rs.next()) {                
                jugador = new JugadorBean(rs.getInt("idjugador"));
                
                
                entrenador = new EntrenadorBean(rs.getInt("identrenador"));
                
                
                entrenadorjugador = new Entrenador_JugadorBean(rs.getInt("identrenaJuga"));
                entrenadorjugador.setIdjugador(jugador);
                entrenadorjugador.setIdentrenador(entrenador);
                entrenadorjugador.setFecha_inicio(rs.getDate("fecha_inicio"));
                entrenadorjugador.setFecha_fin(rs.getDate("fecha_fin"));
                
                lista.add(entrenadorjugador);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
}