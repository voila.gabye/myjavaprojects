package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.EquipoBean;
import modelo.PaisBean;

public class EquipoDao {
    
    Conexion conn;

    public EquipoDao(Conexion conn) {
        this.conn = conn;
    }
    
    public void insertar(EquipoBean equipo){
    String sql = "insert into equipo values(?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, equipo.getIdequipo());
            PaisBean pais = equipo.getIdpais();
            ps.setInt(2, pais.getIdpais());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void editar(EquipoBean equipo){
    String sql ="update equipo set idpais=? where idequipo=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            PaisBean pais = equipo.getIdpais();
            ps.setInt(1, pais.getIdpais());
            ps.setInt(2, equipo.getIdequipo());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public List<EquipoBean> consultar(){
    String sql = "select e.idequipo, p.pais from equipo as e join pais as p on e.idpais=p.idpais;";
    List<EquipoBean> lista = new LinkedList<>();
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
                EquipoBean equipo = new EquipoBean(rs.getInt("idequipo"));
                PaisBean pais = new PaisBean(0);
                pais.setPais(rs.getString("pais"));
                equipo.setIdpais(pais);
                lista.add(equipo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public EquipoBean consultarById(int id){
    String sql = "select * from equipo where idequipo=?";
    EquipoBean equipo = new EquipoBean(0);
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
                equipo.setIdequipo(rs.getInt("idequipo"));
                PaisBean pais = new PaisBean(0);
                pais.setIdpais(rs.getInt("idpais"));
                equipo.setIdpais(pais);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return equipo;
    }
    
    public void eliminar(int id){
    String sql = "delete from equipo where idequipo=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public int getNumEquipos(){
    String sql="select count(idequipo) as equipos from equipo;";
    int conteo=0;
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);
            ResultSet rs=ps.executeQuery();
            while (rs.next()) {
                conteo=rs.getInt("equipos");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conteo;
    }
}
