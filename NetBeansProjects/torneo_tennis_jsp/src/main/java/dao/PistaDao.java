
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.PistaBean;


public class PistaDao {

    Conexion conn;

    public PistaDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(PistaBean pista) {
        String sql = "insert into pista values(?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, pista.getIdpista());
            ps.setString(2, pista.getPista());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean actualizar(PistaBean pista) {
        String sql = "update pista set pista=? where idpista=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, pista.getPista());
            ps.setInt(2, pista.getIdpista());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public PistaBean consultarById(int id) {
        String sql = "select * from pista where idpista=?";

        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            PistaBean pista=new PistaBean(0);
            while (rs.next()) {
                pista.setIdpista(rs.getInt("idpista"));
                pista.setPista(rs.getString("pista"));
                
            }
            return pista;
        } catch (Exception e) {
            return null;
        }

    }
    
    public boolean eliminar(int id){
    String sql="delete from pista where idpista=?";
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public List<PistaBean> consultarAll(){
    String sql="select * from pista";
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);
            ResultSet rs=ps.executeQuery();
            List<PistaBean> lista=new LinkedList<>();
            
            while (rs.next()) {
                PistaBean pista=new PistaBean(rs.getInt("idpista"));
                pista.setPista(rs.getString("pista"));
                lista.add(pista);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
    
    public int getNumPistas(){
    String sql="select count(pista) as pistas from pista;";
    int conteo=0;
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);
            ResultSet rs=ps.executeQuery();
            while (rs.next()) {
                conteo=rs.getInt("pistas");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conteo;
    }

}
