package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.FaseBean;

public class FaseDao {

    Conexion conn;

    public FaseDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(FaseBean faseb) {
        String sql = "insert into fase values (?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, faseb.getIdfase());
            ps.setString(2, faseb.getFase());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

   public boolean actualizar(FaseBean faseb) {
         String sql = "update fase set fase=? where idfase=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, faseb.getFase());
            ps.setInt(2, faseb.getIdfase());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<FaseBean> consultarAll() {
        String sql = "select * from fase";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<FaseBean> lista = new LinkedList<>();
            FaseBean faseb;
            while (rs.next()) {
                faseb = new FaseBean(rs.getInt("idfase"));
                faseb.setFase(rs.getString("fase"));
                lista.add(faseb);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public List<FaseBean> consultarById(int idfase) {
        String sql = "select * from fase where idfase=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idfase);
            ResultSet rs = ps.executeQuery();
            List<FaseBean> lista = new LinkedList<>();
            FaseBean faseb;
            while (rs.next()) {
                faseb = new FaseBean(rs.getInt("idfase"));
                faseb.setFase(rs.getString("fase"));
                lista.add(faseb);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean eliminar(int idfase) {
        String sql = "delete from fase where idfase=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idfase);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
