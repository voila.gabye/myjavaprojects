package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.ModalidadBean;

public class ModalidadDao {

    Conexion conn;

    public ModalidadDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(ModalidadBean modab) {
        String sql = "insert into modalidad values (?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, modab.getIdmodal());
            ps.setString(2, modab.getNombre());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean actualizar(ModalidadBean modab) {
        String sql = "update modalidad set nombre=? where idmodal=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, modab.getNombre());
            ps.setInt(2, modab.getIdmodal());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<ModalidadBean> consultarAll() {
        String sql = "select * from modalidad";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<ModalidadBean> lista = new LinkedList<>();
            ModalidadBean modab;
            while (rs.next()) {
                modab = new ModalidadBean(rs.getInt("idmodal"));
                modab.setNombre(rs.getString("nombre"));
                lista.add(modab);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public List<ModalidadBean> consultarById(int idmodal) {
        String sql = "select * from modalidad where idmodal=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idmodal);
            ResultSet rs = ps.executeQuery();
            List<ModalidadBean> lista = new LinkedList<>();
            ModalidadBean modab;
            while (rs.next()) {
                modab = new ModalidadBean(rs.getInt("idmodal"));
                modab.setNombre(rs.getString("nombre"));
                lista.add(modab);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean eliminar(int idmodal) {
        String sql = "delete from modalidad where idmodal=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idmodal);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
