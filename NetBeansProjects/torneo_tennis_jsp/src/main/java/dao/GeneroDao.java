/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.GeneroBean;


public class GeneroDao {
    ResultSet rs;
    PreparedStatement ps;
    String sql;
    Conexion conn;
    GeneroBean bean;

    public GeneroDao(Conexion conn) {
        this.conn = conn;
    }
    
    public boolean insertar(GeneroBean bean) {
        sql = "insert into genero values(?,?)";

        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, bean.getIdgenero());
            ps.setString(2, bean.getGenero());

            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean actualizar(GeneroBean bean) {
        sql = "update genero set genero=? where idgenero=?";

        try {
            ps = conn.conectar().prepareStatement(sql);

            ps.setString(1, bean.getGenero());
            ps.setInt(2, bean.getIdgenero());

            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean eliminar(int idgenero) {
        sql = "delete from genero where idgenero=?";

        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idgenero);

            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<GeneroBean> consultar() {
        sql = "select * from genero";
        try {
            ps = conn.conectar().prepareStatement(sql);
            rs = ps.executeQuery();

            List<GeneroBean> lista = new LinkedList<>();

            while (rs.next()) {
                bean = new GeneroBean(rs.getInt("idgenero"));
                bean.setGenero(rs.getString("genero"));

                lista.add(bean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public List<GeneroBean> consultarById(int idgenero) {
        sql = "select * from genero where idgenero=?";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idgenero);
            rs = ps.executeQuery();

            List<GeneroBean> lista = new LinkedList<>();

            while (rs.next()) {
                bean = new GeneroBean(rs.getInt("idgenero"));
                bean.setGenero(rs.getString("genero"));

                lista.add(bean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
    
}
