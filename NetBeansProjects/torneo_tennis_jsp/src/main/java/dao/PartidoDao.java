package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.ArbitroBean;
import modelo.FaseBean;
import modelo.Lugar_TorneoBean;
import modelo.ModalidadBean;
import modelo.PartidoBean;
import modelo.PistaBean;
import modelo.PremioBean;


public class PartidoDao {

    Conexion conn;

    public PartidoDao(Conexion conn) {
        this.conn = conn;
    }

    public void insertar(PartidoBean partido) {
        String sql = "insert into partido values (?,?,?,?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, partido.getIdpartido());
            Lugar_TorneoBean lugarT = partido.getLugarT();
            ps.setInt(2, lugarT.getIdlugar());
            ModalidadBean modalidad = partido.getModalidad();
            ps.setInt(3, modalidad.getIdmodal());
            ArbitroBean arbitro = partido.getArbitro();
            ps.setInt(4, arbitro.getIdarbi());
            FaseBean fase = partido.getFase();
            ps.setInt(5, fase.getIdfase());
            PremioBean premio = partido.getPremio();
            ps.setInt(6, premio.getIdpremio());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void editar(PartidoBean partido) {
        String sql = "update partido set idlugar=?,idmodal=?,idarbi=?,idfase=?,idpremio=? where idpartido=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            Lugar_TorneoBean lugarT = partido.getLugarT();
            ps.setInt(1, lugarT.getIdlugar());
            ModalidadBean modalidad = partido.getModalidad();
            ps.setInt(2, modalidad.getIdmodal());
            ArbitroBean arbitro = partido.getArbitro();
            ps.setInt(3, arbitro.getIdarbi());
            FaseBean fase = partido.getFase();
            ps.setInt(4, fase.getIdfase());
            PremioBean premio = partido.getPremio();
            ps.setInt(5, premio.getIdpremio());
            ps.setInt(6, partido.getIdpartido());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void eliminar(int id) {
        String sql = "delete from partido where idpartido=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   public List<PartidoBean> consultar() {
        String sql = "select p.idpartido,pi.pista,m.nombre,a.nombre as arbitro,f.fase,pre.premio from partido as p join lugar_torneo as "
                + "lt on lt.idlugar=p.idlugar join modalidad as m on m.idmodal=p.idmodal join arbitro as a \n"
                + "on a.idarbi=p.idarbi join fase as f on f.idfase=p.idfase join premio as pre on pre.idpremio=p.idpremio "
                + "join pista as pi on pi.idpista=lt.idpista;";
        List<PartidoBean> lista=new LinkedList<>();
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);
            ResultSet rs=ps.executeQuery();
            while (rs.next()) {
                PartidoBean partido=new PartidoBean(rs.getInt("idpartido"));
                PistaBean pista=new PistaBean(0);
                pista.setPista(rs.getString("pista"));
                Lugar_TorneoBean lugarT=new Lugar_TorneoBean(0);
                ModalidadBean modalidad=new ModalidadBean(0);
                modalidad.setNombre(rs.getString("nombre"));
                partido.setModalidad(modalidad);
                lugarT.setPista(pista);
                partido.setLugarT(lugarT);
                ArbitroBean arbitro=new ArbitroBean(0);
                arbitro.setNombre(rs.getString("arbitro"));
                partido.setArbitro(arbitro);
                FaseBean fase=new FaseBean(0);
                fase.setFase(rs.getString("fase"));
                partido.setFase(fase);
                PremioBean premio=new PremioBean(0);
                premio.setPremio(rs.getDouble("premio"));
                partido.setPremio(premio);
                lista.add(partido);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public PartidoBean consultarById(int id){
    String sql="select * from partido where idpartido=?";
    PartidoBean partido=new PartidoBean(0);
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs=ps.executeQuery();
            while (rs.next()) {
                partido.setIdpartido(rs.getInt("idpartido"));
                Lugar_TorneoBean lugarT=new Lugar_TorneoBean(rs.getInt("idlugar"));
                partido.setLugarT(lugarT);
                ModalidadBean modalidad=new ModalidadBean(rs.getInt("idmodal"));
                partido.setModalidad(modalidad);
                ArbitroBean arbitro=new ArbitroBean(rs.getInt("idarbi"));
                partido.setArbitro(arbitro);
                FaseBean fase =new FaseBean(rs.getInt("idfase"));
                partido.setFase(fase);
                PremioBean premio=new PremioBean(rs.getInt("idpremio"));
                partido.setPremio(premio);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return partido;
    }
    
    public int getNumPartido() {
        String sql = "select count(idpartido) as partidos from partido";
        int resultado=0;
        try {

            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                resultado = rs.getInt("partidos");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("ress" +resultado);
        return resultado;
    }
}
