package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import modelo.CiudadBean;
import java.util.LinkedList;
import java.util.List;
import modelo.PaisBean;

public class CiudadDao {

    Conexion conn;

    public CiudadDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(CiudadBean ciub) {
        String sql = "insert into ciudad values (?,?,?)";

        PaisBean idp = ciub.getIdpais();
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, ciub.getIdciudad());
            ps.setString(2, ciub.getCiudad());
            ps.setInt(3, idp.getIdpais());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean actualizar(CiudadBean ciub) {
        String sql = "update ciudad set ciudad=?, idpais=? where idciudad=?";
        PaisBean idp = ciub.getIdpais();
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, ciub.getCiudad());
            ps.setInt(2, idp.getIdpais());
            ps.setInt(3, ciub.getIdciudad());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<CiudadBean> consultarAll() {
        String sql = "select c.idciudad,c.ciudad,p.pais from ciudad as c join pais as p on p.idpais=c.idpais;";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<CiudadBean> lista = new LinkedList<>();
            CiudadBean ciub;
            PaisBean pais;
            while (rs.next()) {
                pais = new PaisBean(0);
                pais.setPais(rs.getString("pais"));
                ciub = new CiudadBean(rs.getInt("idciudad"));
                ciub.setCiudad(rs.getString("ciudad"));
                ciub.setIdpais(pais);
                lista.add(ciub);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public CiudadBean consultarById(int idciudad) {
        String sql = "select * from ciudad where idciudad=?";
        CiudadBean ciudad = new CiudadBean(0);
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idciudad);
            ResultSet rs = ps.executeQuery();

            PaisBean pais;
            while (rs.next()) {
                pais = new PaisBean(rs.getInt("idpais"));

                ciudad.setIdciudad(rs.getInt("idciudad"));
                ciudad.setCiudad(rs.getString("ciudad"));
                ciudad.setIdpais(pais);

            }
            return ciudad;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean eliminar(int idciudad) {
        String sql = "delete from ciudad where idciudad=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idciudad);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
