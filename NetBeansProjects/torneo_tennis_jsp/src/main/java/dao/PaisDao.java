package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.PaisBean;

public class PaisDao {

    ResultSet rs;
    PreparedStatement ps;
    String sql;
    Conexion conn;
    PaisBean bean;

    public PaisDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(PaisBean bean) {
        sql = "insert into pais values(?,?)";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, bean.getIdpais());
            ps.setString(2, bean.getPais());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean actualizar(PaisBean bean) {
        sql = "update pais set pais=? where idpais=?";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, bean.getPais());
            ps.setInt(2, bean.getIdpais());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean eliminar(int idpais) {
        sql = "delete from pais where idpais=?";

        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idpais);

            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<PaisBean> consultarAll() {
        sql = "select * from pais";
        try {
            ps = conn.conectar().prepareStatement(sql);
            rs = ps.executeQuery();

            List<PaisBean> lista = new LinkedList<>();

            while (rs.next()) {
                bean = new PaisBean(rs.getInt("idpais"));
                bean.setPais(rs.getString("pais"));

                lista.add(bean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public List<PaisBean> consultarById(int idpais) {
        sql = "select * from pais where idpais=?";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idpais);
            rs = ps.executeQuery();

            List<PaisBean> lista = new LinkedList<>();

            while (rs.next()) {
                bean = new PaisBean(rs.getInt("idpais"));
                bean.setPais(rs.getString("pais"));

                lista.add(bean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
}
