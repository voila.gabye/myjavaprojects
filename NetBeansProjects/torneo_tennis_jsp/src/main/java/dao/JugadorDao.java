package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.EquipoBean;
import modelo.GeneroBean;
import modelo.JugadorBean;
import modelo.PaisBean;

public class JugadorDao {

    ResultSet rs;
    PreparedStatement ps;
    String sql;
    Conexion conn;
    JugadorBean bean;

    public JugadorDao(Conexion conn) {
        this.conn = conn;
    }

    public boolean insertar(JugadorBean bean) {
        sql = "insert into jugador values(?,?,?,?)";

        GeneroBean beanGenero = bean.getIdgenero();
        EquipoBean beanEquipo = bean.getIdequipo();

        try {

            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, bean.getIdjugador());
            ps.setString(2, bean.getNombre());
            ps.setInt(3, beanGenero.getIdgenero());
            ps.setInt(4, beanEquipo.getIdequipo());

            ps.executeUpdate();

            return true;

        } catch (Exception e) {
            return false;
        }

    }

    public boolean actualizar(JugadorBean bean) {
        sql = "update jugador set nombre=?,idgenero=?,idequipo=? where idjugador=?";

        GeneroBean beanGenero = bean.getIdgenero();
        EquipoBean beanEquipo = bean.getIdequipo();

        try {

            ps = conn.conectar().prepareStatement(sql);

            ps.setString(1, bean.getNombre());
            ps.setInt(2, beanGenero.getIdgenero());
            ps.setInt(3, beanEquipo.getIdequipo());
            ps.setInt(4, bean.getIdjugador());

            ps.executeUpdate();

            return true;

        } catch (Exception e) {
            return false;
        }

    }

    public boolean eliminar(int idjugador) {
        sql = "delete from jugador where idjugador=?";
        try {

            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idjugador);

            ps.executeUpdate();

            return true;

        } catch (Exception e) {
            return false;
        }

    }

    public List<JugadorBean> consultar() {
        sql = "select idjugador,nombre,genero,pais from jugador inner join genero on genero.idgenero=jugador.idgenero"
                + " inner join equipo on equipo.idequipo=jugador.idequipo inner join pais on pais.idpais=equipo.idpais";
        try {
            ps = conn.conectar().prepareStatement(sql);
            rs = ps.executeQuery();
            List<JugadorBean> lista = new LinkedList<>();

            GeneroBean generoBean;
            EquipoBean equipoBean;

            while (rs.next()) {
                generoBean = new GeneroBean(0);
                generoBean.setGenero(rs.getString("genero"));
                
                PaisBean paisBean=new PaisBean(0);
                paisBean.setPais(rs.getString("pais"));
                
                equipoBean = new EquipoBean(0);
                equipoBean.setIdpais(paisBean);

                bean = new JugadorBean(rs.getInt("idjugador"));
                bean.setNombre(rs.getString("nombre"));
                bean.setIdgenero(generoBean);
                bean.setIdequipo(equipoBean);

                lista.add(bean);
            }
            return lista;

        } catch (Exception e) {
            return null;
        }
    }
    public List<JugadorBean> consultarById(int idjugador) {
        sql = "select * from jugador where idjugador=?";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, idjugador);
            rs = ps.executeQuery();
            List<JugadorBean> lista = new LinkedList<>();

            GeneroBean generoBean;
            EquipoBean equipoBean;

            while (rs.next()) {
                generoBean = new GeneroBean(rs.getInt("idgenero"));
                
                equipoBean = new EquipoBean(rs.getInt("idequipo"));

                bean = new JugadorBean(rs.getInt("idjugador"));
                bean.setNombre(rs.getString("nombre"));
                bean.setIdgenero(generoBean);
                bean.setIdequipo(equipoBean);

                lista.add(bean);
            }
            return lista;

        } catch (Exception e) {
            return null;
        }
    }
    
    public int getNumJugadores(){
    String sql="select count(nombre) as jugadores from jugador;";
    int conteo=0;
        try {
            PreparedStatement ps=conn.conectar().prepareStatement(sql);
            ResultSet rs=ps.executeQuery();
            while (rs.next()) {
                conteo=rs.getInt("jugadores");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conteo;
    }
}
