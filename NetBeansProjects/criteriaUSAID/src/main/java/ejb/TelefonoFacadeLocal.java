package ejb;

import entidades.Telefono;
import java.util.List;
import javax.ejb.Local;

@Local
public interface TelefonoFacadeLocal {

    void create(Telefono telefono);

    void edit(Telefono telefono);

    void remove(Telefono telefono);
    
     List<Telefono> findAll();
}
