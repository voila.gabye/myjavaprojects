package ejb;

import entidades.Persona;
import java.util.List;
import javax.ejb.Local;

@Local
public interface PersonaFacadeLocal {

    void create(Persona persona);

    void edit(Persona persona);

    void remove(Persona persona);

    List<Persona> findAll();
    List<Persona> getAllPersonaCriteria();
}
