package ejb;

import entidades.Categoria;
import entidades.Categoria_;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Stateless   //sin estado
public class CategoriaFacade extends AbstractFacade<Categoria> implements CategoriaFacadeLocal {

    @PersistenceContext(unitName = "persistencia")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CategoriaFacade() {
        super(Categoria.class);
    }

    //  public List<Categoria> listaMorosos() {                                     //aca se agregan metodos especificos y en el astract facade solo cosas genericas
    //      CriteriaBuilder cb = em.getCriteriaBuilder();
    //      CriteriaQuery cq = cb.createQuery();
    //   if (true) {   select, having, groupBy //
    // }
    @Override
    public List<Categoria> listaActivos() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Categoria> categoria = cq.from(Categoria.class);
        cq.orderBy(cb.asc(categoria.get("codigo")));
        cq.where(
                cb.equal(categoria.get("estado"), 0)
        );
        Query q = em.createQuery(cq);

        cq.select(categoria);
        return q.getResultList();
    }

    @Override
    public List<Categoria> listaActivosFechas(Date inicio_, Date final_) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Categoria> categoria = cq.from(Categoria.class);
        cq.orderBy(cb.asc(categoria.get(Categoria_.codigo)));
        cq.where(
                cb.equal(categoria.get(Categoria_.estado), 1),
                cb.between(categoria.get(Categoria_.fecCreacion), inicio_, final_)
        );
        Query q = em.createQuery(cq);

        cq.select(categoria);
        return q.getResultList();
    }

    @Override
    public List<Categoria> listaPalabras() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Categoria> categoria = cq.from(Categoria.class);
        cq.orderBy(cb.asc(categoria.get("codigo")));
        cq.where(cb.like(categoria.get(Categoria_.nombre), "%ja%"));

        Query q = em.createQuery(cq);
        cq.select(categoria);
        return q.getResultList();
    }

    @Override
    public List<Categoria> listamayor() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Categoria> categoria = cq.from(Categoria.class);
        cq.orderBy(cb.asc(categoria.get("codigo")));
        cq.where(cb.greaterThan(categoria.get(Categoria_.codigo), 7));
        Query q = em.createQuery(cq);
        cq.select(categoria);
        return q.getResultList();
    }

    @Override
    public int listacontar() {
        int resultado = 0;

        try {
            SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd");
            String fecha = "2018-01-01";
            String fecha2 = "2018-12-31";
            Date inicio = format.parse(fecha);
            Date finale = format.parse(fecha2);

            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();

            Root<Categoria> categoria = cq.from(Categoria.class);

            cq.select(cb.count(categoria.get(Categoria_.fecCreacion)));
            cq.where(cb.between(categoria.get(Categoria_.fecCreacion), inicio, finale));

            Query q = em.createQuery(cq);
            Long conta = (Long) q.getSingleResult();
            resultado = conta.intValue();
        } catch (ParseException e) {

        }
        return resultado;
    }


   
}
