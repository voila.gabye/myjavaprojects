package ejb;

import entidades.Nota;
import entidades.Nota_;
import entidades.Persona;
import entidades.Persona_;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

@Stateless
public class NotaFacade extends AbstractFacade<Nota> implements NotaFacadeLocal {

    @PersistenceContext(unitName = "persistencia")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public NotaFacade() {
        super(Nota.class);
    }

    @Override
    public List<Nota> listarNotas(Persona idpersona) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Nota> nota = cq.from(Nota.class);

        Join<Nota, Persona> person = nota.join(Nota_.codigo_persona);
        cq.where(cb.equal(person.get(Persona_.codigo), idpersona.getCodigo()));
        Query q = em.createQuery(cq);
        return q.getResultList();
    }
}
