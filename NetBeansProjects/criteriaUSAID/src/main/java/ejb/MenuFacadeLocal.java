
package ejb;

import entidades.Menu;
import java.util.List;
import javax.ejb.Local;

@Local
public interface MenuFacadeLocal {
    
    void create(Menu menu);

    void edit(Menu menu);

    void remove(Menu menu);
    
    List<Menu> findAll();
}
