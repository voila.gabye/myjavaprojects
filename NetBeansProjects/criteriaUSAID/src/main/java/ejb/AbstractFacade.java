package ejb;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public abstract class AbstractFacade<T> {

    private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();   //el abstract facade es para capturar un objeto cualquiera.

    public void create(T entity) {                              //insertar
        getEntityManager().persist(entity);
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));  //lo busca, lo obtiene y lo borra
    }

    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    public List<T> findAll() {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();    //creando el criteria builder
        CriteriaQuery cq = cb.createQuery();                              //creando el query 
        cq.select(cq.from(entityClass));      //select * from
        return getEntityManager().createQuery(cq).getResultList();
        // javax.persistence.criteria.CriteriaQuery = getEntityManager().getCriteriaBuilder().createQuery(); //ES LO MISMO DE ARRIBA
    }
}
