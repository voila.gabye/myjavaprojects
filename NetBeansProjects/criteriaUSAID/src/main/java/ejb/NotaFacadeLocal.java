package ejb;

import entidades.Nota;
import entidades.Persona;
import java.util.List;
import javax.ejb.Local;

@Local
public interface NotaFacadeLocal {

    void create(Nota nota);

    void edit(Nota nota);

    void remove(Nota nota);

    List<Nota> findAll();
    
    List<Nota> listarNotas(Persona idpersona);
}
