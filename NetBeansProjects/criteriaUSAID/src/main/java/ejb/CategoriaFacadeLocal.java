package ejb;

import entidades.Categoria;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

@Local
public interface CategoriaFacadeLocal {

    void create(Categoria categoria);

    void edit(Categoria categoria);

    void remove(Categoria categoria);

    List<Categoria> findAll();

    List<Categoria> listaActivos();

    List<Categoria> listaActivosFechas(Date inicio_, Date final_);

    List<Categoria> listaPalabras();
    
    List <Categoria> listamayor ();
    
    int listacontar ();
}
