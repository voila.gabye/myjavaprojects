package ejb;

import entidades.Usuario;
import entidades.Usuario_;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> implements UsuarioFacadeLocal {

    @PersistenceContext(unitName = "persistencia")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }

    @Override
    public Usuario login(Usuario usuario) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Usuario> us = cq.from(Usuario.class);
        cq.select(us).where(cb.equal(us.get(Usuario_.usuario), usuario.getUsuario()), cb.and(cb.equal(us.get(Usuario_.clave), usuario.getClave())));
        Query q = em.createQuery(cq);
        return (Usuario) q.getSingleResult();
    }

}
