package ejb;

import entidades.Usuario;
import java.util.List;
import javax.ejb.Local;

@Local
public interface UsuarioFacadeLocal {

    void create(Usuario usuario);

    void edit(Usuario usuario);

    void remove(Usuario usuario);
    
    List<Usuario> findAll();
    
    Usuario login (Usuario usuario);
}
