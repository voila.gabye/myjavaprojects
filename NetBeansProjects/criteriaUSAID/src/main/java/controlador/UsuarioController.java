package controlador;

import ejb.UsuarioFacadeLocal;
import entidades.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;

import javax.faces.context.FacesContext;


@ManagedBean(name = "usuarioController")
@SessionScoped
public class UsuarioController implements Serializable {

    @EJB
    private UsuarioFacadeLocal usuarioEJB;
    private Usuario usuario;
    private List<Usuario> lista;
    private Usuario username;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Usuario> getLista() {
        return lista;
    }

    public void setLista(List<Usuario> lista) {
        this.lista = lista;
    }

    public Usuario getUsername() {
        return username;
    }

    public void setUsername(Usuario username) {
        this.username = username;
    }

    @PostConstruct
    public void init() {
        usuario = new Usuario();

        listar();
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void guardar() {
        try {
            usuarioEJB.create(usuario);
            if (usuario != null) {
                addMessage("Registro fue guardado ");
                init();
            }
            init();
        } catch (Exception e) {
            addMessage("Registro no fue guardado " + e);
        }
    }

    public void listar() {
        try {
            lista = usuarioEJB.findAll();
        } catch (Exception e) {
        }
    }

    public void eliminar(Usuario usuario) {
        this.usuario = usuario;
        try {
            usuarioEJB.remove(usuario);
            lista = usuarioEJB.findAll();
            if (usuario != null) {
                addMessage("Registro fue eliminado ");
                init();
            }
            init();
        } catch (Exception e) {
            addMessage("Registro no fue eliminado " + e);
        }
    }

    public void modificar() {
        try {
            usuarioEJB.edit(usuario);
            if (usuario != null) {
                addMessage("Registro fue actualizado ");
                init();
            }
            init();
        } catch (Exception e) {
            addMessage("Registro no fue actualizado " + e);
        }
    }

    public void leerId(Usuario usuario) {
        this.usuario = usuario;
    }

    public String login() {
        username = usuarioEJB.login(usuario);
        usuario = new Usuario();
        String goUrl = "";
        if (username != null) {
            if (username.getTipo().equals("A")) {
                addMessage("Bienvenido " + username.getUsuario());
                goUrl = "inicio.xhtml";
            } else {
                if (username.getTipo().equals("O")) {
                    addMessage("Bienvenido ");
                    goUrl = "inicio.xhtml";
                }
            }
        }
        return goUrl;
    }
}
