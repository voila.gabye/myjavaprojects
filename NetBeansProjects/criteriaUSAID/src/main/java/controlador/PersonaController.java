package controlador;

import ejb.PersonaFacadeLocal;
import entidades.Persona;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named(value = "personaController")
@SessionScoped
public class PersonaController implements Serializable {

    @EJB
    private PersonaFacadeLocal personaEJB;
    private Persona persona;
    private List<Persona> lista;

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public List<Persona> getLista() {
        return lista;
    }

    public void setLista(List<Persona> lista) {
        this.lista = lista;
    }

    @PostConstruct
    public void init() {
        persona = new Persona();
        listar();
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void guardar() {
        try {
            personaEJB.create(persona);

            if (persona != null) {
                addMessage("Registro fue guardado ");
                init();
            }
            init();
        } catch (Exception e) {
            addMessage("Registro no fue guardado " +e);
        }
    }

    public void modificar() {
        try {
            personaEJB.edit(persona);
            if (persona != null) {
                addMessage("Registro fue actualizado ");
                init();
            }
            init();
        } catch (Exception e) {
            addMessage("Registro no fue actualizado " +e);
        }
    }

    public void listar() {
        try {
            lista = personaEJB.findAll();
        } catch (Exception e) {
        }
    }

    public void leerId(Persona person) {
        this.persona = person;
    }

    public void eliminar(Persona person) {
        this.persona = person;
        try {
            personaEJB.remove(persona);
            lista = personaEJB.findAll();
            if (persona != null) {
                addMessage("Registro fue eliminado ");
                init();
            }
            init();
        } catch (Exception e) {
            addMessage("Registro no fue eliminado " +e);
        }
    }

}
