package controlador;

import ejb.CategoriaFacadeLocal;
import entidades.Categoria;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

// @ManagedBean (name ="categoriaController")   por si no funciona el named
@Named(value = "categoriaController")
@SessionScoped
public class CategoriaController implements Serializable {

    @EJB    //inyeccion
    private CategoriaFacadeLocal categoriaEJB;
    private Categoria categoria;
    private List<Categoria> lista;
    private Date f_inicio;
    private Date f_final;
    private int resultado;

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public List<Categoria> getLista() {
        return lista;
    }

    public void setLista(List<Categoria> lista) {
        this.lista = lista;
    }

    public Date getF_inicio() {
        return f_inicio;
    }

    public void setF_inicio(Date f_inicio) {
        this.f_inicio = f_inicio;
    }

    public Date getF_final() {
        return f_final;
    }

    public void setF_final(Date f_final) {
        this.f_final = f_final;
    }

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    @PostConstruct
    public void init() {
        categoria = new Categoria();   //inicializar
        listar();
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void guardar() {
        try {
            categoriaEJB.create(categoria);
            if (categoria != null) {
                addMessage("Registro fue guardado ");
                init();
            }
            init();
        } catch (Exception e) {
            addMessage("Registro no fue guardado " + e);
        }
    }

    public void listar() {
        try {
            lista = categoriaEJB.findAll();
        } catch (Exception e) {
        }
    }

    public void eliminar(Categoria cat) {
        this.categoria = cat;
        try {
            categoriaEJB.remove(categoria);
            lista = categoriaEJB.findAll();
            if (categoria != null) {
                addMessage("Registro fue eliminado ");
                init();
            }
            init();
        } catch (Exception e) {
            addMessage("Registro no fue eliminado " + e);
        }
    }

    public void modificar() {
        try {
            categoriaEJB.edit(categoria);
            if (categoria != null) {
                addMessage("Registro fue actualizado ");
                init();
            }
            init();
        } catch (Exception e) {
            addMessage("Registro no fue actualizado " + e);
        }
    }

    public void leerId(Categoria cat) {
        this.categoria = cat;
    }

    public void listarActivos() {
        try {
            lista = categoriaEJB.listaActivos();
        } catch (Exception e) {
        }
    }

    public void listarXFecha() {
        try {
            lista = categoriaEJB.listaActivosFechas(f_inicio, f_final);
        } catch (Exception e) {
        }
    }

    public void listarPalabras() {
        try {
            lista = categoriaEJB.listaPalabras();
        } catch (Exception e) {
        }
    }

    public void listarmayor() {
        try {
            lista = categoriaEJB.listamayor();
        } catch (Exception e) {
        }
    }

    public void listacontar() {
        try {
            resultado = categoriaEJB.listacontar();
        } catch (Exception e) {
        }
    }

}
