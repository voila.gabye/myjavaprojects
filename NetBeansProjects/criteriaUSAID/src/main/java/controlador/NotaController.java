package controlador;

import ejb.NotaFacadeLocal;
import entidades.Categoria;
import entidades.Nota;
import entidades.Persona;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named(value = "notaController")
@SessionScoped
public class NotaController implements Serializable {

    @EJB
    private NotaFacadeLocal notaEJB;
    private Nota nota;
    private List<Nota> lista;
    private Persona persona;
    private Categoria categoria;
 

    public Nota getNota() {
        return nota;
    }

    public void setNota(Nota nota) {
        this.nota = nota;
    }

    public List<Nota> getLista() {
        return lista;
    }

    public void setLista(List<Nota> lista) {
        this.lista = lista;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    @PostConstruct
    public void init() {
        nota = new Nota();
        persona = new Persona();
        categoria = new Categoria();
        listar ();
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void guardar() {
        try {
            nota.setCodigo_persona(persona);
            nota.setCodigo_categoria(categoria);
            notaEJB.create(nota);

            if (nota != null) {
                addMessage("Registro fue guardado ");
                init();
            }
            init();
        } catch (Exception e) {
            addMessage("Registro no fue guardado " + e);
            init();
        }
    }

    public void listar() {
        try {
            lista = notaEJB.findAll();
        } catch (Exception e) {
        }
    }

    public void modificar() {
        try {
            nota.setCodigo_persona(persona);
            nota.setCodigo_categoria(categoria);
            notaEJB.edit(nota);

            if (nota != null) {
                addMessage("Registro fue actualizado ");
                init();
            }
            init();
        } catch (Exception e) {
            addMessage("Registro no fue actualizado " + e);
        }
    }

    public void leerId(Nota nota) {
        persona.setCodigo(nota.getCodigo_persona().getCodigo());
        categoria.setCodigo(nota.getCodigo_categoria().getCodigo());
        this.nota = nota;
    }

    public void eliminar(Nota nota) {
        this.nota = nota;
        try {
            notaEJB.remove(nota);
            lista = notaEJB.findAll();
            if (nota != null) {
                addMessage("Registro fue eliminado ");
                init();
            }
            init();
        } catch (Exception e) {
            addMessage("Registro no fue eliminado " + e);
        }
    }

    public void listarNotas() {
        try {
            lista = notaEJB.listarNotas(persona);
        } catch (Exception e) {
        }
    }

}
