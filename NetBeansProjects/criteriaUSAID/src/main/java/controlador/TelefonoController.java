package controlador;

import ejb.TelefonoFacadeLocal;
import entidades.Persona;
import entidades.Telefono;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named(value = "telefonoController")
@SessionScoped
public class TelefonoController implements Serializable {

    @EJB
    private TelefonoFacadeLocal telefonoEJB;
    private Telefono telefono;
    private List<Telefono> lista;
    private Persona persona;

    public Telefono getTelefono() {
        return telefono;
    }

    public void setTelefono(Telefono telefono) {
        this.telefono = telefono;
    }

    public List<Telefono> getLista() {
        return lista;
    }

    public void setLista(List<Telefono> lista) {
        this.lista = lista;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @PostConstruct
    public void init() {
        telefono = new Telefono();
        persona = new Persona();
        listar ();
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void guardar() {
        try {
            telefono.setCodigo_persona(persona);
            telefonoEJB.create(telefono);
            if (telefono != null) {
                addMessage("Registro fue guardado ");
                init();
            }
            init();
        } catch (Exception e) {
            addMessage("Registro no fue guardado " + e);
            init();
        }
    }

    public void listar() {
        try {
            lista = telefonoEJB.findAll();
        } catch (Exception e) {
        }
    }

    public void eliminar(Telefono telefono) {
        this.telefono = telefono;
        try {
            telefonoEJB.remove(telefono);
            lista = telefonoEJB.findAll();
            if (telefono != null) {
                addMessage("Registro fue eliminado ");
                init();
            }
            init();
        } catch (Exception e) {
            addMessage("Registro no fue eliminado " + e);
        }
    }

    public void modificar() {
        try {
            telefono.setCodigo_persona(persona);
            telefonoEJB.edit(telefono);
            if (telefono != null) {
                addMessage("Registro fue actualizado ");
                init();
            }
            init();
        } catch (Exception e) {
            addMessage("Registro no fue actualizado " + e);
        }
    }

    public void leerId(Telefono telefono) {
        this.telefono = telefono;
        persona.setCodigo(telefono.getCodigo_persona().getCodigo());
        
    }

}
