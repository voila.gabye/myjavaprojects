package controlador;

import ejb.MenuFacadeLocal;
import entidades.Menu;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named(value = "menuController")
@SessionScoped
public class MenuController implements Serializable {

    @EJB
    private MenuFacadeLocal menuEJB;
    private Menu menu;
    private List<Menu> lista;

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public List<Menu> getLista() {
        return lista;
    }

    public void setLista(List<Menu> lista) {
        this.lista = lista;
    }
    
    @PostConstruct
    public void init() {
        menu = new Menu();
    }

    public void guardar() {
        try {
            menuEJB.create(menu);
        } catch (Exception e) {
        }
    }

    public void listar() {
        try {
            lista = menuEJB.findAll();
        } catch (Exception e) {
        }
    }
    
    public void eliminar(Menu menu) {
        this.menu = menu;
        try {
            menuEJB.remove(menu);
            lista = menuEJB.findAll();
        } catch (Exception e) {
        }
    }

    public void modificar() {
        try {
            menuEJB.edit(menu);
        } catch (Exception e) {
        }
    }

    public void leerId(Menu menu) {
        this.menu = menu;
    }

}
