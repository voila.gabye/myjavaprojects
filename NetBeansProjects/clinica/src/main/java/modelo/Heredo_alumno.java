
package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "heredo_alumno")
public class Heredo_alumno implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    
    @Column(name = "descripcion")
    private String descripcion;
    
    @JoinColumn(name = "alumno")
    @ManyToOne
    private Alumno alumno;
    
    @JoinColumn(name = "ante_familiar")
    @ManyToOne
    private Ante_familiar ante_familiar;
    
    @JoinColumn(name = "tipo_cancer")
    @ManyToOne
    private Tipo_cancer tipo_cancer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Ante_familiar getAnte_familiar() {
        return ante_familiar;
    }

    public void setAnte_familiar(Ante_familiar ante_familiar) {
        this.ante_familiar = ante_familiar;
    }

    public Tipo_cancer getTipo_cancer() {
        return tipo_cancer;
    }

    public void setTipo_cancer(Tipo_cancer tipo_cancer) {
        this.tipo_cancer = tipo_cancer;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Heredo_alumno other = (Heredo_alumno) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Heredo_alumno{" + "id=" + id + '}';
    }
}
