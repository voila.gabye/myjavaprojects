/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "diagnostico")
public class Diagnostico implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "motivo_cons")
    private String motivo_cons;
    @Column(name = "presente_enf")
    private String presente_enf;
    @Column(name = "tratamiento")
    private String tratamiento;
    @Column(name = "examenes")
    private String examenes;
    @Column(name = "exp_general")
    private String exp_general;

    @Column(name = "impresion_diag")
    private String impresion_diag;
    
    @JoinColumn(name = "signo_vital")
    @OneToOne
    private Signo_vital signo_vital;
    
    @JoinColumn(name = "alumno")
    @ManyToOne
    private Alumno alumno;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMotivo_cons() {
        return motivo_cons;
    }

    public void setMotivo_cons(String motivo_cons) {
        this.motivo_cons = motivo_cons;
    }

    public String getPresente_enf() {
        return presente_enf;
    }

    public void setPresente_enf(String presente_enf) {
        this.presente_enf = presente_enf;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    public String getExamenes() {
        return examenes;
    }

    public void setExamenes(String examenes) {
        this.examenes = examenes;
    }

    public String getExp_general() {
        return exp_general;
    }

    public void setExp_general(String exp_general) {
        this.exp_general = exp_general;
    }

    public String getImpresion_diag() {
        return impresion_diag;
    }

    public void setImpresion_diag(String impresion_diag) {
        this.impresion_diag = impresion_diag;
    }
    public Signo_vital getSigno_vital() {
        return signo_vital;
    }

    public void setSigno_vital(Signo_vital signo_vital) {
        this.signo_vital = signo_vital;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Diagnostico other = (Diagnostico) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Diagnostico{" + "id=" + id + '}';
    }
}
