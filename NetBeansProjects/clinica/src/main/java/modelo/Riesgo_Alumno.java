package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "riesgo_alumno")
public class Riesgo_Alumno implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "tabaquismo")
    private String tabaquismo;
    @Column(name = "ncigarros")
    private Integer ncigarros;
    @Column(name = "alcoholismo")
    private String alcoholismo;
    @Column(name = "frecuencia")
    private String frecuencia;
    @Column(name = "toxicomonia")
    private String toxicomonia;
    @Column(name = "frecuenciataxi")
    private String frecuenciataxi;
    @Column(name = "tipotoxico")
    private String tipotoxico;
    @Column(name = "violencia")
    private String violencia;
    @Column(name = "tipoviolencia")
    private String tipoviolencia;
    @Column(name = "ansiedad")
    private String ansiedad;
    @Column(name = "depresion")
    private String depresion;

    @JoinColumn(name = "alumno")
    @ManyToOne
    private Alumno alumno;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTabaquismo() {
        return tabaquismo;
    }

    public void setTabaquismo(String tabaquismo) {
        this.tabaquismo = tabaquismo;
    }

    public Integer getNcigarros() {
        return ncigarros;
    }

    public void setNcigarros(Integer ncigarros) {
        this.ncigarros = ncigarros;
    }

    public String getAlcoholismo() {
        return alcoholismo;
    }

    public void setAlcoholismo(String alcoholismo) {
        this.alcoholismo = alcoholismo;
    }

    public String getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(String frecuencia) {
        this.frecuencia = frecuencia;
    }

    public String getToxicomonia() {
        return toxicomonia;
    }

    public void setToxicomonia(String toxicomonia) {
        this.toxicomonia = toxicomonia;
    }

    public String getFrecuenciataxi() {
        return frecuenciataxi;
    }

    public void setFrecuenciataxi(String frecuenciataxi) {
        this.frecuenciataxi = frecuenciataxi;
    }

    public String getTipotoxico() {
        return tipotoxico;
    }

    public void setTipotoxico(String tipotoxico) {
        this.tipotoxico = tipotoxico;
    }

    public String getViolencia() {
        return violencia;
    }

    public void setViolencia(String violencia) {
        this.violencia = violencia;
    }

    public String getTipoviolencia() {
        return tipoviolencia;
    }

    public void setTipoviolencia(String tipoviolencia) {
        this.tipoviolencia = tipoviolencia;
    }

    public String getAnsiedad() {
        return ansiedad;
    }

    public void setAnsiedad(String ansiedad) {
        this.ansiedad = ansiedad;
    }

    public String getDepresion() {
        return depresion;
    }

    public void setDepresion(String depresion) {
        this.depresion = depresion;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Riesgo_Alumno other = (Riesgo_Alumno) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Riesgo_Alumno{" + "id=" + id + '}';
    }
}
