/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author alejandro.suarezusam
 */
@Entity
@Table(name = "vivienda")
public class Vivienda implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "habitante")
    private int habitante;

    @Column(name = "habitacion")
    private int habitacion;

    @Column(name = "banio")
    private int banio;

    @JoinColumn(name = "ecologico_social")
    @OneToOne
    private Ecologico_Social ecologico_social;

    public Ecologico_Social getEcologico_social() {
        return ecologico_social;
    }

    public void setEcologico_social(Ecologico_Social ecologico_social) {
        this.ecologico_social = ecologico_social;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHabitante() {
        return habitante;
    }

    public void setHabitante(int habitante) {
        this.habitante = habitante;
    }

    public int getHabitacion() {
        return habitacion;
    }

    public void setHabitacion(int habitacion) {
        this.habitacion = habitacion;
    }

    public int getBanio() {
        return banio;
    }

    public void setBanio(int banio) {
        this.banio = banio;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vivienda other = (Vivienda) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "vivienda{" + "id=" + id + '}';
    }

}
