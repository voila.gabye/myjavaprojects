
package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ecologico_mascota")
public class Ecologico_Mascota implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    
    @Column(name = "descripcion")
    private String descripcion;
    
    @JoinColumn(name = "ecologico_Social")
    @ManyToOne
    private Ecologico_Social ecologico_Social;
    
    @JoinColumn(name = "mascota")
    @ManyToOne
    private Mascota mascota;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Ecologico_Social getEcologico_Social() {
        return ecologico_Social;
    }

    public void setEcologico_Social(Ecologico_Social ecologico_Social) {
        this.ecologico_Social = ecologico_Social;
    }

    public Mascota getMascota() {
        return mascota;
    }

    public void setMascota(Mascota mascota) {
        this.mascota = mascota;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ecologico_Mascota other = (Ecologico_Mascota) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Ecologico_Mascota{" + "id=" + id + '}';
    }
}
