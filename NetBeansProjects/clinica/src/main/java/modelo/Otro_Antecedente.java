package modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "otro_antecedente")
public class Otro_Antecedente implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "inmunizacion")
    private String inmunizacion;
    @Temporal(TemporalType.DATE)
    @Column(name = "menarca")
    private Date menarca;
    @Temporal(TemporalType.DATE)
    @Column(name = "fum")
    private Date fum;
    @JoinColumn(name = "tipo_sangre")
    @ManyToOne
    private Tipo_sangre tipo_sangre;
    @JoinColumn(name = "metodo_anti")
    @ManyToOne
    private Metodo_anti metodo_anti;
    @JoinColumn(name = "alumno")
    @OneToOne
    private Alumno alumno;

    public Otro_Antecedente() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInmunizacion() {
        return inmunizacion;
    }

    public void setInmunizacion(String inmunizacion) {
        this.inmunizacion = inmunizacion;
    }

    public Date getMenarca() {
        return menarca;
    }

    public void setMenarca(Date menarca) {
        this.menarca = menarca;
    }

    public Date getFum() {
        return fum;
    }

    public void setFum(Date fum) {
        this.fum = fum;
    }

    public Tipo_sangre getTipo_sangre() {
        return tipo_sangre;
    }

    public void setTipo_sangre(Tipo_sangre tipo_sangre) {
        this.tipo_sangre = tipo_sangre;
    }

    public Metodo_anti getMetodo_anti() {
        return metodo_anti;
    }

    public void setMetodo_anti(Metodo_anti metodo_anti) {
        this.metodo_anti = metodo_anti;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Otro_Antecedente other = (Otro_Antecedente) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Otro_Antecedente{" + "id=" + id + '}';
    }
    
    
}
