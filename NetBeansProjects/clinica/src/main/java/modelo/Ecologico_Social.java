package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ecologico_social")
public class Ecologico_Social implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @JoinColumn(name = "alumno")
    @OneToOne
    private Alumno alumno;
    
    @JoinColumn(name = "tipo_Agua")
    @ManyToOne
    private Tipo_Agua tipo_Agua;
    @JoinColumn(name = "cons_pared")
    @ManyToOne
    private Cons_pared cons_pared;
    @JoinColumn(name = "tipo_Techo")
    @ManyToOne
    private Tipo_Techo tipo_Techo;
    @JoinColumn(name = "tipo_piso")
    @ManyToOne
    private Tipo_piso tipo_piso;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Tipo_Agua getTipo_Agua() {
        return tipo_Agua;
    }

    public void setTipo_Agua(Tipo_Agua tipo_Agua) {
        this.tipo_Agua = tipo_Agua;
    }

    public Cons_pared getCons_pared() {
        return cons_pared;
    }

    public void setCons_pared(Cons_pared cons_pared) {
        this.cons_pared = cons_pared;
    }

    public Tipo_Techo getTipo_Techo() {
        return tipo_Techo;
    }

    public void setTipo_Techo(Tipo_Techo tipo_Techo) {
        this.tipo_Techo = tipo_Techo;
    }

    public Tipo_piso getTipo_piso() {
        return tipo_piso;
    }

    public void setTipo_piso(Tipo_piso tipo_piso) {
        this.tipo_piso = tipo_piso;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ecologico_Social other = (Ecologico_Social) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Ecologico_Social{" + "id=" + id + '}';
    }
}
