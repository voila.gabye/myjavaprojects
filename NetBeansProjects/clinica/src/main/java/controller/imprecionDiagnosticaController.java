/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.ImpresionDiagFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Impresion_diag;
import util.Utilidades;

/**
 *
 * @author alejandro.suarezusam
 */
@ManagedBean
@SessionScoped
public class imprecionDiagnosticaController implements Serializable{
    @EJB
    private ImpresionDiagFacadeLocal impresionfacade;
    private Impresion_diag impresion;
    private Utilidades util;
    private List<Impresion_diag> listaimprecion;

    public Impresion_diag getImpresion() {
        return impresion;
    }

    public void setImpresion(Impresion_diag impresion) {
        this.impresion = impresion;
    }

    public Utilidades getUtil() {
        return util;
    }

    public void setUtil(Utilidades util) {
        this.util = util;
    }

    public List<Impresion_diag> getListaimprecion() {
        return listaimprecion;
    }

    public void setListaimprecion(List<Impresion_diag> listaimprecion) {
        this.listaimprecion = listaimprecion;
    }
    
    @PostConstruct
    public void init(){
        this.impresion= new Impresion_diag();
        this.util= new Utilidades();
        this.consultar();
    }
    
    public void consultar(){
    this.listaimprecion= this.impresionfacade.findAll();
    }
    
    public void consultarById(Impresion_diag i){
    this.impresion=i;
    }
    
    public void insertar(){
        try {
            this.impresionfacade.create(impresion);
            this.util.ponerMensaje("Imp. Diagnostico registrado");
            this.init();                        
        } catch (Exception e) {
            util.ponerMensaje("Error:"+e.getMessage());
        }
    }
    
    public void editar(){
        try {
            this.impresionfacade.edit(impresion);
            this.util.ponerMensaje("Imp. Diagnostico Actualizado");
            this.init();                        
        } catch (Exception e) {
            util.ponerMensaje("Error:"+e.getMessage());
        }
    }
    
    public void eliminar(Impresion_diag i){
        try {
            this.impresionfacade.remove(i);
            this.util.ponerMensaje("eliminado");
            this.init();                        
        } catch (Exception e) {
            util.ponerMensaje("Error:"+e.getMessage());
        }
    }
}
