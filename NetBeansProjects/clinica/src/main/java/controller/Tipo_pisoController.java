package controller;

import ejb.Tipo_pisoFacadeLocal;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Tipo_piso;
import util.Utilidades;

@ManagedBean(name = "tipo_pisoController")
@SessionScoped
public class Tipo_pisoController {

    @EJB
    private Tipo_pisoFacadeLocal tipoPisoFacadeLocal;
    private Tipo_piso tipo_piso;
    private List<Tipo_piso> tipo_pisoList;
    private Utilidades util;

    public Tipo_piso getTipo_piso() {
        return tipo_piso;
    }

    public void setTipo_piso(Tipo_piso tipo_piso) {
        this.tipo_piso = tipo_piso;
    }

    public List<Tipo_piso> getTipo_pisoList() {
        return tipo_pisoList;
    }

    public void setTipo_pisoList(List<Tipo_piso> tipo_pisoList) {
        this.tipo_pisoList = tipo_pisoList;
    }

    @PostConstruct
    public void init() {
        tipo_piso = new Tipo_piso();
        util = new Utilidades();
        consultar();
    }

    //CRUD
    public void insertar() {
        try {
            tipoPisoFacadeLocal.create(tipo_piso);
            util.ponerMensaje("Tipo de piso registrado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void editar() {
        try {
            tipoPisoFacadeLocal.edit(tipo_piso);
            util.ponerMensaje("Tipo de piso editado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void eliminar(Tipo_piso tipo) {
        try {
            tipoPisoFacadeLocal.remove(tipo);
            util.ponerMensaje("Tipo de piso eliminado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void consultar() {
        tipo_pisoList = tipoPisoFacadeLocal.findAll();
    }

        
    //Consultar por Id
    public void consultarById(Tipo_piso tipo) {
        tipo_piso = tipo;
    }
}
