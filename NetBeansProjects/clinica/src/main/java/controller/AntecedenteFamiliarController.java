/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.AnteFamiliarFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Ante_familiar;
import util.Utilidades;

/**
 *
 * @author alejandro.suarezusam
 */
@ManagedBean
@SessionScoped
public class AntecedenteFamiliarController implements Serializable{
    @EJB
    private AnteFamiliarFacadeLocal antecedentefacade;
    private Ante_familiar anteFamiliar;
    private List<Ante_familiar> listaAntecedente;
    private Utilidades util;

    public Ante_familiar getAnteFamiliar() {
        return anteFamiliar;
    }

    public void setAnteFamiliar(Ante_familiar anteFamiliar) {
        this.anteFamiliar = anteFamiliar;
    }

    public List<Ante_familiar> getListaAntecedente() {
        return listaAntecedente;
    }

    public void setListaAntecedente(List<Ante_familiar> listaAntecedente) {
        this.listaAntecedente = listaAntecedente;
    }

    public Utilidades getUtil() {
        return util;
    }

    public void setUtil(Utilidades util) {
        this.util = util;
    }
    
    @PostConstruct
    public void init(){
    this.anteFamiliar= new Ante_familiar();
    this.util= new Utilidades();
    consultar();
    }
    
    public void consultar(){
        try {
            this.listaAntecedente=antecedentefacade.findAll();
        } catch (Exception e) {
        }
    }
    
    public void consultarById(Ante_familiar a){
    this.anteFamiliar=a;
    }
    
    public void insertar(){
        try {
            this.antecedentefacade.create(anteFamiliar);
            this.util.ponerMensaje("Antecedente registrado con exito");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error:"+e.getMessage());
        }
    }
    
    public void editar(){
        try {
            this.antecedentefacade.edit(anteFamiliar);
            this.util.ponerMensaje("Modificado");
            this.init();
        } catch (Exception e) {
            util.ponerMensaje("Error:"+e.getMessage());
        }
    }
    
    public void eliminar(Ante_familiar a){
        try {
            this.antecedentefacade.remove(a);
            this.util.ponerMensaje("Antecedente eliminado");
            this.init();
        } catch (Exception e) {
            util.ponerMensaje("Error:"+e.getMessage());
        }
    }
    
}
