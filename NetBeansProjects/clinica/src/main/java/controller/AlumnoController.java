package controller;

import ejb.CarreraFacadeLocal;
import ejb.MunicipioFacadeLocal;
import ejb.AlumnoFacadeLocal;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Alumno;
import modelo.Carrera;
import modelo.Departamento;
import modelo.Estado_civil;
import modelo.Facultad;
import modelo.Genero;
import modelo.Municipio;
import modelo.Usuario;
import org.primefaces.PrimeFaces;
import util.Utilidades;

@ManagedBean
@SessionScoped
public class AlumnoController implements Serializable {

    @EJB
    private AlumnoFacadeLocal alumnoFacadeLocal;
    private Alumno alumno;
    private Carrera carrera;
    private Genero genero;
    private Estado_civil estado_civil;
    private Municipio municipio;
    private Usuario usuario;
    private List<Alumno> alumnoList;
    private Utilidades util;
    private Alumno alumnoRec;
    private Departamento departamento;
    private Facultad facultad;

    @EJB
    private MunicipioFacadeLocal municipioFacadeLocal;
    private List<Municipio> muniList;

    @EJB
    private CarreraFacadeLocal carreraFacadeLocal;
    private List<Carrera> carreList;

    public List<Municipio> getMuniList() {
        return muniList;
    }

    public void setMuniList(List<Municipio> muniList) {
        this.muniList = muniList;
    }

    public List<Carrera> getCarreList() {
        return carreList;
    }

    public void setCarreList(List<Carrera> carreList) {
        this.carreList = carreList;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Facultad getFacultad() {
        return facultad;
    }

    public void setFacultad(Facultad facultad) {
        this.facultad = facultad;
    }

    public Alumno getAlumnoRec() {
        return alumnoRec;
    }

    public void setAlumnoRec(Alumno alumnoRec) {
        this.alumnoRec = alumnoRec;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Carrera getCarrera() {
        return carrera;
    }

    public void setCarrera(Carrera carrera) {
        this.carrera = carrera;
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public Estado_civil getEstado_civil() {
        return estado_civil;
    }

    public void setEstado_civil(Estado_civil estado_civil) {
        this.estado_civil = estado_civil;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Alumno> getAlumnoList() {
        return alumnoList;
    }

    public void setAlumnoList(List<Alumno> alumnoList) {
        this.alumnoList = alumnoList;
    }

    @PostConstruct
    public void init() {
        alumno = new Alumno();
        carrera = new Carrera();
        municipio = new Municipio();
        genero = new Genero();
        estado_civil = new Estado_civil();
        usuario = new Usuario();
        util = new Utilidades();
        departamento = new Departamento();
        facultad = new Facultad();
        consultar();
    }

    public void findById() {
        alumnoList = alumnoFacadeLocal.findGenero(genero);
    }

    //CRUD
    public void insertar() {
        try {
            Date fecha = new Date();
            alumno.setFecharegistro(fecha);
            alumno.setCarrera(carrera);
            alumno.setEstado_civil(estado_civil);
            alumno.setGenero(genero);
            alumno.setUsuario(usuario);
            alumno.setMunicipio(municipio);
            alumnoFacadeLocal.create(alumno);
            recuperarAlumno();
            findById();
            util.ponerMensaje("Alumno registrado");
            init();
        } catch (Exception e) {
            init();
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void editar() {
        try {

            alumno.setCarrera(carrera);
            alumno.setEstado_civil(estado_civil);
            alumno.setGenero(genero);
            alumno.setUsuario(usuario);
            alumno.setMunicipio(municipio);
            alumnoFacadeLocal.edit(alumno);
            util.ponerMensaje("Alumno editado");
            findById();
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void eliminar(Alumno al) {
        try {
            alumnoFacadeLocal.remove(al);
            util.ponerMensaje("Exito Eliminado");
            findById();
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void consultar() {
        try {
            alumnoList = alumnoFacadeLocal.findAll();
        } catch (Exception e) {
        }

    }

    //Consultar por Id
    public void consultarById(Alumno al) {
        alumno = al;
        facultad.setId(al.getCarrera().getFacultad().getId());
        carrera.setId(al.getCarrera().getId());
        departamento.setId(al.getMunicipio().getDepartamento().getId());
        municipio.setId(al.getMunicipio().getId());
        genero.setId(al.getGenero().getId());
        estado_civil.setId(al.getEstado_civil().getId());
        usuario.setId(al.getUsuario().getId());
        municipioDepa();
        carreraFacu();
    }

    public void recuperarAlumno() {
        alumnoRec = alumno;
    }

    //Metodos Ajax
    public void municipioDepa() {

        try {
            muniList = municipioFacadeLocal.municipioLista(departamento);
            PrimeFaces.current().ajax().update("form:muni");
            PrimeFaces.current().ajax().update("alum:muni");
        } catch (Exception e) {
        }
    }

    public void carreraFacu() {

        try {
            carreList = carreraFacadeLocal.carreList(facultad);
            PrimeFaces.current().ajax().update("form:carre");
            PrimeFaces.current().ajax().update(":alum:carre");
        } catch (Exception e) {
        }
    }

    // redirecciones 
    public String redirEcologicoSocial() {
        return "ecologico_social.xhtml?faces-redirect=true";
    }

    public String redirAnteFamiliar() {
        return "heredo_alumno.xhtml?faces-redirect=true";
    }

    public String redirAntePersonal() {
        return "ante_alumno.xhtml?faces-redirect=true";
    }

    public String redirEcoSocial() {
        return "ecologico_social.xhtml?faces-redirect=true";
    }

    public String redirEcoMascota() {
        return "ecologico_mascota.xhtml?faces-redirect=true";
    }

    public String redirAnteRiesgo() {
        return "riesgo_alumno.xhtml?faces-redirect=true";
    }

    public String redirOtroAntecedente() {
        return "otro_antecedente.xhtml?faces-redirect=true";
    }

    public String redirDiagnostico() {
        return "diagnostico.xhtml?faces-redirect=true";
    }

    public String redirVivienda() {
        return "vivienda.xhtml?faces-redirect=true";
    }

    //
}
