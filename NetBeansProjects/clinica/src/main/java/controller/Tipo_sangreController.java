/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.Tipo_sangreFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Tipo_sangre;
import util.Utilidades;

/**
 *
 * @author douglas.ramirezusam
 */
@ManagedBean
@SessionScoped
public class Tipo_sangreController implements Serializable {

    @EJB
    private Tipo_sangreFacadeLocal tipo_sangre_facade;
    private List<Tipo_sangre> lista_de_tipo_sangre;
    private Tipo_sangre tipo_sangre;
    private Utilidades utilidades;

    public List<Tipo_sangre> getLista_de_tipo_sangre() {
        return lista_de_tipo_sangre;
    }

    public void setLista_de_tipo_sangre(List<Tipo_sangre> lista_de_tipo_sangre) {
        this.lista_de_tipo_sangre = lista_de_tipo_sangre;
    }

    public Tipo_sangre getTipo_sangre() {
        return tipo_sangre;
    }

    public void setTipo_sangre(Tipo_sangre tipo_sangre) {
        this.tipo_sangre = tipo_sangre;
    }

    @PostConstruct
    public void init() {
        tipo_sangre = new Tipo_sangre();
        utilidades = new Utilidades();
        listar ();
    }

    public void guardar() {
        try {
            tipo_sangre_facade.create(tipo_sangre);
            lista_de_tipo_sangre = tipo_sangre_facade.findAll();
            this.limpiar();
            if (tipo_sangre != null) {
                utilidades.ponerMensaje("Registro fue guardado");
            }
        } catch (Exception e) {
            utilidades.ponerMensaje("Registro no fue guardado " + e);
        }
    }

    public void listar() {
        try {
            lista_de_tipo_sangre = tipo_sangre_facade.findAll();
        } catch (Exception e) {
        }
    }

    public void eliminar(Tipo_sangre tip) {

        try {
            tipo_sangre_facade.remove(tip);
            lista_de_tipo_sangre = tipo_sangre_facade.findAll();
            if (tip != null) {
                utilidades.ponerMensaje("Registro fue eliminado ");
            }
        } catch (Exception e) {
            utilidades.ponerMensaje("Registro no fue eliminado " + e);
        }
    }

    public void modificar() {
        try {
            tipo_sangre_facade.edit(tipo_sangre);
            lista_de_tipo_sangre = tipo_sangre_facade.findAll();
            this.limpiar();
            if (tipo_sangre != null) {
                utilidades.ponerMensaje("Registro fue actualizado ");
            }
        } catch (Exception e) {
            utilidades.ponerMensaje("Registro no fue actualizado " + e);
        }
    }

    public void cargar(Tipo_sangre tip) {
        tipo_sangre = tip;
    }

    public void limpiar() {
        tipo_sangre = new Tipo_sangre();

    }

}
