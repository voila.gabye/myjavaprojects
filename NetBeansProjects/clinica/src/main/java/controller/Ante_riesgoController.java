package controller;

import ejb.Ante_riesgoFacadeLocal;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Ante_riesgo;
import util.Utilidades;

@ManagedBean(name = "ante_riesgoController")
@SessionScoped
public class Ante_riesgoController {

    @EJB
    private Ante_riesgoFacadeLocal ante_riesgoFacadeLocal;
    private Ante_riesgo ante_riesgo;
    private List<Ante_riesgo> ante_riesgoList;
    private Utilidades util;

    public Ante_riesgo getAnte_riesgo() {
        return ante_riesgo;
    }

    public void setAnte_riesgo(Ante_riesgo ante_riesgo) {
        this.ante_riesgo = ante_riesgo;
    }

    public List<Ante_riesgo> getAnte_riesgoList() {
        return ante_riesgoList;
    }

    public void setAnte_riesgoList(List<Ante_riesgo> ante_riesgoList) {
        this.ante_riesgoList = ante_riesgoList;
    }

    @PostConstruct
    public void init() {
        ante_riesgo = new Ante_riesgo();
        util = new Utilidades();
        consultar();
    }

    //CRUD
    public void insertar() {
        try {
            ante_riesgoFacadeLocal.create(ante_riesgo);
            util.ponerMensaje("Antecedente de riesgo registrado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: "+e.getMessage());
        }
    }
    public void editar(){
        try {
            ante_riesgoFacadeLocal.edit(ante_riesgo);
            util.ponerMensaje("Antecedente de riesgo editado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: "+e.getMessage());
        }
    }
    public void eliminar(Ante_riesgo ante){
        try {
            ante_riesgoFacadeLocal.remove(ante);
            util.ponerMensaje("Antecedente de riesgo eliminado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: "+e.getMessage());
        }
    }
    public void consultar(){
        ante_riesgoList=ante_riesgoFacadeLocal.findAll();
    }
    
    //Consultar por Id
    public void consultarById(Ante_riesgo ante){
        ante_riesgo=ante;
    }
}
