package controller;

import ejb.UsuarioFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import modelo.Usuario;
import util.Utilidades;

@ManagedBean
@SessionScoped
public class UsuarioController implements Serializable {

    @EJB
    private UsuarioFacadeLocal usuario_facade;
    private List<Usuario> lista_usuario;
    private Usuario usuario;
    private Usuario usuario1;
    private Utilidades utilidades;

    public Usuario getUsuario1() {
        return usuario1;
    }

    public void setUsuario1(Usuario usuario1) {
        this.usuario1 = usuario1;
    }

    public List<Usuario> getLista_usuario() {
        return lista_usuario;
    }

    public void setLista_usuario(List<Usuario> lista_usuario) {
        this.lista_usuario = lista_usuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @PostConstruct
    public void init() {
        usuario = new Usuario();
        usuario1 = new Usuario();
        utilidades = new Utilidades();
        listar();
    }

    public void guardar() {
        try {
            usuario_facade.create(usuario);
            lista_usuario = usuario_facade.findAll();
            utilidades.ponerMensaje("Usuario registrado");
            this.limpiar();
        } catch (Exception e) {
        }
    }

    public void listar() {
        try {
            lista_usuario = usuario_facade.findAll();
        } catch (Exception e) {
        }
    }

    public void eliminar(Usuario usu) {

        try {
            usuario_facade.remove(usu);
            utilidades.ponerMensaje("Usuario eliminado");
            lista_usuario = usuario_facade.findAll();
        } catch (Exception e) {
        }
    }

    public void modificar() {
        try {
            usuario_facade.edit(usuario);
            utilidades.ponerMensaje("Usuario actualizado");
            lista_usuario = usuario_facade.findAll();
            this.limpiar();
        } catch (Exception e) {
        }

    }

    public void cargar(Usuario usu) {
        usuario = usu;
    }

    public void limpiar() {
        usuario = new Usuario();
    }

    public String login() {
        String ruta = null;
        try {
            usuario1 = usuario_facade.login(usuario);
            usuario = new Usuario();

            if (usuario1 == null) {
                utilidades.ponerMensaje("Credenciales invalidas");
                init();
            } else {
                if ("A".equals(usuario1.getTipo())) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", usuario1.getUsuario());
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("tipo", usuario1.getTipo());
                    ruta = "padre.xhtml?faces-redirect=true";
                } else {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("tipo", usuario1.getTipo());
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", usuario1.getUsuario());
                    ruta = "participante.xhtml?faces-redirect=true";
                }
            }
        } catch (Exception e) {
        }
        return ruta;
    }

    public String salir() {

        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "index.xhtml?faces-redirect=true";
    }

    public void verificarSesion() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            Usuario us = (Usuario) context.getExternalContext().getSessionMap().get("user");
            if (us == null) {
                context.getExternalContext().redirect("index.xhtml");

            }
        } catch (Exception e) {
        }
    }

    public String inicio() {
        String ruta = "null";

        if (usuario1.getTipo().equals("A")) {
            ruta = "padre.xhtml?faces-redirect=true";
        } else {
            ruta = "participante.xhtml?faces-redirect=true";
        }
        return ruta;
    }

    public String registroA() {
        String ruta = "null";

        if (usuario1.getTipo().equals("A")) {
            ruta = "registroAlumno.xhtml";
        } else {
            ruta = "registroAlumno.xhtml";
        }
        return ruta;
    }
}
