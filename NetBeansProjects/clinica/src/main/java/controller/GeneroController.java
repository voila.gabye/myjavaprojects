package controller;

import ejb.GeneroFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import modelo.Genero;

@ManagedBean(name = "generoController")
@SessionScoped
public class GeneroController implements Serializable{

    @EJB
    private GeneroFacadeLocal facadeLocal;
    private Genero genero;
    private List<Genero> lista;

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public List<Genero> getLista() {
        return lista;
    }

    public void setLista(List<Genero> lista) {
        this.lista = lista;
    }

    @PostConstruct
    public void init() {
        genero = new Genero();
        consultar();
    }

    public void consultar() {
        try {
            lista = facadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void insertar() {
        try {
            facadeLocal.create(genero);
            if (genero != null) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Registro Guardado "));
                init();
            }
            init();
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Error " + e.getMessage()));
        }
    }

    public void editar() {
        try {
            facadeLocal.edit(genero);
            if (genero != null) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Registro Actualizado "));
                init();
            }
            init();
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Error " + e.getMessage()));
        }

    }

    public void eliminar(Genero genero) {
        this.genero = genero;
        try {
            facadeLocal.remove(genero);
        } catch (Exception e) {
        }
    }

    public void consultarById(Genero genero) {
        this.genero = genero;
    }
}
