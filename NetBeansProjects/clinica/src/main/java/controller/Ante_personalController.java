package controller;

import ejb.Ante_personalFacadeLocal;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Ante_personal;
import util.Utilidades;

@ManagedBean(name = "ante_personalController")
@SessionScoped
public class Ante_personalController {

    @EJB
    private Ante_personalFacadeLocal ante_personalFacadeLocal;
    private Ante_personal ante_personal;
    private List<Ante_personal> ante_personalList;
    private Utilidades util;

    public Ante_personal getAnte_personal() {
        return ante_personal;
    }

    public void setAnte_personal(Ante_personal ante_personal) {
        this.ante_personal = ante_personal;
    }

    public List<Ante_personal> getAnte_personalList() {
        return ante_personalList;
    }

    public void setAnte_personalList(List<Ante_personal> ante_personalList) {
        this.ante_personalList = ante_personalList;
    }

    @PostConstruct
    public void init() {
        ante_personal = new Ante_personal();
        util = new Utilidades();
        consultar();
    }

    //CRUD
    public void insertar() {
        try {
            ante_personalFacadeLocal.create(ante_personal);
            util.ponerMensaje("Antecedente personal registrado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: "+e.getMessage());
        }
    }
    public void editar(){
        try {
            ante_personalFacadeLocal.edit(ante_personal);
            util.ponerMensaje("Anetecedente personal editado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: "+e.getMessage());
        }
    }
    public void eliminar(Ante_personal ante){
        try {
            ante_personalFacadeLocal.remove(ante);
            util.ponerMensaje("Antecedente personal eliminado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: "+e.getMessage());
        }
    }
    public void consultar(){
        ante_personalList=ante_personalFacadeLocal.findAll();
    }
    
    //Consultar por Id
    public void consultarById(Ante_personal ante){
        ante_personal=ante;
    }
}
