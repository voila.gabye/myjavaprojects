package controller;

import ejb.Tipo_AguaFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import modelo.Tipo_Agua;

@ManagedBean(name = "tipo_aguaController")
@SessionScoped
public class Tipo_AguaController implements Serializable {

    @EJB
    private Tipo_AguaFacadeLocal facadeLocal;
    private Tipo_Agua tipo_agua;
    private List<Tipo_Agua> lista;

    public Tipo_Agua getTipo_agua() {
        return tipo_agua;
    }

    public void setTipo_agua(Tipo_Agua tipo_agua) {
        this.tipo_agua = tipo_agua;
    }

    public List<Tipo_Agua> getLista() {
        return lista;
    }

    public void setLista(List<Tipo_Agua> lista) {
        this.lista = lista;
    }

    @PostConstruct
    public void init() {
        tipo_agua = new Tipo_Agua();
        consultar();
    }

    public void consultar() {
        try {
            lista = facadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void insertar() {
        try {
            facadeLocal.create(tipo_agua);
            if (tipo_agua != null) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Registro Guardado "));
                init();
            }
            init();
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Error " + e.getMessage()));
        }
    }

    public void editar() {
        try {
            facadeLocal.edit(tipo_agua);
            if (tipo_agua != null) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Registro Actualizado "));
                init();
            }
            init();
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Error " + e.getMessage()));
        }
    }

    public void eliminar(Tipo_Agua tipo_agua) {
        this.tipo_agua = tipo_agua;
        try {
            facadeLocal.remove(tipo_agua);
        } catch (Exception e) {
        }
    }

    public void consultarById(Tipo_Agua tipo_agua) {
        this.tipo_agua = tipo_agua;
    }
}
