package controller;

import ejb.Ante_alumnoFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import modelo.Alumno;
import modelo.Ante_alumno;
import modelo.Ante_personal;
import modelo.Carrera;
import modelo.Facultad;
import modelo.Genero;
import modelo.Municipio;
import org.primefaces.PrimeFaces;
import util.Utilidades;

@ManagedBean
@SessionScoped
public class Ante_alumnoController implements Serializable {

    @ManagedProperty("#{alumnoController}")
    private AlumnoController alumnoController;
    @EJB
    private Ante_alumnoFacadeLocal ante_alumnoEJB;
    private Ante_alumno ante_alumno;
    private List<Ante_alumno> listaAnte_alumno;
    private Alumno alumno;
    private Ante_personal ante_persona, ante_personal;
    private Utilidades util;
    private Genero genero;
    private Carrera carrera;
    private Municipio municipio;
    private String[] manyChecbox;
    private Facultad facultad;
    private String acuerdo;

    private boolean controlCheck = false;

    public Genero getGenero() {
        return genero;
    }

    public Ante_personal getAnte_personal() {
        return ante_personal;
    }

    public void setAnte_personal(Ante_personal ante_personal) {
        this.ante_personal = ante_personal;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public Carrera getCarrera() {
        return carrera;
    }

    public void setCarrera(Carrera carrera) {
        this.carrera = carrera;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public AlumnoController getAlumnoController() {
        return alumnoController;
    }

    public void setAlumnoController(AlumnoController alumnoController) {
        this.alumnoController = alumnoController;
    }

    public Ante_alumno getAnte_alumno() {
        return ante_alumno;
    }

    public void setAnte_alumno(Ante_alumno ante_alumno) {
        this.ante_alumno = ante_alumno;
    }

    public List<Ante_alumno> getListaAnte_alumno() {
        return listaAnte_alumno;
    }

    public void setListaAnte_alumno(List<Ante_alumno> listaAnte_alumno) {
        this.listaAnte_alumno = listaAnte_alumno;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Ante_personal getAnte_persona() {
        return ante_persona;
    }

    public void setAnte_persona(Ante_personal ante_persona) {
        this.ante_persona = ante_persona;
    }

    public String[] getManyChecbox() {
        return manyChecbox;
    }

    public void setManyChecbox(String[] manyChecbox) {
        this.manyChecbox = manyChecbox;
    }

    public Facultad getFacultad() {
        return facultad;
    }

    public void setFacultad(Facultad facultad) {
        this.facultad = facultad;
    }

    public String getAcuerdo() {
        return acuerdo;
    }

    public void setAcuerdo(String acuerdo) {
        this.acuerdo = acuerdo;
    }
    

    @PostConstruct
    public void init() {
        ante_alumno = new Ante_alumno();
        alumno = new Alumno();
        ante_persona = new Ante_personal();
        carrera = new Carrera();
        genero = new Genero();
        municipio = new Municipio();
        util = new Utilidades();
        manyChecbox = new String[]{};
        facultad = new Facultad();
        ante_personal = new Ante_personal();
    }

    public boolean isControlCheck() {
        return controlCheck;
    }

    public void setControlCheck(boolean controlCheck) {
        this.controlCheck = controlCheck;
    }

    public void insertar() {
        try {

            String des2 = ante_alumno.getDescripcion();
            for (String many : manyChecbox) {
                int io = Integer.parseInt(many);
                switch (io) {

                    case 9:
                        ante_persona.setId(io);
                        ante_alumno.setAnte_personal(ante_persona);
                        alumno.setId(1);
                        ante_alumno.setAlumno(alumno);
                        ante_alumno.setDescripcion(des2);
                        ante_alumnoEJB.create(ante_alumno);
                        break;
                    default:
                        ante_persona.setId(io);
                        ante_alumno.setAnte_personal(ante_persona);
                        alumno.setId(1);
                        ante_alumno.setAlumno(alumno);
                        ante_alumno.setDescripcion(null);
                        ante_alumno.setAnte_personal(ante_persona);
                        ante_alumnoEJB.create(ante_alumno);
                        break;
                }
            }
            anteAlumnoXAlumno(ante_alumno.getAlumno());
            util.ponerMensaje("Antecedente personal registrado");
            init();
        } catch (Exception e) {

            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void editar() {
        try {
            ante_alumno.setAlumno(alumno);
            ante_alumnoEJB.create(ante_alumno);
            util.ponerMensaje("Antecedente personal editado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void eliminar(Ante_alumno ata) {
        this.ante_alumno = ata;
        try {
            ante_alumnoEJB.create(ante_alumno);
            util.ponerMensaje("Antecedente personal eliminado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void consultar() {

        listaAnte_alumno = ante_alumnoEJB.findAll();
    }

    public void consultarById(Ante_alumno ata) {
        this.ante_alumno = ata;
    }

    public void listaGenero() {
        try {
            listaAnte_alumno = ante_alumnoEJB.listaGenero(genero.getId());
            System.out.println("Lista" + listaAnte_alumno.get(0));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void listaCarrera() {
        try {
            listaAnte_alumno = ante_alumnoEJB.listaCarrera(carrera.getId());
            System.out.println("Lista" + listaAnte_alumno.get(0));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void listaMunicipio() {
        try {
            listaAnte_alumno = ante_alumnoEJB.listaMunicipio(this.municipio.getId());
            System.out.println("Lista" + listaAnte_alumno.get(0));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void anteAlumnoXAlumno(Alumno alumno) {
        try {
            listaAnte_alumno = ante_alumnoEJB.anteAlumnoXAlumno(alumno);
        } catch (Exception e) {
        }
    }

    public void ajaxChecBox() {
        try {

            for (String ma : manyChecbox) {
                int i = Integer.parseInt(ma);
                switch (i) {
                    case 9:
                        controlCheck = true;
                        PrimeFaces.current().ajax().update("datos:descrip");
                        break;
                    default:
                        ante_alumno.getDescripcion();
                }

            }
        } catch (Exception e) {
        }
    }

   
    public void findXenfermedadYcarrera() {
        try {
            alumno.setCarrera(carrera);
            ante_alumno.setAlumno(alumno);
            ante_alumno.setAnte_personal(ante_persona);
            listaAnte_alumno = ante_alumnoEJB.findXemfermedadYcarrera(ante_alumno,acuerdo);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void findXenfermedadYfacultad() {
        try {
            listaAnte_alumno = ante_alumnoEJB.findXemfermedadYfacultad(ante_personal);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public String detalles(Ante_alumno ante_alumno) {
        alumno= ante_alumno.getAlumno();
        return "detalles_2.xhtml?faces-redirect=true";

    }

}
