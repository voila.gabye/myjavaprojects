package controller;

import ejb.Tipo_TechoFacadeLocal;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Tipo_Techo;
import util.Utilidades;

@ManagedBean
@SessionScoped
public class Tipo_TechoController {

    @EJB
    private Tipo_TechoFacadeLocal facadeLocal;
    private Tipo_Techo tipo_Techo;
    private List<Tipo_Techo> lista;
    private Utilidades utilidades;

    public Tipo_Techo getTipo_Techo() {
        return tipo_Techo;
    }

    public void setTipo_Techo(Tipo_Techo tipo_Techo) {
        this.tipo_Techo = tipo_Techo;
    }

    public List<Tipo_Techo> getLista() {
        return lista;
    }

    public void setLista(List<Tipo_Techo> lista) {
        this.lista = lista;
    }

    @PostConstruct
    public void init() {
        tipo_Techo = new Tipo_Techo();
        utilidades = new Utilidades();
        consultar();
    }

    //inicio metodos basicos
    public void consultar() {
        try {
            lista = facadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void insertar() {
        try {

            facadeLocal.create(tipo_Techo);
            utilidades.ponerMensaje("Tipo Techo Guardado");

            init();
        } catch (Exception e) {
            utilidades.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void editar() {
        try {

            facadeLocal.edit(tipo_Techo);
            utilidades.ponerMensaje("Tipo Techo Actualizado");

            init();
        } catch (Exception e) {
            utilidades.ponerMensaje("Error al actualizar" + e.getMessage());
        }
    }

    public void eliminar(Tipo_Techo tipo_Techo) {
        this.tipo_Techo = tipo_Techo;
        try {
            facadeLocal.remove(tipo_Techo);
            utilidades.ponerMensaje("Tipo Techo Eliminado");
            init();
        } catch (Exception e) {
            utilidades.ponerMensaje("Error " + e.getMessage());
        }
    }

    public void consultarById(Tipo_Techo tipo_Techo) {
        this.tipo_Techo = tipo_Techo;
    }
    // fin metodos basicos
}
