/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.Ante_alumnoFacadeLocal;
import ejb.DiagnosticoFacadeLocal;
import ejb.Ecologico_MascotaFacadeLocal;
import ejb.Ecologico_SocialFacadeLocal;
import ejb.Heredo_alumnoFacadeLocal;
import ejb.Otro_AntecedenteFacadeLocal;
import ejb.Riesgo_AlumnoFacadeLocal;
import ejb.ViviendaFacadeLocal;
import ejb.AlumnoFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import modelo.Alumno;
import modelo.Ante_alumno;
import modelo.Diagnostico;
import modelo.Ecologico_Mascota;
import modelo.Ecologico_Social;
import modelo.Heredo_alumno;
import modelo.Otro_Antecedente;
import modelo.Riesgo_Alumno;
import modelo.Vivienda;

@ManagedBean
@ViewScoped
public class DetallesController implements Serializable {
    
    @ManagedProperty("#{registroAlumnoController}")
    private RegistroAlumnoController registroAlumnoController;
    @EJB
    private AlumnoFacadeLocal alumnoFacadeLocal;
    private List<Alumno> listAlumno;

    @EJB
    private Heredo_alumnoFacadeLocal heredo_alumnoFacadeLocal;
    private List<Heredo_alumno> listHeredo_alumno;
    private Alumno alumno;

    @EJB
    private Ante_alumnoFacadeLocal ante_alumnoFacadeLocal;
    private List<Ante_alumno> listAnte_alumno;

    @EJB
    private Ecologico_SocialFacadeLocal ecologico_SocialFacadeLocal;
    private List<Ecologico_Social> listEcologico_Social;

    @EJB
    private Ecologico_MascotaFacadeLocal ecologico_MascotaFacadeLocal;
    private List<Ecologico_Mascota> listEcologico_Mascota;

    @EJB
    private ViviendaFacadeLocal viviendaFacadeLocal;
    private List<Vivienda> listVivienda;

    @EJB
    private Otro_AntecedenteFacadeLocal otro_AntecedenteFacadeLocal;
    private List<Otro_Antecedente> listOtro_Antecedente;

    @EJB
    private DiagnosticoFacadeLocal diagnosticoFacadeLocal;
    private List<Diagnostico> listDiagnostico;

    @EJB
    private Riesgo_AlumnoFacadeLocal riesgo_AlumnoFacadeLocal;
    private List<Riesgo_Alumno> listriesgo_Alumno;

    public List<Diagnostico> getListDiagnostico() {
        return listDiagnostico;
    }

    public void setListDiagnostico(List<Diagnostico> listDiagnostico) {
        this.listDiagnostico = listDiagnostico;
    }

    public List<Otro_Antecedente> getListOtro_Antecedente() {
        return listOtro_Antecedente;
    }

    public void setListOtro_Antecedente(List<Otro_Antecedente> listOtro_Antecedente) {
        this.listOtro_Antecedente = listOtro_Antecedente;
    }

    public List<Vivienda> getListVivienda() {
        return listVivienda;
    }

    public void setListVivienda(List<Vivienda> listVivienda) {
        this.listVivienda = listVivienda;
    }

    public List<Ecologico_Mascota> getListEcologico_Mascota() {
        return listEcologico_Mascota;
    }

    public void setListEcologico_Mascota(List<Ecologico_Mascota> listEcologico_Mascota) {
        this.listEcologico_Mascota = listEcologico_Mascota;
    }

    public List<Ecologico_Social> getListEcologico_Social() {
        return listEcologico_Social;
    }

    public void setListEcologico_Social(List<Ecologico_Social> listEcologico_Social) {
        this.listEcologico_Social = listEcologico_Social;
    }

    public List<Ante_alumno> getListAnte_alumno() {
        return listAnte_alumno;
    }

    public void setListAnte_alumno(List<Ante_alumno> listAnte_alumno) {
        this.listAnte_alumno = listAnte_alumno;
    }

    public RegistroAlumnoController getRegistroAlumnoController() {
        return registroAlumnoController;
    }

    public void setRegistroAlumnoController(RegistroAlumnoController registroAlumnoController) {
        this.registroAlumnoController = registroAlumnoController;
    }

    public List<Heredo_alumno> getListHeredo_alumno() {
        return listHeredo_alumno;
    }

    public void setListHeredo_alumno(List<Heredo_alumno> listHeredo_alumno) {
        this.listHeredo_alumno = listHeredo_alumno;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public List<Alumno> getListAlumno() {
        return listAlumno;
    }

    public void setListAlumno(List<Alumno> listAlumno) {
        this.listAlumno = listAlumno;
    }

    public List<Riesgo_Alumno> getListriesgo_Alumno() {
        return listriesgo_Alumno;
    }

    public void setListriesgo_Alumno(List<Riesgo_Alumno> listriesgo_Alumno) {
        this.listriesgo_Alumno = listriesgo_Alumno;
    }

    @PostConstruct
    public void init() {
        alumno = new Alumno();
        antecedentesHeredoFamiliares();
        antecedentesAnteAlumno();
        ecologicoSocial();
        ecologicoMascota();
        vivienda();
        findAlumno();
        otroAntecedente();
        findRiesgos();
        findDiagnostico();
    }

    public void antecedentesHeredoFamiliares() {
        try {
           
                alumno = registroAlumnoController.getAlumnoRec();
            listHeredo_alumno = heredo_alumnoFacadeLocal.consultarHeredo_alumnoXalumno(alumno);
            alumno = new Alumno();
        } catch (Exception e) {
        }

    }

    public void antecedentesAnteAlumno() {
        try {
            alumno = registroAlumnoController.getAlumnoRec();
            listAnte_alumno = ante_alumnoFacadeLocal.anteAlumnoXAlumno(alumno);
            alumno = new Alumno();
        } catch (Exception e) {
        }
    }

    public void ecologicoSocial() {
        try {
            
            alumno = registroAlumnoController.getAlumnoRec();
            listEcologico_Social = ecologico_SocialFacadeLocal.findXAlumno(alumno);
            alumno = new Alumno();
        } catch (Exception e) {
        }
    }

    public void ecologicoMascota() {
        try {
             alumno = registroAlumnoController.getAlumnoRec();
            listEcologico_Mascota = ecologico_MascotaFacadeLocal.ecoMascotaByAlumno(alumno);
            alumno = new Alumno();
        } catch (Exception e) {
        }
    }

    public void vivienda() {
        try {
             alumno = registroAlumnoController.getAlumnoRec();
            listVivienda = viviendaFacadeLocal.findVivienda(alumno);
            alumno = new Alumno();
        } catch (Exception e) {
        }
    }

    public void otroAntecedente() {
        try {
             alumno = registroAlumnoController.getAlumnoRec();
            listOtro_Antecedente = otro_AntecedenteFacadeLocal.otro_AntecedenteXAlumno(alumno);
            alumno = new Alumno();
        } catch (Exception e) {
        }
    }

    public void findAlumno() {
        try {
             alumno = registroAlumnoController.getAlumnoRec();
            listAlumno = alumnoFacadeLocal.findId(alumno);
            alumno = new Alumno();
        } catch (Exception e) {
        }
    }

    public void findDiagnostico() {
         alumno = registroAlumnoController.getAlumnoRec();
        listDiagnostico = diagnosticoFacadeLocal.diagnosticoXAlumno(alumno);
        alumno = new Alumno();
    }

    public void findRiesgos() {
        try {
             alumno = registroAlumnoController.getAlumnoRec();
            listriesgo_Alumno = riesgo_AlumnoFacadeLocal.riesgoxAlumno(alumno);
            alumno = new Alumno();
        } catch (Exception e) {
        }
    }
}
