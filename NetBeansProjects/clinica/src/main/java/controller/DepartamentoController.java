
package controller;

import ejb.DepartamentoFacadeLocal;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Departamento;
import util.Utilidades;


@ManagedBean(name = "departamentoController")
@SessionScoped
public class DepartamentoController {
    
    @EJB
    private DepartamentoFacadeLocal facadeLocal;
    private Departamento departamento;
    private Utilidades utilidades;
    private List<Departamento> lista;

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public List<Departamento> getLista() {
        return lista;
    }

    public void setLista(List<Departamento> lista) {
        this.lista = lista;
    }
    
    @PostConstruct
    public void init()
    {
        departamento=new Departamento();
        utilidades=new Utilidades();
        consultar();
    }
    
    //inicio metodos basicos
    public void consultar()
    {
        try {
            lista=facadeLocal.findAll();
        } catch (Exception e) {
        }
    }
    public void insertar()
    {
        try {
            
            facadeLocal.create(departamento);
            utilidades.ponerMensaje("Departamento Guardado");
            
            
            init();
        } catch (Exception e) {
            utilidades.ponerMensaje("Error: "+e.getMessage());
        }
    }
    public void editar()
    {
        try {
            
            facadeLocal.edit(departamento);
            utilidades.ponerMensaje("Departamento Actualizado");
            
            init();
        } catch (Exception e) {
            utilidades.ponerMensaje("Error al actualizar"+e.getMessage());
        }
    }
    public void eliminar(Departamento departamento)
    {
        this.departamento=departamento;
        try {
            facadeLocal.remove(departamento);
            utilidades.ponerMensaje("Departamento Eliminado");
            init();
        } catch (Exception e) {
            utilidades.ponerMensaje("Error "+e.getMessage());
        }
    }
    public void consultarById(Departamento departamento)
    {
        this.departamento=departamento;
    }
    // fin metodos basicos
}
