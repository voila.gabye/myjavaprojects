package controller;

import ejb.Riesgo_AlumnoFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Alumno;
import modelo.Riesgo_Alumno;
import org.primefaces.PrimeFaces;

@ManagedBean
@SessionScoped
public class Riesgo_AlumnoController implements Serializable {

    @EJB
    private Riesgo_AlumnoFacadeLocal riesgo_AlumnoFacadeLocal;
    private Riesgo_Alumno riesgo_Alumno;
    private List<Riesgo_Alumno> listRiesgo_Alumno;
    private Alumno alumno;

    public Riesgo_Alumno getRiesgo_Alumno() {
        return riesgo_Alumno;
    }

    public void setRiesgo_Alumno(Riesgo_Alumno riesgo_Alumno) {
        this.riesgo_Alumno = riesgo_Alumno;
    }

    public List<Riesgo_Alumno> getListRiesgo_Alumno() {
        return listRiesgo_Alumno;
    }

    public void setListRiesgo_Alumno(List<Riesgo_Alumno> listRiesgo_Alumno) {
        this.listRiesgo_Alumno = listRiesgo_Alumno;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    @PostConstruct
    public void init() {
        alumno = new Alumno();
        riesgo_Alumno = new Riesgo_Alumno();
        consultar();
    }

    public void insertar() {
        try {
            alumno.setId(1);
            riesgo_Alumno.setAlumno(alumno);
            riesgo_AlumnoFacadeLocal.create(riesgo_Alumno);
            init();
        } catch (Exception e) {
            init();
            System.out.println("Error " + e.getMessage());
        }
    }

    public void actualizar() {
        try {
            riesgo_Alumno.setAlumno(alumno);
            riesgo_AlumnoFacadeLocal.edit(riesgo_Alumno);
        } catch (Exception e) {
        }
    }

    public void eliminar(Riesgo_Alumno riesgo_Alumno) {
        try {
            this.riesgo_Alumno = riesgo_Alumno;
            riesgo_AlumnoFacadeLocal.remove(riesgo_Alumno);
        } catch (Exception e) {
        }
    }

    public void consultarById(Riesgo_Alumno riesgo_Alumno) {
        this.riesgo_Alumno = riesgo_Alumno;
        alumno.setId(riesgo_Alumno.getAlumno().getId());

    }

    public void consultar() {
        try {
            listRiesgo_Alumno = riesgo_AlumnoFacadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public boolean cigarros = false, alcho = false;

    public boolean isAlcho() {
        return alcho;
    }

    public void setAlcho(boolean alcho) {
        this.alcho = alcho;
    }

    public boolean isCigarros() {
        return cigarros;
    }

    public void setCigarros(boolean cigarros) {
        this.cigarros = cigarros;
    }
    private boolean taxi = false;

    public boolean isTaxi() {
        return taxi;
    }

    public void setTaxi(boolean taxi) {
        this.taxi = taxi;
    }
    private boolean violencia = false;

    public boolean isViolencia() {
        return violencia;
    }

    public void setViolencia(boolean violencia) {
        this.violencia = violencia;
    }

    public void ajaxViolencia() {
        if ("Si".equals(riesgo_Alumno.getViolencia())) {
            violencia = true;
            PrimeFaces.current().ajax().update("formRiesgo:violencia");
        } else {
            violencia = false;
            PrimeFaces.current().ajax().update("formRiesgo:violencia");
        }
    }

    public void ajaxToxicomania() {
        if ("Si".equals(riesgo_Alumno.getToxicomonia())) {
            taxi = true;
            PrimeFaces.current().ajax().update("formRiesgo:tipotaxi");
            PrimeFaces.current().ajax().update("formRiesgo:taxi");
        } else {
            taxi = false;
            PrimeFaces.current().ajax().update("formRiesgo:tipotaxi");
            PrimeFaces.current().ajax().update("formRiesgo:taxi");
        }
    }

    public void ajaxAlchoholismo() {
        if ("Si".equals(riesgo_Alumno.getAlcoholismo())) {
            alcho = true;
            PrimeFaces.current().ajax().update("formRiesgo:alcho");
        } else {
            alcho = false;
            PrimeFaces.current().ajax().update("formRiesgo:alcho");
        }
    }

    public void ajaxTabaquismo() {
        if ("Si".equals(riesgo_Alumno.getTabaquismo())) {
            cigarros = true;
            PrimeFaces.current().ajax().update("formRiesgo:ncigarro");
        } else {
            cigarros = false;
            PrimeFaces.current().ajax().update("formRiesgo:ncigarro");
        }
    }

}
