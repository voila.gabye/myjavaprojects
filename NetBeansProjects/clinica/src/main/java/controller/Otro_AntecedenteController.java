package controller;

import ejb.Otro_AntecedenteFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import modelo.Alumno;
import modelo.Carrera;
import modelo.Genero;
import modelo.Metodo_anti;
import modelo.Municipio;
import modelo.Otro_Antecedente;
import modelo.Tipo_sangre;

@ManagedBean(name = "otro_antecedenteController")
@SessionScoped
public class Otro_AntecedenteController implements Serializable {

    @ManagedProperty("#{alumnoController}")
    private AlumnoController alumnoController;

    @EJB
    private Otro_AntecedenteFacadeLocal facadeLocal;
    private Otro_Antecedente otro_antecedente;
    private List<Otro_Antecedente> lista;
    private Tipo_sangre tipo_sangre;
    private Metodo_anti metodo_anti;
    private Alumno alumno;
    private Genero genero;
    private Carrera carrera;
    private Municipio municipio;

    public Otro_Antecedente getOtro_antecedente() {
        return otro_antecedente;
    }

    public void setOtro_antecedente(Otro_Antecedente otro_antecedente) {
        this.otro_antecedente = otro_antecedente;
    }

    public List<Otro_Antecedente> getLista() {
        return lista;
    }

    public void setLista(List<Otro_Antecedente> lista) {
        this.lista = lista;
    }

    public Tipo_sangre getTipo_sangre() {
        return tipo_sangre;
    }

    public void setTipo_sangre(Tipo_sangre tipo_sangre) {
        this.tipo_sangre = tipo_sangre;
    }

    public Metodo_anti getMetodo_anti() {
        return metodo_anti;
    }

    public void setMetodo_anti(Metodo_anti metodo_anti) {
        this.metodo_anti = metodo_anti;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public AlumnoController getAlumnoController() {
        return alumnoController;
    }

    public void setAlumnoController(AlumnoController alumnoController) {
        this.alumnoController = alumnoController;
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public Carrera getCarrera() {
        return carrera;
    }

    public void setCarrera(Carrera carrera) {
        this.carrera = carrera;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @PostConstruct
    public void init() {
        otro_antecedente = new Otro_Antecedente();
        tipo_sangre = new Tipo_sangre();
        metodo_anti = new Metodo_anti();
        alumno = new Alumno();
        genero = new Genero();
        carrera = new Carrera();
        municipio = new Municipio();
        consultar();
    }

    public void consultar() {
        try {
            lista = facadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void insertar() {
        try {
            this.otro_antecedente.setTipo_sangre(tipo_sangre);
            this.otro_antecedente.setMetodo_anti(metodo_anti);
            this.otro_antecedente.setAlumno(alumnoController.getAlumnoRec());
            facadeLocal.create(otro_antecedente);
            if (otro_antecedente != null) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Registro Guardado "));
                init();
            }
            init();
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Error " + e.getMessage()));
            init();
        }
    }

    public void editar() {
        try {
            this.otro_antecedente.setTipo_sangre(tipo_sangre);
            this.otro_antecedente.setMetodo_anti(metodo_anti);
            this.otro_antecedente.setAlumno(alumno);
            facadeLocal.edit(otro_antecedente);
            if (otro_antecedente != null) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Registro Actualizado "));
                init();
            }
            init();
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Error " + e.getMessage()));
        }
    }

    public void eliminar(Otro_Antecedente antecedente) {
        this.otro_antecedente = antecedente;
        try {
            facadeLocal.remove(otro_antecedente);
        } catch (Exception e) {
        }
    }

    public void consultarById(Otro_Antecedente antecedente) {
        this.otro_antecedente = antecedente;
        tipo_sangre.setId(antecedente.getTipo_sangre().getId());
        metodo_anti.setId(antecedente.getMetodo_anti().getId());
        alumno.setId(antecedente.getAlumno().getId());
        if("Si".equals(antecedente.getInmunizacion()))
        {
            antecedente.setInmunizacion("Si");
        }
        else
        {
            antecedente.setInmunizacion("No");
        }
    }

    public void listaGenero() {
        try {
            lista = facadeLocal.listaGenero(genero.getId());
            System.out.println("Lista" + lista.get(0));
        } catch (Exception e) {
        }
    }

    public void listaCarrera() {
        try {
            lista = facadeLocal.listaCarrera(carrera.getId());
            System.out.println("Lista" + lista.get(0));
        } catch (Exception e) {
        }
    }

    public void listaMunicipio() {
        try {

            lista = facadeLocal.listaMunicipio(municipio);
            System.out.println("Lista" + lista.get(0));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
