package controller;

import ejb.Metodo_antiFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Metodo_anti;
import util.Utilidades;

@ManagedBean
@SessionScoped
public class Metodo_antiController implements Serializable {

    @EJB
    private Metodo_antiFacadeLocal metodo_antiEJB;
    private List<Metodo_anti> listaMetodo_anti;
    private Metodo_anti metodo_anti;
    private Utilidades util;

    public List<Metodo_anti> getListaMetodo_anti() {
        return listaMetodo_anti;
    }

    public void setListaMetodo_anti(List<Metodo_anti> listaMetodo_anti) {
        this.listaMetodo_anti = listaMetodo_anti;
    }

    public Metodo_anti getMetodo_anti() {
        return metodo_anti;
    }

    public void setMetodo_anti(Metodo_anti metodo_anti) {
        this.metodo_anti = metodo_anti;
    }

    @PostConstruct
    public void init() {
        metodo_anti = new Metodo_anti();
        util = new Utilidades();
        consultar();
    }

    public void limpiar() {
        metodo_anti = new Metodo_anti();
    }

    public void insertar() {
        try {
            metodo_antiEJB.create(metodo_anti);
            util.ponerMensaje("Metodo anticonceptivo registrado");
            init();

        } catch (Exception e) {
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void editar() {
        try {
            metodo_antiEJB.edit(metodo_anti);
            util.ponerMensaje("Metodo anticonceptivo actualizado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Erro:" + e.getMessage());
        }
    }

    public void eliminar(Metodo_anti mta) {
        this.metodo_anti = mta;
        try {
            metodo_antiEJB.remove(metodo_anti);
            util.ponerMensaje("Metodo anticonceptivo eliminado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Erro:" + e.getMessage());
        }
    }

    public void consultar() {

        listaMetodo_anti = metodo_antiEJB.findAll();
    }

    public void consultarById(Metodo_anti mta) {
        this.metodo_anti = mta;
    }
}
