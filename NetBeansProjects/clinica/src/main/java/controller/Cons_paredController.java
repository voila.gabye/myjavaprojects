package controller;

import ejb.Cons_paredFacadeLocal;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Cons_pared;
import util.Utilidades;

@ManagedBean(name = "cons_paredController")
@SessionScoped
public class Cons_paredController {

    @EJB
    private Cons_paredFacadeLocal cons_paredEJB;
    private Cons_pared cons_pared;
    private List<Cons_pared> lista;
    private Utilidades utilidades;

    public Cons_pared getCons_pared() {
        return cons_pared;
    }

    public void setCons_pared(Cons_pared cons_pared) {
        this.cons_pared = cons_pared;
    }

    public List<Cons_pared> getLista() {
        return lista;
    }

    public void setLista(List<Cons_pared> lista) {
        this.lista = lista;
    }

    @PostConstruct
    public void init() {
        cons_pared = new Cons_pared();

        utilidades = new Utilidades();
        consultar();
    }

    public void guardar() {
        try {
            cons_paredEJB.create(cons_pared);
            if (cons_pared != null) {
                utilidades.ponerMensaje("Registro fue guardado");
                init();
            }
            init();
        } catch (Exception e) {
            utilidades.ponerMensaje("Registro no fue guardado " + e);
        }
    }

    public void editar() {
        try {
            cons_paredEJB.edit(cons_pared);
            if (cons_pared != null) {
                utilidades.ponerMensaje("Registro fue actualizado ");
                init();
            }
            init();
        } catch (Exception e) {
            utilidades.ponerMensaje("Registro no fue actualizado " + e);
        }
    }

    private void consultar() {
        try {
            lista = cons_paredEJB.findAll();
        } catch (Exception e) {
        }
    }

    public void consultarById(Cons_pared cons_pared) {
        this.cons_pared = cons_pared;
    }

    public void eliminar(Cons_pared cons_pared) {
        this.cons_pared = cons_pared;
        try {
            cons_paredEJB.remove(cons_pared);
            lista = cons_paredEJB.findAll();
            if (cons_pared != null) {
                utilidades.ponerMensaje("Registro fue eliminado ");
                init();
            }
            init();
        } catch (Exception e) {
            utilidades.ponerMensaje("Registro no fue eliminado " + e);
        }
    }

}
