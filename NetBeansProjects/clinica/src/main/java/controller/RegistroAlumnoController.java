package controller;

import ejb.Ante_alumnoFacadeLocal;
import ejb.CarreraFacadeLocal;
import ejb.DiagnosticoFacadeLocal;
import ejb.Ecologico_MascotaFacadeLocal;
import ejb.Ecologico_SocialFacadeLocal;
import ejb.Heredo_alumnoFacadeLocal;
import ejb.MunicipioFacadeLocal;
import ejb.Otro_AntecedenteFacadeLocal;
import ejb.Riesgo_AlumnoFacadeLocal;
import ejb.Signo_vitalFacadeLocal;
import ejb.ViviendaFacadeLocal;
import ejb.AlumnoFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Alumno;
import modelo.Ante_alumno;
import modelo.Ante_familiar;
import modelo.Ante_personal;
import modelo.Carrera;
import modelo.Cons_pared;
import modelo.Departamento;
import modelo.Diagnostico;
import modelo.Ecologico_Mascota;
import modelo.Ecologico_Social;
import modelo.Estado_civil;
import modelo.Facultad;
import modelo.Genero;
import modelo.Heredo_alumno;
import modelo.Impresion_diag;
import modelo.Mascota;
import modelo.Metodo_anti;
import modelo.Municipio;
import modelo.Otro_Antecedente;
import modelo.Riesgo_Alumno;
import modelo.Signo_vital;
import modelo.Tipo_Agua;
import modelo.Tipo_Techo;
import modelo.Tipo_cancer;
import modelo.Tipo_piso;
import modelo.Tipo_sangre;
import modelo.Usuario;
import modelo.Vivienda;
import org.primefaces.PrimeFaces;
import util.Utilidades;

@ManagedBean
@SessionScoped
public class RegistroAlumnoController implements Serializable {

    @EJB
    private AlumnoFacadeLocal alumnoFacadeLocal;
    private Alumno alumno;
    private Carrera carrera;
    private Genero genero;
    private Estado_civil estado_civil;
    private Municipio municipio;
    private Usuario usuario;
    private List<Alumno> alumnoList;
    private Utilidades util;
    private Alumno alumnoRec;
    private Departamento departamento;
    private Facultad facultad;
    private String findnombre,acuerdo;
    private int carnet;

    //facade Antecedentes heredofamiliares
    @EJB
    private MunicipioFacadeLocal municipioFacadeLocal;
    private List<Municipio> muniList;

    @EJB
    private CarreraFacadeLocal carreraFacadeLocal;
    private List<Carrera> carreList;

    @EJB
    private Heredo_alumnoFacadeLocal heredo_alumnoFacadeLocal;
    private Heredo_alumno heredo_alumno;
    private String[] selectAnteHeredo;
    private Tipo_cancer tipo_cancer;
    private Ante_familiar ante_familiar;
    private boolean esOtro = false;
    private boolean esCancer = false;

    @EJB
    private Ante_alumnoFacadeLocal ante_alumnoFacadeLocal;
    private Ante_alumno ante_alumno;
    private Ante_personal ante_personal;
    private boolean controlCheck = false;
    private boolean check = false;
    private String[] manyChecbox;

    @EJB
    private Ecologico_SocialFacadeLocal ecologico_SocialFacadeLocal;
    private Ecologico_Social ecologico_Social;
    private Tipo_Agua tipo_Agua;
    private Cons_pared cons_pared;
    private Tipo_Techo tipo_Techo;
    private Tipo_piso tipo_piso;

    @EJB
    private Ecologico_MascotaFacadeLocal ecologico_MascotaFacadeLocal;
    private Ecologico_Mascota ecologico_Mascota;
    private Mascota mascota;
    private String[] selectMascotas;
    private boolean des = false;

    @EJB
    private ViviendaFacadeLocal viviendaFacadeLocal;
    private Vivienda vivienda;

    @EJB
    private Otro_AntecedenteFacadeLocal otro_AntecedenteFacadeLocal;
    private Otro_Antecedente otro_Antecedente;
    private Tipo_sangre tipo_sangre;
    private Metodo_anti metodo_anti;
    private boolean menarca = true;
    private boolean fum = true;

    @EJB
    private DiagnosticoFacadeLocal diagnosticoFacadeLocal;
    private Diagnostico diagnostico;

    @EJB
    private Signo_vitalFacadeLocal signo_vitalFacadeLocal;
    private Signo_vital signo_vital;
    private Impresion_diag impresion_diag;

    @EJB
    private Riesgo_AlumnoFacadeLocal riesgo_AlumnoFacadeLocal;
    private Riesgo_Alumno riesgo_Alumno;
    private boolean alcho = false, taxi = false, cigarros = false, violencia = false;

    public boolean isAlcho() {
        return alcho;
    }

    public void setAlcho(boolean alcho) {
        this.alcho = alcho;
    }

    public boolean isTaxi() {
        return taxi;
    }

    public void setTaxi(boolean taxi) {
        this.taxi = taxi;
    }

    public boolean isCigarros() {
        return cigarros;
    }

    public void setCigarros(boolean cigarros) {
        this.cigarros = cigarros;
    }

    public String getAcuerdo() {
        return acuerdo;
    }

    public void setAcuerdo(String acuerdo) {
        this.acuerdo = acuerdo;
    }

    public boolean isViolencia() {
        return violencia;
    }

    public void setViolencia(boolean violencia) {
        this.violencia = violencia;
    }

    public Riesgo_Alumno getRiesgo_Alumno() {
        return riesgo_Alumno;
    }

    public void setRiesgo_Alumno(Riesgo_Alumno riesgo_Alumno) {
        this.riesgo_Alumno = riesgo_Alumno;
    }

    public Impresion_diag getImpresion_diag() {
        return impresion_diag;
    }

    public void setImpresion_diag(Impresion_diag impresion_diag) {
        this.impresion_diag = impresion_diag;
    }

    public Tipo_cancer getTipo_cancer() {
        return tipo_cancer;
    }

    public void setTipo_cancer(Tipo_cancer tipo_cancer) {
        this.tipo_cancer = tipo_cancer;
    }

    public String getFindnombre() {
        return findnombre;
    }

    public void setFindnombre(String findnombre) {
        this.findnombre = findnombre;
    }

    public Ante_familiar getAnte_familiar() {
        return ante_familiar;
    }

    public void setAnte_familiar(Ante_familiar ante_familiar) {
        this.ante_familiar = ante_familiar;
    }

    public boolean isEsOtro() {
        return esOtro;
    }

    public boolean isMenarca() {
        return menarca;
    }

    public void setMenarca(boolean menarca) {
        this.menarca = menarca;
    }

    public boolean isFum() {
        return fum;
    }

    public void setFum(boolean fum) {
        this.fum = fum;
    }

    public boolean isControlCheck() {
        return controlCheck;
    }

    public void setControlCheck(boolean controlCheck) {
        this.controlCheck = controlCheck;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public void setEsOtro(boolean esOtro) {
        this.esOtro = esOtro;
    }

    public boolean isDes() {
        return des;
    }

    public void setDes(boolean des) {
        this.des = des;
    }

    public boolean isEsCancer() {
        return esCancer;
    }

    public void setEsCancer(boolean esCancer) {
        this.esCancer = esCancer;
    }

    public List<Municipio> getMuniList() {
        return muniList;
    }

    public void setMuniList(List<Municipio> muniList) {
        this.muniList = muniList;
    }

    public List<Carrera> getCarreList() {
        return carreList;
    }

    public void setCarreList(List<Carrera> carreList) {
        this.carreList = carreList;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Facultad getFacultad() {
        return facultad;
    }

    public void setFacultad(Facultad facultad) {
        this.facultad = facultad;
    }

    public Alumno getAlumnoRec() {
        return alumnoRec;
    }

    public void setAlumnoRec(Alumno alumnoRec) {
        this.alumnoRec = alumnoRec;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Carrera getCarrera() {
        return carrera;
    }

    public void setCarrera(Carrera carrera) {
        this.carrera = carrera;
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public Estado_civil getEstado_civil() {
        return estado_civil;
    }

    public void setEstado_civil(Estado_civil estado_civil) {
        this.estado_civil = estado_civil;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Alumno> getAlumnoList() {
        return alumnoList;
    }

    public void setAlumnoList(List<Alumno> alumnoList) {
        this.alumnoList = alumnoList;
    }

    public Utilidades getUtil() {
        return util;
    }

    public void setUtil(Utilidades util) {
        this.util = util;
    }

    public int getCarnet() {
        return carnet;
    }

    public void setCarnet(int carnet) {
        this.carnet = carnet;
    }

    public Heredo_alumno getHeredo_alumno() {
        return heredo_alumno;
    }

    public void setHeredo_alumno(Heredo_alumno heredo_alumno) {
        this.heredo_alumno = heredo_alumno;
    }

    public String[] getSelectAnteHeredo() {
        return selectAnteHeredo;
    }

    public void setSelectAnteHeredo(String[] selectAnteHeredo) {
        this.selectAnteHeredo = selectAnteHeredo;
    }

    public Ante_alumno getAnte_alumno() {
        return ante_alumno;
    }

    public void setAnte_alumno(Ante_alumno ante_alumno) {
        this.ante_alumno = ante_alumno;
    }

    public Ecologico_Social getEcologico_Social() {
        return ecologico_Social;
    }

    public void setEcologico_Social(Ecologico_Social ecologico_Social) {
        this.ecologico_Social = ecologico_Social;
    }

    public Ecologico_Mascota getEcologico_Mascota() {
        return ecologico_Mascota;
    }

    public void setEcologico_Mascota(Ecologico_Mascota ecologico_Mascota) {
        this.ecologico_Mascota = ecologico_Mascota;
    }

    public Vivienda getVivienda() {
        return vivienda;
    }

    public void setVivienda(Vivienda vivienda) {
        this.vivienda = vivienda;
    }

    public Otro_Antecedente getOtro_Antecedente() {
        return otro_Antecedente;
    }

    public void setOtro_Antecedente(Otro_Antecedente otro_Antecedente) {
        this.otro_Antecedente = otro_Antecedente;
    }

    public Diagnostico getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(Diagnostico diagnostico) {
        this.diagnostico = diagnostico;
    }

    public Signo_vital getSigno_vital() {
        return signo_vital;
    }

    public void setSigno_vital(Signo_vital signo_vital) {
        this.signo_vital = signo_vital;
    }

    public Ante_personal getAnte_personal() {
        return ante_personal;
    }

    public void setAnte_personal(Ante_personal ante_personal) {
        this.ante_personal = ante_personal;
    }

    public Tipo_Agua getTipo_Agua() {
        return tipo_Agua;
    }

    public void setTipo_Agua(Tipo_Agua tipo_Agua) {
        this.tipo_Agua = tipo_Agua;
    }

    public Cons_pared getCons_pared() {
        return cons_pared;
    }

    public void setCons_pared(Cons_pared cons_pared) {
        this.cons_pared = cons_pared;
    }

    public Tipo_Techo getTipo_Techo() {
        return tipo_Techo;
    }

    public void setTipo_Techo(Tipo_Techo tipo_Techo) {
        this.tipo_Techo = tipo_Techo;
    }

    public Tipo_piso getTipo_piso() {
        return tipo_piso;
    }

    public void setTipo_piso(Tipo_piso tipo_piso) {
        this.tipo_piso = tipo_piso;
    }

    public Mascota getMascota() {
        return mascota;
    }

    public void setMascota(Mascota mascota) {
        this.mascota = mascota;
    }

    public Tipo_sangre getTipo_sangre() {
        return tipo_sangre;
    }

    public void setTipo_sangre(Tipo_sangre tipo_sangre) {
        this.tipo_sangre = tipo_sangre;
    }

    public Metodo_anti getMetodo_anti() {
        return metodo_anti;
    }

    public void setMetodo_anti(Metodo_anti metodo_anti) {
        this.metodo_anti = metodo_anti;
    }

    public String[] getSelectMascotas() {
        return selectMascotas;
    }

    public void setSelectMascotas(String[] selectMascotas) {
        this.selectMascotas = selectMascotas;
    }

    public String[] getManyChecbox() {
        return manyChecbox;
    }

    public void setManyChecbox(String[] manyChecbox) {
        this.manyChecbox = manyChecbox;
    }

    @PostConstruct
    public void init() {
        alumno = new Alumno();
        alumnoRec = new Alumno();
        carrera = new Carrera();
        municipio = new Municipio();
        genero = new Genero();
        estado_civil = new Estado_civil();
        usuario = new Usuario();
        util = new Utilidades();
        departamento = new Departamento();
        facultad = new Facultad();

        heredo_alumno = new Heredo_alumno();
        tipo_cancer = new Tipo_cancer();
        ante_familiar = new Ante_familiar();
        selectAnteHeredo = new String[]{};

        ante_alumno = new Ante_alumno();
        manyChecbox = new String[]{};
        ante_personal = new Ante_personal();

        ecologico_Social = new Ecologico_Social();
        tipo_Agua = new Tipo_Agua();
        cons_pared = new Cons_pared();
        tipo_Techo = new Tipo_Techo();
        tipo_piso = new Tipo_piso();

        ecologico_Mascota = new Ecologico_Mascota();
        mascota = new Mascota();
        selectMascotas = new String[]{};

        vivienda = new Vivienda();

        otro_Antecedente = new Otro_Antecedente();
        tipo_sangre = new Tipo_sangre();
        metodo_anti = new Metodo_anti();

        diagnostico = new Diagnostico();
        signo_vital = new Signo_vital();
        impresion_diag = new Impresion_diag();
        riesgo_Alumno = new Riesgo_Alumno();

        consultar();
    }

    public void limpiar() {
        alumno = new Alumno();
        alumnoRec = new Alumno();
        carrera = new Carrera();
        municipio = new Municipio();
        genero = new Genero();
        estado_civil = new Estado_civil();
        usuario = new Usuario();
        util = new Utilidades();
        departamento = new Departamento();
        facultad = new Facultad();

        heredo_alumno = new Heredo_alumno();
        tipo_cancer = new Tipo_cancer();
        ante_familiar = new Ante_familiar();
        selectAnteHeredo = new String[]{};
        riesgo_Alumno = new Riesgo_Alumno();

        ante_alumno = new Ante_alumno();
        manyChecbox = new String[]{};
        ante_personal = new Ante_personal();

        ecologico_Social = new Ecologico_Social();
        tipo_Agua = new Tipo_Agua();
        cons_pared = new Cons_pared();
        tipo_Techo = new Tipo_Techo();
        tipo_piso = new Tipo_piso();

        ecologico_Mascota = new Ecologico_Mascota();
        mascota = new Mascota();
        selectMascotas = new String[]{};

        vivienda = new Vivienda();

        otro_Antecedente = new Otro_Antecedente();
        tipo_sangre = new Tipo_sangre();
        metodo_anti = new Metodo_anti();

        diagnostico = new Diagnostico();
        signo_vital = new Signo_vital();
        impresion_diag = new Impresion_diag();

    }

    public void findByGenero() {
        try {
            alumnoList = alumnoFacadeLocal.findGenero(genero);
        } catch (Exception e) {
        }

    }

    public void findByCarrera() {
        try {
            alumnoList = alumnoFacadeLocal.findCarrera(carrera);
        } catch (Exception e) {
        }

    }

    public void findByMunicipio() {
        try {
            alumnoList = alumnoFacadeLocal.findMunicipio(municipio);
        } catch (Exception e) {
        }

    }
    
     public void findAcuerdo()
    {
        try {
            alumnoList=alumnoFacadeLocal.findAcuerdo(acuerdo);
        } catch (Exception e) {
        }
    }

    //CRUD
    public void insertar() {
        try {

            //Registra el alumno
            alumno.setCarrera(carrera);
            alumno.setEstado_civil(estado_civil);
            alumno.setGenero(genero);       
            alumno.setUsuario(usuario);
            alumno.setMunicipio(municipio);
            alumnoFacadeLocal.create(alumno);

            //Registra antecedentes heredofamiliares
            String des1 = heredo_alumno.getDescripcion();
            for (String selecAnteH : selectAnteHeredo) {
                int heredo = Integer.parseInt(selecAnteH);

                System.out.println("check " + heredo);

                switch (heredo) {
                    case 5:
                        heredo_alumno.setAlumno(alumno);
                         ante_familiar.setId(heredo);
                        heredo_alumno.setAnte_familiar(ante_familiar);
                        heredo_alumno.setDescripcion(null);
                        heredo_alumno.setTipo_cancer(tipo_cancer);
                        heredo_alumnoFacadeLocal.create(heredo_alumno);

                        break;
                    case 6:
                        heredo_alumno.setAlumno(alumno); ante_familiar.setId(heredo);
                         ante_familiar.setId(heredo);
                        heredo_alumno.setAnte_familiar(ante_familiar);
                        heredo_alumno.setDescripcion(des1);
                        heredo_alumno.setTipo_cancer(null);
                        heredo_alumnoFacadeLocal.create(heredo_alumno);

                        break;
                    default:
                        heredo_alumno.setAlumno(alumno);
                        ante_familiar.setId(heredo);
                        heredo_alumno.setAnte_familiar(ante_familiar);
                        heredo_alumno.setDescripcion(null);
                        heredo_alumno.setTipo_cancer(null);
                        heredo_alumnoFacadeLocal.create(heredo_alumno);
                        break;
                }
            }
            //registra antecedentes personales
            String des2 = ante_alumno.getDescripcion();
            for (String many : manyChecbox) {
                int io = Integer.parseInt(many);
                switch (io) {

                    case 9:
                        ante_personal.setId(io);
                        ante_alumno.setAnte_personal(ante_personal);
                        ante_alumno.setAlumno(alumno);
                        ante_alumno.setDescripcion(des2);
                        ante_alumnoFacadeLocal.create(ante_alumno);
                        break;
                    default:
                        ante_personal.setId(io);
                        ante_alumno.setAlumno(alumno);
                        ante_alumno.setDescripcion(null);
                        ante_alumno.setAnte_personal(ante_personal);
                        ante_alumnoFacadeLocal.create(ante_alumno);
                        break;
                }
            }
            // ecologico social
            ecologico_Social.setAlumno(alumno);
            ecologico_Social.setTipo_Agua(tipo_Agua);
            ecologico_Social.setCons_pared(cons_pared);
            ecologico_Social.setTipo_Techo(tipo_Techo);
            ecologico_Social.setTipo_piso(tipo_piso);
            ecologico_SocialFacadeLocal.create(ecologico_Social);

            //ecologico mascota
            String desmas = ecologico_Mascota.getDescripcion();
            for (String selectMascota : selectMascotas) {

                int mas = Integer.parseInt(selectMascota);

                switch (mas) {
                    case 6:
                        ecologico_Mascota.setEcologico_Social(ecologico_Social);
                        mascota.setId(mas);
                        ecologico_Mascota.setMascota(mascota);
                        ecologico_Mascota.setDescripcion(desmas);
                        ecologico_MascotaFacadeLocal.create(ecologico_Mascota);
                        break;
                    default:
                        ecologico_Mascota.setEcologico_Social(ecologico_Social);
                        mascota.setId(mas);
                        ecologico_Mascota.setMascota(mascota);
                        ecologico_Mascota.setDescripcion(null);
                        ecologico_MascotaFacadeLocal.create(ecologico_Mascota);
                        break;
                }
            }

            //vivienda
            vivienda.setEcologico_social(ecologico_Social);
            viviendaFacadeLocal.create(vivienda);

            //riesgo Alumno
            riesgo_Alumno.setAlumno(alumno);
            riesgo_AlumnoFacadeLocal.create(riesgo_Alumno);

            //otros antecedentes
            otro_Antecedente.setTipo_sangre(tipo_sangre);
            otro_Antecedente.setMetodo_anti(metodo_anti);
            otro_Antecedente.setAlumno(alumno);
            otro_AntecedenteFacadeLocal.create(otro_Antecedente);

            //signos vitales
            signo_vitalFacadeLocal.create(signo_vital);

            //Diagnostico
            diagnostico.setSigno_vital(signo_vital);
            diagnostico.setAlumno(alumno);
            diagnosticoFacadeLocal.create(diagnostico);

            recuperarAlumno();
            util.ponerMensaje("Alumno " + alumno.getNombres() + " registrado");
            limpiar();
        } catch (Exception e) {
            limpiar();
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void recuperarAlumno() {
        alumnoRec = alumno;
    }

    //Metodos Ajax
    public void municipioDepa() {

        try {
            muniList = municipioFacadeLocal.municipioLista(departamento);
            PrimeFaces.current().ajax().update("formRegistro:muni");
        } catch (Exception e) {
        }
    }

    public void carreraFacu() {

        try {
            carreList = carreraFacadeLocal.carreList(facultad);
            PrimeFaces.current().ajax().update("formRegistro:carre");
        } catch (Exception e) {
        }
    }

    public void esOtroAnte() {
        try {

            for (String heredo : selectAnteHeredo) {
                int ty = Integer.parseInt(heredo);

                switch (ty) {
                    case 5:

                        esCancer = true;

                        PrimeFaces.current().ajax().update("formRegistro:cancer");
                        break;
                    case 6:
                        esOtro = true;
                        PrimeFaces.current().ajax().update("formRegistro:otro");
                        break;
                    default:

                }
            }
        } catch (Exception e) {
        }
    }
    
    public void descripcion() {

        try {

            for (String selectMascota : selectMascotas) {
                int masco = Integer.parseInt(selectMascota);

                switch (masco) {
                    case 6:
                        des = true;
                        PrimeFaces.current().ajax().update("formRegistro:ren");
                        break;
                }
            }
        } catch (Exception e) {
            System.out.println("Error mascota " + e.getMessage());
            des = false;
            PrimeFaces.current().ajax().update("formRegistro:ren");
        }
    }

    public void imcUpdatePeso() {
        try {

            double peso = getSigno_vital().getPeso();
            double talla = getSigno_vital().getTalla();
            signo_vital.setImc(peso / talla * talla);
            PrimeFaces.current().ajax().update("formRegistro:imc");
        } catch (Exception e) {
            System.out.println("Estoy aqui error diagnostico ajax");
        }
    }

    public void ajaxChecBox() {
        try {

            
            for (String ma : manyChecbox) {
                int i = Integer.parseInt(ma);
                switch (i) {
                    case 9:
                        controlCheck = true;
                        PrimeFaces.current().ajax().update("formRegistro:descripcion");
                        break;
                    default:
                        ante_alumno.getDescripcion();
                }

            }
        } catch (Exception e) {
            controlCheck = false;
            PrimeFaces.current().ajax().update("formRegistro:descripcion");
        }
    }

    public void ajaxGenero() {
        try {
            switch (genero.getId()) {
                case 1:
                    menarca = false;
                    fum = false;
                    PrimeFaces.current().ajax().update("formRegistro:menarca");
                    PrimeFaces.current().ajax().update("formRegistro:fum");
                    break;
                case 2:
                    menarca = true;
                    fum = true;
                    PrimeFaces.current().ajax().update("formRegistro:menarca");
                    PrimeFaces.current().ajax().update("formRegistro:fum");
            }

        } catch (Exception e) {
            menarca = true;
            fum = true;
            PrimeFaces.current().ajax().update("formRegistro:menarca");
            PrimeFaces.current().ajax().update("formRegistro:fum");

        }
    }

    public void consultar() {
        try {
            alumnoList = alumnoFacadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void findByNombre() {
        try {
            alumnoList = alumnoFacadeLocal.findNombre(findnombre);
        } catch (Exception e) {
        }
    }

    public void findByCarnet() {
        try {

            alumnoList = alumnoFacadeLocal.findCarnet(carnet);
            findnombre = new String();
        } catch (NumberFormatException e) {
        }
    }

    public String detalles(Alumno alumno) {
        alumnoRec = new Alumno();
        this.alumnoRec = alumno;
        return "detalles.xhtml?faces-redirect=true";
    }

    public String edit(Alumno alumno) {
        alumnoRec = new Alumno();
        this.alumnoRec = alumno;
        return "detallesEdit.xhtml?faces-redirect=true";
    }

    public void ajaxViolencia() {
        if ("Si".equals(riesgo_Alumno.getViolencia())) {
            violencia = true;
            PrimeFaces.current().ajax().update("formRegistro:violencia");
        } else {
            violencia = false;
            PrimeFaces.current().ajax().update("formRegistro:violencia");
        }
    }

    public void ajaxToxicomania() {
        if ("Si".equals(riesgo_Alumno.getToxicomonia())) {
            taxi = true;
            PrimeFaces.current().ajax().update("formRegistro:tipotaxi");
            PrimeFaces.current().ajax().update("formRegistro:taxi");
        } else {
            taxi = false;
            PrimeFaces.current().ajax().update("formRegistro:tipotaxi");
            PrimeFaces.current().ajax().update("formRegistro:taxi");
        }
    }

    public void ajaxAlchoholismo() {
        if ("Si".equals(riesgo_Alumno.getAlcoholismo())) {
            alcho = true;
            PrimeFaces.current().ajax().update("formRegistro:alcho");
        } else {
            alcho = false;
            PrimeFaces.current().ajax().update("formRegistro:alcho");
        }
    }

    public void ajaxTabaquismo() {
        if ("Si".equals(riesgo_Alumno.getTabaquismo())) {
            cigarros = true;
            PrimeFaces.current().ajax().update("formRegistro:ncigarro");
        } else {
            cigarros = false;
            PrimeFaces.current().ajax().update("formRegistro:ncigarro");
        }
    }
}
