package controller;

import ejb.Ecologico_SocialFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import modelo.Alumno;
import modelo.Cons_pared;
import modelo.Ecologico_Social;
import modelo.Tipo_Agua;
import modelo.Tipo_Techo;
import modelo.Tipo_piso;
import util.Utilidades;

@ManagedBean
@SessionScoped
public class Ecologico_SocialController implements Serializable{

    @EJB
    private Ecologico_SocialFacadeLocal facadeLocal;
    @ManagedProperty("#{alumnoController}")
    private AlumnoController alumnoController;
    private Ecologico_Social ecologico_Social;
    private List<Ecologico_Social> lista;
    private Alumno alumno;
    private Tipo_Agua tipo_Agua;
    private Cons_pared cons_pared;
    private Tipo_Techo tipo_Techo;
    private Tipo_piso tipo_piso;
    public Utilidades utilidades;
    private Ecologico_Social ecoSocialRescate;

    public Ecologico_Social getEcoSocialRescate() {
        return ecoSocialRescate;
    }

    public void setEcoSocialRescate(Ecologico_Social ecoSocialRescate) {
        this.ecoSocialRescate = ecoSocialRescate;
    }
    public AlumnoController getAlumnoController() {
        return alumnoController;
    }

    public void setAlumnoController(AlumnoController alumnoController) {
        this.alumnoController = alumnoController;
    }
    public Ecologico_Social getEcologico_Social() {
        return ecologico_Social;
    }

    public void setEcologico_Social(Ecologico_Social ecologico_Social) {
        this.ecologico_Social = ecologico_Social;
    }

    public List<Ecologico_Social> getLista() {
        return lista;
    }

    public void setLista(List<Ecologico_Social> lista) {
        this.lista = lista;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Tipo_Agua getTipo_Agua() {
        return tipo_Agua;
    }

    public void setTipo_Agua(Tipo_Agua tipo_Agua) {
        this.tipo_Agua = tipo_Agua;
    }

    public Cons_pared getCons_pared() {
        return cons_pared;
    }

    public void setCons_pared(Cons_pared cons_pared) {
        this.cons_pared = cons_pared;
    }

    public Tipo_Techo getTipo_Techo() {
        return tipo_Techo;
    }

    public void setTipo_Techo(Tipo_Techo tipo_Techo) {
        this.tipo_Techo = tipo_Techo;
    }

    public Tipo_piso getTipo_piso() {
        return tipo_piso;
    }

    public void setTipo_piso(Tipo_piso tipo_piso) {
        this.tipo_piso = tipo_piso;
    }

    @PostConstruct
    public void init() {
        ecologico_Social = new Ecologico_Social();
        tipo_Agua = new Tipo_Agua();
        cons_pared = new Cons_pared();
        tipo_Techo = new Tipo_Techo();
        tipo_piso = new Tipo_piso();
        alumno=new Alumno();
        ecoSocialRescate=new Ecologico_Social();
        utilidades = new Utilidades();
        consultar();
    }

    public void consultar() {
        try {
            lista = facadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void insertar() {
        try {
           
            
            ecologico_Social.setAlumno(alumnoController.getAlumnoRec());
            ecologico_Social.setTipo_Agua(tipo_Agua);
            ecologico_Social.setCons_pared(cons_pared);
            ecologico_Social.setTipo_Techo(tipo_Techo);
            ecologico_Social.setTipo_piso(tipo_piso);
            facadeLocal.create(ecologico_Social);
            recuperarEcoSocial();
            utilidades.ponerMensaje("Ecologico Social guardado");
            init();

        } catch (Exception e) {
            utilidades.ponerMensaje("Error " + e.getMessage());
        }
    }

    public void editar() {
        try {

            ecologico_Social.setAlumno(alumno);
            ecologico_Social.setTipo_Agua(tipo_Agua);
            ecologico_Social.setCons_pared(cons_pared);
            ecologico_Social.setTipo_Techo(tipo_Techo);
            ecologico_Social.setTipo_piso(tipo_piso);
            facadeLocal.edit(ecologico_Social);
            utilidades.ponerMensaje("Ecologico Social Actualizado");
            init();

        } catch (Exception e) {
            utilidades.ponerMensaje("Error " + e.getMessage());
        }
    }
    public void recuperarEcoSocial()
    {
        ecoSocialRescate=ecologico_Social;
    }

    public void eliminar(Ecologico_Social ecologico_Social) {
        try {
            this.ecologico_Social = ecologico_Social;

            facadeLocal.remove(ecologico_Social);
            utilidades.ponerMensaje("Ecologico Social Eliminado");
            init();
        } catch (Exception e) {
            utilidades.ponerMensaje("Error "+e.getMessage());
        }
    }
    public void consultarById(Ecologico_Social ecologico_Social)
    {
        this.ecologico_Social=ecologico_Social;
        alumno.setId(ecologico_Social.getAlumno().getId());
        tipo_Agua.setId(ecologico_Social.getTipo_Agua().getId());
        cons_pared.setId(ecologico_Social.getCons_pared().getId());
        tipo_Techo.setId(ecologico_Social.getTipo_Techo().getId());
        tipo_piso.setId(ecologico_Social.getTipo_piso().getId());
                
    }
    public String mascota(Ecologico_Social ecologico_Social)
    {
        this.ecoSocialRescate=ecologico_Social;
        return "ecologico_mascota.xhtml?faces-redirect=true";
    }
}
