
package controller;

import ejb.Heredo_alumnoFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import modelo.Alumno;
import modelo.Ante_familiar;
import modelo.Carrera;
import modelo.Heredo_alumno;
import modelo.Tipo_cancer;
import org.primefaces.PrimeFaces;
import util.Utilidades;

@ManagedBean(name = "heredo_alumnoController")
@SessionScoped
public class Heredo_alumnoController implements Serializable {

    @ManagedProperty("#{alumnoController}")
    private AlumnoController alumnoController;

    @EJB
    private Heredo_alumnoFacadeLocal heredoAlumnoFacadeLocal;
    private Heredo_alumno heredo_alumno;
    private Alumno alumno;
    private Ante_familiar ante_familiar;
    private Ante_familiar ante_familiar2;
    private Tipo_cancer tipo_cancer;
    private List<Heredo_alumno> heredoAlumnoList;
    private Utilidades util;
    private String[] selectAnteHeredo;
    private String nombre;
    private Carrera carrera;
    private int edad;
    private String nombre2, acuerdo;

    private boolean esOtro = false;
    private boolean esCancer = false;

    public AlumnoController getAlumnoController() {
        return alumnoController;
    }

    public boolean isEsOtro() {
        return esOtro;
    }

    public Ante_familiar getAnte_familiar2() {
        return ante_familiar2;
    }

    public void setAnte_familiar2(Ante_familiar ante_familiar2) {
        this.ante_familiar2 = ante_familiar2;
    }

    public void setEsOtro(boolean esOtro) {
        this.esOtro = esOtro;
    }

    public String[] getSelectAnteHeredo() {
        return selectAnteHeredo;
    }

    public void setSelectAnteHeredo(String[] selectAnteHeredo) {
        this.selectAnteHeredo = selectAnteHeredo;
    }

    public void setAlumnoController(AlumnoController alumnoController) {
        this.alumnoController = alumnoController;
    }

    public Heredo_alumno getHeredo_alumno() {
        return heredo_alumno;
    }

    public void setHeredo_alumno(Heredo_alumno heredo_alumno) {
        this.heredo_alumno = heredo_alumno;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Ante_familiar getAnte_familiar() {
        return ante_familiar;
    }

    public void setAnte_familiar(Ante_familiar ante_familiar) {
        this.ante_familiar = ante_familiar;
    }

    public Tipo_cancer getTipo_cancer() {
        return tipo_cancer;
    }

    public void setTipo_cancer(Tipo_cancer tipo_cancer) {
        this.tipo_cancer = tipo_cancer;
    }

    public List<Heredo_alumno> getHeredoAlumnoList() {
        return heredoAlumnoList;
    }

    public void setHeredoAlumnoList(List<Heredo_alumno> heredoAlumnoList) {
        this.heredoAlumnoList = heredoAlumnoList;
    }

    public boolean isEsCancer() {
        return esCancer;
    }

    public void setEsCancer(boolean esCancer) {
        this.esCancer = esCancer;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Carrera getCarrera() {
        return carrera;
    }

    public void setCarrera(Carrera carrera) {
        this.carrera = carrera;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getNombre2() {                 
        return nombre2;
    }

    public void setNombre2(String nombre2) {
        this.nombre2 = nombre2;
    }

    public String getAcuerdo() {
        return acuerdo;
    }

    public void setAcuerdo(String acuerdo) {
        this.acuerdo = acuerdo;
    }
     
    
    @PostConstruct
    public void init() {
        alumno = new Alumno();
        ante_familiar = new Ante_familiar();
        tipo_cancer = new Tipo_cancer();
        heredo_alumno = new Heredo_alumno();
        util = new Utilidades();
        selectAnteHeredo = new String[]{};
        carrera = new Carrera();
        ante_familiar2=new Ante_familiar();
    }

    public void insertar() {
        try {

            String des = heredo_alumno.getDescripcion();

            alumno.setId(1);
            for (String selecAnteH : selectAnteHeredo) {
                int heredo = Integer.parseInt(selecAnteH);

                System.out.println("check " + heredo);

                switch (heredo) {
                    case 5:
                        heredo_alumno.setAlumno(alumno);
                        ante_familiar.setId(heredo);
                        heredo_alumno.setAnte_familiar(ante_familiar);
                        heredo_alumno.setDescripcion(null);
                        heredo_alumno.setTipo_cancer(tipo_cancer);
                        heredoAlumnoFacadeLocal.create(heredo_alumno);

                        break;
                    case 6:
                        heredo_alumno.setAlumno(alumno);
                        ante_familiar.setId(heredo);
                        heredo_alumno.setAnte_familiar(ante_familiar);
                        heredo_alumno.setDescripcion(des);
                        heredo_alumno.setTipo_cancer(null);
                        heredoAlumnoFacadeLocal.create(heredo_alumno);

                        break;
                    default:
                        heredo_alumno.setAlumno(alumno);
                        ante_familiar.setId(heredo);
                        heredo_alumno.setAnte_familiar(ante_familiar);
                        heredo_alumno.setDescripcion(null);
                        heredo_alumno.setTipo_cancer(null);
                        heredoAlumnoFacadeLocal.create(heredo_alumno);

                }
            }
            util.ponerMensaje("Antecedentse heredo familiares registrados");
            heredoAlumnoXAlumno(alumnoController.getAlumnoRec());
            init();

        } catch (NumberFormatException e) {
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void editar() {
        try {
            heredo_alumno.setAlumno(alumno);
            heredo_alumno.setAnte_familiar(ante_familiar);
            heredo_alumno.setTipo_cancer(tipo_cancer);
            heredoAlumnoFacadeLocal.edit(heredo_alumno);
            util.ponerMensaje("Antecedente heredo familiar editado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void eliminar(Heredo_alumno heredo) {
        try {
            heredoAlumnoFacadeLocal.remove(heredo);
            util.ponerMensaje("Antecedente heredo familiar eliminado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void consultar() {
        heredoAlumnoList = heredoAlumnoFacadeLocal.findAll();
    }

    //Consultar por Id
    public void consultarById(Heredo_alumno heredo) {
        heredo_alumno = heredo;
        ante_familiar.setId(heredo.getAnte_familiar().getId());
        alumno.setId(heredo.getAlumno().getId());
        tipo_cancer.setId(heredo.getTipo_cancer().getId());
    }

    public void esOtroAnte() {
        try {

            for (String heredo : selectAnteHeredo) {
                int ty = Integer.parseInt(heredo);

                switch (ty) {
                    case 5:

                        esCancer = true;

                        PrimeFaces.current().ajax().update("formHeredo:cancer");
                        break;
                    case 6:
                        esOtro = true;
                        PrimeFaces.current().ajax().update("formHeredo:otro");
                        break;
                    default:

                }
            }
        } catch (Exception e) {
        }
    }

    public void heredoAlumnoXAlumno(Alumno alumno) {
        try {
            heredoAlumnoList = heredoAlumnoFacadeLocal.consultarHeredo_alumnoXalumno(alumno);
        } catch (Exception e) {
        }
    }

    public void findByEnfermedad() {
        try {
            heredoAlumnoList = heredoAlumnoFacadeLocal.findByEnfermedad(ante_familiar);
            ante_familiar=new Ante_familiar();
        } catch (Exception e) {
        }
    }

    public void findByCarrera() {
        try {
            heredoAlumnoList = heredoAlumnoFacadeLocal.findByCarrera(carrera);
            carrera=new Carrera();
        } catch (Exception e) {
        }
    }

    public void findByNombre() {
        try {
            heredoAlumnoList = heredoAlumnoFacadeLocal.findByNombre(nombre);
        } catch (Exception e) {
        }
    }

    public void findByEdad() {
        try {
            heredoAlumnoList = heredoAlumnoFacadeLocal.findByEdad(edad);
        } catch (Exception e) {
        }
    }
    public void findByCarreraEnfermedad()
    {
        heredoAlumnoList=heredoAlumnoFacadeLocal.findByCarreraEnfermedad(carrera, ante_familiar2,acuerdo);
        ante_familiar=new Ante_familiar();
    }


    public String idAlumno(Heredo_alumno heredo_alumno) {

        this.alumno = heredo_alumno.getAlumno();

        return "detalles_1.xhtml?faces-redirect=true";
    }
}
