package controller;

import ejb.Estado_civilFacadeLocal;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Estado_civil;
import util.Utilidades;

@ManagedBean(name = "estado_civilController")
@SessionScoped
public class Estado_civilController {

    @EJB
    private Estado_civilFacadeLocal estado_civilEJB;
    private Estado_civil estado_civil;
    private List<Estado_civil> lista;
    private Utilidades utilidades;

    public Estado_civil getEstado_civil() {
        return estado_civil;
    }

    public void setEstado_civil(Estado_civil estado_civil) {
        this.estado_civil = estado_civil;
    }

    public List<Estado_civil> getLista() {
        return lista;
    }

    public void setLista(List<Estado_civil> lista) {
        this.lista = lista;
    }

    @PostConstruct
    public void init() {
        estado_civil = new Estado_civil();

        utilidades = new Utilidades();
        consultar();
    }

    public void guardar() {
        try {
            estado_civilEJB.create(estado_civil);
            if (estado_civil != null) {
                utilidades.ponerMensaje("Registro fue guardado");
                init();
            }
            init();
        } catch (Exception e) {
            utilidades.ponerMensaje("Registro no fue guardado " + e);
        }
    }

    public void editar() {
        try {
            estado_civilEJB.edit(estado_civil);
            if (estado_civil != null) {
                utilidades.ponerMensaje("Registro fue actualizado ");
                init();
            }
            init();
        } catch (Exception e) {
            utilidades.ponerMensaje("Registro no fue actualizado " + e);
        }
    }

    private void consultar() {
        try {
            lista = estado_civilEJB.findAll();
        } catch (Exception e) {
        }
    }

    public void consultarById(Estado_civil estado_civil) {
        this.estado_civil = estado_civil;
    }

    public void eliminar(Estado_civil estado_civil) {
        this.estado_civil = estado_civil;
        try {
            estado_civilEJB.remove(estado_civil);
            lista = estado_civilEJB.findAll();
            if (estado_civil != null) {
                utilidades.ponerMensaje("Registro fue eliminado ");
                init();
            }
            init();
        } catch (Exception e) {
            utilidades.ponerMensaje("Registro no fue eliminado " + e);
        }
    }

}
