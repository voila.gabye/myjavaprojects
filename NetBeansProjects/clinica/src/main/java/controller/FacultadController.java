package controller;

import ejb.FacultadFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Facultad;
import util.Utilidades;

@ManagedBean
@SessionScoped
public class FacultadController implements Serializable {

    @EJB
    private FacultadFacadeLocal facultadEJB;
    private Facultad facultad;
    List<Facultad> listaFacultad;
    private Utilidades util;

    public Facultad getFacultad() {
        return facultad;
    }

    public void setFacultad(Facultad facultad) {
        this.facultad = facultad;
    }

    public List<Facultad> getListaFacultad() {
        return listaFacultad;
    }

    public void setListaFacultad(List<Facultad> listaFacultad) {
        this.listaFacultad = listaFacultad;
    }

    @PostConstruct
    public void init() {
        facultad = new Facultad();
        util = new Utilidades();
        consultar();
    }

    public void insertar() {
        try {
            facultadEJB.create(facultad);
            util.ponerMensaje("Facultad registrada");
            init();

        } catch (Exception e) {
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void editar() {
        try {
            facultadEJB.edit(facultad);
            util.ponerMensaje("Facultad actualizada");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void eliminar(Facultad fac) {
        this.facultad = fac;
        try {
            facultadEJB.remove(facultad);
            util.ponerMensaje("Facultad eliminada");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void consultar() {

        listaFacultad = facultadEJB.findAll();
    }

    public void consultarById(Facultad fac) {
        this.facultad = fac;
    }
}
