/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.MascotaFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Mascota;
import util.Utilidades;

/**
 *
 * @author alejandro.suarezusam
 */
@ManagedBean
@SessionScoped
public class MascotaController implements Serializable{
    @EJB
    
    private MascotaFacadeLocal mascotafacade;
    private Mascota mascota;
    private Utilidades util;
    private List<Mascota> listamascota;

    public Mascota getMascota() {
        return mascota;
    }

    public void setMascota(Mascota mascota) {
        this.mascota = mascota;
    }

    public Utilidades getUtil() {
        return util;
    }

    public void setUtil(Utilidades util) {
        this.util = util;
    }

    public List<Mascota> getListamascota() {
       
        return listamascota;
    }

    public void setListamascota(List<Mascota> listamascota) {
        this.listamascota = listamascota;
    }
    
    @PostConstruct
    public void init(){
    this.mascota= new Mascota();
    this.util= new Utilidades();     
    consultar();
    }
    
    public void consultar(){
     this.listamascota=this.mascotafacade.findAll();
    }
    
    public void consultarById(Mascota m){
    this.mascota=m;
    }
    
    public void insertar(){
        try {
            this.mascotafacade.create(mascota);
            this.util.ponerMensaje("Mascota agregada con exito");
            this.init();                        
        } catch (Exception e) {
            util.ponerMensaje("Error:"+e.getMessage());
        }
    }
    
    public void editar(){
        try {
            this.mascotafacade.edit(mascota);
            this.util.ponerMensaje("Actualizado correctamente");
            this.init();                        
        } catch (Exception e) {
            util.ponerMensaje("Error:"+e.getMessage());
        }
    }
    
   public void eliminar(Mascota m){
        try {
            this.mascotafacade.remove(m);
            this.util.ponerMensaje("Eliminado");
            this.init();                        
        } catch (Exception e) {
            util.ponerMensaje("Error:"+e.getMessage());
        }
    }
}
