package controller;

import ejb.Tipo_cancerFacadeLocal;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Tipo_cancer;
import util.Utilidades;

@ManagedBean
@SessionScoped
public class Tipo_cancerController {

    @EJB
    private Tipo_cancerFacadeLocal tipo_cancerEJB;
    private Tipo_cancer tipo_cancer;
    private List<Tipo_cancer> listaTipo_cancer;
    private Utilidades util;

    public Tipo_cancer getTipo_cancer() {
        return tipo_cancer;
    }

    public void setTipo_cancer(Tipo_cancer tipo_cancer) {
        this.tipo_cancer = tipo_cancer;
    }

    public List<Tipo_cancer> getListaTipo_cancer() {
        return listaTipo_cancer;
    }

    public void setListaTipo_cancer(List<Tipo_cancer> listaTipo_cancer) {
        this.listaTipo_cancer = listaTipo_cancer;
    }

    @PostConstruct
    public void init() {
        tipo_cancer = new Tipo_cancer();
        util = new Utilidades();
        consultar();
    }

    public void insertar() {
        try {
            tipo_cancerEJB.create(tipo_cancer);
            util.ponerMensaje("Tipo cáncer registrado");
            init();

        } catch (Exception e) {
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void editar() {
        try {
            tipo_cancerEJB.edit(tipo_cancer);
            util.ponerMensaje("Tipo cáncer actualizado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Erro:" + e.getMessage());
        }
    }

    public void eliminar(Tipo_cancer tpc) {
        this.tipo_cancer = tpc;
        try {
            tipo_cancerEJB.remove(tipo_cancer);
            util.ponerMensaje("Tipo cáncer eliminado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Erro:" + e.getMessage());
        }
    }

    public void consultar() {

        listaTipo_cancer = tipo_cancerEJB.findAll();
    }

    public void consultarById(Tipo_cancer tpc) {
        this.tipo_cancer = tpc;
    }
}
