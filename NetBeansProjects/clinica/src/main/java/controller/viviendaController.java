/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.ViviendaFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import modelo.Ecologico_Social;
import modelo.Vivienda;

/**
 *
 * @author alejandro.suarezusam
 */
@ManagedBean(name = "viviendaController")
@SessionScoped
public class viviendaController implements Serializable {

    @EJB
    private ViviendaFacadeLocal viviendafacade;
    private Vivienda vivienda;
    private List<Vivienda> listavivienda;
    private Ecologico_Social ecologico_social;

    @ManagedProperty("#{Ecologico_SocialController}")
    private Ecologico_SocialController ecologico_socialcontroller;

    public Ecologico_SocialController getEcologico_socialcontroller() {
        return ecologico_socialcontroller;
    }

    public void setEcologico_socialcontroller(Ecologico_SocialController ecologico_socialcontroller) {
        this.ecologico_socialcontroller = ecologico_socialcontroller;
    }

    public Ecologico_Social getEcologico_social() {
        return ecologico_social;
    }

    public void setEcologico_social(Ecologico_Social ecologico_social) {
        this.ecologico_social = ecologico_social;
    }

    public Vivienda getVivienda() {
        return vivienda;
    }

    public void setVivienda(Vivienda vivienda) {
        this.vivienda = vivienda;
    }

    public List<Vivienda> getListavivienda() {
        return listavivienda;
    }

    public void setListavivienda(List<Vivienda> listavivienda) {
        this.listavivienda = listavivienda;
    }

    @PostConstruct
    public void init() {
        ecologico_social = new Ecologico_Social();
        vivienda = new Vivienda();

        consultar();
    }

    public void consultar() {
        try {
            listavivienda = viviendafacade.findAll();
        } catch (Exception e) {
        }
    }

    public void insertar() {
        try {
            vivienda.setEcologico_social(ecologico_socialcontroller.getEcoSocialRescate());
            viviendafacade.create(vivienda);
            if (vivienda != null) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("registro correcto"));
                init();
            }
            init();
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Erro " + e.getMessage()));

        }
    }

    public void editar() {
        try {
            vivienda.setEcologico_social(ecologico_social);
            viviendafacade.create(vivienda);
            if (vivienda != null) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("registro correcto"));
                init();
            }
            init();
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Erro " + e.getMessage()));

        }
    }

    public void eliminar(Vivienda v) {

        this.vivienda = v;
        try {
            vivienda.setEcologico_social(ecologico_social);
            viviendafacade.remove(v);
        } catch (Exception e) {

        }
    }

    public void consultarById(Vivienda v) {
        this.ecologico_social.setId(v.getEcologico_social().getId());
        this.vivienda = v;
    }
}
