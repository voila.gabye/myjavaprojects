package controller;

import ejb.AutoresFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Autores;

@ManagedBean
@SessionScoped
public class AutoresController implements Serializable{
    
    @EJB
    private AutoresFacadeLocal facadeLocal;
    private Autores autores;
    private List<Autores> lista;

    public Autores getAutores() {
        return autores;
    }

    public void setAutores(Autores autores) {
        this.autores = autores;
    }

    public List<Autores> getLista() {
        return lista;
    }

    public void setLista(List<Autores> lista) {
        this.lista = lista;
    }
    
    @PostConstruct
    public void init(){
    autores = new Autores();
    verAutores();
    }
    
    public void verAutores(){
        try {
            lista = facadeLocal.findAll();
        } catch (Exception e) {
        }
    }
}
