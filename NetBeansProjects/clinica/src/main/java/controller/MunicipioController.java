/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.MunicipioFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Departamento;
import modelo.Municipio;
import util.Utilidades;

@ManagedBean
@SessionScoped
public class MunicipioController implements Serializable{

    @EJB
    private MunicipioFacadeLocal facadeLocal;
    private Municipio municipio;
    private List<Municipio> lista;
    private Utilidades utilidades;
    private Departamento departamento;

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public List<Municipio> getLista() {
        return lista;
    }

    public void setLista(List<Municipio> lista) {
        this.lista = lista;
    }

    @PostConstruct
    public void init() {
        municipio = new Municipio();
        utilidades = new Utilidades();
        departamento = new Departamento();
        consultar();
    }

    //inicio metodos basicos
    public void consultar() {
        try {
            lista = facadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void insertar() {
        try {
            municipio.setDepartamento(departamento);
            facadeLocal.create(municipio);
            utilidades.ponerMensaje("Municipio Guardado");

            init();
        } catch (Exception e) {
            utilidades.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void editar() {
        try {
            municipio.setDepartamento(departamento);
            facadeLocal.edit(municipio);
            utilidades.ponerMensaje("Municipio Actualizado");

            init();
        } catch (Exception e) {
            utilidades.ponerMensaje("Error al actualizar" + e.getMessage());
        }
    }

    public void eliminar(Municipio municipio) {
        this.municipio = municipio;
        try {
            facadeLocal.remove(municipio);
            utilidades.ponerMensaje("Municipio Eliminado");
            init();
        } catch (Exception e) {
            utilidades.ponerMensaje("Error " + e.getMessage());
        }
    }

    public void consultarById(Municipio municipio) {
        this.municipio = municipio;
        departamento.setId(municipio.getDepartamento().getId());
    }
    // fin metodos basicos

}
