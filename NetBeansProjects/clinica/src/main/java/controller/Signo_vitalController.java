package controller;

import ejb.Signo_vitalFacadeLocal;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Signo_vital;
import util.Utilidades;

@ManagedBean
@SessionScoped
public class Signo_vitalController {

    @EJB
    private Signo_vitalFacadeLocal signo_vitalEJB;
    private Signo_vital signo_vital;
    List<Signo_vital> signo_vitalList;
    private Utilidades util;

    public Signo_vital getSigno_vital() {
        return signo_vital;
    }

    public void setSigno_vital(Signo_vital signo_vital) {
        this.signo_vital = signo_vital;
    }

    public List<Signo_vital> getSigno_vitalList() {
        return signo_vitalList;
    }

    public void setSigno_vitalList(List<Signo_vital> signo_vitalList) {
        this.signo_vitalList = signo_vitalList;
    }

    @PostConstruct
    public void init() {
        signo_vital = new Signo_vital();
        util = new Utilidades();
        consultar();
    }

    public void insertar() {
        try {
            signo_vitalEJB.create(signo_vital);
            util.ponerMensaje("Signo vital registrado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void editar() {
        try {
            signo_vitalEJB.edit(signo_vital);
            util.ponerMensaje("Signo vital actualizado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void eliminar(Signo_vital sgv) {
        this.signo_vital = sgv;
        try {
            signo_vitalEJB.remove(signo_vital);
            util.ponerMensaje("Signo vital eliminado");
            init();
        } catch (Exception e) {
            util.ponerMensaje("Error: " + e.getMessage());
        }
    }

    public void consultar() {

        signo_vitalList = signo_vitalEJB.findAll();
    }

    public void consultarById(Signo_vital sgv) {

        this.signo_vital = sgv;
    }
   
}
