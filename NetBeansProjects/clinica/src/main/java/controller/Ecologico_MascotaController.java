package controller;

import ejb.Ecologico_MascotaFacadeLocal;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import modelo.Alumno;
import modelo.Ecologico_Mascota;
import modelo.Ecologico_Social;
import modelo.Mascota;
import org.primefaces.PrimeFaces;
import util.Utilidades;

@ManagedBean
@SessionScoped
public class Ecologico_MascotaController implements Serializable {

    @EJB
    private Ecologico_MascotaFacadeLocal facadeLocal;

    @ManagedProperty("#{ecologico_SocialController}")
    private Ecologico_SocialController ecologico_SocialController;

    private Ecologico_Mascota ecologico_Mascota;
    private List<Ecologico_Mascota> lista;
    private Ecologico_Social ecologico_Social;
    private boolean des = false;
    private Mascota mascota;
    private Utilidades utilidades;
    private String[] selectMascotas;
    private Alumno alumno;

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Ecologico_SocialController getEcologico_SocialController() {
        return ecologico_SocialController;
    }

    public void setEcologico_SocialController(Ecologico_SocialController ecologico_SocialController) {
        this.ecologico_SocialController = ecologico_SocialController;
    }

    public Ecologico_Mascota getEcologico_Mascota() {
        return ecologico_Mascota;
    }

    public void setEcologico_Mascota(Ecologico_Mascota ecologico_Mascota) {
        this.ecologico_Mascota = ecologico_Mascota;
    }

    public List<Ecologico_Mascota> getLista() {
        return lista;
    }

    public void setLista(List<Ecologico_Mascota> lista) {
        this.lista = lista;
    }

    public Ecologico_Social getEcologico_Social() {
        return ecologico_Social;
    }

    public void setEcologico_Social(Ecologico_Social ecologico_Social) {
        this.ecologico_Social = ecologico_Social;
    }

    public Mascota getMascota() {
        return mascota;
    }

    public void setMascota(Mascota mascota) {
        this.mascota = mascota;
    }

    public String[] getSelectMascotas() {
        return selectMascotas;
    }

    public void setSelectMascotas(String[] selectMascotas) {
        this.selectMascotas = selectMascotas;
    }

    public boolean isDes() {
        return des;
    }

    public void setDes(boolean des) {
        this.des = des;
    }

    @PostConstruct
    public void init() {
        ecologico_Mascota = new Ecologico_Mascota();
        mascota = new Mascota();
        ecologico_Social = new Ecologico_Social();
        utilidades = new Utilidades();
        selectMascotas = new String[]{};
        alumno = new Alumno();
        consultar();
    }

    public void consultar() {
        try {
            lista = facadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void insertar() {
        try {

            for (String selectMascota : selectMascotas) {

                int mas = Integer.parseInt(selectMascota);
                mascota.setId(mas);
                System.out.println("Mascota " + mas);
                ecologico_Mascota.setEcologico_Social(ecologico_SocialController.getEcoSocialRescate());
                ecologico_Mascota.setMascota(mascota);
                facadeLocal.create(ecologico_Mascota);
            }
            utilidades.ponerMensaje("Exito Mascotas guardadas");
            init();
        } catch (NumberFormatException e) {
            utilidades.ponerMensaje("Error " + e.getMessage());
        }
    }

    public void editar() {
        try {

            ecologico_Mascota.setEcologico_Social(ecologico_Social);
            facadeLocal.edit(ecologico_Mascota);
            init();
        } catch (Exception e) {
        }
    }

    public void eliminar(Ecologico_Mascota ecologico_Mascota) {
        try {
            this.ecologico_Mascota = ecologico_Mascota;
            facadeLocal.remove(ecologico_Mascota);
            utilidades.ponerMensaje("Exito Mascota Eliminada");
            init();

        } catch (Exception e) {
            utilidades.ponerMensaje("Error " + e.getMessage());
        }
    }

    public void consultarById(Ecologico_Mascota ecologico_Mascota) {
        this.ecologico_Mascota = ecologico_Mascota;

    }

    // metodo habilita o desabilita el campo descripcion cuando esta seleccionado otros
    public void descripcion() {

        try {

            if ("6".equals(selectMascotas[0])) {
                System.out.println("mascota " + selectMascotas[0]);
                des = true;
                PrimeFaces.current().ajax().update("formEcologicoMascota:ren");
            }
            if (!"6".equals(selectMascotas[0])) {
                System.out.println("mascota falsa" + selectMascotas[0]);
                des = false;
                PrimeFaces.current().ajax().update("formEcologicoMascota:ren");
            }
        } catch (Exception e) {
            System.out.println("Error mascota " + e.getMessage());
            des = false;
            PrimeFaces.current().ajax().update("formEcologicoMascota:ren");
        }
    }
}
