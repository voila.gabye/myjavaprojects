package controller;

import ejb.CarreraFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import modelo.Carrera;
import modelo.Facultad;

@ManagedBean(name = "carreraController")
@SessionScoped
public class CarreraController implements Serializable{

    @EJB
    private CarreraFacadeLocal facadeLocal;
    private Carrera carrera;
    private List<Carrera> lista;
    private Facultad facultad;

    public Carrera getCarrera() {
        return carrera;
    }

    public void setCarrera(Carrera carrera) {
        this.carrera = carrera;
    }

    public List<Carrera> getLista() {
        return lista;
    }

    public void setLista(List<Carrera> lista) {
        this.lista = lista;
    }

    public Facultad getFacultad() {
        return facultad;
    }

    public void setFacultad(Facultad facultad) {
        this.facultad = facultad;
    }

    @PostConstruct
    public void init() {
        carrera = new Carrera();
        facultad = new Facultad();
        consultar();
    }

    public void consultar() {
        try {
            lista = facadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void insertar() {
        try {
            this.carrera.setFacultad(facultad);
            facadeLocal.create(carrera);
            if (carrera != null) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Registro Guardado "));
                init();
            }
            init();
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Error " + e.getMessage()));
        }
    }

    public void editar() {
        try {
            this.carrera.setFacultad(facultad);
            facadeLocal.edit(carrera);
            if (carrera != null) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Registro Guardado "));
                init();
            }
            init();
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Error " + e.getMessage()));
        }
    }

    public void eliminar(Carrera carrera) {
        this.carrera = carrera;
        try {
            facadeLocal.remove(carrera);
        } catch (Exception e) {
        }
    }

    public void consultarById(Carrera carrera) {
        this.carrera = carrera;
        this.facultad.setId(carrera.getId());
    }
}
