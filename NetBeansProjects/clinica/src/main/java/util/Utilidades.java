
package util;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@ViewScoped
public class Utilidades {
    
    public void ponerMensaje(String mensaje){
        FacesMessage men=new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, men);
    }
}
