

package ejb;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Departamento;
import modelo.Municipio;

@Stateless
public class MunicipioFacade extends AbstractFacade<Municipio> implements MunicipioFacadeLocal{
    
    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    public MunicipioFacade() {
        super(Municipio.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    @Override
    public List<Municipio>  municipioLista(Departamento departamento)
    {
        List<Municipio> lista2 = new ArrayList<>();
        try {
            lista2 = em.createNativeQuery("select * from municipio where departamento ="+departamento.getId(),Municipio.class).getResultList();
        } catch (Exception e) {
        }
        return lista2;
    }
}
