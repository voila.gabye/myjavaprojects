
package ejb;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.Alumno;
import modelo.Ecologico_Social;
import modelo.Ecologico_Social_;

@Stateless
public class Ecologico_SocialFacade extends AbstractFacade<Ecologico_Social> implements Ecologico_SocialFacadeLocal{
    
   @PersistenceContext(unitName = "clinicaPU")
   private EntityManager em;

    public Ecologico_SocialFacade() {
        super(Ecologico_Social.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
   @Override
    public List<Ecologico_Social> findEcologicoSocial(Ecologico_Social ecologico_Social)
    {
        CriteriaBuilder cb=em.getCriteriaBuilder();
        CriteriaQuery cq=cb.createQuery();
        
        Root<Ecologico_Social> ecoSocial=cq.from(Ecologico_Social.class);
        
        cq.where(cb.equal(ecoSocial.get(Ecologico_Social_.id), ecologico_Social.getId()));
        
        Query q=em.createQuery(cq);
        
        return q.getResultList();
    }
   @Override
    public List<Ecologico_Social> findXAlumno(Alumno alumno)
    {
        List<Ecologico_Social> list=new ArrayList<>();
        
        list=em.createNativeQuery("select * from ecologico_social where alumno="+alumno.getId(),Ecologico_Social.class).getResultList();
        return list;
    }
}
