
package ejb;

import java.util.LinkedList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import modelo.Alumno;
import modelo.Diagnostico;

@Stateless
public class DiagnosticoFacade  extends AbstractFacade<Diagnostico> implements DiagnosticoFacadeLocal{

    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    public DiagnosticoFacade() {
        super(Diagnostico.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    @Override
    public List<Diagnostico> diagnosticoXAlumno(Alumno alumno)
    {
        List<Diagnostico> lista = new LinkedList<>();
        try {

            String sql = "select * from diagnostico where alumno="+alumno.getId();
            Query q = em.createNativeQuery(sql, Diagnostico.class);
            lista =(List<Diagnostico>) q.getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }
}
