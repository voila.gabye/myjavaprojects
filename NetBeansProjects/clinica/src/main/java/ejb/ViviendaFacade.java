/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Alumno;
import modelo.Vivienda;

/**
 *
 * @author alejandro.suarezusam
 */
@Stateless
public class ViviendaFacade extends AbstractFacade<Vivienda> implements ViviendaFacadeLocal {

    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    public ViviendaFacade() {
        super(Vivienda.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
    @Override
    public List<Vivienda> findVivienda(Alumno alumno)
    {
        List<Vivienda> list=new ArrayList<>();
        try {
            list=em.createNativeQuery("select * from vivienda inner join ecologico_social on ecologico_social.id=vivienda.ecologico_social where alumno="+alumno.getId(),Vivienda.class).getResultList();
        } catch (Exception e) {
        }
        return list;
    }

}
