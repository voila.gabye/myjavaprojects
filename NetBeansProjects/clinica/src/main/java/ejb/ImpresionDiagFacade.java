/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Impresion_diag;

/**
 *
 * @author alejandro.suarezusam
 */
@Stateless
public class ImpresionDiagFacade extends AbstractFacade<Impresion_diag> implements ImpresionDiagFacadeLocal{
    
    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;
    
    public ImpresionDiagFacade(){
        super(Impresion_diag.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
}
