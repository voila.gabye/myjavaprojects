
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Tipo_Techo;

@Local
public interface Tipo_TechoFacadeLocal {
    void create(Tipo_Techo tipo_Techo);

    void edit(Tipo_Techo tipo_Techo);

    void remove(Tipo_Techo tipo_Techo);

    public List<Tipo_Techo> findAll();
    
}
