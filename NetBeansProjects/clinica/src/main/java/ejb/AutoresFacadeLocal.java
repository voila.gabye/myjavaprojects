package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Autores;

@Local
public interface AutoresFacadeLocal {

    List<Autores> findAll();
}
