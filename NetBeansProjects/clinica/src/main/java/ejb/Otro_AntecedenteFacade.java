package ejb;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import modelo.Alumno;
import modelo.Municipio;
import modelo.Otro_Antecedente;

@Stateless
public class Otro_AntecedenteFacade extends AbstractFacade<Otro_Antecedente> implements Otro_AntecedenteFacadeLocal {

    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public Otro_AntecedenteFacade() {
        super(Otro_Antecedente.class);
    }

    @Override
    public List<Otro_Antecedente> listaGenero(int id) {

        List<Otro_Antecedente> lista = new ArrayList<>();
        String sql = "select  oa from Otro_Antecedente oa where oa.alumno.genero.id = ?1";

        try {
            Query q = em.createQuery(sql);
            q.setParameter(1, id);
            lista = q.getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }

    @Override
    public List<Otro_Antecedente> listaCarrera(int id) {

        List<Otro_Antecedente> lista = new ArrayList<>();
        String sql = "select  oa from Otro_Antecedente oa where oa.alumno.carrera.id = ?1";

        try {
            Query q = em.createQuery(sql);
            q.setParameter(1, id);
            lista = q.getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }

    @Override
    public List<Otro_Antecedente> listaMunicipio(Municipio municipio) {
        List<Otro_Antecedente> lista = new LinkedList<>();
        try {

            String sql = "select  oa from Otro_Antecedente oa where oa.alumno.municipio.id = :id";
            Query q = em.createQuery(sql);
            q.setParameter("id", municipio.getId());
            lista =(List<Otro_Antecedente>) q.getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }
    
    @Override
    public List<Otro_Antecedente> otro_AntecedenteXAlumno(Alumno alumno) {
        List<Otro_Antecedente> lista = new LinkedList<>();
        try {

            String sql = "select *from otro_antecedente where alumno="+alumno.getId();
            Query q = em.createNativeQuery(sql, Otro_Antecedente.class);
            lista =(List<Otro_Antecedente>) q.getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }

}
