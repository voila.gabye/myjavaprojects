package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Alumno;
import modelo.Ante_familiar;
import modelo.Carrera;
import modelo.Heredo_alumno;

@Local
public interface Heredo_alumnoFacadeLocal {

    void create(Heredo_alumno heredo_alumno);

    void edit(Heredo_alumno heredo_alumno);

    void remove(Heredo_alumno heredo_alumno);

    List<Heredo_alumno> findAll();
    
    public List<Heredo_alumno> consultarHeredo_alumnoXalumno(Alumno alumno);
    
    List<Heredo_alumno> findByEnfermedad (Ante_familiar ante_familiar);
    
    List<Heredo_alumno> findByCarrera(Carrera carrera);
    
    List<Heredo_alumno> findByNombre(String nombre);
    
    List<Heredo_alumno> findByEdad(int edad);
    
    public List<Heredo_alumno> findByCarreraEnfermedad(Carrera carrera,Ante_familiar ante_familiar,String acuerdo);
}
