package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Tipo_piso;

@Local
public interface Tipo_pisoFacadeLocal {

    void create(Tipo_piso tipo_piso);

    void edit(Tipo_piso tipo_piso);

    void remove(Tipo_piso tipo_piso);

    List<Tipo_piso> findAll();
}
