package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Estado_civil;

@Stateless
public class Estado_civilFacade extends AbstractFacade<Estado_civil> implements Estado_civilFacadeLocal {

    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public Estado_civilFacade() {
        super(Estado_civil.class);
    }

}
