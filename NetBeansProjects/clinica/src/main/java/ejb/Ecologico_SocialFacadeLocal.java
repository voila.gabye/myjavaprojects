
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Alumno;
import modelo.Ecologico_Social;

@Local
public interface Ecologico_SocialFacadeLocal {
    
    void create(Ecologico_Social ecologico_Social);

    void edit(Ecologico_Social ecologico_Social);

    void remove(Ecologico_Social ecologico_Social);

    public List<Ecologico_Social> findAll();
     public List<Ecologico_Social> findEcologicoSocial(Ecologico_Social ecologico_Social);
     public List<Ecologico_Social> findXAlumno(Alumno alumno);
}
