package ejb;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Alumno;
import modelo.Ecologico_Mascota;

@Stateless
public class Ecologico_MascotaFacade extends AbstractFacade<Ecologico_Mascota> implements Ecologico_MascotaFacadeLocal {

    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    public Ecologico_MascotaFacade() {
        super(Ecologico_Mascota.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public List<Ecologico_Mascota> ecoMascotaByAlumno(Alumno alumno) {

        List<Ecologico_Mascota> lista = new ArrayList<>();
        try {

            lista = em.createNativeQuery("select * from ecologico_mascota inner join ecologico_social on ecologico_social.id=ecologico_mascota.ecologico_social inner join mascota on ecologico_mascota.mascota=mascota.id where ecologico_social.alumno=" + alumno.getId(), Ecologico_Mascota.class).getResultList();

            return lista;
        } catch (Exception e) {
            return null;
        }

    }
}
