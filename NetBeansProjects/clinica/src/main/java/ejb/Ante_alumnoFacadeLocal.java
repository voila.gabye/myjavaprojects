package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Alumno;
import modelo.Ante_alumno;
import modelo.Ante_personal;

@Local
public interface Ante_alumnoFacadeLocal {

    void create(Ante_alumno ante_alumno);

    void edit(Ante_alumno ante_alumno);

    void remove(Ante_alumno ante_alumno);

    List<Ante_alumno> findAll();
    
    public List<Ante_alumno> listaGenero(int id);
    
    public List<Ante_alumno> listaCarrera(int id);
    
     public List<Ante_alumno> listaMunicipio(int id);
     
    public List<Ante_alumno> anteAlumnoXAlumno(Alumno alumno);
    
    public List<Ante_alumno> findXemfermedadYcarrera(Ante_alumno a,String acuerdo);
    
     public List<Ante_alumno> findXemfermedadYfacultad(Ante_personal an);
}
