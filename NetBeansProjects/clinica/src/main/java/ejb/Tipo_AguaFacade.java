package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Tipo_Agua;

@Stateless
public class Tipo_AguaFacade extends AbstractFacade<Tipo_Agua> implements Tipo_AguaFacadeLocal {
    
    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public Tipo_AguaFacade() {
        super(Tipo_Agua.class);
    }

}
