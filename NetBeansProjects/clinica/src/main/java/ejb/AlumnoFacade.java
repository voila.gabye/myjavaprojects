package ejb;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import modelo.Alumno;
import modelo.Carrera;
import modelo.Genero;
import modelo.Municipio;

@Stateless
public class AlumnoFacade extends AbstractFacade<Alumno> implements AlumnoFacadeLocal {

    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    public AlumnoFacade() {
        super(Alumno.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    //buscar Alumno por genero
    @Override
    public List<Alumno> findGenero(Genero genero) {
        List<Alumno> lista = new ArrayList<>();
        String sql = "select  a from Alumno a where a.genero.id = ?1";

        try {
            Query q = em.createQuery(sql);
            q.setParameter(1, genero.getId());
            lista = q.getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }
    @Override
    public List<Alumno> findId(Alumno alumno) {
        List<Alumno> lista = new ArrayList<>();
        String sql = "select  a from Alumno a where a.id = ?1";

        try {
            Query q = em.createQuery(sql);
            q.setParameter(1, alumno.getId());
            lista = q.getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }

    // buscar por municipio
    @Override
    public List<Alumno> findCarrera(Carrera carrera) {

        List<Alumno> lista = new ArrayList<>();
        String sql = "select  c from Alumno c where c.carrera.id = ?1";

        try {
            Query q = em.createQuery(sql);
            q.setParameter(1, carrera.getId());
            lista = q.getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }

    //buscar por municipio
    @Override
    public List<Alumno> findMunicipio(Municipio municipio) {
        List<Alumno> lista = new LinkedList<>();
        try {

            String sql = "select  a from Alumno a where a.municipio.id = :id";
            Query q = em.createQuery(sql);
            q.setParameter("id", municipio.getId());
            lista = (List<Alumno>) q.getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }
    @Override
    public List<Alumno> findNombre(String nombre) {
        List<Alumno> lista = new LinkedList<>();
        try {

            String sql = "select * from alumno where nombres like '%"+nombre+"%'";
            Query q = em.createNativeQuery(sql,Alumno.class);
            lista = (List<Alumno>) q.getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }
    @Override
    public List<Alumno> findCarnet(int carnet) {
        List<Alumno> lista = new LinkedList<>();
        try {

            String sql = "select * from alumno where carnet like '%"+carnet+"%'";
            Query q = em.createNativeQuery(sql,Alumno.class);
            lista = (List<Alumno>) q.getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }
    @Override
    public List<Alumno> findAcuerdo(String acuerdo)
    {
         List<Alumno> lista = new LinkedList<>();
        try {

            String sql = "select * from alumno where acuerdo='"+acuerdo+"'";
            Query q = em.createNativeQuery(sql,Alumno.class);
            lista = (List<Alumno>) q.getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }

}
