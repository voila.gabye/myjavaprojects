package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Cons_pared;

@Local
public interface Cons_paredFacadeLocal {

    void create(Cons_pared pared);

    void edit(Cons_pared pared);

    void remove(Cons_pared pared);

    List<Cons_pared> findAll();

}
