/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Impresion_diag;

/**
 *
 * @author alejandro.suarezusam
 */
@Local
public interface ImpresionDiagFacadeLocal {
    
    void create(Impresion_diag impre);
    
    void edit(Impresion_diag impre);
    
    void remove(Impresion_diag impre);
    
    List<Impresion_diag> findAll();
    
    
}
