
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Tipo_piso;

@Stateless
public class Tipo_pisoFacade extends AbstractFacade<Tipo_piso> implements Tipo_pisoFacadeLocal{
    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    public Tipo_pisoFacade() {
        super(Tipo_piso.class);
    }
    
   @Override
   public EntityManager getEntityManager(){
       return em;
   }
    
}
