package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Departamento;

@Local
public interface DepartamentoFacadeLocal {

    void create(Departamento departamento);

    void edit(Departamento departamento);

    void remove(Departamento departamento);

    public List<Departamento> findAll();
}
