package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Ante_riesgo;

@Local
public interface Ante_riesgoFacadeLocal {

    void create(Ante_riesgo ante_riesgo);

    void edit(Ante_riesgo ante_riesgo);

    void remove(Ante_riesgo ante_riesgo);

    List<Ante_riesgo> findAll();
}
