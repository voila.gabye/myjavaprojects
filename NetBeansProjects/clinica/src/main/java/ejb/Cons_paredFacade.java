package ejb;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Cons_pared;

@Stateless
public class Cons_paredFacade extends AbstractFacade<Cons_pared> implements Cons_paredFacadeLocal {

    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public Cons_paredFacade() {
        super(Cons_pared.class);
    }
    
   
}
