
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Tipo_Techo;

@Stateless
public class Tipo_TechoFacade extends AbstractFacade<Tipo_Techo> implements Tipo_TechoFacadeLocal{

    
    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    public Tipo_TechoFacade() {
        super(Tipo_Techo.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
