package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Estado_civil;

@Local
public interface Estado_civilFacadeLocal {

    void create(Estado_civil estadoCi);

    void edit(Estado_civil estadoCi);

    void remove(Estado_civil estadoCi);

    List<Estado_civil> findAll();

}
