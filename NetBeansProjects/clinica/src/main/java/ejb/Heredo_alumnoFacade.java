package ejb;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Alumno;
import modelo.Ante_familiar;
import modelo.Ante_personal;
import modelo.Carrera;
import modelo.Heredo_alumno;
import static modelo.Heredo_alumno_.ante_familiar;

@Stateless
public class Heredo_alumnoFacade extends AbstractFacade<Heredo_alumno> implements Heredo_alumnoFacadeLocal {

    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    public Heredo_alumnoFacade() {
        super(Heredo_alumno.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    @Override
    public List<Heredo_alumno> consultarHeredo_alumnoXalumno(Alumno alumno) {

        List<Heredo_alumno> lista = new ArrayList<>();
        String sql = "select * from heredo_alumno where alumno=" + alumno.getId();

        try {

            lista = em.createNativeQuery(sql, Heredo_alumno.class).getResultList();

        } catch (Exception e) {

        }
        return lista;
    }

    @Override
    public List<Heredo_alumno> findByEnfermedad(Ante_familiar ante_familiar) {
        String sql ="select * from heredo_alumno where ante_familiar="+ante_familiar.getId();
        List<Heredo_alumno> lista = new ArrayList<>();
        
        try {
            lista = em.createNativeQuery(sql, Heredo_alumno.class).getResultList();
        } catch (Exception e) {
            e.getMessage();
        }
        return lista;
    }
     @Override
    public List<Heredo_alumno> findByCarrera(Carrera carrera) {
        String sql ="select * from heredo_alumno ha inner join alumno a on ha.alumno=a.id where carrera="+carrera.getId();
        List<Heredo_alumno> lista = new ArrayList<>();
        
        try {
            lista = em.createNativeQuery(sql, Heredo_alumno.class).getResultList();
        } catch (Exception e) {
            e.getMessage();
        }
        return lista;
    }

    @Override
     public List<Heredo_alumno> findByNombre(String nombre) {
        String sql ="select * from heredo_alumno ha inner join alumno a on ha.alumno=a.id where a.nombres like '%"+nombre+"%'";
        List<Heredo_alumno> lista = new ArrayList<>();
        
        try {
            lista = em.createNativeQuery(sql, Heredo_alumno.class).getResultList();
        } catch (Exception e) {
            e.getMessage();
        }
        return lista;
    }
     @Override
     public List<Heredo_alumno> findByEdad(int edad) {
        String sql ="select * from heredo_alumno ha inner join alumno a on ha.alumno=a.id where edad="+edad;
        List<Heredo_alumno> lista = new ArrayList<>();
        
        try {
            lista = em.createNativeQuery(sql, Heredo_alumno.class).getResultList();
        } catch (Exception e) {
            e.getMessage();
        }
        return lista;
    }
    @Override
     public List<Heredo_alumno> findByCarreraEnfermedad(Carrera carrera,Ante_familiar ante_familiar,String acuerdo)
     {
         String sql ="select * from heredo_alumno inner join alumno on alumno.id=heredo_alumno.alumno where ante_familiar="+ante_familiar.getId()+" and carrera="+carrera.getId()+" and acuerdo='"+acuerdo+"'";
        List<Heredo_alumno> lista = new ArrayList<>();
        
        try {
            lista = em.createNativeQuery(sql, Heredo_alumno.class).getResultList();
        } catch (Exception e) {
            e.getMessage();
        }
        return lista;
         
     }
}
