package ejb;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Carrera;
import modelo.Facultad;

@Stateless
public class CarreraFacade extends AbstractFacade<Carrera> implements CarreraFacadeLocal {

    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CarreraFacade() {
        super(Carrera.class);
    }
     @Override
    public List<Carrera> carreList(Facultad facultad) {
        
        List<Carrera> lista2 = new ArrayList<>();
        try {
            lista2 = em.createNativeQuery("select * from carrera where facultad ="+facultad.getId(),Carrera.class).getResultList();
        } catch (Exception e) {
        }
        return lista2;
    }
}
