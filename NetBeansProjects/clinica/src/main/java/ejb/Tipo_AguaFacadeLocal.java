package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Tipo_Agua;

@Local
public interface Tipo_AguaFacadeLocal {
    
    void create(Tipo_Agua tipo_agua);

    void edit(Tipo_Agua tipo_agua);

    void remove(Tipo_Agua tipo_agua);

    List<Tipo_Agua> findAll();
}
