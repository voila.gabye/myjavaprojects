/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Alumno;
import modelo.Ecologico_Mascota;

@Local
public interface Ecologico_MascotaFacadeLocal {

    void create(Ecologico_Mascota ecoMascota);

    void edit(Ecologico_Mascota ecoMascota);

    void remove(Ecologico_Mascota ecoMascota);

    public List<Ecologico_Mascota> findAll();

    public List<Ecologico_Mascota> ecoMascotaByAlumno(Alumno alumno );

}
