/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Local;

import modelo.Usuario;

/**
 *
 * @author douglas.ramirezusam
 */
@Local
public interface UsuarioFacadeLocal {
    
    void create(Usuario usuario);

    void edit(Usuario usuario);

    void remove(Usuario usuario);

    List<Usuario> findAll();
    
    public Usuario login(Usuario usuario);
    
}
