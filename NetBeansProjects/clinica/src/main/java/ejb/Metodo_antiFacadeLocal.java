/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Metodo_anti;

@Local
public interface Metodo_antiFacadeLocal {

    void create(Metodo_anti metodo_anti);

    void edit(Metodo_anti metodo_anti);

    void remove(Metodo_anti metodo_anti);

    List<Metodo_anti> findAll();

}
