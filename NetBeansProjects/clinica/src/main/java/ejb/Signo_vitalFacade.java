package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Signo_vital;

@Stateless
public class Signo_vitalFacade extends AbstractFacade<Signo_vital> implements Signo_vitalFacadeLocal {

    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public Signo_vitalFacade() {
        super(Signo_vital.class);
    }

}
