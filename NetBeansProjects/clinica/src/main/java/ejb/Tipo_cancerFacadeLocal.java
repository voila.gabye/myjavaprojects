package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Tipo_cancer;

@Local
public interface Tipo_cancerFacadeLocal {

    void create(Tipo_cancer tipo_cancer);

    void edit(Tipo_cancer tipo_cancer);

    void remove(Tipo_cancer tipo_cancer);

    List<Tipo_cancer> findAll();
}
