package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Alumno;
import modelo.Carrera;
import modelo.Genero;
import modelo.Municipio;

@Local
public interface AlumnoFacadeLocal {

    void create(Alumno alumno);
    void edit(Alumno alumno);
    void remove(Alumno alumno);
    List<Alumno> findAll();
    public List<Alumno> findGenero(Genero genero);
    public List<Alumno> findMunicipio(Municipio municipio);
    public List<Alumno> findCarrera(Carrera carrera);
    public List<Alumno> findNombre(String nombre);
    public List<Alumno> findCarnet(int carnet);
    public List<Alumno> findId(Alumno alumno);
    public List<Alumno> findAcuerdo(String acuerdo);
}
