
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.Usuario;
import modelo.Usuario_;

@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> implements UsuarioFacadeLocal {

    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }

    @Override
    public Usuario login(Usuario usuario) {
        try {

            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();

            Root<Usuario> root = cq.from(Usuario.class);

            cq.where(cb.equal(root.get(Usuario_.usuario), usuario.getUsuario()), cb.and(cb.equal(root.get(Usuario_.clave), usuario.getClave())));
            Query q = em.createQuery(cq);

            usuario = (Usuario) q.getSingleResult();

            if (null == usuario) {
                usuario = null;
            } else {
                return usuario;
            }

        } catch (Exception e) {
            usuario = null;
        }
        return usuario;
    }
    
}

