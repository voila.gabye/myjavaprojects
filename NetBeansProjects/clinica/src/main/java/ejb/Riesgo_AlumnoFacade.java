package ejb;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Alumno;
import modelo.Riesgo_Alumno;

@Stateless
public class Riesgo_AlumnoFacade extends AbstractFacade<Riesgo_Alumno> implements Riesgo_AlumnoFacadeLocal{
    
    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    public Riesgo_AlumnoFacade() {
        super(Riesgo_Alumno.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return  em;
    }
    @Override
    public List<Riesgo_Alumno> riesgoxAlumno(Alumno alumno)
    {
        List<Riesgo_Alumno> list=new ArrayList<>();
        try {
            
            list=em.createNativeQuery("select * from riesgo_alumno where alumno="+alumno.getId(),Riesgo_Alumno.class).getResultList();
        } catch (Exception e) {
        }
        return list;
    }
    
    
}