/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Ante_familiar;

/**
 *
 * @author alejandro.suarezusam
 */
@Stateless
public class AnteFamiliarFacade extends AbstractFacade<Ante_familiar> implements AnteFamiliarFacadeLocal{
    
    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;
    
    public AnteFamiliarFacade(){
        super(Ante_familiar.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
}
