
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Departamento;
import modelo.Municipio;

@Local
public interface MunicipioFacadeLocal {
    
    void create(Municipio municipio);

    void edit(Municipio municipio);

    void remove(Municipio municipio);

    public List<Municipio> findAll();
    public List<Municipio>  municipioLista(Departamento departamento);
    
}
