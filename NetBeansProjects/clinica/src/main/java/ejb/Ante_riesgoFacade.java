
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Ante_riesgo;

@Stateless
public class Ante_riesgoFacade extends AbstractFacade<Ante_riesgo> implements Ante_riesgoFacadeLocal{
    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    public Ante_riesgoFacade() {
        super(Ante_riesgo.class);
    }
    @Override
    public EntityManager getEntityManager(){
        return em;
    }
}
