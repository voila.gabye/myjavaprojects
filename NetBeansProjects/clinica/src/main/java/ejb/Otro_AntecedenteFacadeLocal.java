package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Alumno;
import modelo.Municipio;
import modelo.Otro_Antecedente;

@Local
public interface Otro_AntecedenteFacadeLocal {

    void create(Otro_Antecedente otro_antecedente);

    void edit(Otro_Antecedente otro_antecedente);

    void remove(Otro_Antecedente otro_antecedente);

    public List<Otro_Antecedente> findAll();

    public List<Otro_Antecedente> listaGenero(int id);

    public List<Otro_Antecedente> listaCarrera(int id);

    public List<Otro_Antecedente> listaMunicipio(Municipio municipio);

    public List<Otro_Antecedente> otro_AntecedenteXAlumno(Alumno alumno);
}
