/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Tipo_sangre;

/**
 *
 * @author douglas.ramirezusam
 */
@Stateless
public class Tipo_sangreFacade  extends AbstractFacade<Tipo_sangre> implements Tipo_sangreFacadeLocal {
    
     @PersistenceContext(unitName = "clinicaPU")
 private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
      return em;
    }
    
    public Tipo_sangreFacade(){
    super(Tipo_sangre.class);
    }
    
   
    
}
