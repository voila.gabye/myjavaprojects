package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Ante_personal;

@Local
public interface Ante_personalFacadeLocal {

    void create(Ante_personal ante_personal);

    void edit(Ante_personal ante_personal);

    void remove(Ante_personal ante_personal);

    List<Ante_personal> findAll();
}
