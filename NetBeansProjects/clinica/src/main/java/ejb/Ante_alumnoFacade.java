package ejb;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import modelo.Alumno;
import modelo.Ante_alumno;
import modelo.Ante_personal;

@Stateless
public class Ante_alumnoFacade extends AbstractFacade<Ante_alumno> implements Ante_alumnoFacadeLocal {

    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public Ante_alumnoFacade() {
        super(Ante_alumno.class);
    }

    @Override
    public List<Ante_alumno> listaGenero(int id) {

        List<Ante_alumno> lista = new ArrayList<>();
        String sql = "select  aa from Ante_alumno aa where aa.alumno.genero.id = ?1";

        try {
            Query q = em.createQuery(sql);
            q.setParameter(1, id);
            lista = q.getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }

    @Override
    public List<Ante_alumno> listaCarrera(int id) {

        List<Ante_alumno> lista = new ArrayList<>();
        String sql = "select  aa from Ante_alumno aa where aa.alumno.carrera.id = ?1";

        try {
            Query q = em.createQuery(sql);
            q.setParameter(1, id);
            lista = q.getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }

    @Override
    public List<Ante_alumno> listaMunicipio(int id) {

        List<Ante_alumno> lista = new ArrayList<>();
        String sql = "select  aa from Ante_alumno aa where aa.alumno.municipio.id = ?1";

        try {
            Query q = em.createQuery(sql);
            q.setParameter(1, id);
            lista = q.getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }

    @Override
    public List<Ante_alumno> anteAlumnoXAlumno(Alumno alumno) {
        List<Ante_alumno> lista = new ArrayList<>();

        String sql="select * from ante_alumno where alumno="+alumno.getId();
        try {
            lista = em.createNativeQuery(sql, Ante_alumno.class).getResultList();
        } catch (Exception e) {
        }
        return lista;
    }
     @Override
    public List<Ante_alumno> findXemfermedadYcarrera(Ante_alumno a,String acuerdo) {
        List<Ante_alumno> lista = new LinkedList<>();
        try {

            String sql = "select * from ante_alumno inner join alumno on alumno.id=ante_alumno.alumno where ante_personal=" + a.getAnte_personal().getId() + " and alumno.carrera=" + a.getAlumno().getCarrera().getId() + " and acuerdo='"+acuerdo+"'";
            lista = em.createNativeQuery(sql, Ante_alumno.class).getResultList();
            

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }
    
        @Override
    public List<Ante_alumno> findXemfermedadYfacultad(Ante_personal an) {
        List<Ante_alumno> lista = new ArrayList<>();

        String sql="select * from ante_alumno inner join alumno on alumno.id=ante_alumno.alumno where ante_personal="+an.getId();
        try {
            lista = em.createNativeQuery(sql, Ante_alumno.class).getResultList();
        } catch (Exception e) {
        }
        return lista;
    }
}
