/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Alumno;
import modelo.Riesgo_Alumno;

@Local
public interface Riesgo_AlumnoFacadeLocal {

    void create(Riesgo_Alumno riesgo_Alumno);

    void edit(Riesgo_Alumno riesgo_Alumno);

    void remove(Riesgo_Alumno riesgo_Alumno);

    List<Riesgo_Alumno> findAll();
     public List<Riesgo_Alumno> riesgoxAlumno(Alumno alumno);

}