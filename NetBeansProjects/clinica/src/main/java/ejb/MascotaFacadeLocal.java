/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Mascota;

/**
 *
 * @author alejandro.suarezusam
 */
@Local
public interface MascotaFacadeLocal {
     
    void create(Mascota mascota);
    
    void edit(Mascota mascota);
    
    void remove(Mascota mascota);
    
    List<Mascota> findAll();
}
