/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Alumno;
import modelo.Vivienda;

/**
 *
 * @author alejandro.suarezusam
 */
@Local
public interface ViviendaFacadeLocal {
    void create(Vivienda vivienda);
    
    void edit(Vivienda vivienda);
    
    void remove(Vivienda vivienda);
    
    List<Vivienda> findAll();
    public List<Vivienda> findVivienda(Alumno alumno);
}
