package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Signo_vital;

@Local
public interface Signo_vitalFacadeLocal {

    void create(Signo_vital signo_vital);

    void edit(Signo_vital signo_vital);

    void remove(Signo_vital signo_vital);

    List<Signo_vital> findAll();

}
