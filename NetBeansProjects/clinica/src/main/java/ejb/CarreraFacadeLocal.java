package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Carrera;
import modelo.Facultad;

@Local
public interface CarreraFacadeLocal {

    void create(Carrera carrera);

    void edit(Carrera carrera);

    void remove(Carrera carrera);

    List<Carrera> findAll();
    public List<Carrera> carreList(Facultad facultad);
}
