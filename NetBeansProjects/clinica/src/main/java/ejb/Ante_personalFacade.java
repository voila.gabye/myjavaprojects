
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Ante_personal;

@Stateless
public class Ante_personalFacade extends AbstractFacade<Ante_personal> implements Ante_personalFacadeLocal{
    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    public Ante_personalFacade() {
        super(Ante_personal.class);
    }
    
    public EntityManager getEntityManager(){
        return em;
    }
}
