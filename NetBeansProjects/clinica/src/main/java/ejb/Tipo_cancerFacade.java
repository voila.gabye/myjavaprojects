package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Tipo_cancer;

@Stateless
public class Tipo_cancerFacade extends AbstractFacade<Tipo_cancer> implements Tipo_cancerFacadeLocal {

    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public Tipo_cancerFacade() {
        super(Tipo_cancer.class);
    }

}
