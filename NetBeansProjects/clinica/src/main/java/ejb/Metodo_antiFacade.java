package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Metodo_anti;

@Stateless
public class Metodo_antiFacade extends AbstractFacade<Metodo_anti> implements Metodo_antiFacadeLocal {

    @PersistenceContext(unitName = "clinicaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public Metodo_antiFacade() {
        super(Metodo_anti.class);
    }

}
