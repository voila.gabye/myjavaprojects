/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Ante_familiar;

/**
 *
 * @author alejandro.suarezusam
 */
@Local
public interface AnteFamiliarFacadeLocal {
    void create(Ante_familiar antefamilair);
    
    void edit(Ante_familiar ante_familiar);
    
    void remove(Ante_familiar ante_familiar);
    
    List<Ante_familiar> findAll();
}
