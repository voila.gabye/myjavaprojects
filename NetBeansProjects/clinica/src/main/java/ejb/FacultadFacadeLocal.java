/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Facultad;

@Local
public interface FacultadFacadeLocal {

    void create(Facultad facultada);

    void edit(Facultad facultada);

    void remove(Facultad facultada);

    List<Facultad> findAll();
}
