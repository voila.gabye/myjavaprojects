/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Tipo_sangre;

/**
 *
 * @author douglas.ramirezusam
 */
@Local
public interface Tipo_sangreFacadeLocal {
    void create(Tipo_sangre tipo_sangre);

    void edit(Tipo_sangre tipo_sangre);

    void remove(Tipo_sangre tipo_sangre);

    List<Tipo_sangre> findAll();
}
