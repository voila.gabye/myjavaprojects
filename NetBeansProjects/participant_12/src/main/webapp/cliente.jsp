<%-- 
    Document   : cliente
    Created on : 10-18-2019, 04:48:28 PM
    Author     : gabriela.francousam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cliente</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="js/bootstrap.js" type="text/javascript"></script>
    </head>
    <body>
          <header>
            <center><h1 class="text-dark">Cliente</h1> </center>
        </header>
        <div class="container">
            <div class="row">
                <div class="col-6 text-dark">
                    <form action="cliente?action=insertar" method="POST">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input class="form-control" name="nombre">
                             <label>Apellido</label>
                            <input class="form-control" name="apellido">
                            <label>Direccion</label>
                            <input class="form-control" name="direccion">
                            <label>Fecha de Nacimiento</label>
                            <input class="form-control" name="fecha_nacimiento">
                            <label>Telefono</label>
                            <input class="form-control" name="telefono">
                            <label>Email</label>
                            <input class="form-control" name="email">
                        </div>
                        <button class="btn btn-outline-success">Enviar</button> 
                    </form>
                    ${msg}
                    <br>
                     <a class="btn btn-outline-info" href="cliente?action=consultar" role="button">Consultar</a>   
                </div>          
            </div>
        </div>
    </body>
</html>
