package conexion;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {

    static String bd = "participante_12";
    static String user = "root";
    static String pass = "root";
    static String url = "jdbc:mysql://localhost/" + bd + "?useSSL=false";
    Connection conn = null;

    public Conexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            conn = DriverManager.getConnection(url, user, pass);
            if (conn != null) {
                System.out.println("Exito en la conexion");
            }
        } catch (Exception e) {
            System.out.println("Error en la conexion");
        }
    }

    public Connection connectar() {
        return conn;
    }

    public void desconectar() throws Exception {
        conn.close();
    }
}
