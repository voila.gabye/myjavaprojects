
package controlador;

import conexion.Conexion;
import dao.ClienteDao;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Cliente;


public class ClienteServlet extends HttpServlet {

    Conexion conn = new Conexion();
    ClienteDao cliented = new ClienteDao(conn);
    RequestDispatcher rd;
    String msg;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
         String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "actualizar":
                actualizar(request, response);
                break;
            case "consultarAll":
                consultarAll(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            default:
                throw new AssertionError();
        }
    }
protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        SimpleDateFormat formato = new SimpleDateFormat("yy-MM-dd");
        String nombre = request.getParameter("nombre");
        String apellido = request.getParameter("apellido");
        String direccion = request.getParameter("direccion");
        Date fecha_nacimiento = formato.parse(request.getParameter("fecha_nacimiento"));
        String telefono = request.getParameter("telefono");
        String email = request.getParameter("email");

        Cliente clienteb = new Cliente(0);
        clienteb.setNombre(nombre);
        clienteb.setApellido(apellido);
        clienteb.setDireccion(direccion);
        clienteb.setFecha_nacimiento(fecha_nacimiento);
        clienteb.setTelefono(telefono);
        clienteb.setEmail(email);

        boolean respuesta = cliented.guardarClientes(clienteb);
        if (respuesta) {
            msg = "Registro fue guardado con éxito";
        } else {
            msg = "Registro no fue guardado";
        }

        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/cliente.jsp");
        rd.forward(request, response);
    }

    protected void actualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    protected void consultarAll(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(ClienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(ClienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

   

}
