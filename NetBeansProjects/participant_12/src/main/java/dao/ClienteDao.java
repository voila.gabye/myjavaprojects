
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import modelo.Cliente;

public class ClienteDao {
 
     Conexion conn;

    public ClienteDao(Conexion conn) {
        this.conn = conn;
    }

    //Clientes
    public boolean guardarClientes(Cliente clienteb) {
        String sql = "insert into cliente values (?,?,?,?,?,?,?)";
        try {
            SimpleDateFormat formato = new SimpleDateFormat("yy-MM-dd");
            PreparedStatement ps = conn.connectar().prepareStatement(sql);
            ps.setInt(1, clienteb.getId_cliente());
            ps.setString(2, clienteb.getNombre());
            ps.setString(3, clienteb.getApellido());
            ps.setString(4, clienteb.getDireccion());
            ps.setString(5, formato.format(clienteb.getFecha_nacimiento()).toString());
            ps.setString(6, clienteb.getTelefono());
            ps.setString(7, clienteb.getEmail());
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }
}
