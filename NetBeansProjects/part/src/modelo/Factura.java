package modelo;

public class Factura {

    private int num_factura;

    private Cliente id_cliente;

    private String fecha;
    
    private Modo_Pago num_pago;

    public Factura(int num_factura) {
        this.num_factura = num_factura;
    }
    
    public int getNum_factura() {
        return num_factura;
    }

    public void setNum_factura(int num_factura) {
        this.num_factura = num_factura;
    }

    public Cliente getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Cliente id_cliente) {
        this.id_cliente = id_cliente;
    }

 
    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Modo_Pago getNum_pago() {
        return num_pago;
    }

    public void setNum_pago(Modo_Pago num_pago) {
        this.num_pago = num_pago;
    }
    
}
