package controller;

import ejb.EntrenadorJugadorFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import modelo.Entrenador;
import modelo.Entrenador_Jugador;
import modelo.Jugador;

@Named(value = "entrenadorJugadorController")
@SessionScoped
public class EntrenadorJugadorController implements Serializable {

    @EJB
    private EntrenadorJugadorFacadeLocal entrenadorJugadorFacadeLocal;
    private Entrenador_Jugador entrenador_jugador;
    private List<Entrenador_Jugador> lista;
    private Jugador jugador;
    private Entrenador entrenador;

    @PostConstruct
    public void init() {
        entrenador_jugador = new Entrenador_Jugador();
        entrenador = new Entrenador();
        jugador = new Jugador();
        consultar();
    }

    public Entrenador_Jugador getEntrenador_jugador() {
        return entrenador_jugador;
    }

    public void setEntrenador_jugador(Entrenador_Jugador entrenador_jugador) {
        this.entrenador_jugador = entrenador_jugador;
    }

    public List<Entrenador_Jugador> getLista() {
        lista=entrenadorJugadorFacadeLocal.findAll();
        return lista;
    }

    public void setLista(List<Entrenador_Jugador> lista) {
        this.lista = lista;
    }

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public Entrenador getEntrenador() {
        return entrenador;
    }

    public void setEntrenador(Entrenador entrenador) {
        this.entrenador = entrenador;
    }

    public void insertar() {
        entrenador_jugador.setEntrenador(entrenador);
        entrenador_jugador.setIdjugador(jugador);
        entrenadorJugadorFacadeLocal.create(entrenador_jugador);
        init();
    }

    public void editar() {
        entrenador_jugador.setEntrenador(entrenador);
        entrenador_jugador.setIdjugador(jugador);
        entrenadorJugadorFacadeLocal.edit(entrenador_jugador);
        init();
    }

    public void eliminar(Entrenador_Jugador entrenadorJ) {
        
        entrenadorJugadorFacadeLocal.remove(entrenadorJ);
        consultar();
    }

    public void consultarById(Entrenador_Jugador entrenadorJ) {
        jugador.setIdjugador(entrenadorJ.getIdjugador().getIdjugador());
        entrenador.setIdentre(entrenadorJ.getEntrenador().getIdentre());
        this.entrenador_jugador=entrenadorJ;
    }
    
    public void consultar(){
        try {
            lista=entrenadorJugadorFacadeLocal.findAll();
        } catch (Exception e) {
        }        
    }

}
