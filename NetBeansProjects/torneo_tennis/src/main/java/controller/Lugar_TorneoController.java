/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.Lugar_TorneoFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import modelo.Ciudad;
import modelo.Lugar_Torneo;
import modelo.Pista;
import modelo.Torneo;

@Named(value = "lugar_TorneoController")
@SessionScoped
public class Lugar_TorneoController implements Serializable {
    
    @EJB
    private Lugar_TorneoFacadeLocal lugar_TorneoFacadeLocal;
    private Lugar_Torneo lugar;
    private List<Lugar_Torneo> lista;
    private Ciudad ciudad;
    private Pista pista;
    private Torneo torneo;
    private boolean boton=true;

    public boolean getBoton() {
        return boton;
    }

    public void setBoton(boolean boton) {
        this.boton = boton;
    }

    public Lugar_Torneo getLugar() {
        return lugar;
    }

    public void setLugar(Lugar_Torneo lugar) {
        this.lugar = lugar;
    }

    public List<Lugar_Torneo> getLista() {
        return lista;
    }

    public void setLista(List<Lugar_Torneo> lista) {
        this.lista = lista;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public Pista getPista() {
        return pista;
    }

    public void setPista(Pista pista) {
        this.pista = pista;
    }

    public Torneo getTorneo() {
        return torneo;
    }

    public void setTorneo(Torneo torneo) {
        this.torneo = torneo;
    }
    
    @PostConstruct
    public void init() {
        lugar = new Lugar_Torneo();
        ciudad = new Ciudad();
        pista = new Pista();
        torneo = new Torneo();
        consultar();
    }

    public void consultar() {
        try {
            lista = lugar_TorneoFacadeLocal.findAll();
        } catch (Exception e) {
        }
    }
    
    public void insertar() {
        try {
            
            this.lugar.setIdciudad(ciudad);
            this.lugar.setIdpista(pista);
            this.lugar.setIdtorneo(torneo);
            
            lugar_TorneoFacadeLocal.create(lugar);
            init();
            
        } catch (Exception e) {
        }
    }
    
    public void actualizar() {
        try {
            this.lugar.setIdciudad(ciudad);
            this.lugar.setIdpista(pista);
            this.lugar.setIdtorneo(torneo);
            
            lugar_TorneoFacadeLocal.edit(lugar);
            boton=true;
            init();
            
        } catch (Exception e) {
        }
    }

    public void eliminar(Lugar_Torneo lugar) {
        try {
            this.lugar = lugar;
            
            lugar_TorneoFacadeLocal.remove(lugar);
            init();
            
        } catch (Exception e) {
        }
    }
    public void consultarById(Lugar_Torneo lugar)
    {
        this.lugar=lugar;
        this.ciudad.setIdciudad(this.lugar.getIdciudad().getIdciudad());
        this.pista.setIdpista(this.lugar.getIdpista().getIdpista());
        this.torneo.setIdtorneo(this.lugar.getIdtorneo().getIdtorneo());
        boton=false;
    }
}
