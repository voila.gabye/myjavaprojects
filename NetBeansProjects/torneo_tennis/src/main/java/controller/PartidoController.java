package controller;

import ejb.PartidoFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import modelo.Arbitro;
import modelo.Fase;
import modelo.Premio;
import modelo.Lugar_Torneo;
import modelo.Modalidad;
import modelo.Partido;

@Named(value = "partidoController")
@SessionScoped
public class PartidoController implements Serializable {

    @EJB
    private PartidoFacadeLocal partidoFacadeLocal;
    private Partido partido;
    private List<Partido> lista;
    private Lugar_Torneo lugar;
    private Modalidad modalidad;
    private Arbitro arbitro;
    private Premio premio;
    private Fase fase;
    boolean boton=true;
    

    public Partido getPartido() {
        return partido;
    }

    public void setPartido(Partido partido) {
        this.partido = partido;
    }

    public List<Partido> getLista() {
        lista = partidoFacadeLocal.findAll();
        return lista;
    }

    public void setLista(List<Partido> lista) {
        this.lista = lista;
    }

    public Lugar_Torneo getLugar() {
        return lugar;
    }

    public void setLugar(Lugar_Torneo lugar) {
        this.lugar = lugar;
    }

    public Modalidad getModalidad() {
        return modalidad;
    }

    public void setModalidad(Modalidad modalidad) {
        this.modalidad = modalidad;
    }

    public Arbitro getArbitro() {
        return arbitro;
    }

    public void setArbitro(Arbitro arbitro) {
        this.arbitro = arbitro;
    }

    public Premio getPremio() {
        return premio;
    }

    public void setPremio(Premio premio) {
        this.premio = premio;
    }

    public Fase getFase() {
        return fase;
    }

    public void setFase(Fase fase) {
        this.fase = fase;
    }

    public boolean isBoton() {
        return boton;
    }

    public void setBoton(boolean boton) {
        this.boton = boton;
    }
    

    @PostConstruct
    public void init() {
        partido = new Partido();
        lugar = new Lugar_Torneo();
        modalidad = new Modalidad();
        arbitro = new Arbitro();
        premio = new Premio();
        fase = new Fase();
        consultar();
    }

    public void consultar() {
        try {
            lista = partidoFacadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void insertar() {
        try {
            this.partido.setIdfase(fase);
            this.partido.setIdlugar(lugar);
            this.partido.setIdmodal(modalidad);
            this.partido.setIdarbi(arbitro);
            this.partido.setIdpremio(premio);

            partidoFacadeLocal.create(partido);
            init();
        } catch (Exception e) {
        }
    }

    public void actualizar() {
        try {
            this.partido.setIdfase(fase);
            this.partido.setIdlugar(lugar);
            this.partido.setIdmodal(modalidad);
            this.partido.setIdarbi(arbitro);
            this.partido.setIdpremio(premio);

            partidoFacadeLocal.edit(partido);
            boton=true;
            init();
        } catch (Exception e) {
        }
    }

    public void eliminar(Partido partido) {
        try {
            this.partido = partido;

            partidoFacadeLocal.remove(partido);
            init();
        } catch (Exception e) {
        }
    }

    public void consultarById(Partido partido) {
        this.partido = partido;
        this.fase.setIdfase(partido.getIdfase().getIdfase());
        this.lugar.setIdlugar(this.partido.getIdlugar().getIdlugar());
        this.modalidad.setIdmodal(this.partido.getIdmodal().getIdmodal());
        this.arbitro.setIdarbi(this.partido.getIdarbi().getIdarbi());
        this.premio.setIdpremio(this.partido.getIdpremio().getIdpremio());
        boton=false;
    }
}
