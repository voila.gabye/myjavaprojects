
package controller;

import ejb.JugadorFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import modelo.Equipo;
import modelo.Genero;
import modelo.Jugador;


@Named(value = "jugadorController")
@SessionScoped
public class JugadorController implements Serializable{
    
    @EJB
    private JugadorFacadeLocal jugadorFacadeLocal;
    private Jugador jugador;
    private Genero genero;
    private Equipo equipo;
    private List<Jugador> lista;

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public List<Jugador> getLista() {
        lista=jugadorFacadeLocal.findAll();
        return lista;
    }

    public void setLista(List<Jugador> lista) {
        this.lista = lista;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }
    
    
    @PostConstruct
    public void init()
    {
        jugador=new Jugador();
        genero=new Genero();
        equipo=new Equipo();
        consultar();
    }
    public void insertar()
    {
        try {
            this.jugador.setIdgenero(genero);
            this.jugador.setIdequipo(equipo);
            jugadorFacadeLocal.create(jugador);
            
            init();
        } catch (Exception e) {
        }
    }
    public void actualizar()
    {
        try {
            this.jugador.setIdgenero(genero);
            this.jugador.setIdequipo(equipo);
            jugadorFacadeLocal.edit(jugador);
            init();
        } catch (Exception e) {
        }
    }
    public void eliminar(Jugador jugador)
    {
        this.jugador=jugador;
        try {
            jugadorFacadeLocal.remove(jugador);
            init();
            
        } catch (Exception e) {
        }
    }
    public void consultar()
    {
        try {
            lista=jugadorFacadeLocal.findAll();
        } catch (Exception e) {
        }
    }
    public void consultarById(Jugador jugador)
    {
        this.jugador=jugador;
        this.genero.setIdgenero(this.jugador.getIdgenero().getIdgenero());
        this.equipo.setIdequipo(this.jugador.getIdequipo().getIdequipo());
    }
    public String nacion(Jugador jugador)
    {
        this.jugador=jugador;
        return "nacionalidad.xhtml";
    }
}
