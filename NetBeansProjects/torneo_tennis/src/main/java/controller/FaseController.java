package controller;

import ejb.FaseFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import modelo.Fase;

@Named(value = "faseController")
@SessionScoped
public class FaseController implements Serializable {

    @EJB
    private FaseFacadeLocal faseFacadeLocal;
    private Fase fase;
    private List<Fase> lista;

    public Fase getFase() {
        return fase;
    }

    public void setFase(Fase fase) {
        this.fase = fase;
    }

    public List<Fase> getLista() {
        return lista;
    }

    public void setLista(List<Fase> lista) {
        this.lista = lista;
    }

    @PostConstruct
    public void init() {
        fase = new Fase();
        consultar();
    }

    public void insertar() {
        try {
            faseFacadeLocal.create(fase);
            init();
        } catch (Exception e) {
        }
    }

    public void actualizar() {
        try {
            faseFacadeLocal.edit(fase);
            init();
        } catch (Exception e) {
        }
    }

    public void eliminar(Fase fase) {
        this.fase = fase;
        try {
            faseFacadeLocal.remove(fase);
            init();

        } catch (Exception e) {
        }
    }

    public void consultar() {
        try {
            lista = faseFacadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void consultarById(Fase fase) {
        this.fase = fase;
    }

}
