package controller;

import ejb.EquipoFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import modelo.Equipo;
import modelo.Pais;

@Named(value = "equipoController")
@SessionScoped
public class EquipoController implements Serializable {

    @EJB
    private EquipoFacadeLocal equipoFacadeLocal;
    private Equipo equipo;
   
    private Pais pais;
    private List<Equipo> lista;
    private boolean boton=true;

    public boolean isBoton() {
        return boton;
    }

    public void setBoton(boolean boton) {
        this.boton = boton;
    }
    
    

    @PostConstruct
    public void init() {
        equipo = new Equipo();
        
        pais = new Pais();
        consultar();
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }

  

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public List<Equipo> getLista() {
        lista = equipoFacadeLocal.findAll();
        return lista;
    }

    public void setLista(List<Equipo> lista) {
        this.lista = lista;
    }

    public void insertar() {
        
        equipo.setPais(pais);
        equipoFacadeLocal.create(equipo);
        init();
    }

    public void editar() {
        
        equipo.setPais(pais);
        equipoFacadeLocal.edit(equipo);
        boton=true;
        init();
    }

    public void eliminar(Equipo equipo) {
        equipoFacadeLocal.remove(equipo);
        init();
    }

    public void consultarById(Equipo equipo) {
        
        
        this.equipo = equipo;
        pais.setIdpais(equipo.getPais().getIdpais());
        boton=false;
    }

    public void consultar() {
        lista = equipoFacadeLocal.findAll();
    }

}
