package controller;

import ejb.PistaFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import modelo.Pista;

@Named(value = "pistaController")
@SessionScoped
public class PistaController implements Serializable {

    @EJB
    private PistaFacadeLocal pistaFacadeLocal;
    private Pista pista;
    private List<Pista> lista;

    public Pista getPista() {
        return pista;
    }

    public void setPista(Pista pista) {
        this.pista = pista;
    }

    public List<Pista> getLista() {
        lista = pistaFacadeLocal.findAll();
        return lista;
    }

    public void setLista(List<Pista> lista) {
        this.lista = lista;
    }

    @PostConstruct
    public void init() {
        pista = new Pista();
        consultar();

    }

    public void insertar() {
        try {
            pistaFacadeLocal.create(pista);
            init();
        } catch (Exception e) {
        }
    }

    public void editar() {
        pistaFacadeLocal.edit(pista);
        init();
    }

    public void eliminar(Pista pista) {
        pistaFacadeLocal.remove(pista);
        init();
    }

    public void consultar() {
        try {
            lista = pistaFacadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void consultarById(Pista pista) {
        this.pista = pista;
    }
}
