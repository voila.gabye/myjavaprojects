package controller;

import ejb.EntrenadorFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import modelo.Entrenador;

@Named(value = "entrenadorController")
@SessionScoped
public class EntrenadorController implements Serializable {

    @EJB
    private EntrenadorFacadeLocal entrenadorFacadeLocal;
    private Entrenador entrenador;
    private List<Entrenador> lista;

    public Entrenador getEntrenador() {
        return entrenador;
    }

    public void setEntrenador(Entrenador entrenador) {
        this.entrenador = entrenador;
    }

    public List<Entrenador> getLista() {
        return lista;
    }

    public void setLista(List<Entrenador> lista) {
        this.lista = lista;
    }
    
    @PostConstruct
    public void init(){
    entrenador = new Entrenador();
    consultar();
    }
    
    public void insertar(){
        try {
            entrenadorFacadeLocal.create(entrenador);
            init();
        } catch (Exception e) {
        }
    }
    
    public void actualizar(){
        try {
            entrenadorFacadeLocal.edit(entrenador);
            init();
        } catch (Exception e) {
        }
    }
    
    public void eliminar(Entrenador entrenador){
    this.entrenador = entrenador;
        try {
            entrenadorFacadeLocal.remove(entrenador);
            init();
        } catch (Exception e) {
        }
    }
    
    public void consultar(){
        try {
            lista = entrenadorFacadeLocal.findAll();
        } catch (Exception e) {
        }
    }
    
    public void consultarById(Entrenador entrenador){
    this.entrenador = entrenador;
    }
    
}
