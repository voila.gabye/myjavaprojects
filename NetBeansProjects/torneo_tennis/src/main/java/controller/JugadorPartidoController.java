package controller;

import ejb.JugadorPartidoFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import modelo.Jugador;
import modelo.Jugador_Partido;
import modelo.Partido;

@Named(value = "jugadorPartidoController")
@SessionScoped
public class JugadorPartidoController implements Serializable {

    @EJB
    private JugadorPartidoFacadeLocal jugadorPartidoFacadeLocal;
    private Jugador_Partido jugadorPartido;
    private List<Jugador_Partido> lista;
    private Jugador jugador;
    private Partido partido;

    @PostConstruct
    public void init() {
        jugador = new Jugador();
        partido = new Partido();
        jugadorPartido = new Jugador_Partido();
    }

    public Jugador_Partido getJugadorPartido() {
        return jugadorPartido;
    }

    public void setJugadorPartido(Jugador_Partido jugadorPartido) {
        this.jugadorPartido = jugadorPartido;
    }

    public List<Jugador_Partido> getLista() {
        lista = jugadorPartidoFacadeLocal.findAll();
        return lista;
    }

    public void setLista(List<Jugador_Partido> lista) {
        this.lista = lista;
    }

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public Partido getPartido() {
        return partido;
    }

    public void setPartido(Partido partido) {
        this.partido = partido;
    }

    public void insertar() {
        jugadorPartido.setIdjugador(jugador);
        jugadorPartido.setIdpartido(partido);
        jugadorPartidoFacadeLocal.create(jugadorPartido);
        init();
    }

    public void editar() {
        jugadorPartido.setIdjugador(jugador);
        jugadorPartido.setIdpartido(partido);
        jugadorPartidoFacadeLocal.edit(jugadorPartido);
        init();
    }

    public void eliminar(Jugador_Partido jugadorP) {
        jugadorPartidoFacadeLocal.remove(jugadorP);
        consultar();
    }

    public void consultar() {
        lista = jugadorPartidoFacadeLocal.findAll();
    }

    public void consultarById(Jugador_Partido jugadorP) {
        jugador.setIdjugador(jugadorP.getIdjugador().getIdjugador());
        partido.setIdpartido(jugadorP.getIdpartido().getIdpartido());
        this.jugadorPartido = jugadorP;
    }

}
