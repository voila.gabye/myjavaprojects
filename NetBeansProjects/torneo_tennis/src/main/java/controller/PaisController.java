
package controller;

import ejb.PaisFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import modelo.Pais;


@Named(value = "paisController")
@SessionScoped
public class PaisController implements Serializable {
    
    @EJB
    private PaisFacadeLocal paisFacadeLocal;
    private Pais pais;
    private List<Pais> lista;

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public List<Pais> getLista() {
        return lista;
    }

    public void setLista(List<Pais> lista) {
        this.lista = lista;
    }
    
    @PostConstruct
    public void init()
    {
        pais=new Pais();
        consultar();
    }
    public void insertar()
    {
        try {
            paisFacadeLocal.create(pais);
            init();
        } catch (Exception e) {
        }
    }
    public void actualizar()
    {
        try {
            paisFacadeLocal.edit(pais);
            init();
        } catch (Exception e) {
        }
    }
    public void eliminar(Pais pais)
    {
        this.pais=pais;
        try {
            paisFacadeLocal.remove(pais);
            init();
            
        } catch (Exception e) {
        }
    }
    public void consultar()
    {
        try {
            lista=paisFacadeLocal.findAll();
        } catch (Exception e) {
        }
    }
    public void consultarById(Pais pais)
    {
        this.pais=pais;
    }

}
