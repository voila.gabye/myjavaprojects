package controller;

import ejb.ArbitroFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import modelo.Arbitro;

@Named(value = "arbitroController")
@SessionScoped
public class ArbitroController implements Serializable {

    @EJB
    private ArbitroFacadeLocal arbitroFacadeLocal;
    private Arbitro arbitro;
    private List<Arbitro> lista;

    public Arbitro getArbitro() {
        return arbitro;
    }

    public void setArbitro(Arbitro arbitro) {
        this.arbitro = arbitro;
    }

    public List<Arbitro> getLista() {
        return lista;
    }

    public void setLista(List<Arbitro> lista) {
        this.lista = lista;
    }

    @PostConstruct
    public void init() {
        arbitro = new Arbitro();
        consultar();
    }

    public void insertar() {
        try {
            arbitroFacadeLocal.create(arbitro);
            init();
        } catch (Exception e) {
        }
    }

    public void actualizar() {
        try {
            arbitroFacadeLocal.edit(arbitro);
            init();
        } catch (Exception e) {
        }
    }

    public void eliminar(Arbitro arbitro) {
        this.arbitro = arbitro;
        try {
            arbitroFacadeLocal.remove(arbitro);
            init();
        } catch (Exception e) {
        }
    }

    public void consultar() {
        try {
            lista = arbitroFacadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void consultarById(Arbitro arbitro) {
        this.arbitro = arbitro;
    }

}
