package controller;

import ejb.FaseFacadeLocal;
import ejb.PremioFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import modelo.Fase;
import modelo.Premio;

@Named(value = "premioController")
@SessionScoped
public class PremioController implements Serializable {

    @EJB
    private PremioFacadeLocal premioFacadeLocal;
    private Premio premio;
    private List<Premio> lista;
    @EJB
    private FaseFacadeLocal faseFacadeLocal;
    private Fase fase;
    private List<Fase> listafase;
    boolean boton=true;

    public boolean isBoton() {
        return boton;
    }

    public void setBoton(boolean boton) {
        this.boton = boton;
    }
    public Premio getPremio() {
        return premio;
    }

    public void setPremio(Premio premio) {
        this.premio = premio;
    }

    public List<Premio> getLista() {
        return lista;
    }

    public void setLista(List<Premio> lista) {
        this.lista = lista;
    }

    public Fase getFase() {
        return fase;
    }

    public void setFase(Fase fase) {
        this.fase = fase;
    }

    public List<Fase> getListafase() {
        return listafase;
    }

    public void setListafase(List<Fase> listafase) {
        this.listafase = listafase;
    }
    
    @PostConstruct
    public void init(){
    premio = new Premio();
    fase = new Fase();
    consultar();
    listafase();
    }
    
    public void insertar(){
        try {
           this.premio.setIdfase(fase);
           premioFacadeLocal.create(premio);
           init();
        } catch (Exception e) {
        }
    }
    
    public void actualizar(){
        try {
            this.premio.setIdfase(fase);
           premioFacadeLocal.edit(premio);
           boton=true;
           init();
        } catch (Exception e) {
        }
    }
    
    public void eliminar(Premio premio){
    this.premio = premio;
        try {
            premioFacadeLocal.remove(premio);
            init();
        } catch (Exception e) {
        }
    }
    
    public void consultar(){
        try {
            lista = premioFacadeLocal.findAll();
        } catch (Exception e) {
        }
    }
    
    public void consultarById(Premio premio){
    this.premio = premio;
    boton=false;
    }
    
    public void listafase(){
        try {
            listafase = faseFacadeLocal.findAll();
        } catch (Exception e) {
        }
    }
    
}
