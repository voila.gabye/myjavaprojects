package controller;

import ejb.PaisFacadeLocal;
import ejb.TorneoFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import modelo.Pais;
import modelo.Torneo;

@Named(value = "torneoController")
@SessionScoped
public class TorneoController implements Serializable {

    @EJB
    private TorneoFacadeLocal torneoFacadeLocal;
    private Torneo torneo;
    private List<Torneo> lista;
    private Pais pais;

    public Torneo getTorneo() {
        return torneo;
    }

    public void setTorneo(Torneo torneo) {
        this.torneo = torneo;
    }

    public List<Torneo> getLista() {
        return lista;
    }

    public void setLista(List<Torneo> lista) {
        this.lista = lista;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    @PostConstruct
    public void init() {
        torneo = new Torneo();
        pais = new Pais();
        consultar();
    }

    public void insertar() {
        try {
            this.torneo.setIdpais(pais);
            torneoFacadeLocal.create(torneo);
            init();
        } catch (Exception e) {
        }
    }

    public void actualizar() {
        try {
            this.torneo.setIdpais(pais);
            torneoFacadeLocal.edit(torneo);
            init();
        } catch (Exception e) {
        }
    }

    public void eliminar(Torneo torneo) {
        this.torneo = torneo;
        try {
            torneoFacadeLocal.remove(torneo);
        } catch (Exception e) {
        }
    }

    public void consultar() {
        try {
            lista = torneoFacadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void consultarById(Torneo torneo) {
        this.torneo = torneo;
    }
}
