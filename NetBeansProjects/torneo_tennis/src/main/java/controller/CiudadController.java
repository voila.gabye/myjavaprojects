
package controller;

import ejb.CiudadFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import modelo.Ciudad;
import modelo.Pais;

@Named(value = "ciudadController")
@SessionScoped
public class CiudadController implements Serializable {

    @EJB
    private CiudadFacadeLocal ciudadFacadeLocal;
    private Ciudad ciudad;
    private Pais pais;
    private List<Ciudad> lista;

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public List<Ciudad> getLista() {
        return lista;
    }

    public void setLista(List<Ciudad> lista) {
        this.lista = lista;
    }
    
    @PostConstruct
    public void init()
    {
        ciudad=new Ciudad();
        pais=new Pais();
        consultar();
        
    }
    public void insertar()
    {
        try {
            
            this.ciudad.setIdpais(pais);
            ciudadFacadeLocal.create(ciudad);
            init();
        } catch (Exception e) {
        }
    }
    public void actualizar()
    {
        try {
            
            this.ciudad.setIdpais(pais);
            ciudadFacadeLocal.edit(ciudad);
            init();
        } catch (Exception e) {
        }
    }
    public void eliminar(Ciudad ciudad)
    {
        this.ciudad=ciudad;
        try {
            ciudadFacadeLocal.remove(ciudad);
            init();
        } catch (Exception e) {
        }
    }
    public void consultar()
    {
        try {
            lista=ciudadFacadeLocal.findAll();
            
        } catch (Exception e) {
        }
    }
    public void consultarById(Ciudad ciudad)
    {
        
        this.ciudad=ciudad;
        this.pais.setIdpais(this.ciudad.getIdpais().getIdpais());
    }
}
