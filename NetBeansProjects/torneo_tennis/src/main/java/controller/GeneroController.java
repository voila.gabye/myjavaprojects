/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.GeneroFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import modelo.Genero;

@Named(value = "generoController")
@SessionScoped
public class GeneroController implements Serializable {

    @EJB
    private GeneroFacadeLocal generoFacadeLocal;
    private Genero genero;
    private List<Genero> lista;

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public List<Genero> getLista() {
        return lista;
    }

    public void setLista(List<Genero> lista) {
        this.lista = lista;
    }

    @PostConstruct
    public void init() {
        genero = new Genero();
        consultar();
    }

    public void insertar() {
        try {
            generoFacadeLocal.create(genero);
            init();
        } catch (Exception e) {
        }
    }

    public void actualizar() {
        try {
            generoFacadeLocal.edit(genero);
            init();
        } catch (Exception e) {
        }
    }

    public void eliminar(Genero genero) {
        this.genero = genero;
        try {
            generoFacadeLocal.remove(genero);
            init();

        } catch (Exception e) {
        }
    }

    public void consultar() {
        try {
            lista = generoFacadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void consultarById(Genero genero) {
        this.genero = genero;
    }
}
