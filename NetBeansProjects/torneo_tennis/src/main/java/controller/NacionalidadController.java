package controller;

import ejb.NacionalidadFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import modelo.Jugador;
import modelo.Nacionalidad;
import modelo.Pais;

/**
 *
 * @author fredy.alfarousam
 */
@Named(value = "nacionalidadController")
@SessionScoped
public class NacionalidadController implements Serializable {

    @EJB
    private NacionalidadFacadeLocal nacionalidadFacadeLocal;
    private Nacionalidad nacionalidad;
    private List<Nacionalidad> lista;
    private Pais pais;
    private Jugador jugador;

    public List<Nacionalidad> getLista() {
        return lista;
    }

    public void setLista(List<Nacionalidad> lista) {
        this.lista = lista;
    }

    public Nacionalidad getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(Nacionalidad nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    @PostConstruct
    public void init() {
        nacionalidad = new Nacionalidad();
        pais = new Pais();
        jugador = new Jugador();
        consultar();
    }

    public void insertar() {
        try {
            this.nacionalidad.setIdjugador(jugador);
            this.nacionalidad.setIdpais(pais);
            nacionalidadFacadeLocal.create(nacionalidad);
            init();
        } catch (Exception e) {
        }
    }

    public void actualizar() {
        try {
            this.nacionalidad.setIdpais(pais);
            this.nacionalidad.setIdjugador(jugador);

            nacionalidadFacadeLocal.edit(nacionalidad);
            init();
        } catch (Exception e) {
        }
    }

    public void eliminar(Nacionalidad nacionalidad) {
        this.nacionalidad = nacionalidad;
        try {

            nacionalidadFacadeLocal.remove(nacionalidad);
            init();
        } catch (Exception e) {
        }
    }

    public void consultar() {
        try {
            lista = nacionalidadFacadeLocal.findAll();

        } catch (Exception e) {
        }
    }

    public void consultarById(Nacionalidad nacionalidad) {
        this.nacionalidad = nacionalidad;
        this.jugador.setIdjugador(this.nacionalidad.getIdjugador().getIdjugador());
        this.pais.setIdpais(this.nacionalidad.getIdpais().getIdpais());
        
    }
}
