package controller;

import ejb.ModalidadFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import modelo.Modalidad;

@Named(value = "modalidadController")
@SessionScoped
public class ModalidadController implements Serializable {

    @EJB
    private ModalidadFacadeLocal modalidadFacadeLocal;
    private Modalidad modalidad;
    private List<Modalidad> lista;

    public Modalidad getModalidad() {
        return modalidad;
    }

    public void setModalidad(Modalidad modalidad) {
        this.modalidad = modalidad;
    }

    public List<Modalidad> getLista() {
        lista = modalidadFacadeLocal.findAll();
        return lista;
    }

    public void setLista(List<Modalidad> lista) {
        this.lista = lista;
    }

    @PostConstruct
    public void init() {
        modalidad = new Modalidad();
        consultar();
    }

    public void insertar() {
        modalidadFacadeLocal.create(modalidad);
        init();
    }

    public void editar() {
        modalidadFacadeLocal.edit(modalidad);
        init();
    }

    public void eliminar(Modalidad modalidad) {
        modalidadFacadeLocal.remove(modalidad);
        init();
    }

    public void consultar() {
        try {
            lista = modalidadFacadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void consultarById(Modalidad modalidad) {
        this.modalidad = modalidad;
    }

}
