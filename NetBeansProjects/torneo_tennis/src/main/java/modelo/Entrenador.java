package modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "entrenador")
public class Entrenador {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "identre")
    private int identre;
    @Column(name = "nombre")
    private String nombre;

    public int getIdentre() {
        return identre;
    }

    public void setIdentre(int identre) {
        this.identre = identre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + this.identre;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Entrenador other = (Entrenador) obj;
        if (this.identre != other.identre) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entrenador{" + "identre=" + identre + '}';
    }
    
    
}
