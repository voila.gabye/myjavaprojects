/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "lugar_torneo")
public class Lugar_Torneo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idlugar")
    private int idlugar;

    @JoinColumn(name = "idciudad")
    @ManyToOne
    private Ciudad idciudad;

    @JoinColumn(name = "idpista")
    @ManyToOne
    private Pista idpista;

    @JoinColumn(name = "idtorneo")
    @ManyToOne
    private Torneo idtorneo;

    public int getIdlugar() {
        return idlugar;
    }

    public void setIdlugar(int idlugar) {
        this.idlugar = idlugar;
    }

    public Ciudad getIdciudad() {
        return idciudad;
    }

    public void setIdciudad(Ciudad idciudad) {
        this.idciudad = idciudad;
    }

    public Pista getIdpista() {
        return idpista;
    }

    public void setIdpista(Pista idpista) {
        this.idpista = idpista;
    }

    public Torneo getIdtorneo() {
        return idtorneo;
    }

    public void setIdtorneo(Torneo idtorneo) {
        this.idtorneo = idtorneo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + this.idlugar;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Lugar_Torneo other = (Lugar_Torneo) obj;
        if (this.idlugar != other.idlugar) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Lugar_Torneo{" + "idlugar=" + idlugar + '}';
    }
}
