
package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "modalidad")
public class Modalidad implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "idmodal")
    private int idmodal;
    
    @Column (name = "nombre")
    private String nombre;

    public int getIdmodal() {
        return idmodal;
    }

    public void setIdmodal(int idmodal) {
        this.idmodal = idmodal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + this.idmodal;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Modalidad other = (Modalidad) obj;
        if (this.idmodal != other.idmodal) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modalidad{" + "idmodal=" + idmodal + ", nombre=" + nombre + '}';
    }
    
    
    
}
