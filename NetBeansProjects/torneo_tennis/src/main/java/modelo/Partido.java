package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "partido")
public class Partido implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpartido")
    private int idpartido;
    @JoinColumn(name = "idlugar")
    @ManyToOne
    private Lugar_Torneo idlugar;
    @JoinColumn(name = "idmodal")
    @ManyToOne
    private Modalidad idmodal;
    @JoinColumn(name = "idarbi")
    @ManyToOne
    private Arbitro idarbi;
    @JoinColumn(name = "idpremio")
    @ManyToOne
    private Premio idpremio;
    @JoinColumn(name = "idfase")
    @ManyToOne
    private Fase idfase;

    public int getIdpartido() {
        return idpartido;
    }

    public void setIdpartido(int idpartido) {
        this.idpartido = idpartido;
    }

    public Lugar_Torneo getIdlugar() {
        return idlugar;
    }

    public void setIdlugar(Lugar_Torneo idlugar) {
        this.idlugar = idlugar;
    }

    public Modalidad getIdmodal() {
        return idmodal;
    }

    public void setIdmodal(Modalidad idmodal) {
        this.idmodal = idmodal;
    }

    public Arbitro getIdarbi() {
        return idarbi;
    }

    public void setIdarbi(Arbitro idarbi) {
        this.idarbi = idarbi;
    }

    public Premio getIdpremio() {
        return idpremio;
    }

    public void setIdpremio(Premio idpremio) {
        this.idpremio = idpremio;
    }

    public Fase getIdfase() {
        return idfase;
    }

    public void setIdfase(Fase idfase) {
        this.idfase = idfase;
    }

    

    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + this.idpartido;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Partido other = (Partido) obj;
        if (this.idpartido != other.idpartido) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Partido{" + "idpartido=" + idpartido + '}';
    }
    
    
}
