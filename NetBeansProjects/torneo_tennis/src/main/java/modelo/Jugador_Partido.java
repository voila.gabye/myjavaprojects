package modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "jugador_partido")
public class Jugador_Partido {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idjugadorP")
    private int idjugadorP;
    
    @JoinColumn(name = "idjugador")
    @ManyToOne
    private Jugador idjugador;
    
    @JoinColumn (name = "idpartido")
    @ManyToOne
    private Partido idpartido;
    
    @Column(name = "resultado")
    private String resultado;
    
    @Column(name = "ganador")
    private boolean ganador;

    public int getIdjugadorP() {
        return idjugadorP;
    }

    public void setIdjugadorP(int idjugadorP) {
        this.idjugadorP = idjugadorP;
    }

    public Jugador getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(Jugador idjugador) {
        this.idjugador = idjugador;
    }

    

    public Partido getIdpartido() {
        return idpartido;
    }

    public void setIdpartido(Partido idpartido) {
        this.idpartido = idpartido;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public boolean isGanador() {
        return ganador;
    }

    public void setGanador(boolean ganador) {
        this.ganador = ganador;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.idjugadorP;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Jugador_Partido other = (Jugador_Partido) obj;
        if (this.idjugadorP != other.idjugadorP) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Jugado_Partido{" + "idjugadorP=" + idjugadorP + '}';
    }
    
    
}
