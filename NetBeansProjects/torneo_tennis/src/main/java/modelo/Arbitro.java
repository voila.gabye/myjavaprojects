package modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "arbitro")
public class Arbitro {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idarbi")
    private int idarbi;
    @Column(name = "nombre")
    private String nombre;

    public int getIdarbi() {
        return idarbi;
    }

    public void setIdarbi(int idarbi) {
        this.idarbi = idarbi;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + this.idarbi;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Arbitro other = (Arbitro) obj;
        if (this.idarbi != other.idarbi) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Arbitro{" + "idarbi=" + idarbi + '}';
    }
    
    
}
