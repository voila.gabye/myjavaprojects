
package modelo;

import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "entrenador_jugador")
public class Entrenador_Jugador {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "identrenaJuga")
    private int identrenaJuga;
    @JoinColumn(name = "idjugador")
    @ManyToOne
    private Jugador idjugador;
    @JoinColumn(name = "identrenador")
    @ManyToOne
    private Entrenador entrenador;
    @Temporal(TemporalType.DATE)
    private Date fecha_inicio;
    @Temporal(TemporalType.DATE)
    private Date fecha_fin;

    public int getIdentrenaJuga() {
        return identrenaJuga;
    }

    public void setIdentrenaJuga(int identrenaJuga) {
        this.identrenaJuga = identrenaJuga;
    }

    public Jugador getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(Jugador idjugador) {
        this.idjugador = idjugador;
    }

    public Entrenador getEntrenador() {
        return entrenador;
    }

    public void setEntrenador(Entrenador entrenador) {
        this.entrenador = entrenador;
    }

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public Date getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(Date fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.idjugador);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Entrenador_Jugador other = (Entrenador_Jugador) obj;
        if (!Objects.equals(this.idjugador, other.idjugador)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entrenador_Jugador{" + "idjugador=" + idjugador + '}';
    }
    
    
    
}
