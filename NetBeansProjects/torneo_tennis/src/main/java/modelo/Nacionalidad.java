package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "nacionalidad")
public class Nacionalidad implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idnacion")
    private int idnacion;

    @JoinColumn(name = "idpais")
    @ManyToOne
    private Pais idpais;

    @JoinColumn(name = "idjugador")
    @ManyToOne
    private Jugador idjugador;

    public int getIdnacion() {
        return idnacion;
    }

    public void setIdnacion(int idnacion) {
        this.idnacion = idnacion;
    }

    public Pais getIdpais() {
        return idpais;
    }

    public void setIdpais(Pais idpais) {
        this.idpais = idpais;
    }

    public Jugador getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(Jugador idjugador) {
        this.idjugador = idjugador;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + this.idnacion;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Nacionalidad other = (Nacionalidad) obj;
        if (this.idnacion != other.idnacion) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Nacionalidad{" + "idnacion=" + idnacion + '}';
    }
}
