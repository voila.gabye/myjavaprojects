
package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "pais")
public class Pais implements Serializable{
    
    @Id
    @GeneratedValue(strategy =GenerationType.IDENTITY)
    @Column(name = "idpais")
    private int idpais;
    
    @Column(name = "pais")
    private String pais;

    public int getIdpais() {
        return idpais;
    }

    public void setIdpais(int idpais) {
        this.idpais = idpais;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + this.idpais;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pais other = (Pais) obj;
        if (this.idpais != other.idpais) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Pais{" + "idpais=" + idpais + '}';
    }
}
