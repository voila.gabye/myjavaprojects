package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Genero;

@Local
public interface GeneroFacadeLocal {

    void create(Genero genero);

    void edit(Genero genero);

    void remove(Genero genero);

    List<Genero> findAll();

}
