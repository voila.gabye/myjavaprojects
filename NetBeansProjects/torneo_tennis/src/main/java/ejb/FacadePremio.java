package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Premio;

@Stateless
public class FacadePremio extends AbstractFacade<Premio> implements PremioFacadeLocal {

    @PersistenceContext(unitName = "tennisPU")
    private EntityManager em;

    public FacadePremio() {
        super(Premio.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
