
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Equipo;

@Stateless
public class FacadeEquipo extends AbstractFacade<Equipo> implements EquipoFacadeLocal{
    @PersistenceContext(unitName = "tennisPU")
    private EntityManager em;

    public FacadeEquipo() {
        super(Equipo.class);
    }
    
    public EntityManager getEntityManager(){
    return em;
    }
}
