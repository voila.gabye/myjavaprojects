package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Arbitro;

@Local
public interface ArbitroFacadeLocal {

    void create(Arbitro arbitro);

    void edit(Arbitro arbitro);

    void remove(Arbitro arbitro);

    List<Arbitro> findAll();
}
