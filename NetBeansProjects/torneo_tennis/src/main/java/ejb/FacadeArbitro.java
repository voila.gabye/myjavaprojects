package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Arbitro;

@Stateless
public class FacadeArbitro extends AbstractFacade<Arbitro> implements ArbitroFacadeLocal{
    
    @PersistenceContext(unitName = "tennisPU")
    private EntityManager em;
    
    public FacadeArbitro(){
    super(Arbitro.class);
    }
    
    @Override
    protected EntityManager getEntityManager(){
    return em;
    }
}
