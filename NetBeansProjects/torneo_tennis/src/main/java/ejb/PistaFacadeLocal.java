/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Pista;

/**
 *
 * @author susana.menjivarusam
 */
@Local
public interface PistaFacadeLocal {

    void create(Pista pista);

    void edit(Pista pista);

    void remove(Pista pista);

    List<Pista> findAll();
}
