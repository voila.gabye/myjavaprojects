package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Fase;

@Stateless
public class FacadeFase extends AbstractFacade<Fase> implements FaseFacadeLocal {

    @PersistenceContext(unitName = "tennisPU")
    private EntityManager em;

    public FacadeFase() {
        super(Fase.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
