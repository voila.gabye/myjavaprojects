/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import modelo.Modalidad;

/**
 *
 * @author susana.menjivarusam
 */
public interface ModalidadFacadeLocal {

    void create(Modalidad modalidad);

    void edit(Modalidad modalidad);

    void remove(Modalidad modalidad);

    List<Modalidad> findAll();
}
