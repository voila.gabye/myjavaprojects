package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Entrenador;

@Stateless
public class FacadeEntrenador extends AbstractFacade<Entrenador> implements EntrenadorFacadeLocal {

    @PersistenceContext(unitName = "tennisPU")
    private EntityManager em;

    public FacadeEntrenador() {
        super(Entrenador.class);
    }
    
   @Override
   protected EntityManager getEntityManager(){
   return em;
   }
}
