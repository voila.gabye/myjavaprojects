
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Ciudad;


@Stateless
public class CiudadFacade extends AbstractFacade<Ciudad> implements CiudadFacadeLocal{
    
    @PersistenceContext(unitName = "tennisPU")
    private EntityManager em;

    public CiudadFacade() {
        super(Ciudad.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
    
}
