package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Partido;

@Stateless
public class FacadePartido extends AbstractFacade<Partido> implements PartidoFacadeLocal {

    @PersistenceContext(unitName = "tennisPU")
    private EntityManager em;

    public FacadePartido() {
        super(Partido.class);
    }
    
    @Override
    protected EntityManager getEntityManager(){
    return em;
    }
}
