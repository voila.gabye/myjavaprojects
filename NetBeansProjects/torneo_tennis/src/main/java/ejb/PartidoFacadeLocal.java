package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Partido;

@Local
public interface PartidoFacadeLocal {

    void create(Partido partido);

    void edit(Partido partido);

    void remove(Partido partido);

    List<Partido> findAll();

}
