
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Genero;


@Stateless
public class GeneroFacade extends AbstractFacade<Genero> implements GeneroFacadeLocal
{
    @PersistenceContext(unitName = "tennisPU")
    private EntityManager em;

    public GeneroFacade() {
        super(Genero.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
