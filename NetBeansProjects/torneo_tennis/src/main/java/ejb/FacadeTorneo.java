package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Torneo;

@Stateless
public class FacadeTorneo extends AbstractFacade<Torneo> implements TorneoFacadeLocal {

    @PersistenceContext(unitName = "tennisPU")
    private EntityManager em;

    public FacadeTorneo() {
        super(Torneo.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
