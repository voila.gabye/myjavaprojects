
package ejb;

import java.util.List;
import modelo.Entrenador_Jugador;


public interface EntrenadorJugadorFacadeLocal {

    void create(Entrenador_Jugador entrenador_jugador);

    void edit(Entrenador_Jugador entrenador_jugador);

    void remove(Entrenador_Jugador entrenador_jugador);

    List<Entrenador_Jugador> findAll();
}
