
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Jugador;

@Local
public interface JugadorFacadeLocal 
{
    void create(Jugador jugador);

    void edit(Jugador jugador);

    void remove(Jugador jugador);

    List<Jugador> findAll();
    
}
