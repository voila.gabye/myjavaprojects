/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Pista;

/**
 *
 * @author susana.menjivarusam
 */
@Stateless
public class FacadePista extends AbstractFacade<Pista> implements PistaFacadeLocal {

    @PersistenceContext(unitName = "tennisPU")
    private EntityManager em;

    public FacadePista() {
        super(Pista.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
