
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Lugar_Torneo;

@Local
public interface Lugar_TorneoFacadeLocal {
    void create(Lugar_Torneo lugar_Torneo);

    void edit(Lugar_Torneo lugar_Torneo);

    void remove(Lugar_Torneo lugar_Torneo);

    List<Lugar_Torneo> findAll();
    
}
