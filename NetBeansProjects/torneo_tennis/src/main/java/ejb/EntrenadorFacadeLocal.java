package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Entrenador;

@Local
public interface EntrenadorFacadeLocal {

    void create(Entrenador entrenador);

    void edit(Entrenador entrenador);

    void remove(Entrenador entrenador);

    List<Entrenador> findAll();
}
