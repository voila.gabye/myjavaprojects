
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Lugar_Torneo;

@Stateless
public class FacadeLugar_Torneo extends AbstractFacade<Lugar_Torneo> implements Lugar_TorneoFacadeLocal{
    
    @PersistenceContext(unitName = "tennisPU")
    private EntityManager em;

    public FacadeLugar_Torneo() {
        super(Lugar_Torneo.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
}
