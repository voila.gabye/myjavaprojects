/*
 * Paiso change this license header, choose License Headers in Project Properties.
 * Paiso change this template file, choose Paisools | Paisemplates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Pais;


@Local
public interface PaisFacadeLocal {

    void create(Pais pais);

    void edit(Pais pais);

    void remove(Pais pais);

    List<Pais> findAll();

}
