
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Nacionalidad;

@Stateless
public class FacadeNacionalidad extends AbstractFacade<Nacionalidad> implements NacionalidadFacadeLocal{
    
    @PersistenceContext(unitName = "tennisPU")
    private EntityManager em;

    public FacadeNacionalidad() {
        super(Nacionalidad.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
    
}
