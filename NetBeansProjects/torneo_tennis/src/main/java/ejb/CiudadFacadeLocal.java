package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Ciudad;

@Local
public interface CiudadFacadeLocal {

    void create(Ciudad ciudad);

    void edit(Ciudad ciudad);

    void remove(Ciudad ciudad);

    List<Ciudad> findAll();

}
