package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Fase;

@Local
public interface FaseFacadeLocal {

    void create(Fase fase);

    void edit(Fase fase);

    void remove(Fase fase);

    List<Fase> findAll();

}
