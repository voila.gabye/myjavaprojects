package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Premio;

@Local
public interface PremioFacadeLocal {

    void create(Premio premio);

    void edit(Premio premio);

    void remove(Premio premio);

    List<Premio> findAll();
}
