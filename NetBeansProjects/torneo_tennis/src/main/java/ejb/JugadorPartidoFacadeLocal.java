package ejb;

import java.util.List;
import modelo.Jugador_Partido;

public interface JugadorPartidoFacadeLocal {

    void create(Jugador_Partido jugadorPartido);

    void edit(Jugador_Partido jugadorPartido);

    void remove(Jugador_Partido jugadorPartido);

    List<Jugador_Partido> findAll();

}
