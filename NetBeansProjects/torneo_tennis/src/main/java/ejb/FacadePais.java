
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Pais;


@Stateless
public class FacadePais extends AbstractFacade<Pais> implements PaisFacadeLocal{
    
    @PersistenceContext(unitName = "tennisPU")
    private EntityManager em;

    public FacadePais() {
        super(Pais.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
    
}
