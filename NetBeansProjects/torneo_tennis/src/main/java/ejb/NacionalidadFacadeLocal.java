/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Nacionalidad;

@Local
public interface NacionalidadFacadeLocal {
    
    void create(Nacionalidad nacionalidad);

    void edit(Nacionalidad nacionalidad);

    void remove(Nacionalidad nacionalidad);

    List<Nacionalidad> findAll();
    
}
