/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Jugador;

@Stateless
public class JugadorFacade extends AbstractFacade<Jugador> implements JugadorFacadeLocal{
    
    @PersistenceContext(unitName = "tennisPU")
    private EntityManager em;

    public JugadorFacade() {
        super(Jugador.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
}
