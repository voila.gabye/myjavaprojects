
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Jugador_Partido;

@Stateless
public class FacadeJugadorPartido extends AbstractFacade<Jugador_Partido> implements JugadorPartidoFacadeLocal{
    @PersistenceContext(unitName = "tennisPU")
    private EntityManager em;

    public FacadeJugadorPartido() {
        super(Jugador_Partido.class);
    }
    
    @Override
    public EntityManager getEntityManager(){
        return em;
    }
}
