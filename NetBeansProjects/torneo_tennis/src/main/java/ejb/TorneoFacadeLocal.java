package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Torneo;

@Local
public interface TorneoFacadeLocal {

    void create(Torneo torneo);

    void remove(Torneo torneo);

    void edit(Torneo torneo);

    List<Torneo> findAll();
}
