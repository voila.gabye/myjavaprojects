
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Modalidad;


@Stateless
public class FacadeModalidad extends AbstractFacade<Modalidad> implements ModalidadFacadeLocal{
    @PersistenceContext(unitName = "tennisPU")
    private EntityManager em;
    
    public FacadeModalidad(){
    super(Modalidad.class);
    }
    
    @Override
    public EntityManager getEntityManager(){
        return em;
    }
}
