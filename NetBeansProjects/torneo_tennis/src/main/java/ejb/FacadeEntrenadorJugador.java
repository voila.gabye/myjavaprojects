
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Entrenador_Jugador;


@Stateless
public class FacadeEntrenadorJugador extends AbstractFacade<Entrenador_Jugador> implements EntrenadorJugadorFacadeLocal{
    @PersistenceContext(unitName = "tennisPU")
    private EntityManager em;
    
    public FacadeEntrenadorJugador()
    {
        super(Entrenador_Jugador.class);
    }
    
    @Override
    public EntityManager getEntityManager()
    {
        return em;
    }
}
