package controller;

import dao.ViviendaD;
import model.Vivienda;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author gabriela.francousam
 */
@Controller
public class ViviendaC {

    private ViviendaD vd = new ViviendaD();
    private final ModelAndView mav = new ModelAndView();

    
    @RequestMapping("index.html")
    public ModelAndView consulta() {
        return vd.consultar();
    }
    
    @RequestMapping(value = "agregar.html", method = RequestMethod.GET)
    public ModelAndView registrar() {
        mav.addObject(new Vivienda());
        mav.setViewName("agregar");
        return mav;
    }
    
    @RequestMapping(value = "agregar.html", method = RequestMethod.POST)
    public ModelAndView registrar(Vivienda v) {
        return vd.registrar(v);
    }
}
