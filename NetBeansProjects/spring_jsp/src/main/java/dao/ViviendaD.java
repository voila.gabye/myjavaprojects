package dao;

import confs.Conexion;
import java.util.List;
import model.Vivienda;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author gabriela.francousam
 */
public class ViviendaD {

    private String[] viviendas = {"select id,tipo_vivienda,direccion,n_habitantes from vivienda order by id",
        "insert into vivienda(tipo_vivienda,direccion,n_habitantes) value(?,?,?)"};

    Vivienda v = new Vivienda();
    private final Conexion con = new Conexion();
    private final JdbcTemplate jdbct = new JdbcTemplate(con.conectar());
    private final ModelAndView mav = new ModelAndView();
    private List listvi;

    public ModelAndView consultar() {
        List listvi = this.jdbct.queryForList(viviendas[0]);
        mav.addObject("listvis", listvi);
        mav.setViewName("index");
        return mav;
    }

    public ModelAndView registrar(Vivienda v) {
        this.jdbct.update(viviendas[1], v.getTipo_vivienda(), v.getDireccion(), v.getN_habitantes());
        mav.setViewName("redirect:/index.html");
        return mav;
    }

}
