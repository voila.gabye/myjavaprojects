package model;

public class Vivienda {

    private int id;
    private String tipo_vivienda;
    private String direccion;
    private String n_habitantes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo_vivienda() {
        return tipo_vivienda;
    }

    public void setTipo_vivienda(String tipo_vivienda) {
        this.tipo_vivienda = tipo_vivienda;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getN_habitantes() {
        return n_habitantes;
    }

    public void setN_habitantes(String n_habitantes) {
        this.n_habitantes = n_habitantes;
    } 
}
