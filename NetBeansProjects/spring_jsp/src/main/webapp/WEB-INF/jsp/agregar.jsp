<%-- 
    Document   : agregar
    Created on : 11-07-2019, 08:57:44 AM
    Author     : gabriela.francousam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro Vivienda</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </head>
        <body>
        <div class="container" style="margin-top:40px;"> 

            <div class="row"> </div>
            <h3>Registro de Viviendas</h3>
            <div class="row">
                <div class="col-6"> 
                    <form method="post">

                        <label>Tipo de vivienda</label>
                        <input  class="form-control" type="text" name="tipo_vivienda" placeholder="Digite el tipo de vivienda"/><br><br>

                        <label>Direccion</label>
                        <input class="form-control" type="text" name="direccion" placeholder="Digite su direccion"/><br><br>

                        <label>Número de Habitantes</label>
                        <input class="form-control" type="text" name="n_habitantes" placeholder="Digite el número de habitantes"/><br><br>

                        <input type="submit" value="Ingresar" class="btn btn-primary"/> 
                        <a href="index.html">Volver</a> 
                    </form>
                </div>
                <div class="col-6"> 
                </div>
            </div>
        </div>

    </body>
</html>
