<%@ taglib prefix="k" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Spring Web MVC project</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </head>

    <body>
        <div class="container" style="margin-top:40px;"> 
            
            <div><a href="agregar.html" >Agregar</a> </div>
            <br>
            <div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Tipo de Vivienda</th>
                            <th>Dirección</th>
                            <th>N° de Habitantes</th>
                        </tr>
                    </thead>
                    <tbody>
                        <k:forEach var="ver" items="${listvis}">
                            <tr>
                                <td>${ver.tipo_vivienda}</td>
                                <td>${ver.direccion}</td>
                                <td>${ver.n_habitantes}</td>
                            </tr>
                        </k:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
