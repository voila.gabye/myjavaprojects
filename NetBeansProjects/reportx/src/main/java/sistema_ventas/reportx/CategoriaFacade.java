/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema_ventas.reportx;


import com.ventasZ.model.Categoria;
import com.ventasZ.util.AbstractFacade;
import org.hibernate.SessionFactory;

/**
 *
 * @author gabriela.francousam
 */

public class CategoriaFacade extends AbstractFacade<Categoria> {
   // @PersistenceContext(unitName = "pu") 
   // private EntityManager em;
    
    private SessionFactory sf;

   // @Override
   // protected EntityManager getEntityManager() {
  //      return em;
  //  }

    public CategoriaFacade(SessionFactory sf, Class<Categoria> entityClass) {
        super(entityClass);
        this.sf = sf;
    }
    
  //  public CategoriaFacade() {
     //   super(Categoria.class);
   // }
    
    @Override
    protected SessionFactory sessionFactory() {
        return sf;
    }
}
