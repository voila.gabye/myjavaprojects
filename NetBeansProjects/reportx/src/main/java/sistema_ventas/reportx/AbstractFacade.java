package sistema_ventas.reportx;

import java.util.List;
import javax.persistence.criteria.CriteriaQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gabriela.francousam
 */
public abstract class AbstractFacade<T> {

    private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    // protected abstract EntityManager getEntityManager();  //el que genera
    @Autowired                                            // lo que se agrega
    protected abstract SessionFactory sessionFactory();   // lo que se agrega
    protected Session session;                             // lo que se agrega

    @Transactional // se agrega
    public void create(T entity) {
        //   getEntityManager().persist(entity); // lo que se genera
        session.save(entity); // lo que se agrega
    }

    @Transactional
    public void edit(T entity) {
        //  getEntityManager().merge(entity); // lo que se genera
        session.update(entity); // lo que se agrega
    }

    @Transactional
    public void remove(T entity) {
        // getEntityManager().remove(getEntityManager().merge(entity)); // lo que se genera
        session.delete(entity); // lo que se agrega
    }

    @Transactional
    public T find(Object id) {
        //  return getEntityManager().find(entityClass, id);  // lo que se genera
        session = sessionFactory().getCurrentSession(); // lo que se agrega 
        return session.find(entityClass, id);             // lo que se agrega
    }

    @Transactional
    public List<T> findAll() {
        //  javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery(); //lo que se genera
        session = sessionFactory().getCurrentSession();                // lo que se agrega 
        CriteriaQuery cq = session.getCriteriaBuilder().createQuery();      // lo que se agrega 
        cq.select(cq.from(entityClass)); // lo que se genera
        //  return getEntityManager().createQuery(cq).getResultList();     //lo que se genera
        return session.createQuery(cq).getResultList();                  // lo que se agrega 
    }
}
