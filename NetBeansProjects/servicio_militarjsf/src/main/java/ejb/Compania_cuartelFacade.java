package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Compania_cuartel;

@Stateless
public class Compania_cuartelFacade extends AbstractFacade<Compania_cuartel> implements Compania_cuartelFacadeLocal {

    @PersistenceContext(unitName = "militarPu")
    private EntityManager em;

    public Compania_cuartelFacade() {
        super(Compania_cuartel.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
