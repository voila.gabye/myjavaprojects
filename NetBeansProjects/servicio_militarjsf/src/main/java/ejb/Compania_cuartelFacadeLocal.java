package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Compania_cuartel;

@Local
public interface Compania_cuartelFacadeLocal {

    void create(Compania_cuartel compania_cuartel);

    void edit(Compania_cuartel compania_cuartel);

    void remove(Compania_cuartel compania_cuartel);

    List<Compania_cuartel> findAll();
}
