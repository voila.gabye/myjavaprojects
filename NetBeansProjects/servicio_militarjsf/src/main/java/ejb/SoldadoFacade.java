package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Soldado;

@Stateless
public class SoldadoFacade extends AbstractFacade<Soldado> implements SoldadoFacadeLocal {

    @PersistenceContext(name = "militarPu")
    private EntityManager em;

    public SoldadoFacade() {
        super(Soldado.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
}
