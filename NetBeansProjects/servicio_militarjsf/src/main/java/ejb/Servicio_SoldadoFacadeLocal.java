package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Servicio_Soldado;

@Local
public interface Servicio_SoldadoFacadeLocal {

    void create(Servicio_Soldado servicio_soldado);

    void edit(Servicio_Soldado servicio_soldado);

    void remove(Servicio_Soldado servicio_soldado);

    List< Servicio_Soldado> findAll();

}
