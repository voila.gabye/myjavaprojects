package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Compania;

@Local
public interface CompaniaFacadeLocal {

    void create(Compania compania);

    void edit(Compania compania);

    void remove(Compania compania);

    List<Compania> findAll();
}
