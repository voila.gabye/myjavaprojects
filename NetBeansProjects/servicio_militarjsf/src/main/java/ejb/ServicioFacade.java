/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Servicio;

@Stateless
public class ServicioFacade extends AbstractFacade<Servicio> implements ServicioFacadeLocal {

    @PersistenceContext(unitName = "militarPu")
    private EntityManager em;

    public ServicioFacade() {
        super(Servicio.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
