package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Cuerpo;

@Local
public interface CuerpoFacadeLocal {

    void create(Cuerpo cuerpo);

    void edit(Cuerpo cuerpo);

    void remove(Cuerpo cuerpo);

    List<Cuerpo> findAll();
    
}
