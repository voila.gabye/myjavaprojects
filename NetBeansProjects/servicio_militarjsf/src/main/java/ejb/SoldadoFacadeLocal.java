package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Soldado;

@Local
public interface SoldadoFacadeLocal {

    void create(Soldado soldado);

    void edit(Soldado soldado);

    void remove(Soldado soldado);

    List<Soldado> findAll();
}
