package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Cuerpo;

@Stateless
public class CuerpoFacade extends AbstractFacade<Cuerpo> implements CuerpoFacadeLocal 
{
    @PersistenceContext(unitName = "militarPu")
    private EntityManager em;

    public CuerpoFacade() {
        super(Cuerpo.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    

}
