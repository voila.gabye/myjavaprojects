package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Cuartel;

@Stateless
public class CuartelFacade extends AbstractFacade<Cuartel> implements CuartelFacadeLocal {

    @PersistenceContext(unitName = "militarPu")
    private EntityManager em;

    public CuartelFacade() {
        super(Cuartel.class);
    }
    
    @Override
    protected EntityManager getEntityManager(){
    return em;
    }
}
