package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Servicio_Soldado;

@Stateless
public class Servicio_SoldadoFacade extends AbstractFacade<Servicio_Soldado> implements Servicio_SoldadoFacadeLocal{

    @PersistenceContext(unitName = "militarPu")
    private EntityManager em;

    public Servicio_SoldadoFacade() {
        super(Servicio_Soldado.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
}
