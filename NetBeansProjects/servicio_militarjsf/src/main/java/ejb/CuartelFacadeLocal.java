package ejb;

import java.util.List;
import javax.ejb.Local;
import modelo.Cuartel;

@Local
public interface CuartelFacadeLocal {

    void create(Cuartel cuartel);

    void edit(Cuartel cuartel);

    void remove(Cuartel cuartel);

    List<Cuartel> findAll();
}
