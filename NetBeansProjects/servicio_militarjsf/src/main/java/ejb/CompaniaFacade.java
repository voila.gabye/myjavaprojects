package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Compania;

@Stateless
public class CompaniaFacade extends AbstractFacade<Compania> implements CompaniaFacadeLocal {

    @PersistenceContext(unitName = "militarPu")
    private EntityManager em;

    public CompaniaFacade() {
        super(Compania.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
