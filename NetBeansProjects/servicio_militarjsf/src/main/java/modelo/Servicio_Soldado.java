
package modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "servicio_soldado")
public class Servicio_Soldado implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idserviciosoldado")
    private int idserviciosoldado;

    @JoinColumn(name = "idsoldado")
    @ManyToOne
    private Soldado idsoldado;

    @JoinColumn(name = "idservicio")
    @ManyToOne
    private Servicio idservicio;

    @Temporal(TemporalType.DATE)
    @Column(name = "fecha")
    private Date fecha;

    public int getIdserviciosoldado() {
        return idserviciosoldado;
    }

    public void setIdserviciosoldado(int idserviciosoldado) {
        this.idserviciosoldado = idserviciosoldado;
    }

    public Soldado getIdsoldado() {
        return idsoldado;
    }

    public void setIdsoldado(Soldado idsoldado) {
        this.idsoldado = idsoldado;
    }

    public Servicio getIdservicio() {
        return idservicio;
    }

    public void setIdservicio(Servicio idservicio) {
        this.idservicio = idservicio;
    }
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + this.idserviciosoldado;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Servicio_Soldado other = (Servicio_Soldado) obj;
        if (this.idserviciosoldado != other.idserviciosoldado) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Servicio_Soldado{" + "idserviciosoldado=" + idserviciosoldado + '}';
    }
}
