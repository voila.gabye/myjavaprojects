package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cuerpo")
public class Cuerpo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "idcuerpo")
    private int idcuerpo;

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "denominacion")
    private String denominacion;

    public int getIdcuerpo() {
        return idcuerpo;
    }

    public void setIdcuerpo(int idcuerpo) {
        this.idcuerpo = idcuerpo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDenominacion() {
        return denominacion;
    }

    public void setDenominacion(String denominacion) {
        this.denominacion = denominacion;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + this.idcuerpo;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cuerpo other = (Cuerpo) obj;
        if (this.idcuerpo != other.idcuerpo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cuerpo{" + "idcuerpo=" + idcuerpo + '}';
    }
}
