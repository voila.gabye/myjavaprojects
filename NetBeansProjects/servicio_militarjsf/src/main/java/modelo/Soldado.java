package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "soldado")
public class Soldado implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idsoldado")
    private int idsoldado;
    
    @Column (name = "codigo")
    private String codigo;
    
    @JoinColumn(name = "idcompaniacuartel")
    @ManyToOne
    private Compania_cuartel idcompaniacuartel;
    
    @Column(name = "nombre")
    private String nombre;
    
    @Column(name = "apellido")
    private String apellido;
    
    @Column(name = "graduacion")
    private String graduacion;
    
    @JoinColumn(name = "idcuerpo")
    @ManyToOne
    private Cuerpo idcuerpo;

    public int getIdsoldado() {
        return idsoldado;
    }

    public void setIdsoldado(int idsoldado) {
        this.idsoldado = idsoldado;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    public Compania_cuartel getIdcompaniacuartel() {
        return idcompaniacuartel;
    }

    public void setIdcompaniacuartel(Compania_cuartel idcompaniacuartel) {
        this.idcompaniacuartel = idcompaniacuartel;
    }

   
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getGraduacion() {
        return graduacion;
    }

    public void setGraduacion(String graduacion) {
        this.graduacion = graduacion;
    }

    public Cuerpo getIdcuerpo() {
        return idcuerpo;
    }

    public void setIdcuerpo(Cuerpo idcuerpo) {
        this.idcuerpo = idcuerpo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + this.idsoldado;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Soldado other = (Soldado) obj;
        if (this.idsoldado != other.idsoldado) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Soldado{" + "idsoldado=" + idsoldado + '}';
    }
    
    
    
}
