package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "compania_cuartel")
public class Compania_cuartel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcompaniacuartel")
    private int idcompaniacuartel;
    @JoinColumn(name = "idcuartel")
    @ManyToOne
    private Cuartel idcuartel;
    @JoinColumn(name = "idcompania")
    @ManyToOne
    private Compania idcompania;

    public int getIdcompaniacuartel() {
        return idcompaniacuartel;
    }

    public void setIdcompaniacuartel(int idcompaniacuartel) {
        this.idcompaniacuartel = idcompaniacuartel;
    }

    public Cuartel getIdcuartel() {
        return idcuartel;
    }

    public void setIdcuartel(Cuartel idcuartel) {
        this.idcuartel = idcuartel;
    }

    public Compania getIdcompania() {
        return idcompania;
    }

    public void setIdcompania(Compania idcompania) {
        this.idcompania = idcompania;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + this.idcompaniacuartel;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Compania_cuartel other = (Compania_cuartel) obj;
        if (this.idcompaniacuartel != other.idcompaniacuartel) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Compania_cuartel{" + "idcompaniacuartel=" + idcompaniacuartel + '}';
    }

}
