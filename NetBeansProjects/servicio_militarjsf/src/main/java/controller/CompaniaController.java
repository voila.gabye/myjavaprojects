package controller;

import ejb.CompaniaFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import modelo.Compania;

@Named(value = "companiaController")
@SessionScoped
public class CompaniaController implements Serializable {

    @EJB
    private CompaniaFacadeLocal facadeLocal;
    private Compania compania;
    private List<Compania> lista;

    public Compania getCompania() {
        return compania;
    }

    public void setCompania(Compania compania) {
        this.compania = compania;
    }

    public List<Compania> getLista() {
        return lista;
    }

    public void setLista(List<Compania> lista) {
        this.lista = lista;
    }

    @PostConstruct
    public void init() {
        compania = new Compania();
        consultar();
    }

    public void consultar() {
        try {
            lista = facadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void insertar() {
        try {
            facadeLocal.create(compania);
            if (compania != null) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Registro Guardado "));
                init();
            }
            init();
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Error " + e.getMessage()));
        }
    }

    public void actualizar() {
        try {
            facadeLocal.edit(compania);
            init();
        } catch (Exception e) {
        }
    }

    public void eliminar(Compania compania) {
        this.compania = compania;
        try {
            facadeLocal.remove(compania);
            init();
        } catch (Exception e) {
        }
    }

    public void consultarById(Compania compania) {
        this.compania = compania;
    }
}
