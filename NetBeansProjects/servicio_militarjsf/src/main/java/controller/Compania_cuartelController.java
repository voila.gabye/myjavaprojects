package controller;

import ejb.Compania_cuartelFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import modelo.Compania;
import modelo.Compania_cuartel;
import modelo.Cuartel;

@Named(value = "compania_cuartelController")
@SessionScoped
public class Compania_cuartelController implements Serializable {

    @EJB
    private Compania_cuartelFacadeLocal facadeLocal;
    private Compania_cuartel compania_cuartel;
    private List<Compania_cuartel> lista;
    private Compania compania;
    private Cuartel cuartel;
    private boolean boton=true;

    public boolean isBoton() {
        return boton;
    }

    public void setBoton(boolean boton) {
        this.boton = boton;
    }

    
    
    public Compania_cuartel getCompania_cuartel() {
        return compania_cuartel;
    }

    public void setCompania_cuartel(Compania_cuartel compania_cuartel) {
        this.compania_cuartel = compania_cuartel;
    }

    public List<Compania_cuartel> getLista() {
        return lista;
    }

    public void setLista(List<Compania_cuartel> lista) {
        this.lista = lista;
    }

    public Compania getCompania() {
        return compania;
    }

    public void setCompania(Compania compania) {
        this.compania = compania;
    }

    public Cuartel getCuartel() {
        return cuartel;
    }

    public void setCuartel(Cuartel cuartel) {
        this.cuartel = cuartel;
    }

    @PostConstruct
    public void init() {
        compania_cuartel = new Compania_cuartel();
        compania = new Compania();
        cuartel = new Cuartel();
        consultar();
    }

    public void consultar() {
        try {
            lista = facadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void insertar() {
        try {
            compania_cuartel.setIdcuartel(cuartel);
            compania_cuartel.setIdcompania(compania);
            facadeLocal.create(compania_cuartel);
            
            if (compania_cuartel != null) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Registro Guardado "));
                init();
            }
            init();
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Error " + e.getMessage()));
        }
    }

    public void actualizar() {
        try {

            compania_cuartel.setIdcuartel(cuartel);
            compania_cuartel.setIdcompania(compania);
            facadeLocal.edit(compania_cuartel);
            boton=true;
            init();
        } catch (Exception e) {
        }
    }

    public void eliminar(Compania_cuartel compania_cuartel) {
        this.compania_cuartel = compania_cuartel;
        try {
            facadeLocal.remove(compania_cuartel);
            init();
        } catch (Exception e) {
        }
    }

    public void consultarById(Compania_cuartel compania_cuartel) {
        this.compania_cuartel = compania_cuartel;
        cuartel.setIdcuartel(compania_cuartel.getIdcuartel().getIdcuartel());
        compania.setIdcompania(compania_cuartel.getIdcompania().getIdcompania());
        boton=false;
    }
}
