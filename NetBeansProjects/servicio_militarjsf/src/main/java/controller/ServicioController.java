/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.ServicioFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import modelo.Servicio;

@Named(value = "servicioController")
@SessionScoped
public class ServicioController implements Serializable {

    @EJB
    private ServicioFacadeLocal facadeLocal;
    private Servicio servicio;
    private List<Servicio> lista;

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public List<Servicio> getLista() {
        return lista;
    }

    public void setLista(List<Servicio> lista) {
        this.lista = lista;
    }

    @PostConstruct
    public void init() {
        servicio = new Servicio();
        consultar();
    }

    public void consultar() {
        try {
            lista = facadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void insertar() {
        try {
            facadeLocal.create(servicio);
            if (servicio != null) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Registro Guardado "));
                init();
            }
            init();
        } catch (Exception e) {

            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Error " + e.getMessage()));

        }
    }

    public void actualizar() {
        try {
            facadeLocal.edit(servicio);
            init();
        } catch (Exception e) {
        }
    }

    public void eliminar(Servicio servicio) {
        this.servicio = servicio;
        try {
            facadeLocal.remove(servicio);
            init();

        } catch (Exception e) {
        }
    }

    public void consultarById(Servicio servicio) {
        this.servicio = servicio;
    }

}
