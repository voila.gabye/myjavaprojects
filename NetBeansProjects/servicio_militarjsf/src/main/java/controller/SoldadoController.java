package controller;

import ejb.SoldadoFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import modelo.Compania_cuartel;
import modelo.Cuerpo;
import modelo.Soldado;

@Named(value = "soldadoController")
@SessionScoped
public class SoldadoController implements Serializable {

    @EJB
    private SoldadoFacadeLocal soldadoFacadeLocal;
    private Soldado soldado;
    private List<Soldado> soldadoList;
    private Compania_cuartel companiaCuartel;
    private Cuerpo cuerpo;

    public Soldado getSoldado() {
        return soldado;
    }

    public void setSoldado(Soldado soldado) {
        this.soldado = soldado;
    }

    public List<Soldado> getSoldadoList() {
        soldadoList = soldadoFacadeLocal.findAll();
        return soldadoList;
    }

    public void setSoldadoList(List<Soldado> soldadoList) {
        this.soldadoList = soldadoList;
    }

    public Compania_cuartel getCompaniaCuartel() {
        return companiaCuartel;
    }

    public void setCompaniaCuartel(Compania_cuartel companiaCuartel) {
        this.companiaCuartel = companiaCuartel;
    }

    
    public Cuerpo getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(Cuerpo cuerpo) {
        this.cuerpo = cuerpo;
    }

    @PostConstruct
    public void init() {
        cuerpo = new Cuerpo();
        companiaCuartel = new Compania_cuartel();
        soldado = new Soldado();
        consultar();
    }

    public void insertar() {
        try {
            soldado.setIdcuerpo(cuerpo);
            soldado.setIdcompaniacuartel(companiaCuartel);
            soldadoFacadeLocal.create(soldado);
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void actualizar() {
        try {
            soldado.setIdcuerpo(cuerpo);
            soldado.setIdcompaniacuartel(companiaCuartel);
            soldadoFacadeLocal.edit(soldado);
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void consultarById(Soldado sol) {
        soldado = sol;
        cuerpo.setIdcuerpo(soldado.getIdcuerpo().getIdcuerpo());
        companiaCuartel.setIdcompaniacuartel(soldado.getIdcompaniacuartel().getIdcompaniacuartel());
        
    }

    public void eliminar(Soldado sold) {
        try {
            soldadoFacadeLocal.remove(sold);
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void consultar() {
        try {
            soldadoList = soldadoFacadeLocal.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
