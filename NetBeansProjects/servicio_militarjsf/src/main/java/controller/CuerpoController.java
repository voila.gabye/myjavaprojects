package controller;

import ejb.CuerpoFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import modelo.Cuerpo;

@Named(value = "cuerpoController")
@SessionScoped
public class CuerpoController implements Serializable {

    @EJB
    private CuerpoFacadeLocal facadeLocal;
    private Cuerpo cuerpo;
    private List<Cuerpo> lista;

    public Cuerpo getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(Cuerpo cuerpo) {
        this.cuerpo = cuerpo;
    }

    public List<Cuerpo> getLista() {
        return lista;
    }

    public void setLista(List<Cuerpo> lista) {
        this.lista = lista;
    }

    @PostConstruct
    public void init() {
        cuerpo = new Cuerpo();
        consultar();
    }

    public void consultar() {
        try {
            lista = facadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void insertar() {
        try {
            facadeLocal.create(cuerpo);
            if (cuerpo != null) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Registro Guardado "));
                init();
            }
            init();
        } catch (Exception e) {

            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Error " + e.getMessage()));

        }
    }

    public void actualizar() {
        try {
            facadeLocal.edit(cuerpo);
            init();
        } catch (Exception e) {
        }
    }

    public void eliminar(Cuerpo cuerpo) {
        this.cuerpo = cuerpo;
        try {
            facadeLocal.remove(cuerpo);
            init();

        } catch (Exception e) {
        }
    }

    public void consultarById(Cuerpo cuerpo) {
        this.cuerpo = cuerpo;
    }

}
