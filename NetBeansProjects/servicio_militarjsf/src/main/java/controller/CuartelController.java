package controller;

import ejb.CuartelFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import modelo.Cuartel;

@Named(value = "cuartelController")
@SessionScoped
public class CuartelController implements Serializable {

    @EJB
    private CuartelFacadeLocal facadelocal;
    private Cuartel cuartel;
    private List<Cuartel> lista;

    public Cuartel getCuartel() {
        return cuartel;
    }

    public void setCuartel(Cuartel cuartel) {
        this.cuartel = cuartel;
    }

    public List<Cuartel> getLista() {
        return lista;
    }

    public void setLista(List<Cuartel> lista) {
        this.lista = lista;
    }

    @PostConstruct
    public void init() {
        cuartel = new Cuartel();
        consultar();
    }

    public void consultar() {
        try {
            lista = facadelocal.findAll();
        } catch (Exception e) {
        }
    }

    public void insertar() {
        try {
            facadelocal.create(cuartel);
            if (cuartel != null) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Registro Guardado "));
                init();
            }
            init();
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Error " + e.getMessage()));
        }
    }

    public void actualizar() {
        try {
            facadelocal.edit(cuartel);
            init();
        } catch (Exception e) {
        }
    }

    public void eliminar(Cuartel cuartel) {
        this.cuartel = cuartel;
        try {
            facadelocal.remove(cuartel);
            init();
        } catch (Exception e) {
        }
    }

    public void consultarById(Cuartel cuartel) {
        this.cuartel = cuartel;
    }
}
