
package controller;

import ejb.Servicio_SoldadoFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import modelo.Servicio;
import modelo.Servicio_Soldado;
import modelo.Soldado;

/**
 *
 * @author fredy.alfarousam
 */
@Named(value = "servicio_SoldadoController")
@SessionScoped
public class Servicio_SoldadoController implements Serializable {

    @EJB
    private Servicio_SoldadoFacadeLocal facadeLocal;
    private Servicio_Soldado servicio_Soldado;
    private List<Servicio_Soldado> lista;
    private Soldado soldado;
    private Servicio servicio;

    public Servicio_Soldado getServicio_Soldado() {
        return servicio_Soldado;
    }

    public void setServicio_Soldado(Servicio_Soldado servicio_Soldado) {
        this.servicio_Soldado = servicio_Soldado;
    }

    public List<Servicio_Soldado> getLista() {
        return lista;
    }

    public void setLista(List<Servicio_Soldado> lista) {
        this.lista = lista;
    }

    public Soldado getSoldado() {
        return soldado;
    }

    public void setSoldado(Soldado soldado) {
        this.soldado = soldado;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    @PostConstruct
    public void init() {
        servicio_Soldado = new Servicio_Soldado();
        soldado = new Soldado();
        servicio = new Servicio();
        consultar();
    }

    public void consultar() {
        try {
            lista = facadeLocal.findAll();
        } catch (Exception e) {
        }
    }

    public void insertar() {
        try {
            this.servicio_Soldado.setIdsoldado(soldado);
            this.servicio_Soldado.setIdservicio(servicio);

            facadeLocal.create(servicio_Soldado);
            init();
        } catch (Exception e) {
        }
    }

    public void actualizar() {
        try {
            
            this.servicio_Soldado.setIdsoldado(soldado);
            this.servicio_Soldado.setIdservicio(servicio);

            facadeLocal.edit(servicio_Soldado);
            init();
        } catch (Exception e) {
        }
    }

    public void eliminar(Servicio_Soldado servicio_Soldado) {
        this.servicio_Soldado = servicio_Soldado;
        try {
            facadeLocal.remove(servicio_Soldado);
            init();
        } catch (Exception e) {
        }
    }

    public void consultarById(Servicio_Soldado servicio_Soldado) {
        
        this.servicio_Soldado = servicio_Soldado;
        this.soldado =this.servicio_Soldado.getIdsoldado();
        this.servicio =this.servicio_Soldado.getIdservicio();
        
    }
}
