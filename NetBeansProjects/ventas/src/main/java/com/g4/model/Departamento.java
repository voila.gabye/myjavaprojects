package com.g4.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.validator.constraints.Length;

/**
 *
 * @author gabriela.francousam
 */
@Entity
public class Departamento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_dep;
    @Length(min = 5, max = 24)
    private String departamento;
    @Length(min = 10, max = 24)

    public Departamento() {
        super ();
    }

    public Departamento(Integer id_dep, String departamento) {
        this.id_dep = id_dep;
        this.departamento = departamento;
    }

    public Integer getId_dep() {
        return id_dep;
    }

    public void setId_dep(Integer id_dep) {
        this.id_dep = id_dep;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
 
}
