package com.sistema_ventas.ctrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;
import com.sistema_ventas.util.Dao;
import com.sistema_ventas.util.MunicipioDao;
import com.sistema_ventas.model.*;

@Component
@SessionScope
@ManagedBean
public class SupervicionBean implements Serializable {

    @Autowired
    @Qualifier("sucursal")
    public Dao<Sucursal> Sucursal;
    @Autowired
    @Qualifier("empresa")
    public Dao<Empresa> Empresa;
    @Autowired
    @Qualifier("tipoempresa")
    public Dao<TipoEmpresa> TipoEmpresa;
    @Autowired
    @Qualifier("municipio")
    public MunicipioDao municipio;

    private int sucursalID;
    private int empresaID;
    private int muniID;
    private int tipoID;

    TipoEmpresa tempresa = new TipoEmpresa();
    Empresa empresa = new Empresa();
    Sucursal sucursales = new Sucursal();
    Municipios muni = new Municipios();

    public List<Empresa> listempresa;
    public List<Sucursal> listsucursal;
    public List<TipoEmpresa> listTempresa;
    public List<Municipios> listmunicipios;

    public int getSucursalID() {
        return sucursalID;
    }

    public void setSucursalID(int sucursalID) {
        this.sucursalID = sucursalID;
    }

    public int getEmpresaID() {
        return empresaID;
    }

    public void setEmpresaID(int empresaID) {
        this.empresaID = empresaID;
    }

    public int getMuniID() {
        return muniID;
    }

    public void setMuniID(int muniID) {
        this.muniID = muniID;
    }

    public int getTipoID() {
        return tipoID;
    }

    public void setTipoID(int tipoID) {
        this.tipoID = tipoID;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Sucursal getSucursales() {
        return sucursales;
    }

    public void setSucursales(Sucursal sucursales) {
        this.sucursales = sucursales;
    }

    public TipoEmpresa getTempresa() {
        return tempresa;
    }

    public void setTempresa(TipoEmpresa tempresa) {
        this.tempresa = tempresa;
    }

    public Municipios getMuni() {
        return muni;
    }

    public void setMuni(Municipios muni) {
        this.muni = muni;
    }

    public List<Empresa> getListempresa() {
        this.listempresa = Empresa.findAll();
        return listempresa;
    }

    public void setListempresa(List<Empresa> listempresa) {
        this.listempresa = listempresa;
    }

    public List<Sucursal> getListsucursal() {
        this.listsucursal = Sucursal.findAll();
        return listsucursal;
    }

    public void setListsucursal(List<Sucursal> listsucursal) {
        this.listsucursal = listsucursal;
    }

    public List<TipoEmpresa> getListTempresa() {
        this.listTempresa = TipoEmpresa.findAll();
        return listTempresa;
    }

    public void setListTempresa(List<TipoEmpresa> listTempresa) {
        this.listTempresa = listTempresa;
    }

    public List<Municipios> getListmunicipios() {
        this.listmunicipios = municipio.findAll();
        return listmunicipios;
    }

    public void setListmunicipios(List<Municipios> listmunicipios) {
        this.listmunicipios = listmunicipios;
    }

    @PostConstruct

    public void init() {
        tempresa = new TipoEmpresa();
        empresa = new Empresa();
        sucursales = new Sucursal();
        muni = new Municipios();
    }

    public void crearTipoEmpresa() {
        try {

            TipoEmpresa.create(tempresa);
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Registro Correcto",
                            "Registro de Tipo de Empresa Creado Correctamente"));;

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    public void crearEmpresa() {
        try {
            tempresa.setIdtEm(tipoID);
            muni.setIdmuni(muniID);

            this.empresa.setTipoEmpresa(tempresa);
            this.empresa.setMunicipios(muni);
            Empresa.create(empresa);
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Registro Correcto",
                            "Registro de Empresa Creado Correctamente"));;
        } catch (Exception e) {
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "ERROR AL REGISTRAR",
                            "Registro actualmente No disponible.. contacte con el soporte"));;
        }

    }

    public void crearSucursal() {
        try {

            tempresa.setIdtEm(tipoID);
            muni.setIdmuni(muniID);
            empresa.setIdempresa(empresaID);
            sucursales.setEmpresa(empresa);
            sucursales.setMunicipios(muni);

            Sucursal.create(sucursales);

            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Registro Correcto",
                            "Registro De nueva Sucursal Creado Correctamente"));;
        } catch (Exception e) {
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "ERROR AL REGISTRAR",
                            "Registro actualmente No disponible.. contacte con el soporte"));;

        }

    }

    public void editTipoEmpresa() {
        try {

            TipoEmpresa.edit(tempresa);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void editEmpresa() {
        try {
            tempresa.setIdtEm(tipoID);
            muni.setIdmuni(muniID);

            this.empresa.setTipoEmpresa(tempresa);
            this.empresa.setMunicipios(muni);
            Empresa.edit(empresa);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void editSucursal() {
        try {

            tempresa.setIdtEm(tipoID);
            muni.setIdmuni(muniID);
            empresa.setIdempresa(empresaID);
            sucursales.setEmpresa(empresa);
            sucursales.setMunicipios(muni);

            Sucursal.edit(sucursales);

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    public void removeTipoEmpresa() {
        try {

            TipoEmpresa.remove(tempresa);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void removeEmpresa(Empresa em) {
        try {
            tempresa.setIdtEm(tipoID);
            muni.setIdmuni(muniID);

            this.empresa.setTipoEmpresa(tempresa);
            this.empresa.setMunicipios(muni);
            Empresa.remove(em);
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Registro Eliminado",
                            "Registro de Empresa Eliminado Correctamente"));;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void removeSucursal() {
        try {

            tempresa.setIdtEm(tipoID);
            muni.setIdmuni(muniID);
            empresa.setIdempresa(empresaID);
            sucursales.setEmpresa(empresa);
            sucursales.setMunicipios(muni);

            Sucursal.remove(sucursales);

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

}
