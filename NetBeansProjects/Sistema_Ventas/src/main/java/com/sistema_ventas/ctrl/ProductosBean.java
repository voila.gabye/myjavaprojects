/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sistema_ventas.ctrl;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import com.sistema_ventas.model.Categorias;
import com.sistema_ventas.model.Marcas;
import com.sistema_ventas.model.Modelos;
import com.sistema_ventas.model.Movimientos;
import com.sistema_ventas.model.Productos;
import com.sistema_ventas.model.Sucursal;
import com.sistema_ventas.util.Dao;
import com.sistema_ventas.util.ProductoInterfaz;

/**
 *
 * @author militza.landaverdeus
 */
@Component
@SessionScope
@ManagedBean
public class ProductosBean implements Serializable {

    //private static final long serialVersionUID = 1L;
    @Autowired
    @Qualifier("producto")
    private ProductoInterfaz daoProd;
    @Autowired
    @Qualifier("marca")
    private Dao<Marcas> daoMar;
    @Autowired
    @Qualifier("modelo")
    private Dao<Modelos> daoMod;
    @Autowired
    @Qualifier("categoria")
    private Dao<Categorias> daoCat;
    @Autowired
    @Qualifier("sucursal")
    private Dao<Sucursal> daoSuc;

    private int categoria;
    private int modelo;
    private int producto;
    private int marca;
    private int nuevoStock = 0;

    Productos prod = new Productos();
    Marcas mar = new Marcas();
    Modelos mode = new Modelos();
    Categorias cat = new Categorias();
    Sucursal suc = new Sucursal();
    Movimientos mov = new Movimientos();

    List<Productos> listaProd;
    List<Movimientos> listaProdSuc;
    List<Marcas> listaMar;
    List<Modelos> listaMod;
    List<Categorias> listaCat;
    List<Sucursal> listaSuc;

    public List<Productos> getListaProd() {
        listaProd = daoProd.findAll();
        return listaProd;
    }

    public void setListaProd(List<Productos> listaProd) {
        this.listaProd = listaProd;
    }

    public int getCategoria() {
        return categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }

    public int getModelo() {
        return modelo;
    }

    public void setModelo(int modelo) {
        this.modelo = modelo;
    }

    public Productos getProd() {
        return prod;
    }

    public void setProd(Productos prod) {
        this.prod = prod;
    }

    public Marcas getMar() {
        return mar;
    }

    public void setMar(Marcas mar) {
        this.mar = mar;
    }

    public Modelos getMode() {
        return mode;
    }

    public void setMode(Modelos mode) {
        this.mode = mode;
    }

    public Categorias getCat() {
        return cat;
    }

    public void setCat(Categorias cat) {
        this.cat = cat;
    }

    public Sucursal getSuc() {
        return suc;
    }

    public void setSuc(Sucursal suc) {
        this.suc = suc;
    }

    public Movimientos getMov() {
        return mov;
    }

    public void setMov(Movimientos mov) {
        this.mov = mov;
    }

    public List<Modelos> getListaMod() {
        listaMod = daoMod.findAll();
        return listaMod;
    }

    public void setListaMod(List<Modelos> listaMod) {
        this.listaMod = listaMod;
    }

    public List<Categorias> getListaCat() {
        listaCat = daoCat.findAll();
        return listaCat;
    }

    public void setListaCat(List<Categorias> listaCat) {
        this.listaCat = listaCat;
    }

    public int getProducto() {
        return producto;
    }

    public void setProducto(int producto) {
        this.producto = producto;
    }

    public int getMarca() {
        return marca;
    }

    public void setMarca(int marca) {
        this.marca = marca;
    }

    public List<Marcas> getListaMar() {
        this.listaMar = this.daoMar.findAll();
        return listaMar;
    }

    public void setListaMar(List<Marcas> listaMar) {
        this.listaMar = listaMar;
    }

    public int getNuevoStock() {
        return nuevoStock;
    }

    public void setNuevoStock(int nuevoStock) {
        this.nuevoStock = nuevoStock;
    }

    @PostConstruct
    public void init() {
        prod = new Productos();
        mar = new Marcas();
        mode = new Modelos();
        cat = new Categorias();
        suc = new Sucursal();
        mov = new Movimientos();
    }

    public void nuevoProducto() {
        mode.setIdproduct(modelo);
        cat.setIdcat(categoria);
        prod.setCategorias(cat);
        prod.setModelos(mode);
        daoProd.create(prod);
    }

    public void nuevoPedido() {
        mode.setIdproduct(modelo);
        cat.setIdcat(categoria);
        prod.setCategorias(cat);
        prod.setModelos(mode);
        int stock = prod.getStock();
        prod.setStock(stock + nuevoStock);
        daoProd.edit(prod);
    }

    public void cargarProd(Productos p) {
        this.categoria = p.getCategorias().getIdcat();
        this.modelo = p.getModelos().getIdproduct();
        this.marca = p.getModelos().getMarcas().getIdmarca();
        this.nuevoStock = 0;
        this.prod = p;
    }

}
