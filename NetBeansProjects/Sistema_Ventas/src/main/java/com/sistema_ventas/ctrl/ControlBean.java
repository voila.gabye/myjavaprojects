package com.sistema_ventas.ctrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.sistema_ventas.model.Municipios;
import com.sistema_ventas.model.Ocupacion;
import com.sistema_ventas.model.Persona;
import com.sistema_ventas.util.Dao;



@Component
@SessionScoped
@ManagedBean
public class ControlBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Qualifier("daopersona")
	private Dao daoPersona;
	
	private String dui;
	private List<Persona> personaList;
	private List<Municipios> municipiosList;
	private List<Ocupacion> ocupacinesList;
	
	Persona persona = new Persona();

	
	
	public List<Municipios> getMunicipiosList() {
		return municipiosList;
	}

	public void setMunicipiosList(List<Municipios> municipiosList) {
		this.municipiosList = municipiosList;
	}

	public List<Ocupacion> getOcupacinesList() {
		return ocupacinesList;
	}

	public void setOcupacinesList(List<Ocupacion> ocupacinesList) {
		this.ocupacinesList = ocupacinesList;
	}

	public String getDui() {
		return dui;
	}

	public void setDui(String dui) {
		this.dui = dui;
	}

	public List<Persona> getPersonaList() {
		return personaList;
	}

	public void setPersonaList(List<Persona> personaList) {
		this.personaList = personaList;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	
	@PostConstruct
	public void init() {
		persona = new Persona();
	}
	
	public void nombre() {
		try {
			//persona = daoPersona.buscar(dui);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
