package com.sistema_ventas.ctrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.sistema_ventas.model.Departamento;
import com.sistema_ventas.model.Municipios;
import com.sistema_ventas.model.Persona;
import com.sistema_ventas.util.Dao;
import com.sistema_ventas.util.MunicipioDao;
import com.sistema_ventas.util.PersonaDao;

@Component
@SessionScoped
@ManagedBean
public class NuevoCliente implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	@Qualifier("daopersona")
	private PersonaDao DaoPerson;

	@Autowired
	@Qualifier("departamento")
	private Dao DaoDepartamento;
	private List<Departamento> listaDepartamentos;

	@Autowired
	@Qualifier("municipio")
	private MunicipioDao DaoMunicipio;
	private List<Municipios> listaMunicipios;

	private int municipio;
	private int departamento;
	private Persona persona = new Persona();
	Municipios municipios = new Municipios();

	public int getMunicipio() {
		return municipio;
	}

	public void setMunicipio(int municipio) {
		this.municipio = municipio;
	}

	public List<Municipios> getListaMunicipios() {
		return listaMunicipios;
	}

	public void setListaMunicipios(List<Municipios> listaMunicipios) {
		this.listaMunicipios = listaMunicipios;
	}

	public List<Departamento> getListaDepartamentos() {
		listaDepartamentos = DaoDepartamento.findAll();
		return listaDepartamentos;
	}

	public void setListaDepartamentos(List<Departamento> listaDepartamentos) {
		this.listaDepartamentos = listaDepartamentos;
	}

	public int getDepartamento() {
		return departamento;
	}

	public void setDepartamento(int departamento) {
		this.departamento = departamento;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Municipios getMunicipios() {
		return municipios;
	}

	public void setMunicipios(Municipios municipios) {
		this.municipios = municipios;
	}

	@PostConstruct
	public void init() {
		persona = new Persona();
		municipios = new Municipios();
	}

	public void consultarDepartamentos() {
		listaMunicipios = DaoMunicipio.buscarMunicipios(departamento);
	}

	public void ingresarCliente() {
		try {
			municipios.setIdmuni(municipio);
			persona.setMunicipios(municipios);
			DaoPerson.create(persona);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
