package com.sistema_ventas.ctrl;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import com.sistema_ventas.model.Devoluciones;
import com.sistema_ventas.model.FacturaDetalle;
import com.sistema_ventas.model.FacturaEncabezado;
import com.sistema_ventas.model.Productos;
import com.sistema_ventas.util.Dao;
import com.sistema_ventas.util.FacturaDetalleInterfaz;
import com.sistema_ventas.util.FacturaEncabezadoInterfaz;

@Component
@SessionScope
@ManagedBean
public class DevolucionBean implements Serializable {

	/**
	 * 
	 */
//	private static final long serialVersionUID = 1L;

	@Autowired
	@Qualifier("devolucion")
	 public Dao<Devoluciones> Devoluciones;

	@Autowired
	@Qualifier("facturaDevDao")
	private FacturaDetalleInterfaz facturaDevDao;

	@Autowired
	@Qualifier("facturaEncDao")
	private FacturaEncabezadoInterfaz facturaEncDao;

	
	Devoluciones dev = new Devoluciones();
	FacturaEncabezado facturacionEncabezado = new FacturaEncabezado();
	FacturaDetalle facturacionDetalle = new FacturaDetalle();
	Productos productos = new Productos();
	
	
	private int productoID;
	private int facturaCID;
	private int FacturaDID;
	private int DevID;
	
	private List<Devoluciones> lista;
	private List<FacturaDetalle> listaDetalle;
	private List<FacturaEncabezado> listEncabezado;	
	private List<Productos> listaproducto;

	// Variable
	int NumFactura;
	String clienteFactura;
	String clienteApellido;

	// Getter Y setter de Devoluciones



	public String getClienteFactura() {
		return clienteFactura;
	}

	public Devoluciones getDev() {
		return dev;
	}

	public void setDev(Devoluciones dev) {
		this.dev = dev;
	}

	public void setClienteFactura(String clienteFactura) {
		this.clienteFactura = clienteFactura;
	}

	public FacturaDetalle getFacturacionDetalle() {
		return facturacionDetalle;
	}

	public void setFacturacionDetalle(FacturaDetalle facturacionDetalle) {
		this.facturacionDetalle = facturacionDetalle;
	}

	public FacturaEncabezado getFacturacionEncabezado() {
		return facturacionEncabezado;
	}

	public void setFacturacionEncabezado(FacturaEncabezado facturacionEncabezado) {
		this.facturacionEncabezado = facturacionEncabezado;
	}

	public List<FacturaDetalle> getListaDetalle() {
		return listaDetalle;
	}

	public void setListaDetalle(List<FacturaDetalle> listaDetalle) {
		this.listaDetalle = listaDetalle;
	}

	public List<FacturaEncabezado> getListEncabezado() {

		return listEncabezado;
	}

	public void setListEncabezado(List<FacturaEncabezado> listEncabezado) {
		this.listEncabezado = listEncabezado;
	}

	public List<Devoluciones> getLista() {
		return lista;
	}

	public void setLista(List<Devoluciones> lista) {
		this.lista = lista;
	}

	public Productos getProductos() {
		return productos;
	}

	public void setProductos(Productos productos) {
		this.productos = productos;
	}

	public List<Productos> getListaproducto() {
		return listaproducto;
	}

	public void setListaproducto(List<Productos> listaproducto) {
		this.listaproducto = listaproducto;
	}

	public int getNumFactura() {
		return NumFactura;
	}

	public void setNumFactura(int numFactura) {
		NumFactura = numFactura;
	}
	

	public String getClienteApellido() {
		return clienteApellido;
	}

	public void setClienteApellido(String clienteApellido) {
		this.clienteApellido = clienteApellido;
	}
	
	public int getProductoID() {
		return productoID;
	}

	public void setProductoID(int productoID) {
		this.productoID = productoID;
	}

	public int getFacturaCID() {
		return facturaCID;
	}

	public void setFacturaCID(int facturaCID) {
		this.facturaCID = facturaCID;
	}

	public int getFacturaDID() {
		return FacturaDID;
	}

	public void setFacturaDID(int facturaDID) {
		FacturaDID = facturaDID;
	}

	public int getDevID() {
		return DevID;
	}

	public void setDevID(int devID) {
		DevID = devID;
	}

	@PostConstruct
	public void init() {
 
		dev = new Devoluciones();
		facturacionDetalle = new FacturaDetalle();
		facturacionEncabezado = new FacturaEncabezado();
		productos = new Productos();

	}

	// Metodos de Devolucion

	public void llenar() {
		try {
			this.lista = Devoluciones.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void generarDevolucion() {
		try {
			
			System.out.println("id de factura"+FacturaDID);
			System.out.println("Lo intento al menos");
			facturacionDetalle.setIdfdet(FacturaDID);			
			this.dev.setFacturaDetalle(facturacionDetalle);
			
			

			LocalDate fecha =  java.time.LocalDate.now();					
			Date date = java.sql.Date.valueOf(fecha);			
			this.dev.setFecha(date);
		
			Devoluciones.create(dev);
			
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Credenciales Incorrectas",
                            "Por favor verifica"));;
                            
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void cargarPorId(Devoluciones dev) {
		// this.facturaDetalle.setIdfdet(dev.getFacturaDetalle().getIdfdet());
		this.dev = dev;
	}

	public void factura() {
		try {

			this.listaDetalle = facturaDevDao.Fdetquery(NumFactura);

			String[] nombre = new String[listaDetalle.size()];
			String[] apellido = new String[listaDetalle.size()];
			String datofac = "";
			String datoapp = "";
			for (int i = 0; i < listaDetalle.size(); i++) {

				nombre[i] = listaDetalle.get(i).getFacturaEncabezado().getClientes().getPersona().getNombre();				
				datofac = nombre[i];
				
				apellido[i] = listaDetalle.get(i).getFacturaEncabezado().getClientes().getPersona().getApellido();				
				datoapp = apellido[i];

			}			
			clienteFactura = datofac;
			clienteApellido = datoapp;
			

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error" + e.getMessage());
		}

	}
	
	public void seleccionarProducto(FacturaDetalle fdet) {
		try {
		
			this.facturacionDetalle = fdet;
			FacturaDID = fdet.getIdfdet();
			System.out.println("id de factura"+FacturaDID);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void CancelDev() {
		try {
			System.out.println("Intento de init");
			
			dev = new Devoluciones();
			
			facturacionDetalle = new FacturaDetalle();
			facturacionEncabezado = new FacturaEncabezado();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
				
	}
		

}
