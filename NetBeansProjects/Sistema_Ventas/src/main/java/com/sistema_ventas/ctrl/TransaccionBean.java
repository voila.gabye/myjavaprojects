package com.sistema_ventas.ctrl;

import com.sistema_ventas.model.Abonos;
import com.sistema_ventas.model.FacturaDetalle;
import com.sistema_ventas.model.FacturaEncabezado;
import com.sistema_ventas.model.MedioPago;
import com.sistema_ventas.model.Movimientos;
import com.sistema_ventas.model.Productos;
import com.sistema_ventas.model.Sucursal;
import com.sistema_ventas.model.TipoAbono;
import com.sistema_ventas.model.TiposMovimientos;
import com.sistema_ventas.util.Dao;
import com.sistema_ventas.util.FacturaDetalleInterfaz;
import com.sistema_ventas.util.FacturaEncabezadoInterfaz;
import com.sistema_ventas.util.Mensaje;
import com.sistema_ventas.util.ProductoInterfaz;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@SessionScoped
@ManagedBean
public class TransaccionBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /* Para transacciones de Movimientos*/
    @Autowired
    @Qualifier("producto")
    private ProductoInterfaz daoProd;

    @Autowired
    @Qualifier("sucursal")
    private Dao<Sucursal> daoSuc;

    @Autowired
    @Qualifier("tipoMovimiento")
    private Dao<TiposMovimientos> daoTipoMov;

    @Autowired
    @Qualifier("movimiento")
    private Dao<Movimientos> daoMov;

    /* Para transacciones de Abonos */
    @Autowired
    @Qualifier("abono")
    private Dao<Abonos> daoAbo;

    @Autowired
    @Qualifier("tipoAbono")
    private Dao<TipoAbono> daoTipoAbo;

    @Autowired
    @Qualifier("medioPago")
    private Dao<MedioPago> daoMedPago;

    @Autowired
    @Qualifier("facturaDevDao")
    private FacturaDetalleInterfaz facturaDevDao;

    @Autowired
    @Qualifier("facturaEncDao")
    private FacturaEncabezadoInterfaz facturaEncDao;

    /* Para transacciones de Movimientos*/
    Productos prod = new Productos();
    Sucursal suc = new Sucursal();
    TiposMovimientos tipoMov = new TiposMovimientos();
    Movimientos mov = new Movimientos();

    /* Para transacciones de Abonos */
    private Abonos abo = new Abonos();
    private TipoAbono tipoAbo = new TipoAbono();
    private MedioPago medPago = new MedioPago();
    private FacturaDetalle factudet = new FacturaDetalle();
    private FacturaEncabezado factuEnca = new FacturaEncabezado();

    /* Para transacciones de Movimientos*/
    List<Productos> listaProd;
    List<Sucursal> listaSuc;
    List<TiposMovimientos> listaTipoMov;
    List<Movimientos> listaMov;

    /* Para transacciones de Abonos */
    private List<Abonos> listAbono;
    private List<TipoAbono> listTipoAbono;
    private List<MedioPago> listMPago;
    private List<FacturaDetalle> listaDetalle;
    private List<FacturaEncabezado> listEncabezado;

    /* Variables para transacciones de Movimientos*/
    private int sucursal;
    private int productos;
    private int tipoMovimiento;

    /*Variables para Abonos */
    private int NumFactura;
    private String clienteFactura;
    private String clienteApellido;

    /*Utilidades*/
    private Mensaje util;

    /* Set y get de Mensaje */
    public Mensaje getUtil() {
        return util;
    }

    public void setUtil(Mensaje util) {
        this.util = util;
    }

    /**
     * Set y Get transacciones de Movimientos
     *
     */
    public Productos getProd() {
        return prod;
    }

    public void setProd(Productos prod) {
        this.prod = prod;
    }

    public Sucursal getSuc() {
        return suc;
    }

    public void setSuc(Sucursal suc) {
        this.suc = suc;
    }

    public TiposMovimientos getTipoMov() {
        return tipoMov;
    }

    public void setTipoMov(TiposMovimientos tipoMov) {
        this.tipoMov = tipoMov;
    }

    public Movimientos getMov() {
        return mov;
    }

    public void setMov(Movimientos mov) {
        this.mov = mov;
    }

    public List<Productos> getListaProd() {
        this.listaProd = this.daoProd.findAll();
        return listaProd;
    }

    public void setListaProd(List<Productos> listaProd) {
        this.listaProd = listaProd;
    }

    public List<Sucursal> getListaSuc() {
        this.listaSuc = this.daoSuc.findAll();
        return listaSuc;
    }

    public void setListaSuc(List<Sucursal> listaSuc) {
        this.listaSuc = listaSuc;
    }

    public List<TiposMovimientos> getListaTipoMov() {
        this.listaTipoMov = this.daoTipoMov.findAll();
        return listaTipoMov;
    }

    public void setListaTipoMov(List<TiposMovimientos> listaTipoMov) {
        this.listaTipoMov = listaTipoMov;
    }

    public List<Movimientos> getListaMov() {
        this.listaMov = this.daoMov.findAll();
        return listaMov;
    }

    public void setListaMov(List<Movimientos> listaMov) {
        this.listaMov = listaMov;
    }

    public int getSucursal() {
        return sucursal;
    }

    public void setSucursal(int sucursal) {
        this.sucursal = sucursal;
    }

    public int getProductos() {
        return productos;
    }

    public void setProductos(int productos) {
        this.productos = productos;
    }

    public int getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(int tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    /**
     * Set y Get transacciones de Abonos
     *
     */
    public Abonos getAbo() {
        return abo;
    }

    public void setAbo(Abonos abo) {
        this.abo = abo;
    }

    public TipoAbono getTipoAbo() {
        return tipoAbo;
    }

    public void setTipoAbo(TipoAbono tipoAbo) {
        this.tipoAbo = tipoAbo;
    }

    public MedioPago getMedPago() {
        return medPago;
    }

    public void setMedPago(MedioPago medPago) {
        this.medPago = medPago;
    }

    public List<Abonos> getListAbono() {
        /*    this.listAbono = this.daoAbo.findAll(); */
        return listAbono;
    }

    public void setListAbono(List<Abonos> listAbono) {
        this.listAbono = listAbono;
    }

    public List<TipoAbono> getListTipoAbono() {
        this.listTipoAbono = this.daoTipoAbo.findAll();
        return listTipoAbono;
    }

    public void setListTipoAbono(List<TipoAbono> listTipoAbono) {
        this.listTipoAbono = listTipoAbono;
    }

    public List<MedioPago> getListMPago() {
        this.listMPago = this.daoMedPago.findAll();
        return listMPago;
    }

    public void setListMPago(List<MedioPago> listMPago) {
        this.listMPago = listMPago;
    }

    public FacturaDetalle getFactudet() {
        return factudet;
    }

    public void setFactudet(FacturaDetalle factudet) {
        this.factudet = factudet;
    }

    public FacturaEncabezado getFactuEnca() {
        return factuEnca;
    }

    public void setFactuEnca(FacturaEncabezado factuEnca) {
        this.factuEnca = factuEnca;
    }

    public List<FacturaDetalle> getListaDetalle() {
        return listaDetalle;
    }

    public void setListaDetalle(List<FacturaDetalle> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }

    public List<FacturaEncabezado> getListEncabezado() {
        return listEncabezado;
    }

    public void setListEncabezado(List<FacturaEncabezado> listEncabezado) {
        this.listEncabezado = listEncabezado;
    }

    public int getNumFactura() {
        return NumFactura;
    }

    public void setNumFactura(int NumFactura) {
        this.NumFactura = NumFactura;
    }

    public String getClienteFactura() {
        return clienteFactura;
    }

    public void setClienteFactura(String clienteFactura) {
        this.clienteFactura = clienteFactura;
    }

    public String getClienteApellido() {
        return clienteApellido;
    }

    public void setClienteApellido(String clienteApellido) {
        this.clienteApellido = clienteApellido;
    }

    @PostConstruct

    public void init() {

        /*Movimientos */
        prod = new Productos();
        suc = new Sucursal();
        mov = new Movimientos();
        tipoMov = new TiposMovimientos();

        /*Abonos */
        abo = new Abonos();
        tipoAbo = new TipoAbono();
        medPago = new MedioPago();
        factudet = new FacturaDetalle();
        factuEnca = new FacturaEncabezado();
    }

    /**
     **********************************
     ****** Métodos Movimientos *******
     */
    public void findProd() {
        try {
            this.suc.setIdsucursal(sucursal);
            this.prod.setIdproduct(productos);
            this.tipoMov.setIdtmov(tipoMovimiento);
            this.mov.setProductos(prod);
            this.mov.setSucursal(suc);
            this.mov.setTiposMovimientos(tipoMov);
            this.listaMov = this.daoProd.findProd(mov);
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    public void findProdSuc() {
        try {
            this.suc.setIdsucursal(sucursal);
            this.prod.setIdproduct(productos);
            this.tipoMov.setIdtmov(tipoMovimiento);
            this.mov.setProductos(prod);
            this.mov.setSucursal(suc);
            this.mov.setTiposMovimientos(tipoMov);
            this.listaMov = this.daoProd.findProdSuc(mov);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    /**
     ******************************
     ****** Métodos Abonos ********
     */
    public void createAb() {
        try {
            Date fecha = new Date();
            abo.setFecha(fecha);
            abo.setTipoAbono(tipoAbo);
            abo.setMedioPago(medPago);
            abo.setFacturaDetalle(factudet);
            daoAbo.create(abo);
            init();
            util.mostrarMensaje("Su registro fue guardado");
        } catch (Exception e) {
            util.mostrarMensaje("Su registro no fue guardado" + e.getMessage());
        }
    }

    public void deleteAb(Abonos abonos) {
        this.abo = abonos;
        try {
            daoAbo.remove(abonos);
            listAbono = daoAbo.findAll();
            init();

        } catch (Exception e) {

        }
    }

    public void findAllAb() {
        try {
            this.listAbono = daoAbo.findAll();
        } catch (Exception e) {
        }
    }

    public void findByIdAb(Abonos abonos) {
        this.abo = abonos;
        this.tipoAbo.setIdtabono(abonos.getTipoAbono().getIdtabono());
        this.medPago.setIdmp(abonos.getMedioPago().getIdmp());
    }

    public void findByIdDetalles(FacturaDetalle factudet) {
        this.factudet = factudet;
    }

    public void factura() {
        try {

            this.listaDetalle = facturaDevDao.Fdetquery(NumFactura);

            String[] nombre = new String[listaDetalle.size()];
            String[] apellido = new String[listaDetalle.size()];
            String datofac = "";
            String datoapp = "";
            for (int i = 0; i < listaDetalle.size(); i++) {

                nombre[i] = listaDetalle.get(i).getFacturaEncabezado().getClientes().getPersona().getNombre();
                datofac = nombre[i];

                apellido[i] = listaDetalle.get(i).getFacturaEncabezado().getClientes().getPersona().getApellido();
                datoapp = apellido[i];

            }
            clienteFactura = datofac;
            clienteApellido = datoapp;
            System.out.println("Nombre = " + clienteFactura + "Apellido =" + clienteApellido);
            init();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error" + e.getMessage());
        }

    }

}
