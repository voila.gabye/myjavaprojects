package com.sistema_ventas.model;
// Generated 11-13-2019 11:40:49 AM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * FacturaEncabezado generated by hbm2java
 */
@Entity
@Table(name="factura_encabezado"
    ,catalog="sistema_ventas"
)
public class FacturaEncabezado  implements java.io.Serializable {


     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idfe;
     private Clientes clientes;
     private Empleado empleado;
     private MedioPago medioPago;
     private Sucursal sucursal;
     private TipoPago tipoPago;
     private Date fecha;
     private Double total;
     private String estado;
     private Set<FacturaDetalle> facturaDetalles = new HashSet<FacturaDetalle>(0);

    public FacturaEncabezado() {
    }

	
    public FacturaEncabezado(Date fecha, String estado) {
        this.fecha = fecha;
        this.estado = estado;
    }
    public FacturaEncabezado(Clientes clientes, Empleado empleado, MedioPago medioPago, Sucursal sucursal, TipoPago tipoPago, Date fecha, Double total, String estado, Set<FacturaDetalle> facturaDetalles) {
       this.clientes = clientes;
       this.empleado = empleado;
       this.medioPago = medioPago;
       this.sucursal = sucursal;
       this.tipoPago = tipoPago;
       this.fecha = fecha;
       this.total = total;
       this.estado = estado;
       this.facturaDetalles = facturaDetalles;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="idfe", unique=true, nullable=false)
    public Integer getIdfe() {
        return this.idfe;
    }
    
    public void setIdfe(Integer idfe) {
        this.idfe = idfe;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cliente")
    public Clientes getClientes() {
        return this.clientes;
    }
    
    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="empleado")
    public Empleado getEmpleado() {
        return this.empleado;
    }
    
    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="medio")
    public MedioPago getMedioPago() {
        return this.medioPago;
    }
    
    public void setMedioPago(MedioPago medioPago) {
        this.medioPago = medioPago;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="sucursal")
    public Sucursal getSucursal() {
        return this.sucursal;
    }
    
    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="tipo_pago")
    public TipoPago getTipoPago() {
        return this.tipoPago;
    }
    
    public void setTipoPago(TipoPago tipoPago) {
        this.tipoPago = tipoPago;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fecha", nullable=false, length=19)
    public Date getFecha() {
        return this.fecha;
    }
    
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    
    @Column(name="total", precision=10)
    public Double getTotal() {
        return this.total;
    }
    
    public void setTotal(Double total) {
        this.total = total;
    }

    
    @Column(name="estado", nullable=false, length=2)
    public String getEstado() {
        return this.estado;
    }
    
    public void setEstado(String estado) {
        this.estado = estado;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="facturaEncabezado")
    public Set<FacturaDetalle> getFacturaDetalles() {
        return this.facturaDetalles;
    }
    
    public void setFacturaDetalles(Set<FacturaDetalle> facturaDetalles) {
        this.facturaDetalles = facturaDetalles;
    }




}


