package com.sistema_ventas.model;
// Generated 11-13-2019 11:40:49 AM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * TipoUsuario generated by hbm2java
 */
@Entity
@Table(name="tipo_usuario"
    ,catalog="sistema_ventas"
)
public class TipoUsuario  implements java.io.Serializable {


     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idtusu;
     private String tipo;
     private Set<Usuarios> usuarioses = new HashSet<Usuarios>(0);

    public TipoUsuario() {
    }

	
    public TipoUsuario(String tipo) {
        this.tipo = tipo;
    }
    public TipoUsuario(String tipo, Set<Usuarios> usuarioses) {
       this.tipo = tipo;
       this.usuarioses = usuarioses;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="idtusu", unique=true, nullable=false)
    public Integer getIdtusu() {
        return this.idtusu;
    }
    
    public void setIdtusu(Integer idtusu) {
        this.idtusu = idtusu;
    }

    
    @Column(name="tipo", nullable=false, length=65535)
    public String getTipo() {
        return this.tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="tipoUsuario")
    public Set<Usuarios> getUsuarioses() {
        return this.usuarioses;
    }
    
    public void setUsuarioses(Set<Usuarios> usuarioses) {
        this.usuarioses = usuarioses;
    }




}


