package com.sistema_ventas.config;

import com.sistema_ventas.impl.AbonosDAO;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.sistema_ventas.impl.CategoriaDAO;
import com.sistema_ventas.impl.DepartamentoImpl;
import com.sistema_ventas.impl.DevolucionesDAO;
import com.sistema_ventas.impl.FacturaDetalleDaoDev;
import com.sistema_ventas.impl.FacturaEncabezadoDaoForDev;
import com.sistema_ventas.impl.MarcaDAO;
import com.sistema_ventas.impl.MedioPagoDAO;
import com.sistema_ventas.impl.ModeloDAO;
import com.sistema_ventas.impl.MovimientoDAO;
import com.sistema_ventas.impl.MunicipiosImpl;
import com.sistema_ventas.impl.PersonaDAO;
import com.sistema_ventas.impl.ProductoDAO;
import com.sistema_ventas.impl.SucursalDAO;
import com.sistema_ventas.impl.TipoAbonoDAO;
import com.sistema_ventas.impl.TipoMovimientoDAO;
import com.sistema_ventas.model.Abonos;
import com.sistema_ventas.model.Categorias;
import com.sistema_ventas.model.Departamento;
import com.sistema_ventas.model.Sucursal;
import com.sistema_ventas.model.Devoluciones;
import com.sistema_ventas.model.FacturaDetalle;
import com.sistema_ventas.model.FacturaEncabezado;
import com.sistema_ventas.model.Marcas;
import com.sistema_ventas.model.MedioPago;
import com.sistema_ventas.model.Modelos;
import com.sistema_ventas.model.Municipios;
import com.sistema_ventas.model.Persona;
import com.sistema_ventas.model.Productos;
import com.sistema_ventas.model.Movimientos;
import com.sistema_ventas.model.TipoAbono;
import com.sistema_ventas.model.TiposMovimientos;
import com.sistema_ventas.util.Dao;
import com.sistema_ventas.util.FacturaDetalleInterfaz;
import com.sistema_ventas.util.FacturaEncabezadoInterfaz;
import com.sistema_ventas.util.MunicipioDao;
import com.sistema_ventas.util.PersonaDao;
import com.sistema_ventas.util.ProductoInterfaz;

@Configuration
@ComponentScan(basePackages = "com.sistema_ventas")
@EnableTransactionManagement
public class AppContextConfig {

    @Bean(name = "dataSource")
    public DataSource getDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/sistema_ventas?useSSL=false");
        dataSource.setUsername("root");
        dataSource.setPassword("root");
        return dataSource;
    }

    private Properties getHibernateProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        return properties;
    }

    @Autowired
    @Bean(name = "sessionFactory")
    public SessionFactory getsessionFactory(DataSource dataSource) {
        LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
        sessionBuilder.scanPackages("com.sistema_ventas.model");
        sessionBuilder.addProperties(getHibernateProperties());
        return sessionBuilder.buildSessionFactory();
    }

    @Autowired
    @Bean(name = "transactionManager")
    public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);
        return transactionManager;
    }

    @Autowired
    @Bean(name = "devolucion")
    public Dao Devoluciones(SessionFactory sessionFactory) {
        return new DevolucionesDAO(sessionFactory, Devoluciones.class);
    }

    @Autowired
    @Bean(name = "daopersona")
    public PersonaDao Persona(SessionFactory sessionFactory) {
        return new PersonaDAO(sessionFactory, Persona.class);
    }

    @Autowired
    @Bean(name = "departamento")
    public Dao departamento(SessionFactory sessionFactory) {
        return new DepartamentoImpl(sessionFactory, Departamento.class);
    }

    @Autowired
    @Bean(name = "municipio")
    public MunicipioDao municipio(SessionFactory sessionFactory) {
        return new MunicipiosImpl(sessionFactory, Municipios.class);
    }

    @Autowired
    @Bean(name = "facturaDevDao")
    public FacturaDetalleInterfaz facturaDevDao(SessionFactory sessionFactory) {
        return new FacturaDetalleDaoDev(sessionFactory, FacturaDetalle.class);
    }

    @Autowired
    @Bean(name = "producto")
    public ProductoInterfaz Producto(SessionFactory sessionFactory) {
        return new ProductoDAO(sessionFactory, Productos.class);
    }

    @Autowired
    @Bean(name = "categoria")
    public Dao Categoria(SessionFactory sessionFactory) {
        return new CategoriaDAO(sessionFactory, Categorias.class);
    }

    @Autowired
    @Bean(name = "marca")
    public Dao Marca(SessionFactory sessionFactory) {
        return new MarcaDAO(sessionFactory, Marcas.class);
    }

    @Autowired
    @Bean(name = "sucursal")
    public Dao Sucursal(SessionFactory sessionFactory) {
        return new SucursalDAO(sessionFactory, Sucursal.class);
    }

    @Autowired
    @Bean(name = "modelo")
    public Dao Modelo(SessionFactory sessionFactory) {
        return new ModeloDAO(sessionFactory, Modelos.class);
    }

    @Autowired
    @Bean(name = "facturaEncDao")
    public FacturaEncabezadoInterfaz facturaEncDao(SessionFactory sessionFactory) {
        return new FacturaEncabezadoDaoForDev(sessionFactory, FacturaEncabezado.class);
    }

    @Autowired
    @Bean(name = "movimiento")
    public Dao Movimientos(SessionFactory sessionFactory) {
        return new MovimientoDAO(sessionFactory, Movimientos.class);
    }

    @Autowired
    @Bean(name = "tipoMovimiento")
    public Dao TiposMovimientos(SessionFactory sessionFactory) {
        return new TipoMovimientoDAO(sessionFactory, TiposMovimientos.class);
    }

    @Autowired
    @Bean(name = "abono")
    public Dao Abonos(SessionFactory sessionFactory) {
        return new AbonosDAO(sessionFactory, Abonos.class);
    }

    @Autowired
    @Bean(name = "tipoAbono")
    public Dao TipoAbonos(SessionFactory sessionFactory) {
        return new TipoAbonoDAO(sessionFactory, TipoAbono.class);
    }

    @Autowired
    @Bean(name = "medioPago")
    public Dao MedioPagos(SessionFactory sessionFactory) {
        return new MedioPagoDAO(sessionFactory, MedioPago.class);
    }
}
