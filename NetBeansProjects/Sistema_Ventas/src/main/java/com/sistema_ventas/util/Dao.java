package com.sistema_ventas.util;

import java.util.List;

import com.sistema_ventas.model.FacturaEncabezado;

public interface Dao<T> {

    public void create(T e);

    public void remove(T e);

    public void edit(T e);

    public T find(Object e);

    public List<T> findAll();


}
