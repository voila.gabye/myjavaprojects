package com.sistema_ventas.util;

import com.sistema_ventas.model.Usuarios;

public interface LoginInterfaz extends Dao<Usuarios>{

	 public Usuarios login(Usuarios usuario);
}
