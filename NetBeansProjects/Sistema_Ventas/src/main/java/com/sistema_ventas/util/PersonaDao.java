package com.sistema_ventas.util;

import com.sistema_ventas.model.Persona;

public interface PersonaDao extends Dao<Persona> {
	
	public Persona buscar(String dui);
}
