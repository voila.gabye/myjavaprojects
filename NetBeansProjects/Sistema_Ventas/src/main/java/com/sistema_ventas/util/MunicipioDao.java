package com.sistema_ventas.util;

import java.util.List;

import com.sistema_ventas.model.Municipios;

public interface MunicipioDao extends Dao<Municipios> {
	
	List<Municipios> buscarMunicipios(int departamento);
	
}
