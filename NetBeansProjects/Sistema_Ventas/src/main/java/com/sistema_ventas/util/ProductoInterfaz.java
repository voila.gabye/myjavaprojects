package com.sistema_ventas.util;

import com.sistema_ventas.model.Movimientos;
import com.sistema_ventas.model.Productos;
import java.util.List;

/**
 *
 * @author militza.landaverdeus
 */
public interface ProductoInterfaz extends Dao<Productos> {

    public List<Movimientos> findProd(Movimientos m);

    public List<Movimientos> findProdSuc(Movimientos m);

}
