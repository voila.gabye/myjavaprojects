package com.sistema_ventas.util;

import com.sistema_ventas.model.Departamento;
import com.sistema_ventas.model.Persona;

public interface DepartamentosInterfaz extends Dao<Departamento> {
	
	public Persona buscar(String dui);

}
