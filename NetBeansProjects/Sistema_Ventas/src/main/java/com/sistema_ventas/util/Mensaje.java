
package com.sistema_ventas.util;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author gabriela.francousam
 */
@ManagedBean
@ViewScoped
public class Mensaje {

    public void mostrarMensaje(String mensaje) {
        FacesMessage men = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, men);
    }
}
