package com.sistema_ventas.impl;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.sistema_ventas.model.*;
import com.sistema_ventas.util.*;

@Transactional
public class SucursalDAO extends AbstractFacade<Sucursal> implements Dao<Sucursal> {

	private SessionFactory sf;

	public SucursalDAO(SessionFactory sf, Class<Sucursal> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}

}
