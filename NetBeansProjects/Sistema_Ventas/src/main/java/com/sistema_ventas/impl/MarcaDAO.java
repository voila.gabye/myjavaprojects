package com.sistema_ventas.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sistema_ventas.model.*;
import com.sistema_ventas.util.*;
@Transactional
public class MarcaDAO extends AbstractFacade<Marcas> implements Dao<Marcas> {

	private SessionFactory sf;

    public MarcaDAO(SessionFactory sf, Class<Marcas> entityClass) {
        super(entityClass);
        this.sf = sf;
    }
    
    @Override
    protected SessionFactory sessionFactory() {
        return sf;
    }
    
   

}
