package com.sistema_ventas.impl;

import org.hibernate.SessionFactory;

import com.sistema_ventas.model.Empresa;
import com.sistema_ventas.util.AbstractFacade;
import com.sistema_ventas.util.Dao;

public class EmpresaDao extends AbstractFacade<Empresa> implements Dao<Empresa>{

	
	private SessionFactory sf;
	
	public EmpresaDao(SessionFactory sf, Class<Empresa> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}
	
}
