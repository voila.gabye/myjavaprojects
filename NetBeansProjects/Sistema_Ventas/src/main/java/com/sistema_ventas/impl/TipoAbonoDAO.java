package com.sistema_ventas.impl;

import com.sistema_ventas.model.TipoAbono;
import com.sistema_ventas.util.AbstractFacade;
import com.sistema_ventas.util.Dao;
import org.hibernate.SessionFactory;

/**
 *
 * @author gabriela.francousam
 */
public class TipoAbonoDAO extends AbstractFacade<TipoAbono> implements Dao<TipoAbono> {

    private SessionFactory sf;

    public TipoAbonoDAO(SessionFactory sf, Class<TipoAbono> entityClass) {
        super(entityClass);
        this.sf = sf;
    }

    @Override
    protected SessionFactory sessionFactory() {
        return sf;
    }
}
