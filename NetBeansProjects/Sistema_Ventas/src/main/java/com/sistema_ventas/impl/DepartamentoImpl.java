package com.sistema_ventas.impl;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.sistema_ventas.model.Departamento;
import com.sistema_ventas.model.Persona;
import com.sistema_ventas.util.AbstractFacade;
import com.sistema_ventas.util.Dao;

@Transactional
public class DepartamentoImpl extends AbstractFacade<Departamento> implements Dao<Departamento> {

	private SessionFactory sf;

	public DepartamentoImpl(SessionFactory sf, Class<Departamento> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}
	
	
	
}
