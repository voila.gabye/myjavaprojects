package com.sistema_ventas.impl;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.sistema_ventas.model.Persona;
import com.sistema_ventas.util.AbstractFacade;
import com.sistema_ventas.util.PersonaDao;

@Transactional
public class PersonaDAO extends AbstractFacade<Persona> implements PersonaDao {
	
	private SessionFactory sf;
	
	public PersonaDAO(SessionFactory sf, Class<Persona> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}
	
	
	public Persona buscar(String dui) {
		Persona per = new Persona();
		CriteriaBuilder cb = sf.getCriteriaBuilder();
		CriteriaQuery<Persona> cq = cb.createQuery(Persona.class);
		Root<Persona> persona = cq.from(Persona.class);
		cq.where(cb.equal(persona.get("dui"), dui));
		for(Persona p : sf.getCurrentSession().createQuery(cq).getResultList()) {
			per.setNombre(p.getNombre());
			per.setApellido(p.getApellido());
			per.setDui(p.getDui());
			per.setNit(p.getNit());
			per.setMunicipios(p.getMunicipios());
			per.setDireccion(p.getDireccion());
			per.setTelefono(p.getTelefono());
			per.setOcupacion(p.getOcupacion());
		}
		System.out.print("Nombre: ********" + per.getNombre());
		System.out.print("Apellido: ********" + per.getApellido());
		System.out.print("Muni: ********" + per.getMunicipios());
		return per;
	}

}
