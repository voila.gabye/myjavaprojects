package com.sistema_ventas.impl;

import org.hibernate.SessionFactory;


import com.sistema_ventas.model.TipoEmpresa;
import com.sistema_ventas.util.AbstractFacade;
import com.sistema_ventas.util.Dao;

public class TipoEmpresaDao extends AbstractFacade<TipoEmpresa> implements Dao<TipoEmpresa>{

	
	
	private SessionFactory sf;
	
	public TipoEmpresaDao(SessionFactory sf, Class<TipoEmpresa> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}
}
