package com.sistema_ventas.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sistema_ventas.model.FacturaDetalle;
import com.sistema_ventas.model.FacturaEncabezado;
import com.sistema_ventas.util.AbstractFacade;
import com.sistema_ventas.util.Dao;
import com.sistema_ventas.util.FacturaDetalleInterfaz;
import org.hibernate.Transaction;

@Transactional
@Repository(value = "facturaDevDao")
public class FacturaDetalleDaoDev extends AbstractFacade<FacturaDetalle> implements FacturaDetalleInterfaz {

    private SessionFactory sf;

    public FacturaDetalleDaoDev(SessionFactory sf, Class<FacturaDetalle> entityClass) {
        super(FacturaDetalle.class);
        this.sf = sf;
    }

    @Override
    protected SessionFactory sessionFactory() {
        return sf;
    }

    
    @Override
    public List<FacturaDetalle> Fdetquery(int id) {
        System.out.println("Entre al metodo");

        List<FacturaDetalle> lis = new ArrayList<>();
        try {
            System.out.println("hola: ");
            
            session=sf.openSession();
            
            Transaction tx=session.beginTransaction();
            tx=session.getTransaction();
            
            lis = session.createNativeQuery("select * from factura_detalle fd inner join factura_encabezado o on fd.factura = o.idfe inner join empleado p on fd.empleado = p.idemp inner join productos pro on fd.producto = pro.idproduct inner join persona perso on p.persona = perso.id  where o.idfe =" + id, FacturaDetalle.class).getResultList();
            tx.commit();
            
            
            System.out.println("Size: " + lis.size());

        } catch (Exception e) {
            e.printStackTrace();
            return null;

        }

        return lis;

    }

}
