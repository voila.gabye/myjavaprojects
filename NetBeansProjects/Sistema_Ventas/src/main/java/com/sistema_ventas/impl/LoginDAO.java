package com.sistema_ventas.impl;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sistema_ventas.model.Usuarios;
import com.sistema_ventas.util.AbstractFacade;
import com.sistema_ventas.util.LoginInterfaz;
import org.springframework.stereotype.Repository;

@Transactional
@Repository(value = "login")
public class LoginDAO extends AbstractFacade<Usuarios> implements LoginInterfaz {

    private SessionFactory sf;

    public LoginDAO(SessionFactory sf, Class<Usuarios> entityClass) {
        super(entityClass);

        this.sf = sf;
    }

    @Override
    protected SessionFactory sessionFactory() {
        return sf;
    }

    
    @Override
    public Usuarios login(Usuarios usuario) {

        System.out.println("Entre al metodo");
        Session session = sf.openSession();
        session.beginTransaction();

        try {
            CriteriaBuilder cb = sf.getCriteriaBuilder();
            CriteriaQuery<Usuarios> cq = cb.createQuery(Usuarios.class);
            Root<Usuarios> root = cq.from(Usuarios.class);
            cq.where(
                    cb.equal(root.get("user"), usuario.getUser()),
                    cb.and(cb.equal(root.get("password"), usuario.getPassword())
                    ));
            Query q = sf.createEntityManager().createQuery(cq);

            usuario = (Usuarios) q.getSingleResult();
            
           
            if (null == usuario) {
                usuario = null;
            } else {
                return usuario;
            }
        } catch (Exception e) {
            usuario = null;
        }
        
        session.getTransaction().commit();
        return usuario;
    }
}
