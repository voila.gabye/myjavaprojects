package com.sistema_ventas.impl;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sistema_ventas.model.*;
import com.sistema_ventas.util.*;

@Transactional
public class ModeloDAO extends AbstractFacade<Modelos> implements Dao<Modelos> {
	
	private SessionFactory sf;

    public ModeloDAO(SessionFactory sf, Class<Modelos> entityClass) {
        super(entityClass);
        this.sf = sf;
    }
    
    @Override
    protected SessionFactory sessionFactory() {
        return sf;
    }
}
