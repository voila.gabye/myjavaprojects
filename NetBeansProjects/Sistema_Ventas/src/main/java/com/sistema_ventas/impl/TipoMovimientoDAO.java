package com.sistema_ventas.impl;

import com.sistema_ventas.model.TiposMovimientos;
import com.sistema_ventas.util.AbstractFacade;
import com.sistema_ventas.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class TipoMovimientoDAO extends AbstractFacade<TiposMovimientos> implements Dao<TiposMovimientos> {

    private SessionFactory sf;

    public TipoMovimientoDAO(SessionFactory sf, Class<TiposMovimientos> entityClass) {
        super(entityClass);
        this.sf = sf;
    }

    @Override
    protected SessionFactory sessionFactory() {
        return sf;
    }
}
