package com.sistema_ventas.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.sistema_ventas.model.Municipios;
import com.sistema_ventas.model.Persona;
import com.sistema_ventas.util.AbstractFacade;
import com.sistema_ventas.util.MunicipioDao;

@Transactional
public class MunicipiosImpl extends AbstractFacade<Municipios> implements MunicipioDao {

	private SessionFactory sf;

	public MunicipiosImpl(SessionFactory sf, Class<Municipios> entityClass) {
		super(entityClass);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}

	@Override
	public List<Municipios> buscarMunicipios(int departamento) {
		List<Municipios> lista = new ArrayList<>();
		CriteriaBuilder cb = sf.getCriteriaBuilder();
		CriteriaQuery<Municipios> cq = cb.createQuery(Municipios.class);
		Root<Municipios> root = cq.from(Municipios.class);
		cq.where(cb.equal(root.get("departamento"), departamento));
		lista = sf.getCurrentSession().createQuery(cq).getResultList();
		return lista;
	}

}
