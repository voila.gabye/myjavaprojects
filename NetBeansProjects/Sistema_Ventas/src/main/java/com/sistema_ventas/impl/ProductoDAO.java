package com.sistema_ventas.impl;

import com.sistema_ventas.model.Movimientos;
import com.sistema_ventas.model.Productos;
import com.sistema_ventas.util.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author militza.landaverdeus
 */
@Transactional
public class ProductoDAO extends AbstractFacade<Productos> implements ProductoInterfaz {

    private SessionFactory sf;

    public ProductoDAO(SessionFactory sf, Class<Productos> entityClass) {
        super(entityClass);
        this.sf = sf;
    }

    @Override
    protected SessionFactory sessionFactory() {
        return sf;
    }

    public List<Movimientos> findProd(Movimientos m) {
        List<Movimientos> lista = new ArrayList<>();

        CriteriaBuilder cb = sf.getCriteriaBuilder();
        CriteriaQuery<Movimientos> cq = cb.createQuery(Movimientos.class);
        Root<Movimientos> root = cq.from(Movimientos.class);
        cq.where(
                cb.equal(root.get("sucursal"), m.getSucursal()),
                cb.equal(root.get("tiposMovimientos"), m.getTiposMovimientos()),
                cb.equal(root.get("productos"), m.getProductos())
        );
        lista = sf.getCurrentSession().createQuery(cq).getResultList();
        return lista;
    }

    public List<Movimientos> findProdSuc(Movimientos m) {
        List<Movimientos> lista = new ArrayList<>();
        CriteriaBuilder cb = sf.getCriteriaBuilder();
        CriteriaQuery<Movimientos> cq = cb.createQuery(Movimientos.class);
        Root<Movimientos> root = cq.from(Movimientos.class);
        //cq.multiselect(cb.max(root.<Integer>get("idmov")));	
        //cq.select(cb.max(root.get()));
        //Join<Productos, Movimientos> join02 = root.join(Notas_.categoria);
        cq.where(
                cb.equal(root.get("sucursal"), m.getSucursal()),
                //cb.equal(root.get("tiposMovimientos"), m.getTiposMovimientos()),
                cb.equal(root.get("productos"), m.getProductos())
        );
        lista = sf.getCurrentSession().createQuery(cq).getResultList();
        return lista;
    }
}
