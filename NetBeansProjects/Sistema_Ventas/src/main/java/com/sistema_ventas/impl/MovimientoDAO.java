package com.sistema_ventas.impl;

import com.sistema_ventas.model.Movimientos;
import com.sistema_ventas.util.AbstractFacade;
import com.sistema_ventas.util.Dao;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class MovimientoDAO extends AbstractFacade<Movimientos> implements Dao<Movimientos> {

    private SessionFactory sf;

    public MovimientoDAO(SessionFactory sf, Class<Movimientos> entityClass) {
        super(entityClass);
        this.sf = sf;
    }

    @Override
    protected SessionFactory sessionFactory() {
        return sf;
    }

    public List<Movimientos> filtro(Movimientos m) {
        List<Movimientos> lista = new ArrayList<>();

        return lista;
    }
}
