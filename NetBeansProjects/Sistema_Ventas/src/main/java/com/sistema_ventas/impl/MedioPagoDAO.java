package com.sistema_ventas.impl;

import com.sistema_ventas.model.MedioPago;
import com.sistema_ventas.util.AbstractFacade;
import com.sistema_ventas.util.Dao;
import org.hibernate.SessionFactory;

/**
 *
 * @author gabriela.francousam
 */
public class MedioPagoDAO extends AbstractFacade<MedioPago> implements Dao<MedioPago> {

    private SessionFactory sf;

    public MedioPagoDAO(SessionFactory sf, Class<MedioPago> entityClass) {
        super(entityClass);
        this.sf = sf;
    }

    @Override
    protected SessionFactory sessionFactory() {
        return sf;
    }
}
