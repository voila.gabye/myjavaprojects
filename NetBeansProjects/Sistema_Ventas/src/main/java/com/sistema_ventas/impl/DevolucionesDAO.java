package com.sistema_ventas.impl;

import org.hibernate.SessionFactory;

import org.springframework.transaction.annotation.Transactional;

import com.sistema_ventas.model.Devoluciones;
import com.sistema_ventas.util.AbstractFacade;
import com.sistema_ventas.util.Dao;

@Transactional
public class DevolucionesDAO extends AbstractFacade<Devoluciones> implements Dao<Devoluciones> {

	private SessionFactory sf;

	public DevolucionesDAO(SessionFactory sf, Class<Devoluciones> entityClass) {
		super(Devoluciones.class);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sf;
	}
	
	

}