package com.sistema_ventas.impl;

import com.sistema_ventas.model.Abonos;
import com.sistema_ventas.util.AbstractFacade;
import com.sistema_ventas.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gabriela.francousam
 */
@Transactional
public class AbonosDAO extends AbstractFacade<Abonos> implements Dao<Abonos> {

    private SessionFactory sf;

    public AbonosDAO(SessionFactory sf, Class<Abonos> entityClass) {
        super(entityClass);
        this.sf = sf;
    }

    @Override
    protected SessionFactory sessionFactory() {
        return sf;
    }
}
