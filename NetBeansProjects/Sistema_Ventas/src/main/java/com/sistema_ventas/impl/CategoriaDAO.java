package com.sistema_ventas.impl;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.sistema_ventas.model.*;
import com.sistema_ventas.util.*;


@Transactional
public class CategoriaDAO extends AbstractFacade<Categorias> implements Dao<Categorias> {
	private SessionFactory sf;

    public CategoriaDAO(SessionFactory sf, Class<Categorias> entityClass) {
        super(entityClass);
        this.sf = sf;
    }
    
    @Override
    protected SessionFactory sessionFactory() {
        return sf;
    }
}
