package com.sistema_ventas.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sistema_ventas.model.FacturaEncabezado;
import com.sistema_ventas.util.AbstractFacade;
import com.sistema_ventas.util.FacturaEncabezadoInterfaz;

@Transactional
@Repository(value = "facturaEncDao")
public class FacturaEncabezadoDaoForDev extends AbstractFacade<FacturaEncabezado> implements FacturaEncabezadoInterfaz {

	private SessionFactory sf;

	public FacturaEncabezadoDaoForDev(SessionFactory sf, Class<FacturaEncabezado> entityClass) {
		super(FacturaEncabezado.class);
		this.sf = sf;
	}

	@Override
	protected SessionFactory sessionFactory() {
		// TODO Auto-generated method stub
		return null;
	}


}
