package com.ventasZ.config;

import com.ventasZ.impl.CategoriaImpl;
import com.ventasZ.impl.ClienteImpl;
import com.ventasZ.impl.DetalleImpl;
import com.ventasZ.impl.FacturaImpl;
import com.ventasZ.impl.ModoPagoImpl;
import com.ventasZ.impl.ProductoImpl;
import com.ventasZ.model.Categoria;
import com.ventasZ.model.Cliente;
import com.ventasZ.model.Detalle;
import com.ventasZ.model.Factura;
import com.ventasZ.model.ModoPago;
import com.ventasZ.model.Producto;
import com.ventasZ.util.Dao;
import org.apache.commons.dbcp2.BasicDataSource;
import java.util.Properties;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;

import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author gabriela.francousam
 */
@Configuration
@ComponentScan(basePackages = "com.ventasZ")
@EnableTransactionManagement
public class AppContextConfig {

    @Bean(name = "dataSource")
    public DataSource getDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/ventas?useSSL=false");
        dataSource.setUsername("root");
        dataSource.setPassword("root");
        return dataSource;
    }

    private Properties getHibernateProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        return properties;
    }

    @Autowired
    @Bean(name = "sessionFactory")
    public SessionFactory getsessionFactory(DataSource dataSource) {
        LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
        sessionBuilder.scanPackages("com.ventasZ.model");
        sessionBuilder.addProperties(getHibernateProperties());
        return sessionBuilder.buildSessionFactory();
    }

    @Autowired
    @Bean(name = "transactionManager")
    public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);
        return transactionManager;
    }

    @Autowired
    @Bean(name = "cliente")
    public Dao Cliente(SessionFactory sf) {
        return new ClienteImpl(sf, Cliente.class);
    }

    @Autowired
    @Bean(name = "categoria")
    public Dao Categoria(SessionFactory sf) {
        return new CategoriaImpl(sf, Categoria.class);
    }

    @Autowired
    @Bean(name = "producto")
    public Dao Producto(SessionFactory sf) {
        return new ProductoImpl(sf, Producto.class);
    }

    @Autowired
    @Bean(name = "modoPago")
    public Dao ModoPago(SessionFactory sf) {
        return new ModoPagoImpl(sf, ModoPago.class);
    }

    @Autowired
    @Bean(name = "factura")
    public Dao Factura(SessionFactory sf) {
        return new FacturaImpl(sf, Factura.class);
    }

    @Autowired
    @Bean(name = "detalle")
    public Dao Detalle(SessionFactory sf) {
        return new DetalleImpl(sf, Detalle.class);
    }
}
