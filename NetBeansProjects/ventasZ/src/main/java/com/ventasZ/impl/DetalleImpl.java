package com.ventasZ.impl;

import com.ventasZ.model.Detalle;
import com.ventasZ.util.AbstractFacade;
import com.ventasZ.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gabriela.francousam
 */
@Transactional
public class DetalleImpl extends AbstractFacade<Detalle> implements Dao<Detalle> {

    private SessionFactory sf;

    public DetalleImpl(SessionFactory sf, Class<Detalle> entityClass) {
        super(entityClass);
        this.sf = sf;
    }

    @Override
    protected SessionFactory sessionFactory() {
        return sf;
    }

}
