
package com.ventasZ.impl;

import com.ventasZ.model.Cliente;
import com.ventasZ.util.AbstractFacade;
import com.ventasZ.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gabriela.francousam
 */
@Transactional
public class ClienteImpl extends AbstractFacade<Cliente> implements Dao<Cliente>{
    private SessionFactory sf;

    public ClienteImpl(SessionFactory sf, Class<Cliente> entityClass) {
        super(entityClass);
        this.sf = sf;
    }
    
    @Override
    protected SessionFactory sessionFactory() {
        return sf;
    }
}
