
package com.ventasZ.impl;


import com.ventasZ.model.Categoria;
import com.ventasZ.util.AbstractFacade;
import com.ventasZ.util.Dao;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gabriela.francousam
 */
@Transactional
public class CategoriaImpl extends AbstractFacade<Categoria> implements Dao<Categoria> {
	private SessionFactory sf;

    public CategoriaImpl(SessionFactory sf, Class<Categoria> entityClass) {
        super(entityClass);
        this.sf = sf;
    }
    
    @Override
    protected SessionFactory sessionFactory() {
        return sf;
    }
}
