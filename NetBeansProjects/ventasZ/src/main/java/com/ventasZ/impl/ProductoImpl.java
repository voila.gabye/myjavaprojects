
package com.ventasZ.impl;

import com.ventasZ.model.Producto;
import com.ventasZ.util.AbstractFacade;
import com.ventasZ.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gabriela.francousam
 */
@Transactional
public class ProductoImpl extends AbstractFacade<Producto> implements Dao<Producto> {
   private final SessionFactory sf;

    public ProductoImpl(SessionFactory sf, Class<Producto> entityClass) {
        super(entityClass);
        this.sf = sf;
    }
   
   @Override
   protected SessionFactory sessionFactory (){
   return sf;
   }
}
