package com.ventasZ.impl;

import com.ventasZ.model.ModoPago;
import com.ventasZ.util.AbstractFacade;
import com.ventasZ.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gabriela.francousam
 */
@Transactional
public class ModoPagoImpl extends AbstractFacade<ModoPago> implements Dao<ModoPago> {

    private final SessionFactory sf;

    public ModoPagoImpl(SessionFactory sf, Class<ModoPago> entityClass) {
        super(entityClass);
        this.sf = sf;
    }

    @Override
    protected SessionFactory sessionFactory() {
        return sf;
    }
}
