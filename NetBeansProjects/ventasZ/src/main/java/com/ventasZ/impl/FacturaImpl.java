
package com.ventasZ.impl;

import com.ventasZ.model.Factura;
import com.ventasZ.util.AbstractFacade;
import com.ventasZ.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gabriela.francousam
 */
@Transactional
public class FacturaImpl extends AbstractFacade<Factura> implements Dao<Factura>{
    private final SessionFactory sf;

    public FacturaImpl(SessionFactory sf, Class<Factura> entityClass) {
        super(entityClass);
        this.sf = sf;
    }
    
    @Override
    protected SessionFactory sessionFactory (){
    return sf;
    }
}
