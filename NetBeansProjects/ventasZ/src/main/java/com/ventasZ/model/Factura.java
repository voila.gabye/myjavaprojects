package com.ventasZ.model;
// Generated 01-03-2020 09:20:46 AM by Hibernate Tools 4.3.1


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Factura generated by hbm2java
 */
@Entity
@Table(name="factura"
    ,catalog="ventas"
)
public class Factura  implements java.io.Serializable {


     private Integer numFactura;
     private Cliente cliente;
     private ModoPago modoPago;
     private Date fecha;
     private List <Detalle>detalles = new ArrayList<Detalle>();

    public Factura() {
    }

	
    public Factura(Cliente cliente, ModoPago modoPago) {
        this.cliente = cliente;
        this.modoPago = modoPago;
    }
    public Factura(Cliente cliente, ModoPago modoPago, Date fecha, List <Detalle>detalles) {
       this.cliente = cliente;
       this.modoPago = modoPago;
       this.fecha = fecha;
       this.detalles = detalles;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="num_factura", unique=true, nullable=false)
    public Integer getNumFactura() {
        return this.numFactura;
    }
    
    public void setNumFactura(Integer numFactura) {
        this.numFactura = numFactura;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_cliente", nullable=false)
    public Cliente getCliente() {
        return this.cliente;
    }
    
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="num_pago", nullable=false)
    public ModoPago getModoPago() {
        return this.modoPago;
    }
    
    public void setModoPago(ModoPago modoPago) {
        this.modoPago = modoPago;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="fecha", length=10)
    public Date getFecha() {
        return this.fecha;
    }
    
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="factura")

    public List<Detalle> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<Detalle> detalles) {
        this.detalles = detalles;
    }
}


