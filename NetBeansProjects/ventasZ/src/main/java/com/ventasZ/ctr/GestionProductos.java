
package com.ventasZ.ctr;

import com.ventasZ.model.Categoria;
import com.ventasZ.model.Producto;
import com.ventasZ.util.Dao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 *
 * @author gabriela.francousam
 */
@Component
@SessionScoped
@ManagedBean
public class GestionProductos {
    
    @Autowired
    @Qualifier ("producto")
    public Dao<Producto> producto;
    
    @Autowired
    @Qualifier ("categoria")
    public Dao<Categoria> categoria;
    
    private Producto productos = new Producto ();
    private Categoria categorias = new Categoria ();
    
    private List <Producto> listProd;
    private List <Categoria> listCat;

    public Producto getProductos() {
        return productos;
    }

    public void setProductos(Producto productos) {
        this.productos = productos;
    }

    public Categoria getCategorias() {
        return categorias;
    }

    public void setCategorias(Categoria categorias) {
        this.categorias = categorias;
    }

    public List<Producto> getListProd() {
        return listProd;
    }

    public void setListProd(List<Producto> listProd) {
        this.listProd = producto.findAll();
        this.listProd = listProd;
    }

    public List<Categoria> getListCat() {
        return listCat;
    }

    public void setListCat(List<Categoria> listCat) {
        this.listCat = categoria.findAll();
        this.listCat = listCat;
    }
    
    @PostConstruct
    public void init (){
    productos = new Producto ();
    categorias = new Categoria ();
    }
    
    public void findProductById (Producto prod){
    this.productos = prod;
    }
    
    public void findCatById (Categoria cat ){
    this.categorias = cat;
    }
}
