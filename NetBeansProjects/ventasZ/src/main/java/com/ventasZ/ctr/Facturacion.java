
package com.ventasZ.ctr;

import com.ventasZ.model.Detalle;
import com.ventasZ.model.Factura;
import com.ventasZ.model.ModoPago;
import com.ventasZ.util.Dao;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 *
 * @author gabriela.francousam
 */
public class Facturacion {
    @Autowired
    @Qualifier ("modoPago")
    public Dao <ModoPago> modoPago;
    @Autowired
    @Qualifier ("factura")
    public Dao <Factura> factura;
    
    @Autowired
    @Qualifier ("detalle")
    public Dao <Detalle> detalle;
    
    private ModoPago modopagos = new ModoPago();
    private Factura facturas = new Factura ();
    private Detalle detalles = new Detalle ();
    
    private List<ModoPago> listMp;
    private List<Factura> listFactura;
    private List<Detalle> listDetalle;

    public ModoPago getModopagos() {
        return modopagos;
    }

    public void setModopagos(ModoPago modopagos) {
        this.modopagos = modopagos;
    }

    public Factura getFacturas() {
        return facturas;
    }

    public void setFacturas(Factura facturas) {
        this.facturas = facturas;
    }

    public Detalle getDetalles() {
        return detalles;
    }

    public void setDetalles(Detalle detalles) {
        this.detalles = detalles;
    }

    public List<ModoPago> getListMp() {
        this.listMp = modoPago.findAll();
        return listMp;
    }

    public void setListMp(List<ModoPago> listMp) {
        this.listMp = listMp;
    }

    public List<Factura> getListFactura() {
        this.listFactura = factura.findAll();
        return listFactura;
    }

    public void setListFactura(List<Factura> listFactura) {
        this.listFactura = listFactura;
    }

    public List<Detalle> getListDetalle() {
        this.listDetalle = detalle.findAll();
        return listDetalle;
    }

    public void setListDetalle(List<Detalle> listDetalle) {
        this.listDetalle = listDetalle;
    }
    
   @PostConstruct
    public void init (){
    modopagos = new ModoPago();
    facturas = new Factura ();
    detalles = new Detalle ();
    }
    
    public void findMPById (ModoPago mp){
    this.modopagos = mp;
    }
    
    public void findFacturaById (Factura fac){
    this.facturas = fac;
    }
    
    public void findDetById (Detalle det){
    this.detalles = det;
    }
}
