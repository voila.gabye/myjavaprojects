
package com.ventasZ.ctr;

import com.ventasZ.model.Cliente;
import com.ventasZ.util.Dao;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 *
 * @author gabriela.francousam
 */
@Component
@SessionScoped
@ManagedBean
public class GestionPersonas implements Serializable {
    
    @Autowired
    @Qualifier ("cliente")
    public Dao<Cliente> cliente;
    
    private Cliente clientes = new Cliente ();
    private List<Cliente> listClient;

    public Cliente getClientes() {
        return clientes;
    }

    public void setClientes(Cliente clientes) {
        this.clientes = clientes;
    }

    public List<Cliente> getListClient() {
        this.listClient = cliente.findAll();
        return listClient;
    }

    public void setListClient(List<Cliente> listClient) {
        this.listClient = listClient;
    }
    
    @PostConstruct
    public void Init(){
    clientes = new Cliente ();
    }
    
    public void createC (){
        try {
            cliente.create(clientes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void editC (){
        try {
            cliente.edit(clientes);
        } catch (Exception e) {
             e.printStackTrace();
        }
    }
    
    public void deleteC (Cliente cli){
        try {
            this.clientes = cli;
            cliente.remove(clientes);
        } catch (Exception e) {
        }
    }
    
    public void findClientById (Cliente client){
    this.clientes = client;
    }
}
