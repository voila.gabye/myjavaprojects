package com.java.controller;

import com.java.entities.Area;
import com.java.entities.Departamento;
import com.java.entities.Escolaridad;
import com.java.entities.NivelPersonal;
import com.java.entities.Persona;
import com.java.entities.Sexo;
import com.java.impl.PersonaImpl;
import com.java.utils.Dao;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author gabriela.francousam
 */
@ManagedBean
@SessionScoped
public class PersonaC implements Serializable {

    /*Persona */
    private PersonaImpl personad;
    private Dao pd;
    private Persona personas;
    private List<Persona> listPerso;

    /*Foreign keys de Persona */
    private Sexo sexo;
    private Escolaridad escolaridad;
    private Area area;
    private Departamento departamento;
    private NivelPersonal nivelPersonal;

    /*Getter y Setters */
    public PersonaImpl getPersonad() {
        return personad;
    }

    public void setPersonad(PersonaImpl personad) {
        this.personad = personad;
    }

    public Dao getPd() {
        return pd;
    }

    public void setPd(Dao pd) {
        this.pd = pd;
    }

    public Persona getPersonas() {
        return personas;
    }

    public void setPersonas(Persona personas) {
        this.personas = personas;
    }

    public List<Persona> getListPerso() {
        return listPerso;
    }

    public void setListPerso(List<Persona> listPerso) {
        this.listPerso = listPerso;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public Escolaridad getEscolaridad() {
        return escolaridad;
    }

    public void setEscolaridad(Escolaridad escolaridad) {
        this.escolaridad = escolaridad;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public NivelPersonal getNivelPersonal() {
        return nivelPersonal;
    }

    public void setNivelPersonal(NivelPersonal nivelPersonal) {
        this.nivelPersonal = nivelPersonal;
    }

    @PostConstruct
    public void Init() {
        this.personas = new Persona();
        consultarAll();
    }

    /*Métodos de Persona*/
    public void insertar() {
        try {
            personas.setSexo(sexo);
            personas.setEscolaridad(escolaridad);
            personas.setArea(area);
            personas.setDepartamento(departamento);
            personas.setNivelPersonal(nivelPersonal);
            pd.create(personas);
        } catch (Exception e) {
        }
    }

    public void consultarAll() {
        try {
            listPerso = pd.findAll();
        } catch (Exception e) {
        }
    }

    public void consultarByiD(Persona personas) {
        try {
             this.personas = personas;
        } catch (Exception e) {
        }
    }
    
    public void actualizar (Persona personas){
        try {
            pd.edit(personas);
        } catch (Exception e) {
        }
    }
    
    public void eliminar (Persona personas){
        this.personas = personas;
        try {
           pd.remove(personas);
        } catch (Exception e) {
        }
    }
}
