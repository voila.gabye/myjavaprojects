
package com.java.impl;

import com.java.utils.AbstractFacade;
import com.java.entities.NivelPersonal;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author gabriela.francousam
 */
public class NivelPersonalImpl extends AbstractFacade<NivelPersonal> {

    @PersistenceContext(unitName = "conexion")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public NivelPersonalImpl() {
        super(NivelPersonal.class);
    }
    
}
