package com.java.impl;

import com.java.utils.AbstractFacade;
import com.java.entities.Persona;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author gabriela.francousam
 */
public class PersonaImpl extends AbstractFacade<Persona> {

    @PersistenceContext(unitName = "conexion")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PersonaImpl() {
        super(Persona.class);
    }

}
