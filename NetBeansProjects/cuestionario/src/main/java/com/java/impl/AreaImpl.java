package com.java.impl;

import com.java.utils.AbstractFacade;
import com.java.entities.Area;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author gabriela.francousam
 */
public class AreaImpl extends AbstractFacade<Area> {

    @PersistenceContext(unitName = "conexion")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AreaImpl() {
        super(Area.class);
    }

}
