package com.java.impl;

import com.java.utils.AbstractFacade;
import com.java.entities.Escolaridad;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author gabriela.francousam
 */
public class EscolaridadImpl extends AbstractFacade<Escolaridad> {

    @PersistenceContext(unitName = "conexion")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EscolaridadImpl() {
        super(Escolaridad.class);
    }

}
