package com.java.impl;

import com.java.utils.AbstractFacade;
import com.java.entities.Sexo;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author gabriela.francousam
 */
public class SexoImpl extends AbstractFacade<Sexo> {

    @PersistenceContext(unitName = "conexion")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SexoImpl() {
        super(Sexo.class);
    }

}
