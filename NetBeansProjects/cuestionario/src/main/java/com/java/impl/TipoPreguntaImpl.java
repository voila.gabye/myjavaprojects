
package com.java.impl;

import com.java.utils.AbstractFacade;
import com.java.entities.TipoPregunta;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author gabriela.francousam
 */

public class TipoPreguntaImpl extends AbstractFacade<TipoPregunta> {

    @PersistenceContext(unitName = "conexion")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipoPreguntaImpl() {
        super(TipoPregunta.class);
    }
    
}
