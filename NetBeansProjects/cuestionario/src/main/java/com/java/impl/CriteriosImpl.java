package com.java.impl;

import com.java.utils.AbstractFacade;
import com.java.entities.Criterios;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author gabriela.francousam
 */
public class CriteriosImpl extends AbstractFacade<Criterios> {

    @PersistenceContext(unitName = "conexion")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CriteriosImpl() {
        super(Criterios.class);
    }

}
