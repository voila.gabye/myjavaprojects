package com.java.impl;

import com.java.utils.AbstractFacade;
import com.java.entities.Departamento;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author gabriela.francousam
 */
public class DepartamentoImpl extends AbstractFacade<Departamento> {

    @PersistenceContext(unitName = "conexion")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DepartamentoImpl() {
        super(Departamento.class);
    }

}
