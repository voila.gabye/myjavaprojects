package com.java.impl;

import com.java.utils.AbstractFacade;
import com.java.entities.Preguntas;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author gabriela.francousam
 */
public class PreguntasImpl extends AbstractFacade<Preguntas> {

    @PersistenceContext(unitName = "conexion")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PreguntasImpl() {
        super(Preguntas.class);
    }

}
