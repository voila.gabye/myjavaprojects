package com.java.impl;

import com.java.utils.AbstractFacade;
import com.java.entities.Dimension;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author gabriela.francousam
 */
public class DimensionImpl extends AbstractFacade<Dimension> {

    @PersistenceContext(unitName = "conexion")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DimensionImpl() {
        super(Dimension.class);
    }

}
