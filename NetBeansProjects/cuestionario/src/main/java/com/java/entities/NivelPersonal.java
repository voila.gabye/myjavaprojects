
package com.java.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author gabriela.francousam
 */
@Entity
@Table(name = "nivel_personal")

public class NivelPersonal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    
    @Column(name = "id_nivel")
    private Integer idNivel;
  
    @Column(name = "nivel_personal")
    private String nivelPersonal;
  
    public NivelPersonal() {
    }

    public NivelPersonal(Integer idNivel) {
        this.idNivel = idNivel;
    }

    public NivelPersonal(Integer idNivel, String nivelPersonal) {
        this.idNivel = idNivel;
        this.nivelPersonal = nivelPersonal;
    }

    public Integer getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(Integer idNivel) {
        this.idNivel = idNivel;
    }

    public String getNivelPersonal() {
        return nivelPersonal;
    }

    public void setNivelPersonal(String nivelPersonal) {
        this.nivelPersonal = nivelPersonal;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNivel != null ? idNivel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NivelPersonal)) {
            return false;
        }
        NivelPersonal other = (NivelPersonal) object;
        if ((this.idNivel == null && other.idNivel != null) || (this.idNivel != null && !this.idNivel.equals(other.idNivel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.java.entities.NivelPersonal[ idNivel=" + idNivel + " ]";
    }
    
}
