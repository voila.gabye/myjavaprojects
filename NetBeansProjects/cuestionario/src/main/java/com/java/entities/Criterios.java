
package com.java.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 *
 * @author gabriela.francousam
 */
@Entity
@Table(name = "criterios")

public class Criterios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id_criterios")
    private Integer idCriterios;

    @Column(name = "criterios")
    private String criterios;
    

    public Criterios() {
    }

    public Criterios(Integer idCriterios) {
        this.idCriterios = idCriterios;
    }

    public Criterios(Integer idCriterios, String criterios) {
        this.idCriterios = idCriterios;
        this.criterios = criterios;
    }

    public Integer getIdCriterios() {
        return idCriterios;
    }

    public void setIdCriterios(Integer idCriterios) {
        this.idCriterios = idCriterios;
    }

    public String getCriterios() {
        return criterios;
    }

    public void setCriterios(String criterios) {
        this.criterios = criterios;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCriterios != null ? idCriterios.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Criterios)) {
            return false;
        }
        Criterios other = (Criterios) object;
        if ((this.idCriterios == null && other.idCriterios != null) || (this.idCriterios != null && !this.idCriterios.equals(other.idCriterios))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.java.entities.Criterios[ idCriterios=" + idCriterios + " ]";
    }
    
}
