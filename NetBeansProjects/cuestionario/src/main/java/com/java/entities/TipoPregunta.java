
package com.java.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author gabriela.francousam
 */
@Entity
@Table(name = "tipo_pregunta")

public class TipoPregunta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
  
    @Column(name = "id_tipop")
    private Integer idTipop;
   
    @Column(name = "tipo")
    private String tipo;
   
    public TipoPregunta() {
    }

    public TipoPregunta(Integer idTipop) {
        this.idTipop = idTipop;
    }

    public TipoPregunta(Integer idTipop, String tipo) {
        this.idTipop = idTipop;
        this.tipo = tipo;
    }

    public Integer getIdTipop() {
        return idTipop;
    }

    public void setIdTipop(Integer idTipop) {
        this.idTipop = idTipop;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipop != null ? idTipop.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoPregunta)) {
            return false;
        }
        TipoPregunta other = (TipoPregunta) object;
        if ((this.idTipop == null && other.idTipop != null) || (this.idTipop != null && !this.idTipop.equals(other.idTipop))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.java.entities.TipoPregunta[ idTipop=" + idTipop + " ]";
    }
    
}
