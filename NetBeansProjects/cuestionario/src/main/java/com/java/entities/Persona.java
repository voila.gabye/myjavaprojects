
package com.java.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author gabriela.francousam
 */
@Entity
@Table(name = "persona")

public class Persona implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id_persona")
    private Integer idPersona;
   
    @Column(name = "edad")
    private int edad;
    
    @JoinColumn(name = "area", referencedColumnName = "id_area")
    @ManyToOne(optional = false)
    private Area area;
    
    @JoinColumn(name = "departamento", referencedColumnName = "id_departamento")
    @ManyToOne(optional = false)
    private Departamento departamento;
   
    @JoinColumn(name = "escolaridad", referencedColumnName = "id_escolaridad")
    @ManyToOne(optional = false)
    private Escolaridad escolaridad;
   
    @JoinColumn(name = "nivel_personal", referencedColumnName = "id_nivel")
    @ManyToOne(optional = false)
    private NivelPersonal nivelPersonal;
   
    @JoinColumn(name = "sexo", referencedColumnName = "id_sexo")
    @ManyToOne(optional = false)
    private Sexo sexo;
   

    public Persona() {
    }

    public Persona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Persona(Integer idPersona, int edad) {
        this.idPersona = idPersona;
        this.edad = edad;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Escolaridad getEscolaridad() {
        return escolaridad;
    }

    public void setEscolaridad(Escolaridad escolaridad) {
        this.escolaridad = escolaridad;
    }

    public NivelPersonal getNivelPersonal() {
        return nivelPersonal;
    }

    public void setNivelPersonal(NivelPersonal nivelPersonal) {
        this.nivelPersonal = nivelPersonal;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPersona != null ? idPersona.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persona)) {
            return false;
        }
        Persona other = (Persona) object;
        if ((this.idPersona == null && other.idPersona != null) || (this.idPersona != null && !this.idPersona.equals(other.idPersona))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.java.entities.Persona[ idPersona=" + idPersona + " ]";
    }
    
}
