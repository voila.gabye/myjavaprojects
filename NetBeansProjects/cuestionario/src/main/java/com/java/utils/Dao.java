package com.java.utils;


import java.util.List;


/**
 *
 * @author gabriela.francousam
 */
public interface Dao<T> {

    public void create(T e);

    public List<T> findAll();

    public void edit(T e);

    public void remove(T e);

}
