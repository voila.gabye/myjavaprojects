create database kuska_tours;
use kuska_tours;

create table paises_residencia (
id_pais int not null auto_increment primary key,
nombre_pais varchar (150) not null
);

create table personas (
id_persona int not null auto_increment primary key,
nombre varchar (150) not null,
apellido varchar (150) not null,
telefono varchar (10) not null,
correo varchar (150) not null
);

create table usuarios (
id_usuario int not null primary key auto_increment,
usuario varchar (150) not null,
pass varchar (150) not null,
rol_persona varchar (150) not null,
id_persona int null null,
unique key per_us (id_persona),
constraint  us_per foreign key (id_persona) references personas (id_persona)
);

create table clientes (
id_cliente int not null auto_increment,
id_persona int not null,
pais_residencia int not null,
primary key (id_cliente,id_persona),
unique key per_cli (id_persona),
constraint cli_per foreign key (id_persona) references personas(id_persona) ON DELETE CASCADE ON UPDATE CASCADE,
constraint cli_res foreign key  (pais_residencia) references paises_residencia(id_pais)
);

create table empleados (
id_empleado int not null auto_increment,
direccion_emp varchar (150) not null,
dui_emp varchar (10) not null,
nit_emp varchar (20) not null,
estado_emp varchar(8) not null,
fotografia varchar(1024) not null,
id_persona int not null,
primary key (id_empleado,id_persona),
unique key per_empl (id_persona),
constraint  empl_per foreign key (id_persona) references personas (id_persona)
);

create table tipo_transaccion (
id_transaccion int not null auto_increment primary key,
transaccion varchar (150) not null
);

create table reserva (
id_reserva int not null auto_increment primary key,
id_persona int not null,
cantidad_reserva int not null,
id_transaccion int not null,
fecha_reserva  datetime not null,
horario_ruta int not null,
constraint res_per foreign key (id_persona) references personas (id_persona),
constraint res_tran foreign key (id_transaccion) references tipo_transaccion (id_transaccion)
);

create table rutas (
id_ruta int not null auto_increment primary key,
nombre_ruta varchar (150) not null,
breve_descripcion varchar (200) not null,
descripcion_ruta varchar (200) not null,
costo_ruta decimal(7,2) ,
cupo_max_ruta int not null,
cupo_disponible int not null,
imagen_ruta varchar(1024) not null
);

create table tipo_peticiones (
id_tipo_peticion int not null auto_increment primary key,
tipo_peticion varchar (100) not null
);

create table peticiones (
id_peticion int not null auto_increment primary key,
id_persona int not null,
tipo_peticion int not null,
peticion_razon varchar (500) not null,
estado_pet varchar(10) not null,
constraint pe_per foreign key (id_persona) references personas (id_persona),
constraint pe_tp foreign key (tipo_peticion) references tipo_peticiones (id_tipo_peticion)
);

create table horarios_rutas (
id_horario int not null auto_increment primary key,
id_reserva int not null,
id_cliente int not null,
cantidad_reserva int not null,
id_transaccion int not null,
fecha_reserva datetime not null,
constraint hor_res foreign key (id_reserva) references reserva(id_reserva),
constraint hor_per foreign key (id_cliente) references clientes(id_cliente)
);



/*Procedimiento almacenado para GUARDAR EMPLEADOS,personas y usuarios*/
DELIMITER //
CREATE PROCEDURE sp_ing_empleado (
IN p_id_persona INT, 
IN p_nombre varchar (150), 
IN p_apellido varchar (150), 
IN p_telefono varchar (10), 
IN p_correo varchar (150), 
IN p_direccion_emp varchar (150),
IN p_dui_emp varchar (10),
IN p_nit_emp varchar (20),
IN p_estado_emp varchar(8),
IN p_fotografia  varchar(1024),
IN p_usuario varchar (150),
IN p_pass varchar (150),
IN p_rol_persona varchar (150)
 )
 
BEGIN  
DECLARE v_id_persona int;

 INSERT INTO personas (id_persona, nombre, apellido, telefono, correo) values (p_id_persona,p_nombre,p_apellido,p_telefono,p_correo);
 SELECT id_persona INTO v_id_persona FROM personas ORDER BY id_persona DESC LIMIT 1;
 INSERT INTO empleados (id_empleado,direccion_emp,dui_emp,nit_emp,estado_emp,fotografia,id_persona) values (default,p_direccion_emp,p_dui_emp,p_nit_emp,p_estado_emp,p_fotografia,v_id_persona);
 INSERT INTO usuarios (id_usuario,usuario,pass,rol_persona,id_persona) values (default,p_usuario,p_pass,p_rol_persona,v_id_persona);
END //
DELIMITER ;


CALL sp_ing_empleado(null,'Alberto','Henriquez','11234645','a@a.com','Col. CA','123-123','324-324','Activo','http:img','alb','alb456','admin');


/*Procedimiento almacenado para ACTUALIZAR EMPLEADOS,personas y usuarios*/
DELIMITER //
CREATE PROCEDURE sp_update_empleado (
IN p_id_persona INT, 
IN p_nombre varchar (150), 
IN p_apellido varchar (150), 
IN p_telefono varchar (10), 
IN p_correo varchar (150), 
IN p_direccion_emp varchar (150),
IN p_dui_emp varchar (10),
IN p_nit_emp varchar (20),
IN p_estado_emp varchar(8),
IN p_fotografia  varchar(1024),
IN p_usuario varchar (150),
IN p_pass varchar (150),
IN p_rol_persona varchar (150)
 )
 
BEGIN  
DECLARE v_id_persona int;
SET v_id_persona = p_id_persona;

  UPDATE personas
  SET nombre = p_nombre,
      apellido = p_apellido,
      telefono = p_telefono,
      correo = p_correo
  WHERE id_persona = p_id_persona;

  UPDATE empleados
  SET direccion_emp = p_direccion_emp,
      dui_emp = p_dui_emp,
      nit_emp = p_nit_emp,
      estado_emp = p_estado_emp,
      fotografia = p_fotografia
  WHERE id_persona = v_id_persona;
  
  UPDATE usuarios 
  SET usuario = p_usuario,
      pass = p_pass,
      rol_persona = p_rol_persona
  WHERE id_persona = v_id_persona;
END //
DELIMITER ;

CALL sp_update_empleado(7,'Beberly','Sayes','11234645','a@a.com','Col. CA','123-123','324-324','Activo','http:img','bebs','bebs456','operario');




/*Vista para obtener clientes,personas y usuarios*/
CREATE VIEW view_clientes AS
SELECT personas.id_persona, personas.nombre, personas.apellido, personas.telefono, personas.correo, 
paises_residencia.nombre_pais, 
usuarios.usuario, usuarios.pass
FROM (((personas
INNER JOIN clientes ON personas.id_persona = clientes.id_cliente) 
INNER JOIN paises_residencia ON clientes.pais_residencia = paises_residencia.id_pais)
INNER JOIN usuarios ON personas.id_persona = usuarios.id_persona
);


SELECT * FROM personas p
INNER JOIN clientes c on p.id_persona = c.id_cliente;

/*Procedimiento almacenado para GUARDAR CLIENTES,personas y usuarios*/
DELIMITER //
CREATE PROCEDURE sp_ing_cliente (
IN p_id_persona INT, 
IN p_nombre varchar (150), 
IN p_apellido varchar (150), 
IN p_telefono varchar (10), 
IN p_correo varchar (150), 
IN p_pais_residencia int, 
IN p_usuario varchar (150),
IN p_pass varchar (150)
 )
 
BEGIN  
DECLARE v_id_persona int;
DECLARE v_rol_persona varchar (150);
SET v_rol_persona = 'cliente';

 INSERT INTO personas (id_persona, nombre, apellido, telefono, correo) values (p_id_persona,p_nombre,p_apellido,p_telefono,p_correo);
 SELECT id_persona INTO v_id_persona FROM personas ORDER BY id_persona DESC LIMIT 1;
 INSERT INTO clientes (id_cliente, id_persona,pais_residencia) values (default,v_id_persona,p_pais_residencia);
 INSERT INTO usuarios (id_usuario,usuario,pass,rol_persona,id_persona) values (default,p_usuario,p_pass,v_rol_persona,v_id_persona);
END //
DELIMITER ;

select * from personas order by id_persona desc limit 1;

CALL sp_ing_cliente(null,'Mariana','Marroquin','11234645','m@m.com',1,'mari','mair456');

Insert into personas values (default,'Gaby','Franco','123456','a@a.com');
Insert into personas values (null,'Gaby','Franco','123456','a@a.com');



/*Procedimiento almacenado para ACTUALIZAR CLIENTES,personas y usuarios*/
DELIMITER //
CREATE PROCEDURE sp_update_cliente (
IN p_id_persona INT, 
IN p_nombre varchar (150), 
IN p_apellido varchar (150), 
IN p_telefono varchar (10), 
IN p_correo varchar (150), 
IN p_pais_residencia int, 
IN p_usuario varchar (150),
IN p_pass varchar (150)
 )
 
BEGIN  
DECLARE v_id_persona int;
DECLARE v_rol_persona varchar (150);
SET v_rol_persona = 'cliente';
SET v_id_persona = p_id_persona;

  UPDATE personas
  SET nombre = p_nombre,
      apellido = p_apellido,
      telefono = p_telefono,
      correo = p_correo
  WHERE id_persona = p_id_persona;

  UPDATE clientes
  SET pais_residencia = p_pais_residencia
  WHERE id_persona = v_id_persona;

  UPDATE usuarios 
  SET usuario = p_usuario,
      pass = p_pass,
      rol_persona = v_rol_persona
  WHERE id_persona = v_id_persona;

END //
DELIMITER ;


call sp_update_cliente (1,'Lucia','Acosta','11234645','m@m.com',1,'mar','mar456');



/*Hasta aca estamos bien*/




create table operarios_rutas (
id_relacion int not null auto_increment primary key,
id_persona int not null,
id_horario int not null,
nombre_ruta int not null
dia_salida datetime not null,
hora_inicio datetime not null,
hora_final datetime not null,
nombre_persona 
apellido
);


/*Ruta Dao Y ESTE PUEDE SER un select de ruta*/
CREATE VIEW view_descripcion_ruta (
id_ruta
nombre_ruta
breve_descripcion
descripcion_ruta
costo_ruta
cupo_disponible
imagen_ruta
);

/*Ruta Dao*/
CREATE VIEW view_horario_ruta AS 
SELECT (
id_horario
dia_salida
hora_inicio
hora_final
);

CREATE VIEW view_operarios_rutas AS
SELECT
(
id_horario
nombre
apellido
fotografia
);